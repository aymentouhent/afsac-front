import React, { Fragment } from 'react';

export const trans = {

    "fr": {
        navbar: {
            contact: "Contactez-nous",
            aboutUs: "Qui sommes nous",
            training: "OACI Formations",
            trainingDesc: `Vous vous sentez impliqué dans ce domaine et souhaitez y travailler,l’AFSAC Centre AVSEC / OACI vous propose des formations "Qualifiantes" et "Certifiantes" afin de devenir un expert en Sûreté de l’Aviation Civile conformément aux standards internationaux de l’Organisation de l’Aviation Civile Internationale (OACI)`,
            avsecCourses: "CONSULTER LES COURS AVSEC / OACI",
            tppCourses: "CONSULTER LES COURS TRAINAIR PLUS",
            virtualCourses: "CONSULTER LES COURS VIRTUELS",
            onlineCourses: "CONSULTER LES COURS EN LIGNE",
            sessions: "Prochaines sessions",
            sessionsDesc: "Découvrez les sessions à venir",
            accredations: "Nos accreditations & reconnaissances",
			x1: "Les Cours AVSEC / OACI",
			x2: "Les Cours TRAINAIR PLUS",
			x4: "Les Cours VIRTUELS",
			x3: "Les Cours EN LIGNE",
            news: "Actualités",
            searchPlaceholder: "Rechercher...",
            y1: "Consulter tous les cours AVSEC / OACI",
            y2: "Consulter tous les cours TRAINAIR PLUS",
            y3: "Consulter tous les cours en ligne",
            y4: "Consulter tous les cours virtuel"
        },

        promoBanner: {
            seeMoreButton: "Voir Plus"
        },

        footer: {
            sitemap: "Plan du site",
            courses: "Cours",
        },

        contactForm: {
            title: "Contactez-nous",
            lastName: 'Nom',
            firstName: 'Prénom',
            email: "Email",
            phone: "Téléphone",
            country: "Pays",
            organism: "Organisme",
            object: "Objet",
            objectPlaceholder: "Choisissez",
            object1: "Demande de formation",
            object2: "Donner des cours à l’AFSAC", // DONT REORDER THE OBJECTS !!!!!! CONTACT THE DEVELOPER FIRST
            object3: "Demande de partenariat",
            object4: "Autre",
            cv: "Curriculum vitae (PDF/DOCX/JPG/PNG)",
            letter: "Lettre de motivation (PDF/DOCX/JPG/PNG)",
            browsePlaceholder: "Choisir",
            diploma: "Certifs/Diplome (PDF/DOCX/JPG/PNG)",
            website: "Site web",
            message: "Message",
            submitButton: "Envoyer",
            formExist: "Nous avons déja reçu votre demande de contact auparavant !",
            formSuccess: "Nous avons bien reçu votre demande de contact !",
            formError: "Erreur de connexion, verifier votre connexion internet !",

        },

        homePage: {
            sloganHome: `Notre devoir est de représenter, diriger et promouvoir l'industrie de l'aviation civile`,
            afsacPresentation: `L’AFSAC de Tunis-Tunisie, est un centre de formation aéronautique disposant de deux labels internationaux d’excellence délivrés par l’Organisation de l’Aviation Civile Internationale (OACI), à savoir Centre Régional de Formation à la Sûreté de l’Aviation de l’OACI et TRAINAIR PLUS FULL MEMBER GAT/OACI. A ce titre, il est le seul centre privé au monde disposant de ces deux labels. L’AFSAC de Tunis-TUNISIE est en mesure de dispenser des formations d’excellence, pour couvrir toutes les normes et exigences de l’OACI objets des 19 annexes à la Convention de Chicago, à tous les acteurs aéronautiques à l’échelle mondiale, dans ses propres locaux et/ou sur site, attestées par des certificats délivrés par l’OACI.`,
            programTitle: `PROGRAMME TRAINAIR PLUS`,
            securityTitle: `SÛRETÉ ET FACILITATION`,
            securityButton: "Voir plus"
        },

        breadcrumb: {
            home: "Accueil",
        },

        contactPage: {
            contactDesc: `Que vous ayez une question sur une formation existante ou une demande de personnalisation de l'une, ou encore une demande de formation dans un autre pays, N'hésitez pas à nous contacter. Notre équipe est toujours à votre disposition pour vous renseigner et répondre à toutes vos questions.`,
            visitUs: "Rendez nous visite:",
            CallUs: "Appelez nous:",
            EmailUs: "Envoyez-nous un email:",
            SocialMedia: "Réseaux sociaux:",
            requestDesc: "Souhaitez-vous suivre une formation dans votre propre établissement?",
            requestDesc1: "Cliquez sur le bouton ci-dessous pour remplir le formulaire et soumettre une demande.",
            requestButton: "SOUMETTRE UNE DEMANDE"
        },

        aboutUsPage: {
            title1: "Mot du DG",
            // motDG: `Chers internautes / visiteurs, L’AFSAC Centre Régional de Formation à la Sûreté de l’Aviation de l’OACI de Tunis – TUNISIE et TRAINAIR PLUS FULL MEMEBER GAT / ICAO vous souhaite la bienvenue sur son site web.Notre plateforme numérique présente un lieu de rencontre et de convergence pour l’ensemble des acteurs et intervenants dans tous les secteurs de l’aviation civile. Nous aspirons par ce moyen de communication nous rapprocher de notre public cible, se faire mieux...`,
            motDG: <Fragment>
                <p className="registration-modal-content">Chers internautes / visiteurs,</p>
                <p className="registration-modal-content">L’AFSAC Centre Régional de Formation à la Sûreté de l’Aviation de l’OACI de Tunis – TUNISIE et TRAINAIR PLUS FULL MEMEBER GAT / ICAO vous souhaite la bienvenue sur son site web.</p>
                <p className="registration-modal-content">Notre plateforme numérique présente un lieu de rencontre et de convergence pour l’ensemble des acteurs et intervenants dans tous les secteurs de l’aviation civile.</p>
                <p className="registration-modal-content">Nous aspirons par ce moyen de communication nous rapprocher de notre public cible, se faire mieux connaître et surtout mieux le servir.</p>
                <p className="registration-modal-content">Le secteur de l’aviation civile dans le monde qui ne cesse de se développer fait face constamment aux défis majeurs qui sont la sécurité et la sûreté. Ainsi, il demeure indispensable de continuer à élaborer et à développer les réglementations, les procédures de supervision de la sécurité de l’aviation civile, de la sécurité aérienne et de la sûreté aéroportuaire et les référentiels techniques permettant de répondre aux attentes des instances de l’aviation civile internationale.</p>
                <p className="registration-modal-content">L’armature règlementaire, procédurale et technique doit nécessairement être associée à une politique optimale de formation et un personnel empreint d’expertise, d’efficacité et de rigueur.</p>
                <p className="registration-modal-content">C’est dans ce sens et dans le but de soutenir les Etats dans l’application des normes et des pratiques recommandées de l’Annexe 17 que l’OACI avait mis en place un réseau de trente-cinq centres de formation en sûreté de l’aviation (CRFSA) dans le monde, dont l’AFSAC, qui a pour objectif de développer la formation et la qualification du personnel des organismes intervenants dans le domaine de l’aviation civile en s’appuyant sur des supports pédagogiques élaborés et reconnus par l’OACI.</p>
                <p className="registration-modal-content">C’est ainsi que nous pourrons ensemble   participer à l’effort permettant d’atteindre l’objectif qui pourrait faire de l’aviation civile internationale, une aviation sûre.</p>
                <p className="registration-modal-content"><strong>Chers Internautes,</strong></p>
                <p className="registration-modal-content">Vos observations, suggestions et critiques constructives sont attendues en vue de l’amélioration de la qualité de cette plateforme</p>
                <p className="registration-modal-content">Bonne navigation !</p>
                <p className="registration-modal-content"><strong>Le Directeur Général</strong></p>
                <p className="registration-modal-content"><strong>Mr. Hassan SEDDIK</strong></p>
            </Fragment>,
            title2: "La création de l'AFSAC",
            title2Desc: "L'AFSAC fait partie des 35 ASTC de l'OACI dans le monde et appartient à la région EUR NAT. Il est maintenant devenu le premier et unique centre de formation privé au monde à être accrédité par l'OACI en tant que membre à part entière de TRAINAIR PLUS et centre de formation à la sécurité aérienne de l'OACI (ASTC).",

            title3: "Nos formations OACI",
            allCourses: "Consulter tous les cours",
            coursesAll: "Consulter les cours",
            checkUpcommingSessions: "Consultez les prochaines sessions",
            UpcommingSessions: "Consulter nos cours",
			OnlineCourses : "Consulter nos cours en ligne",
            ourReferences: "Nos références",

            title4: "Hébergement",
            accomodationDesc: `L’AFSAC Centre Régional de formation à la Sûreté de l’Aviation de l’OACI Tunis-TUNISIE et TRAINAIR PLUS Full Member GAT/OACI met à la disposition de ces participants étrangers plusieurs facilitations  telque : Obtention du visa à l’aéroport, réservation d’hôtel, accueil  l’aéroport, transport interne, services médicaux, Des tarifs d’hébergement préférentiels et Restauration. Pour de plus amples renseignements, nous vous invitons à consulter le document PDF ci-dessous qui a été élaboré par l’AFSAC.`,
            accomodationButton: "Télécharger la fiche de facilitation",

        },

        coursesSearchBox: {
            checkboxCours: "Cours",
            checkboxWorkshop: "Ateliers",
            categoriesLabel: "catégories",
            categoriesPlaceholder: "Tous",
            keywordLabel: "Mot-clé",
            keywordPlaceholder: "Nom du cours...",
            searchButton: "Rechercher",
            clearButton: "Réinitialiser",
            noResultsText: "Aucun résultat trouvé",
        },

        avsecPages: {
            upcommingAvsecTitle: "Prochaines sessions AVSEC",
            upcommingVirtualTitle: "Prochaines sessions virtuels",
        },

        TppPages: {
            bannerTitle1: `"Apprenez à tout moment"`,
            bannerTitle2: `"Apprenez n'importe où"`,
            bannerDesc: `L’AFSAC Centre TRAINAIR PLUS FULL MEMBER GAT / OACI est en mesure de dispenser des formations d’excellence, pour couvrir toutes les normes et exigences de l’OACI objets des 19 annexes à la Convention de Chicago, à tous les acteurs aéronautiques à l’échelle mondiale, dans ses propres locaux et/ou sur site, attestées par des certificats délivrés par l’OACI.`,
            upcommingTppTitle: "Prochaines Sessions trainair plus",
            TppTitleDesc: "",

            // Brochure:
            downloadBrochure: "Téléchargez notre catalogue",
            downloadBrochureDesc: "Envoyez votre email pour télécharger un catalogue imprimable avec la description de nos formations.",
            downloadEmailPlaceholder: "E-mail",
            downloadBrochureButton: "Téléchargez",
            brochureFormSuccess: "Merci ! le catalogue sera envoyé par email",
            brochureFormError1: "Le catalogue a été envoyé déja a cette adresse",
            brochureFormConnexionError: "Erreur de connexion. Réessayer !",
        },

        accredationsPage: {
            title1: "NOS ACCREDITATIONS & RECONNAISSANCES",
            desc1: `L’académie Tuniso-Francaise de Formation en Sûreté et sécurité de l’Aviation Civile « AFSAC » a été créée en vertu de la réglementation Tunisienne. `,
            desc2: `Son siège social est situé sise 7 avenue Taha Hussein , 1008 – Montfleury – Tunis – TUNISIE. Elle est agrée par le Ministère de l’Emploi et de la Formation Professionnelle sous l’ID : 10-1064-1:1 du 9 août 2010 sous le dénommé « AFSAC » et par la Direction Générale de l’Aviation Civile.`,
            desc3: `Elle est le premier et unique Centre de formation privé dans le monde accrédité par l’OACI en tant que centre OACI de formation à la sûreté de l’aviation (CFSA) et membre à part entière du programme TRAINAIR PLUS GAT/OACI. `,
            desc4: `l’AFSAC fait partie des 35 centres de formation dans le monde accrédités par l’OACI en tant que centre de formation à la sûreté de l’aviation de l’OACI.`,
            desc5: `Ces accréditations présentent une reconnaissance par l’OACI de la crédibilité et du professionnalisme de l’AFSAC.`, //new description added by Habib AROUA
            // Recogintions:
            recognitionTitle: `L’AFSAC a obtenu d'importantes accréditations et reconnaissances : `,
            recognitionElements: [
                "OACI : Centre de formation à la sûreté de l’aviation de l’OACI « ASTC »",
                "OACI : Membre à part entière du Programme TRAINAIR PLUS GAT/OACI.",
                "AFRAA : Association des Compagnies Aériennes Africaines.",
                "ACAO : Organisation Arabe de l’Aviation Civile.",
                "AACO : Organisation Arabe du Transport aérien.",
                "IAAA : Académie d'Aviation de l’Aéroport d'Incheon.",
                "ERNAM : École Régionale de la Navigation Aérienne et du Management.",
                "ACI : Conseil International des Aéroports."
            ],

            // Security Training:
            securityTitle: `Centre de formation à la sûreté de l’aviation de l’OACI (CFSA)`,
            securityDesc: `Dans le cadre de sa mission de formation pour le personnel de l’aéronautique, le Centre de Formation à la sûreté de l’aviation de l'OACI de Tunis - TUNISIE possède une équipe de professionnels et des instructeurs hautement qualifiés qui veillent sur l’organisation des cours dans le domaine de l’aviation civile et il offre des solutions adaptées aux besoins de ces clients et aux différents acteurs dans le domaine de l’aviation directement sur site et partout dans le monde  ce qui lui a donné une réputation nationale et internationale.`,

            // Full Membership Program:
            membershipTitle: `Membre à part entière du Programme TRAINAIR PLUS GAT/OACI.`,
            membershipDesc: `L’organisation internationale de l’aviation civile (OACI) a lancé le programme TRAINAIR PLUS (TPP) afin d’améliorer la collaboration entre les états membres en matière de formation pour promouvoir la sûreté, la sécurité, et le développement durable du transport aérien.`,

            slogan: "SLOGAN",
            sloganDesc1: `Notre devoir est de représenter,`,
            sloganDesc2: `diriger et promouvoir l'industrie de l'aviation civile`,

            // VISION & MISSION:
            visonTitle: "Notre vision",
            visionDesc: `Se positionner entant que partenaire crédible dans la formation et le développement des compétences dans l’industrie de l’aviation civile.`,

            missionTitle: "Notre mission",
            missionDesc: `Fournir un soutien continu pour le développement des compétences de nos clients conformément aux exigences de l’OACI.`,
        },

        references: {
            referencesTitle: "Références",
            referencesDesc: " Lorem ipsum dolor sit amet, at nibh necessitatibus sit, fugit nobis detraxit ea quo. Vim ea posse commune accumsan.",
        },

        mediaNews: {
            bigTitle: 'Articles & Médias',
            news: "ARTICLES",
            lastNews: "Dernières News",
            aLaUne: 'A LA UNE',
            titleAlbums: 'PHOTOS ALBUMS',
            titleVideos: 'VIDÉOS & TÉMOIGNAGES',
            titleComminiques: 'COMMUNIQUÉS',
            seeMore: "Voir plus",
            seeAlbum: "Voir album",
            seeVideo: "Voir la vidéo",

        },

        newsletter: {
            newsletterDesc: "Inscrivez-vous a notre newsletter et restez informé de notre planning de formations",
            namePlaceholder: "Nom",
            emailPlaceholder: "E-mail",
            buttonText: "Souscrire",
            newsNameErr: "Merci de taper votre NOM !",
            newsEmailErr: "Merci de taper un EMAIL valide",
            newsEmailExist: "Vous êtes déjà inscrit !",
            newsSuccess: "Merci ! Votre inscription est enregistrée. Vous recevrez notre NEWSLETTER chaque mardi.",
            newsConexErr: "Erreur, Merci de vérifier votre connexion internet !",
        },

        partners: {
            title: "Nos partenaires"
        },

        soloPage: {
            trainingButton: `Demander cette formation`,
            subscribeButton: `S'inscrire a cette formation`,
            brochureButton: `Télécharger la brochure`,
            objectives: `Objectifs du cours:`,
            content: `Le contenu du cours:`,
            population: `Population cible:`,
            entry: `Conditions d'entrée:`,
            place: `Lieu de formation:`,
            duration: `Durée`,
            price: `Prix`,
            language: `Langue`,
            delivery: `Mode de livraison`,
            category: `Domaine de formation:`
        },

        courseCard: {
            objectives: "Objectifs d'apprentissage:",
            seeMoreButton: "Voir plus",
            workshop: "ATELIER",
            course: "COURS",
            classroom: "Présentiel",
            virtualClassroom: "Classe virtuelle",
            online: "En ligne",
        },

        registration: {
            breadCrumbTitle: "Inscription",
            backButton: "Retour",
            registrationTitle: "Inscription au cours",

            personalInfo: "Informations personnelles",
            personalTitle: "Titre personnel",
            mrValue: "Mr",
            mrsValue: "Mme",
            firstName: "Prénom",
            firstNamePlaceholder: "Mohamed",
            lastName: "Nom",
            lastNamePlaceholder: "Ben mohamed",
            email: "Email",
            emailPlaceholder: "mohamed@email.com",
            emailCaption: "Un e-mail de confirmation sera envoyé à cette adresse.",


            EmploymentInfo: "Informations sur l'emploi",
            jobTitle: "Profession",
            organization: "Type d'organisation",
            organizationType: "Nom de l'organisation",
            organizationsOptions: [
                "Entreprise",
            ],
            supervisorName: "Nom du superviseur",
            supervisorJob: "Profession du superviseur",
            supervisorEmail: "Email du superviseur",
            supervisorPhone: "Téléphone du superviseur",

            contactInfo: "Informations de contact",
            address: "Adresse",
            city: "Ville",
            state: "Etat",
            postalCode: "Code postal",
            country: "Pays",
            phone: "Téléphone",
            secondPhone: "Second Téléphone",

            invoicingInfo: "Informations de facturation",
            invoicingName: "Facturation à (nom complet)",
            invoicingAdresse: "Adresse de facturation",

            checkboxLabel: "J'accepte la politique d'inscription et de paiement",
            buttonSubmit: "Envoyer ma demande de formation",


            // error msgs :
            firstNameErr: `Merci de taper un prénom valide !`,
            lastNameErr: "Merci de taper un nom valide !",
            emailErr: "Merci de taper un email valide",

            jobTitleErr: "Merci de taper une profession valide !",
            organizationErr: "Merci de taper un nom d'organisation valide !",
            organizationTypeErr: "Merci de choisir un type d'organisation !",
            supervisorNameErr: "Merci de taper un Nom du superviseur valide !",
            supervisorJobErr: "Merci de taper la Profession du superviseur !",
            supervisorEmailErr: "Merci de taper un Email du superviseur valide !",
            supervisorPhoneErr: "Merci de taper un numéro Téléphone du superviseur valide !",

            addressErr: "Merci de taper une adresse valide !",
            cityErr: "Merci de taper une Ville valide !",
            stateErr: "Merci de taper un etat valide !",
            postalCodeErr: "Merci de taper un Code postal valide !",
            countryErr: "Merci de taper le nom de votre Pays !",
            phoneErr: "Merci de taper un numéro de Téléphone valide !",
            // secondPhoneErr: "Second Téléphone",

            invoicingNameErr: "Merci de remplir le champ Facturation à (nom complet) !",
            invoicingAdresseErr: "Merci de taper une adresse de facturation valide !",


            modalTitleAvsec: <Fragment><p className="mr-auto registration-modal-title"><strong>Politique d'inscription, du paiement et d'annulation</strong></p></Fragment>,
            modalTitleTpp: <Fragment><p className="mr-auto registration-modal-title"><strong>Termes et conditions</strong></p></Fragment>,
            modalContentAvsec: <Fragment>
                <p className="registration-modal-content">1. Une fois votre formulaire d'inscription approuvé, une facture pro-forma sera envoyée par courrier électronique à votre attention.</p>
                <p className="registration-modal-content">2. Votre inscription ne sera confirmée qu'une fois le paiement reçu.</p>
                <p className="registration-modal-content">3. Le paiement intégral est dû à la réception de la facture.</p>
                <p className="registration-modal-content">4. Le paiement du cours doit être effectué au moins 30 jours avant le début du cours.</p>
                <p className="registration-modal-content">5. Les participants ne seront pas autorisés à participer à un cours sans preuve de paiement.</p>
                <p className="registration-modal-content">6. L'OACI et / ou AFSAC ont le droit de reporter / annuler les cours et de supprimer l'enregistrement.</p>
                <p className="registration-modal-content">7. Si un cours est annulé par l'OACI ou l'AFSAC, les frais d'inscription seront remboursés et AFSAC paiera les frais de virement bancaire.</p>
                <p className="registration-modal-content">8. L'annulation du participant doit être faite par écrit et reçue par l'OACI / AFSAC au moins 2 semaines avant le cours.</p>
            </Fragment>,
            modalContentTpp: <Fragment>
                <p className="registration-modal-content mb-2">Veuillez lire attentivement avant de soumettre votre formulaire d'inscription:</p>
                <p className="registration-modal-content-title" style={{ margin: 0 }}><strong>Inscription:</strong></p>
                <p className="registration-modal-content mb-2">Le paiement intégral est dû à la réception de la facture. Le paiement du cours doit être effectué au moins 2  semaines avant le début du cours. Les stagiaires ne seront pas autorisés à participer à un cours sans preuve de paiement.</p>

                <p className="registration-modal-content-title" style={{ margin: 0 }}><strong>Annulation:</strong></p>
                <p className="registration-modal-content mb-2">Les membres de l'OACI et du TPP ont le droit de reporter / d'annuler des cours et de refuser l'inscription. Dans le cas où un cours est annulé par l'OACI, les frais d'inscription seront soit transférés à un futur cours OACI de valeur identique ou inférieure, soit remboursés. L'annulation du participant doit être faite par écrit et reçue par l'OACI au moins 2 semaines avant le cours.</p>

                <p className="registration-modal-content-title" style={{ margin: 0 }}><strong>Transferts:</strong></p>
                <p className="registration-modal-content mb-2">L'inscription au cours est transférable à d'autres membres individuels ou aux cours avec notification écrite, au plus tard 2 semaines avant la date de début du cours. Des frais d'administration peuvent s'appliquer.</p>
            </Fragment>,
            modalBtn: "Accepter",
            modalCloseBtn: "Fermer",
            checkboxErr: "Vous devez accepter la politique d'inscription et de paiement d'abord !",

            formSuccess: "Merci pour votre inscription !",
            formError1: "L'adresse mail existe déjà !",
            formConnexionError: "Erreur de connexion. Réessayer !",
        },

        search: {
            loading: "Chargement des résultats...",
            title: "Les résultats de recherche",
            found: "Résultat(s) trouvé(s) pour ",
            notFound: "Aucun résultat(s) trouvé(s)",
            error: "Erreur de connexion, merci de réessayer !",
        }
    },

    "en": {
        navbar: {
            contact: "Contact us",
            aboutUs: "About us",
            training: "ICAO Courses",
            trainingDesc: `You feel involved in this field and wish to work there, AFSAC Center AVSEC / OACI offers you "Qualifying" and "Certifying" training in order to become an expert in Civil Aviation Security in accordance with the international standards of the Organization of International Civil Aviation (ICAO)`,
            avsecCourses: "CONSULT AVSEC / ICAO COURSES",
            tppCourses: "CONSULT TRAINAIR PLUS COURSES",
            virtualCourses: "CONSULT VIRTUAL CLASSROOM COURSES",
            onlineCourses: "CONSULT ONLINE COURSES",
			x1: "AVSEC / OACI courses",
			x2: "TRAINAIR PLUS courses",
			x4: "VIRTUELS courses",
			x3: "Online courses",
            sessions: "Upcoming sessions",
            sessionsDesc: "Discover our upcoming sessions",
            accredations: "Our Accreditations And Acknowledgments",
            news: "Media & News",
            searchPlaceholder: "Search...",
            y1: "Consult all courses AVSEC / OACI",
            y2: "Consult all courses TRAINAIR PLUS",
            y3: "Consult all online courses",
            y4: "Consult all virtual courses"
        },

        promoBanner: {
            seeMoreButton: "See more"
        },

        footer: {
            sitemap: "Site plan",
            courses: "Courses",
        },

        contactForm: {
            title: "Contact us",
            lastName: 'Last name',
            firstName: 'First name',
            email: "Email",
            phone: "Phone",
            country: "Country",
            organism: "Organism",
            object: "Object",
            objectPlaceholder: "Choose",
            object1: "Training Application",
            object2: "Teach courses at AFSAC", // DONT REORDER THE OBJECTS !!!!!! CONTACT THE DEVELOPER FIRST
            object3: "Partnership request",
            object4: "Other",
            cv: "Curriculum vitae (PDF/DOCX/JPG/PNG)",
            letter: "Cover letter (PDF/DOCX/JPG/PNG)",
            browsePlaceholder: "Choisir",
            diploma: "Certificates / Diploma (PDF/DOCX/JPG/PNG)",
            website: "Website",
            message: "Message",
            submitButton: "Send",
            formExist: "We have already received your contact request before!",
            formSuccess: "We have received your contact request!",
            formError: "Connection error, check your internet connection!",

        },

        homePage: {
            sloganHome: `Our duty is to represent, lead and promote the civil aviation industry`,
            afsacPresentation: `AFSAC of Tunis-Tunisia, is an aeronautical training center with two international labels of excellence issued by the International Civil Aviation Organization (ICAO), namely the Regional Aviation Safety Training Center ICAO and TRAINAIR PLUS FULL MEMBER GAT / OACI. As such, it is the only private center in the world with these two labels. AFSAC of Tunis-TUNISIA is able to provide excellent training, to cover all the standards and requirements of the ICAO covered by the 19 annexes to the Chicago Convention, to all aeronautical players on a global scale, in its own premises and / or on site, attested by certificates issued by ICAO.`,
            programTitle: `TRAINAIR PLUS PROGRAM`,
            securityTitle: `SECURITY AND FACILITATION`,
            securityButton: "See more"
        },

        breadcrumb: {
            home: "Home",
        },

        contactPage: {
            contactDesc: `Whether you have a question about an existing training or a request for customization of one, or a request for training in another country, do not hesitate to contact us. Our team is always at your disposal to inform you and answer all your questions.`,
            visitUs: "Visit us:",
            CallUs: "Call us:",
            EmailUs: "Send us an email:",
            SocialMedia: "Social media:",
            requestDesc: "Would you like to take a training course in your own establishment?",
            requestDesc1: "Click the button below to complete the form and submit a request.",
            requestButton: "SUBMIT A REQUEST"
        },

        aboutUsPage: {
            title1: "Word of the CEO",
            // motDG: `Dear Internet users / visitors, AFSAC : The ICAO Regional Aviation Security Training Center of Tunis - TUNISIA and TRAINAIR PLUS FULL MEMEBER GAT / ICAO welcome you to its website. Our digital platform presents a meeting place and convergence for all stakeholders in all sectors of civil aviation. By this means of communication, we aspire to get closer to our target audience, to make ourselves better known and to serve them better, above all. The growing civil...`,
            motDG: <Fragment>
                <p className="registration-modal-content">Dear Internet users / visitors,</p>
                <p className="registration-modal-content">AFSAC : The ICAO Regional Aviation Security Training Center of Tunis - TUNISIA and TRAINAIR PLUS FULL MEMEBER GAT / ICAO welcome you to its website.</p>
                <p className="registration-modal-content">Our digital platform presents a meeting place and convergence for all stakeholders in all sectors of civil aviation.</p>
                <p className="registration-modal-content">By this means of communication, we aspire to get closer to our target audience, to make ourselves better known and to serve them better, above all.</p>
                <p className="registration-modal-content">The growing civil aviation sector around the world constantly faces major security and safety challenges. Thus, it remains essential to continue elaborating and developing regulations, civil aviation safety supervision procedures, aviation safety and airport security, and the technical benchmarks to meet the expectations of the international civil aviation’s authorities.</p>
                <p className="registration-modal-content">The regulatory, procedural and technical framework must necessarily be associated with an optimal training policy and a staff imbued with expertise, efficiency and rigor.</p>
                <p className="registration-modal-content">It is in this sense and in order to support States in the application of the Standards and Recommended Practices of Annex 17 that the ICAO had set up a network of thirty-five Aviation Security Training Centers (CRFSA) around the world, including AFSAC, which aims to develop the training and qualification of personnel from organizations working in the field of civil aviation by relying on educational materials developed and recognized by ICAO.</p>
                <p className="registration-modal-content">This is how we can together participate in the effort to achieve the goal that could make international civil aviation safe and secure.</p>
                <p className="registration-modal-content"><strong>Dear Internet users,</strong></p>
                <p className="registration-modal-content">Your observations, suggestions and constructive criticism are awaited to improve the quality of this platform.</p>
                <p className="registration-modal-content">Good navigation !</p>
                <p className="registration-modal-content"><strong>CEO</strong></p>
                <p className="registration-modal-content"><strong>Mr. Hassan SEDDIK</strong></p>
            </Fragment>,
            title2: "The creation of the AFSAC",
            title2Desc: "AFSAC is part of the 35 ASTC of the OACI in the world and belongs to the EUR NAT region. It is now the first and only private training center in the world to be accredited by the OACI as a full member of TRAINAIR PLUS and a training center for OACI air safety (ASTC).",

            title3: "Our ICAO Courses",
            allCourses: "All courses",
            coursesAll: "Consult the courses",
            checkUpcommingSessions: "Consult the upcomiong sessions",
            UpcommingSessions: "Consult our courses of ",
			OnlineCourses : "Consult our online ccourses",
            ourReferences: "Our references",

            title4: "Accommodation",
            accomodationDesc: `The AFSAC Regional Training Center for the Aviation Security of the OACI Tunis-TUNISIE and TRAINER PLUS Full Member GAT / OACI has at its disposal these foreign participants several facilities such as: Obtaining an airport visa, reservation of hotel, airport reception, internal transport, medical services, Preferred accommodation rates and Restaurant. For further information, we invite you to consult the PDF document below, which was developed by AFSAC.`,
            accomodationButton: "Download the facilitation sheet",

        },

        coursesSearchBox: {
            checkboxCours: "Courses",
            checkboxWorkshop: "Workshops",
            categoriesLabel: "Categories",
            categoriesPlaceholder: "All",
            keywordLabel: "Keyword",
            keywordPlaceholder: "Course name...",
            searchButton: "Search",
            clearButton: "Reset",
            noResultsText: "No results found",
        },

        avsecPages: {
            upcommingAvsecTitle: "Upcoming sessions AVSEC",
            upcommingVirtualTitle: "Upcoming virtual sessions",
        },

        TppPages: {
            bannerTitle1: `"Learn any time"`,
            bannerTitle2: `"Learn anywhere"`,
            bannerDesc: `The AFSAC TRAINAIR PLUS FULL MEMBER GAT / OACI is in a position to dispense with training in excellence, to cover all the standards and requirements of the OACI objects of the 19 annexes to the Chicago Convention, to all aeronautical actors in the 'échelle global, in its own premises and / or on site, certified by the certificates issued by the OACI.`,
            upcommingTppTitle: "Upcoming Sessions trainair plus",
            TppTitleDesc: "",

            // Brochure:
            downloadBrochure: "Download our catalog",
            downloadBrochureDesc: "Insert your email to download an printable catalog with the description of our trainings.",
            downloadEmailPlaceholder: "E-mail",
            downloadBrochureButton: "Download",
            brochureFormSuccess: "Thank you ! the catalog will be sent by email",
            brochureFormError1: "The catalog has already been sent to this address",
            brochureFormConnexionError: "Connexion error. Try again!",
        },

        accredationsPage: {
            title1: "Our Accreditations And Acknowledgments",
            desc1: `The Tunisian-French Academy of Civil Aviation Safety and Security "AFSAC" was created under Tunisian regulations, Its head office is located at 7 avenue Taha Hussein, 1008 - Montfleury - Tunis - TUNISIA`,
            desc2: `It is approved by the Directorate General of Civil Aviation and by the Ministry of Employment and Vocational Training under ID: 10-1064-1: 1 on August 9, 2010 under the name "AFSAC".`,
            desc3: `It is the first and only Private Training Center in the world accredited by ICAO as an ICAO Aviation Security Training Center (ASTC) and a TRAINAIR PLUS Full Member GAT / ICAO program.`,
            desc4: `AFSAC is one of 35 training centers worldwide accredited by ICAO as an ICAO Aviation Security Training Center. `,
            desc5: `These accreditations represent recognition by ICAO of the credibility and professionalism of AFSAC. `,
            // Recogintions:
            recognitionTitle: `AFSAC has obtained important accreditations and recognitions:`,
            recognitionElements: [
                "ICAO : Aviation Security Training Center ",
                "ICAO : TRAINAIRPLUS Program Full Member-GAT ",
                "AFRAA : African Airlines Association ",
                "ACAO : Arab Civil Aviation Organization ",
                "IAAAA : Incheon Airport Aviation Academy ",
                "ERNAM : Regional School for Air Navigation and Management ",
                "ACI : Airport Council International ",
            ],

            // Security Training:
            securityTitle: `ICAO Aviation Security Training Center (ASTC)`,
            securityDesc: `As part of its training mission for aeronautical personnel, the ICAO Aviation Security Training Center of Tunis - TUNISIA has a team of professionals and highly qualified instructors who watch over the organization of courses in the field of civil aviation. ASTC Tunis also offers solutions adapted to the needs of its customers, and to the various players in the field of aviation directly on site and all over the world which has given it a national and international reputation.`,

            // Full Membership Program:
            membershipTitle: `TRAINAIRPLUS Full Membership Program GAT /ICAO:`,
            membershipDesc: `The International Civil Aviation Organization (ICAO) has launched the TRAINAIR PLUS (TPP) program to improve collaboration between member states in training to promote the safety, security, and sustainable development of air transport.`,

            slogan: "SLOGAN",
            sloganDesc1: `Our duty is to represent,`,
            sloganDesc2: `lead and promote the civil aviation industry`,

            // VISION & MISSION:
            visonTitle: "Our vision",
            visionDesc: `To position ourself as a credible partner in training and skills' development in the aviation industry.`,

            missionTitle: "Our mission",
            missionDesc: `Provide ongoing support for the skills' development of our clients in accordance with ICAO requirements.`,
        },

        references: {
            referencesTitle: "References",
            referencesDesc: " Lorem ipsum dolor sit amet, at nibh necessitatibus sit, fugit nobis detraxit ea quo. Vim ea posse commune accumsan.",
        },

        mediaNews: {
            bigTitle: 'Media & News',
            news: "News",
            lastNews: "Latest News",
            aLaUne: 'On the front page',
            titleAlbums: 'PHOTOS ALBUMS',
            titleVideos: 'VIDEOS & TESTIMONIALS',
            titleComminiques: 'PRESS RELEASES',
            seeMore: "See more",
            seeAlbum: "See album",
            seeVideo: "See video",
        },

        newsletter: {
            newsletterDesc: "Subscribe to our newsletter and stay informed on our training schedule",
            namePlaceholder: "Name",
            emailPlaceholder: "Email",
            buttonText: "Subscribe",
            newsNameErr: "Please enter your NAME!",
            newsEmailErr: "Please enter a valid EMAIL",
            newsEmailExist: "You are already registered !",
            newsSuccess: "Thank you ! Your registration is saved. You will receive our NEWSLETTER every Tuesday.",
            newsConexErr: "Error, Please check your internet connection!",
        },

        partners: {
            title: "Our partners"
        },

        soloPage: {
            trainingButton: `Request this course`,
            subscribeButton: `Register to this course`,
            brochureButton: `Download the brochure`,
            objectives: `Learning Objectives:`,
            content: `Purpose of the Course:`,
            population: `Target population:`,
            entry: `Entry conditions:`,
            place: `Training site:`,
            duration: `Duration`,
            price: `Price`,
            language: `Language`,
            delivery: `Delivery Mode`,
            category: `Training Area:`
        },

        courseCard: {
            objectives: "Learning objectives:",
            seeMoreButton: "See more",
            workshop: "WORKSHOP",
            course: "COURSE",
            classroom: "Classroom",
            virtualClassroom: "Virtual Classroom",
            online: "Online",
        },

        registration: {
            breadCrumbTitle: "Registration",
            backButton: "Back",
            registrationTitle: "Course registration",

            personalInfo: "Personal informations",
            personalTitle: "Personal title",
            mrValue: "Mr",
            mrsValue: "Mrs",
            firstName: "First name",
            firstNamePlaceholder: "",
            lastName: "Last name",
            lastNamePlaceholder: "",
            email: "Email",
            emailPlaceholder: "",
            emailCaption: "A confirmation email will be sent to this address.",


            EmploymentInfo: "Job information",
            jobTitle: "Profession",
            organization: "Organization Type",
            organizationType: "Organization Name",
            organizationsOptions: [
                "Company",
            ],
            supervisorName: "Supervisor's name",
            supervisorJob: "Supervisor's profession",
            supervisorEmail: "Supervisor email",
            supervisorPhone: "Supervisor's phone",

            contactInfo: "Contact information",
            address: "Address",
            city: "City",
            state: "State",
            postalCode: "Postal code",
            country: "Country",
            phone: "Phone",
            secondPhone: "Second Phone",

            invoicingInfo: "Invoice information",
            invoicingName: "Billing to (full name)",
            invoicingAdresse: "Billing address",

            checkboxLabel: "I accept the registration and payment policy",
            buttonSubmit: "Send my training request",


            // error msgs :
            firstNameErr: `Please enter a valid first name!`,
            lastNameErr: "Please enter a valid name!",
            emailErr: "Please enter a valid email !",

            jobTitleErr: "Please enter a valid profession!",
            organizationErr: "Please enter a valid organization name !",
            organizationTypeErr: "Please choose an organization type !",
            supervisorNameErr: "Please enter a valid Supervisor Name !",
            supervisorJobErr: "Please enter the Supervisor's Profession !",
            supervisorEmailErr: "Please enter a valid Supervisor Email!",
            supervisorPhoneErr: "Please enter a valid Supervisor Phone number!",

            addressErr: "Please enter a valid address !",
            cityErr: "Please enter a valid City !",
            stateErr: "Please enter a valid state !",
            postalCodeErr: "Please enter a valid Postal Code !",
            countryErr: "Please enter the name of your Country !",
            phoneErr: "Please enter a valid phone number !",
            // secondPhoneErr: "Second Téléphone",

            invoicingNameErr: "Please fill in the Billing to (full name) field !",
            invoicingAdresseErr: "Please enter a valid billing address !",

            modalTitleAvsec: <Fragment><p className="mr-auto registration-modal-title"><strong>Registration, payment and cancellation policy:</strong></p></Fragment>,
            modalTitleTpp: <Fragment><p className="mr-auto registration-modal-title"><strong>Terms and Conditions</strong></p></Fragment>,
            modalContentAvsec: <Fragment>
                <p className="registration-modal-content">1. Once your registration form has been approved, a pro-forma invoice will be send to you. </p>
                <p className="registration-modal-content">2. Your registration will only be confirmed once payment has been received.</p>
                <p className="registration-modal-content">3. Integral payment must be done once the invoice has been received.</p>
                <p className="registration-modal-content">4. Payment must be done at least 30 days before the start of the course.</p>
                <p className="registration-modal-content">5. Participants will not be allowed to participate in a course without a proof of payment.</p>
                <p className="registration-modal-content">6. ICAO and / or AFSAC have the right to postpone / cancel courses and delete registration.</p>
                <p className="registration-modal-content">7. If ICAO or AFSAC cancels a course, the registration fee will be refunded, and AFSAC will pay the bank transfer fee.</p>
                <p className="registration-modal-content">8. ICAO / AFSAC must receive the participant’s cancellation letter at least 2 weeks before the course beginning.</p>
            </Fragment>,
            modalContentTpp: <Fragment>
                <p className="registration-modal-content mb-2">Please read carefully before submitting your registration form:</p>
                <p className="registration-modal-content-title" style={{ margin: 0 }}><strong>Registration:</strong></p>
                <p className="registration-modal-content mb-2">Full payment is due upon receipt of the invoice. Payment of the course must be made at least 2 weeks prior to the start of the course. Students will not be allowed to participate in a course without proof of payment.</p>

                <p className="registration-modal-content-title" style={{ margin: 0 }}><strong>Cancellation:</strong></p>
                <p className="registration-modal-content mb-2">ICAO and TPP Members have the right to postpone/ cancel courses and decline registration. In the event that a course is cancelled by ICAO the registration fee will be either transferred to a future ICAO course of same or inferior value, or refunded. Participant cancellation must be made in writing and received by ICAO at least 2 weeks before the course.</p>

                <p className="registration-modal-content-title" style={{ margin: 0 }}><strong>Transfers:</strong></p>
                <p className="registration-modal-content mb-2">Course registration is transferable to other individual members or courses with written notification, no later than 2 weeks prior to the course start date. Administration fees may apply.</p>
            </Fragment>,
            modalBtn: "Accept",
            modalCloseBtn: "Close",
            checkboxErr: "You must agree to the registration and payment policy first !",

            formSuccess: "Thank you for your registration !",
            formError1: "The email address already exists !",
            formConnexionError: "Connexion error. Try again !",
        },

        search: {
            loading: "Loading results...",
            title: "Search results",
            found: "Result(s) found for",
            notFound: "No result(s) found",
            error: "Connection error, please try again!",
        }
    },

    "ar": {

    },
};

