// Fetching APi's :
import axios from 'axios';

// Global Config :
import {
  config,
} from '../../constants/AppConfig';
// import Swal from 'sweetalert2/dist/sweetalert2.js';
// import { trans } from '../../constants/Translations';
/*
-------------------------------
  SEARCH FOR COURSES
-------------------------------
*/
export const searchCourses = (value) => async (dispatch) =>
{
    let objectToSend =
    {
        titre: value
    };
  // console.log("Sending value :D =>", objectToSend);

  return axios.post(`${config.url}Search`, objectToSend)
    .then((res) => {
      let searchResults = [];

      for (let i = 0; i < 4; i++) {
        searchResults = searchResults.concat(res.data.formationEn[i]);
      };
      // console.log("Filtred search results => ", searchResults);

      dispatch({
        type: 'GET_SEARCH_RESULTS',
        payload: searchResults,
      });

    })
    .catch((err) => {
      console.log("Search error", err);
    });
};


/*
-------------------------------
  REGISTRATION TO COURSE FORM
-------------------------------
*/
export const sendBrochureToUser = (email, language) => async (dispatch) => {

  let objectToSend = {
    email: email,
    lang: language,
  };

  console.log("Sending sendBrochureToUser  =>", objectToSend);
  return axios.post(`${config.url}emailCatalogue`, objectToSend)
};


/*
-------------------------------
  REGISTRATION TO COURSE FORM
-------------------------------
*/
export const sendRegistration = (objectToSend) => async (dispatch) => {
  console.log("Sending Registration form =>", objectToSend);
  return axios.post(`${config.url}ParticiperFormation`, objectToSend)
};


/*
-------------------------------
  CONTACT FORM
-------------------------------
*/
export const sendContactForm = (name, fname, email, phone, objet, site, message) => async (dispatch) => {

  let objectToSend = {
    nom: name,
    prenom: fname,
    email: email,
    phone: phone,
    objet: objet,
    site_web: site,
    message: message,
  }

  return axios.post(`${config.url}contact`, objectToSend)
    .then(response => {
      console.log("Sending contact form result ==>", response.data);
    })
    .catch(error => {
      console.log("Sending contact form Error ==>", error);
    });
};



/*
-------------------------------
  NEWSLETTER
-------------------------------
*/

export const sendNewsletterForm = (name, email) => async (dispatch) => {

  let objectToSend = {
    nom: name,
    email: email,
  };

  return axios.post(`${config.url}newsLetter`, objectToSend)
  // .then(response => {
  //   console.log("Sending newsletter data result =>", response.data);

  //   if(response.data === false) {
  //     Swal.fire({
  //       position: 'center',
  //       icon: 'error',
  //       title: 'Vous êtes déjà inscrit !',
  //       showConfirmButton: false,
  //       timer: 3000
  //     });
  //   } else {
  //     Swal.fire({
  //       position: 'center',
  //       icon: 'success',
  //       title: 'Merci ! Votre inscription est enregistrée. Vous recevrez notre NEWSLETTER chaque mardi.',
  //       showConfirmButton: false,
  //       timer: 3000
  //     });
  //   }

  // })
  // .catch(error => {
  //   console.log("Sending newsletter data Error =>", error);
  //   Swal.fire({
  //     position: 'center',
  //     icon: 'error',
  //     title: 'Erreur, Merci de vérifier votre connexion internet !',
  //     showConfirmButton: false,
  //     timer: 2000
  //   })

  // });
};



/*
-------------------------------
  CATALOGUE
-------------------------------
*/

export const getBrochure = (lang) => async (dispatch) => {

  // console.log("Calling getAvsecCourses witth :D ", lang);

  return axios.get(`${config.url}PDFCatalogue`)
    .then(response => {
      console.log("getBrochure response Api =>", response.data);

      // let brochure = lang === "fr" ? response.data.fichier_fr[0].download_link : lang === "en" ? response.data.fichier_en[0].download_link : response.data.fichier_ar[0].download_link;

      let brochure = response.data.fichier_en[0].download_link;

      dispatch({
        type: 'GET_BROCHURE',
        payload: brochure,
      });
    })
    .catch(error => {
      console.log("getBrochure error Api", error);
    });
};

/*
-------------------------------
  UPCOMMING COURSES
-------------------------------
*/
export const getUpcommingCourses = (lang) => async (dispatch) => {

  // console.log("Calling getAvsecCourses witth :D ", lang);

  return axios.get(`${config.url}upcomingSession/${lang}`)
    .then(response => {
      // console.log("getUpcommingCourses response Api =>", response.data);

      let top = [];
      let top1 = [];
      let top2 = [];

      let bottom = [];
      let bottom1 = [];
      let bottom2 = [];

      let avsecSortedByLang = [];
      let trainAirSortedByLang = [];
      let VirtualSortedByLang = [];


      if (lang === "fr") {

        top = response.data.AvsecCourses.filter((c) => c.langue.toLowerCase() === "francais");
        top1 = response.data.TrainairPlus.filter((c) => c.langue.toLowerCase() === "francais");
        top2 = response.data.VirtualClassrooms.filter((c) => c.langue.toLowerCase() === "francais");

        bottom = response.data.AvsecCourses.filter((c) => c.langue.toLowerCase() !== "francais");
        bottom1 = response.data.TrainairPlus.filter((c) => c.langue.toLowerCase() !== "francais");
        bottom2 = response.data.VirtualClassrooms.filter((c) => c.langue.toLowerCase() !== "francais");

      } else if (lang === "en") {

        top = response.data.AvsecCourses.filter((c) => c.langue.toLowerCase() === "anglais");
        top1 = response.data.TrainairPlus.filter((c) => c.langue.toLowerCase() === "anglais");
        top2 = response.data.VirtualClassrooms.filter((c) => c.langue.toLowerCase() === "anglais");

        bottom = response.data.AvsecCourses.filter((c) => c.langue.toLowerCase() !== "anglais");
        bottom1 = response.data.TrainairPlus.filter((c) => c.langue.toLowerCase() !== "anglais");
        bottom2 = response.data.VirtualClassrooms.filter((c) => c.langue.toLowerCase() !== "anglais");


      } else {

        top = response.data.AvsecCourses.filter((c) => c.langue.toLowerCase() === "anglais");
        top1 = response.data.TrainairPlus.filter((c) => c.langue.toLowerCase() === "anglais");
        top2 = response.data.VirtualClassrooms.filter((c) => c.langue.toLowerCase() === "anglais");

        bottom = response.data.AvsecCourses.filter((c) => c.langue.toLowerCase() !== "anglais");
        bottom1 = response.data.TrainairPlus.filter((c) => c.langue.toLowerCase() !== "anglais");
        bottom2 = response.data.VirtualClassrooms.filter((c) => c.langue.toLowerCase() !== "anglais");


      };
      avsecSortedByLang = top.concat(bottom);
      trainAirSortedByLang = top1.concat(bottom1);
      VirtualSortedByLang = top2.concat(bottom2);


      dispatch({
        type: 'GET_UPCOMMING_COURSES',
        // payload : response.data.AvsecCourses,
        // payload1 : response.data.TrainairPlus,
        // payload2 : response.data.VirtualClassrooms,
        payload: avsecSortedByLang,
        payload1: trainAirSortedByLang,
        payload2: VirtualSortedByLang,
      });
    })
    .catch(error => {
      console.log("getUpcommingCourses error Api", error);
    });
};



/*
-------------------------------
  GETTING COURSES
-------------------------------
*/
// Get Courses Category:
export const getTppCategories = (lang) => async (dispatch) => {

  return axios.get(`${config.url}Category/${lang}`)
    .then(response => {
      // console.log("getTppCategories response Api =>", response.data.TrainairCategory);

      dispatch({
        type: 'GET_TPP_CATEGORIES',
        payload: response.data.TrainairCategory,
      });
    })
    .catch(error => {
      console.log("getTppCategories error Api", error);
    });

};

// VIRTUAL
export const getVirtualCourses = (lang) => async (dispatch) => {

  return axios.get(`${config.url}VirtualClassroom/${lang}`)
    .then(response => {
      // console.log("getVirtualCourses response Api =>", response.data);

      let top = [];
      let bottom = [];
      let sortedCoursesByLang = [];


      if (lang === "fr") {

        top = response.data.VirtualClassromEn.filter((c) => c.langue.toLowerCase() === "francais");
        bottom = response.data.VirtualClassromEn.filter((c) => c.langue.toLowerCase() !== "francais");

      } else if (lang === "en") {

        top = response.data.VirtualClassromEn.filter((c) => c.langue.toLowerCase() === "anglais");
        bottom = response.data.VirtualClassromEn.filter((c) => c.langue.toLowerCase() !== "anglais");

      } else {

        top = response.data.VirtualClassromEn.filter((c) => c.langue.toLowerCase() === "arabe");
        bottom = response.data.VirtualClassromEn.filter((c) => c.langue.toLowerCase() !== "arabe");

      };

      sortedCoursesByLang = top.concat(bottom);
      // console.log("Oh sorted =>", sortedCoursesByLang);

      dispatch({
        type: 'GET_VIRTUAL_COURSES',
        // payload : response.data.VirtualClassromEn,
        payload: sortedCoursesByLang,
      });
    })
    .catch(error => {
      console.log("getVirtualCourses error Api", error);
    });
};

// ONLINE
export const getOnlineCourses = (lang) => async (dispatch) => {

  return axios.get(`${config.url}OnlineCourses/${lang}`)
    .then(response => {
      // console.log("getOnlineCourses response Api =>", response.data);

      let top = [];
      let bottom = [];
      let sortedCoursesByLang = [];

      if (lang === "fr") {

        top = response.data.OnlineCoursesEn.filter((c) => c.langue.toLowerCase() === "francais");
        bottom = response.data.OnlineCoursesEn.filter((c) => c.langue.toLowerCase() !== "francais");

      } else if (lang === "en") {

        top = response.data.OnlineCoursesEn.filter((c) => c.langue.toLowerCase() === "anglais");
        bottom = response.data.OnlineCoursesEn.filter((c) => c.langue.toLowerCase() !== "anglais");

      } else {

        top = response.data.OnlineCoursesEn.filter((c) => c.langue.toLowerCase() === "arabe");
        bottom = response.data.OnlineCoursesEn.filter((c) => c.langue.toLowerCase() !== "arabe");

      };

      sortedCoursesByLang = top.concat(bottom);
      // console.log("Oh sorted =>", sortedCoursesByLang);


      dispatch({
        type: 'GET_ONLINE_COURSES',
        payload: sortedCoursesByLang,
      });
    })
    .catch(error => {
      console.log("getOnlineCourses error Api", error);
    });
};

// AVSEC
export const getAvsecCourses = (lang) => async (dispatch) => {

  // console.log("Calling getAvsecCourses witth :D ", lang);

  return axios.get(`${config.url}AvsecCourses/${lang}`)
    .then(response => {
      // console.log("AvsecCourses response Api =>", response.data)

      let top = [];
      let bottom = [];
      let sortedCoursesByLang = [];


      if (lang === "fr") {

        top = response.data.avsecCoursesEn.filter((c) => c.langue.toLowerCase() === "francais");
        bottom = response.data.avsecCoursesEn.filter((c) => c.langue.toLowerCase() !== "francais");

      } else if (lang === "en") {

        top = response.data.avsecCoursesEn.filter((c) => c.langue.toLowerCase() === "anglais");
        bottom = response.data.avsecCoursesEn.filter((c) => c.langue.toLowerCase() !== "anglais");

      } else {

        top = response.data.avsecCoursesEn.filter((c) => c.langue.toLowerCase() === "arabe");
        bottom = response.data.avsecCoursesEn.filter((c) => c.langue.toLowerCase() !== "arabe");

      };

      sortedCoursesByLang = top.concat(bottom);
      // console.log("Oh sorted =>", sortedCoursesByLang);

      dispatch({
        type: 'GET_AVSEC_COURSES',
        // payload : response.data.avsecCourses,
        payload: sortedCoursesByLang,
      });
    })
    .catch(error => {
      console.log("AvsecCourses error Api", error);
    });
};

export const getSoloAvsecCourse = (platCourse, idCourse, lang) => async (dispatch) => {

  return axios.get(`${config.url}SoloFormation/${platCourse}/${idCourse}/${lang}`)
    .then(response => {
      console.log("getSoloAvsecCourse response Api =>", response.data);

      // Filter Formation:
      let filterFormation = [];
      if (response.data.formationEn.objectif) {
        filterFormation = response.data.formationEn.objectif.split(/\r?\n/);
      }
      response.data.formationEn.objectif = filterFormation;


      filterFormation = [];
      if (response.data.formationEn.description) {
        filterFormation = response.data.formationEn.description.split(/\r?\n/);
      }
      response.data.formationEn.description = filterFormation;

      filterFormation = [];
      if (response.data.formationEn.entry) {
        filterFormation = response.data.formationEn.entry.split(/\r?\n/);
      }
      response.data.formationEn.entry = filterFormation;


      // Filter Similar:
      response.data.formationSimilarEn.map(similar => {
        let filterSimilar = [];
        if (similar.objectif) {
          filterSimilar = similar.objectif.split(/\r?\n/);
        }
        similar.objectif = filterSimilar;

        filterSimilar = [];
        if (similar.description) {
          filterSimilar = similar.description.split(/\r?\n/);
        }
        similar.description = filterSimilar;

        filterSimilar = [];
        if (similar.entry) {
          filterSimilar = similar.entry.split(/\r?\n/);
        }
        similar.entry = filterSimilar;
      });

      dispatch({
        type: 'GET_SOLO_AVSEC_COURSE',
        payload: response.data.formationEn,
        payload1: response.data.formationSimilarEn,
      });
    })
    .catch(error => {
      console.log("getSoloAvsecCourse error Api", error);
    });
};


// TPP
export const getTppCourses = (lang) => async (dispatch) => {

  return axios.get(`${config.url}TrainAirPlus/${lang}`)
    .then(response => {
      // console.log("getTppCourses response Api =>", response.data);

      // Filter Tpp:
      response.data.TrainAirPlusEn.map(tpp => {
        let filter = [];
        if (tpp.objectif) {
          filter = tpp.objectif.split(';');
        }
        tpp.objectif = filter;
      });

      let top = [];
      let bottom = [];
      let sortedCoursesByLang = [];


      if (lang === "fr") {

        top = response.data.TrainAirPlusEn.filter((c) => c.langue.toLowerCase() === "francais");
        bottom = response.data.TrainAirPlusEn.filter((c) => c.langue.toLowerCase() !== "francais");

      } else if (lang === "en") {

        top = response.data.TrainAirPlusEn.filter((c) => c.langue.toLowerCase() === "anglais");
        bottom = response.data.TrainAirPlusEn.filter((c) => c.langue.toLowerCase() !== "anglais");

      } else {

        top = response.data.TrainAirPlusEn.filter((c) => c.langue.toLowerCase() === "arabe");
        bottom = response.data.TrainAirPlusEn.filter((c) => c.langue.toLowerCase() !== "arabe");

      };

      sortedCoursesByLang = top.concat(bottom);

      dispatch({
        type: 'GET_TPP_COURSES',
        // payload : response.data.TrainAirPlusEn,
        payload: sortedCoursesByLang,
      });
    })
    .catch(error => {
      console.log("getTppCourses error Api", error);
    });
};

export const getSoloTppCourse = (platCourse, idCourse, lang) => async (dispatch) => {

  return axios.get(`${config.url}SoloFormation/${platCourse}/${idCourse}/${lang}`)
    .then(response => {
      console.log("getSoloTppCourse response Api =>", response.data);

      // Filter Formation:
      let filterFormation = [];
      if (response.data.formationEn.objectif) {
        filterFormation = response.data.formationEn.objectif.split(/\r?\n/);
      }
      response.data.formationEn.objectif = filterFormation;


      filterFormation = [];
      if (response.data.formationEn.description) {
        filterFormation = response.data.formationEn.description.split(/\r?\n/);
      }
      response.data.formationEn.description = filterFormation;

      filterFormation = [];
      if (response.data.formationEn.entry) {
        filterFormation = response.data.formationEn.entry.split(/\r?\n/);
      }
      response.data.formationEn.entry = filterFormation;

      // Filter Similar:
      response.data.formationSimilarEn.map(similar => {
        let filterSimilar = [];
        if (similar.objectif) {
          filterSimilar = similar.objectif.split(/\r?\n/);
        }
        similar.objectif = filterSimilar;

        filterSimilar = [];
        if (similar.description) {
          filterSimilar = similar.description.split(/\r?\n/);
        }
        similar.description = filterSimilar;

        filterSimilar = [];
        if (similar.entry) {
          filterSimilar = similar.entry.split(/\r?\n/);
        }
        similar.entry = filterSimilar;
      });

      // console.log("sho", response.data.formationSimilar)

      dispatch({
        type: 'GET_SOLO_TPP_COURSE',
        payload: response.data.formationEn,
        payload1: response.data.formationSimilarEn,
      });
    })
    .catch(error => {
      console.log("getSoloAvsecCourse error Api", error);
    });
};


export const getUpcommingSessionsData = () => async (dispatch) => {

  return axios.get(`${config.url}upcomingSession/fr`)
    .then(response => {
      // console.log("getUpcommingSessionsData Data =>", response.data);

      // dispatch({
      //     type : 'GET_ACTUALITES_INTERNATIONALE',
      //     payload : response.data,
      // });
    })
    .catch(error => {
      console.log("erreur redux getUpcommingSessionsData", error);
    });
};


/*
-------------------------------
  GETTING PAGES DATA
-------------------------------
*/
export const getHomeData = (lang) => async (dispatch) => {

  return axios.get(`${config.url}homePage/${lang}`)
    .then(response => {
      // console.log("Home Page Data =>", response.data);

      dispatch({
        type: 'GET_HOME_DATA',
        payload: response.data,
      });
    })
    .catch(error => {
      console.log("erreur redux getHomeData", error);
    });

};

export const getAboutUsData = (lang) => async (dispatch) =>
{
    if(lang === "fr")
    {
        return axios.get(`${config.url}aboutUs/fr`)
            .then(response => {
                console.log("aboutUs Data =>", response.data);

                dispatch({
                    type: 'GET_ABOUT_US_DATA',
                    payload: response.data,
                });
            })
            .catch(error => {
                console.log("erreur redux getAboutUsData", error);
            });
    }

        if(lang === "en")
        {
            return axios.get(`${config.url}aboutUs/en`)
                .then(response => {
                    console.log("aboutUs Data =>", response.data);

                    dispatch({
                        type: 'GET_ABOUT_US_DATA',
                        payload: response.data,
                    });
                })
                .catch(error => {
                    console.log("error redux getAboutUsData", error);
                });
        }



};

export const getContactData = (lang) => async (dispatch) => {

  return axios.get(`${config.url}contactUs/${lang}`)
    .then(response => {
      // console.log("getContactData Data =>", response.data[0]);

      dispatch({
        type: 'GET_CONTACT_DATA',
        payload: response.data[0],
      });
    })
    .catch(error => {
      console.log("erreur redux getContactData", error);

    });
};

export const getMediaNewsData = (lang) => async (dispatch) => {

  return axios.get(`${config.url}mediaNews/${lang}`)
    .then(response => {
      console.log("getMediaNewsData Data =>", response.data);

      dispatch({
        type: 'GET_MEDIA_NEWS',
        payload: response.data.mediaNews.filter(a => a.titre !== null),
        payload1: response.data.MediaAlbums,
        payload2: response.data.MediaVideos,
        payload3: response.data.MediaCommuniques,
      });
    })
    .catch(error => {
      console.log("erreur redux getMediaNewsData", error);
    });
};


/*
-------------------------------
  SET SITE LANGUAGE
-------------------------------
*/
export const getSiteLanguage = () => async (dispatch) => {

  let lang = await localStorage.getItem('site-lang');

  // console.log("Site Language choosen =>", lang);

  if (lang) {
    dispatch({
      type: 'SET_SITE_LANGUAGE',
      payload: lang,
    });
  }
};

export const setSiteLanguage = (newLang, history) => async (dispatch) => {
  let goTo = history.location.pathname.replace("/", "");

  await localStorage.setItem('site-lang', newLang);

  history.go(goTo);
  window.location.href = "/"; // add by Habib Aroua
  dispatch({
    type: 'SET_SITE_LANGUAGE',
    payload: newLang,
  });

};

// Get About-us:
export const getAbout_us = (lang) => async (dispatch) => {

  return axios.get(`${config.url}About/${lang}`)
    .then(response => {
      console.log("getAbout_us response Api =>", response.data);

      dispatch({
        type: 'GET_ABOUT_US',
        payload: response.data,
      });
    })
    .catch(error => {
      console.log("getAbout_us error Api", error);
    });

};


export const getAccreditations = (lang) => async (dispatch) => {

  return axios.get(`${config.url}Accreditations_and_recognitions/${lang}`)
    .then(response => {
      console.log("getAccreditations =>", response);

      dispatch({
        type: 'GET_ACCREDITATIONS',
        payload: response,
      });
    })
    .catch(error => {
      console.log("erreur redux getAccreditations", error);
    });
};

//add by Habib
export const setSiteLanguageFr = (newLang, history) => async (dispatch) => {
    let goTo = history.location.pathname.replace("/", "");

    await localStorage.setItem('site-lang', "fr");


    history.go(goTo);

    dispatch({
        type: 'SET_SITE_LANGUAGE',
        payload: "fr",
    });
};

export const setSiteLanguageEn = (newLang, history) => async (dispatch) => {
    let goTo = history.location.pathname.replace("/", "");

    await localStorage.setItem('site-lang', "en");

    history.go(goTo);

    dispatch({
        type: 'SET_SITE_LANGUAGE',
        payload: "en",
    });
};