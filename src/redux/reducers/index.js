import { combineReducers } from 'redux';
import AfsacReducer from './AfsacReducer';

export default combineReducers({
    afsacR: AfsacReducer,
});
