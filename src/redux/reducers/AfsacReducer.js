

// let lang = localStorage.getItem('site-lang');

const initialState = {
    // siteLanguage: lang ? lang : 'en',
    siteLanguage: 'fr',
    homeData: null,
    aboutUsData: null,
    contactData: null,

    // Media & news
    mediaNews: [],
    mediaAlbums: [],
    mediaVideos: [],
    mediaCommuniques: [],

    // Courses Avsec:
    AvsecCoursesData: null,
    SoloAvsecCourseData: null,
    SoloAvsecCourseSimilarData: null,

    // Courses Tpp:
    TppCoursesData: null,
    SoloTppCourseData: null,
    SoloTppCourseSimilarData: null,
    TppCategories: null,

    // Courses Online:
    OnlineCoursesData: null,

    // Courses Virtual:
    VirtualCoursesData: null,

    // Catalogues
    brochure: null,

    // About-US
    about: '',

    // ACCREDITATIONS
    accreditations: '',

    // Search
    searchResults: [],
};

const AfsacReducer = (state = initialState, action) => {
    switch (action.type) {

        // Search:
        case 'GET_SEARCH_RESULTS':
            return {
                ...state,
                searchResults: action.payload,
            }

        // Catalogues:
        case 'GET_BROCHURE':
            return {
                ...state,
                brochure: action.payload,
            }


        // Upcomming Courses:
        case 'GET_UPCOMMING_COURSES':
            return {
                ...state,
                sessionsAvsecCoursesData: action.payload,
                sessionsTppCoursesData: action.payload1,
                sessionsVirtualCoursesData: action.payload2,
            }


        // Courses Virtual:
        case 'GET_VIRTUAL_COURSES':
            return {
                ...state,
                VirtualCoursesData: action.payload,
            }

        // Courses Online:
        case 'GET_ONLINE_COURSES':
            return {
                ...state,
                OnlineCoursesData: action.payload,
            }

        // Courses Tpp:
        case 'GET_TPP_CATEGORIES':
            return {
                ...state,
                TppCategories: action.payload,
            }

        case 'GET_SOLO_TPP_COURSE':
            return {
                ...state,
                SoloTppCourseData: action.payload,
                SoloTppCourseSimilarData: action.payload1,
            }

        case 'GET_TPP_COURSES':
            return {
                ...state,
                TppCoursesData: action.payload,
            }

        // Courses Avsec:
        case 'GET_SOLO_AVSEC_COURSE':
            return {
                ...state,
                SoloAvsecCourseData: action.payload,
                SoloAvsecCourseSimilarData: action.payload1,
            }

        case 'GET_AVSEC_COURSES':
            return {
                ...state,
                AvsecCoursesData: action.payload,
            }

        case 'GET_HOME_DATA':
            return {
                ...state,
                homeData: action.payload,
            }

        case 'GET_ABOUT_US_DATA':
            return {
                ...state,
                aboutUsData: action.payload,
            }

        case 'GET_CONTACT_DATA':
            return {
                ...state,
                contactData: action.payload,
            }

        // MEDIA & NEWS
        case 'GET_MEDIA_NEWS':
            return {
                ...state,
                mediaNews: action.payload,
                mediaAlbums: action.payload1,
                mediaVideos: action.payload2,
                mediaCommuniques: action.payload3,
            }

        case 'SET_SITE_LANGUAGE':
            return {
                ...state,
                siteLanguage: action.payload,
            }
        // ABOUT_US:
        case 'GET_ABOUT_US':
            return {
                ...state,
                about: action.payload,
            }
        // ACCREDITATIONS:
        case 'GET_ACCREDITATIONS':
            return {
                ...state,
                accreditations: action.payload,
            }
        default:
            return state;
    }
};

export default AfsacReducer;
