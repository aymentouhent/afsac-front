import React, { useEffect, useState } from "react";
import {Container, Col, Row, Image, Breadcrumb} from "react-bootstrap";

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import Lightbox from 'react-image-lightbox';


import { connect } from 'react-redux';
import { getMediaNewsData } from '../../redux/actions/AfsacActions';

import {Link} from "react-router-dom";
import { trans } from '../../constants/Translations';

import { IoMdArrowForward } from 'react-icons/io';

function AllMediaAlbums(props) {

  let {
    siteLanguage,
    mediaAlbums,
    // Functions
    getMediaNewsData,
  } = props;

  const [photoIndex,setPhotoIndex] = useState(0);
  const [isOpen,setIsOpen] = useState(false);
  const [imagesLight, setImagesLight] = useState([]);
  
  
  const handleLightBox = (photos,indexOfDeb) => {
    setImagesLight(photos);
    setPhotoIndex(indexOfDeb);
    setIsOpen(true);
  };

  useEffect(() => {
    getMediaNewsData(siteLanguage);
  },[]);

  return (
    <Container fluid>
      <Row className="justify-content-center my-5"> 
        <Col xs={12} xl={8}>
            <Row>
              <Col xs={12} className="d-flex">
                <h3 className="course-light-blue-text m-auto" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.titleAlbums}</h3>
              </Col>

              <Col xs={12} className="d-flex">
                    <Image src={images.center_divider}  className="m-auto" alt="afsac-divider" />
                </Col>

              {mediaAlbums
              &&
              <Col xs={12} className="d-flex mt-3">
                <p className="m-auto course-gray-media-text">"{mediaAlbums.length}" {trans[siteLanguage].mediaNews.titleAlbums}</p>
              </Col>
              }

              <Desktop>
                {mediaAlbums
                &&
                <Col xs={12} className="mt-4 mb-3">
                  {mediaAlbums.map((album,i) => 
                  <Col xs={12} key={i * Math.random() * 5}>
                    <Row>
                        <Col xs={12} lg={5}>
                          <Row>
                              <Col xs={11} lg={10} className="mb-3 p-0">
                                <Row className="mb-2" style={{paddingRight: '30px'}}>
                                  <Col xs={12} style={{cursor: album.photos[0] && 'pointer'}} onClick={() => album.photos[0] && handleLightBox(album.photos,0)}>
                                    <div className="media-album-big-image" style={{ backgroundImage: `url(${album.photos[0]})` }} />
                                  </Col>
                                </Row>

                                <Row className="mb-2" style={{paddingRight: '30px'}}>
                                  <Col xs={12}>
                                    <Row>
                                        <Col xs={4} style={{cursor: album.photos[1] && 'pointer'}} onClick={() => album.photos[1] && handleLightBox(album.photos,1)}>
                                          <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[1]})`}} />
                                        </Col>

                                        <Col className="px-2" xs={4} style={{cursor: album.photos[2] && 'pointer'}} onClick={() => album.photos[2] && handleLightBox(album.photos,2)}>
                                          <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[2]})` }} />
                                        </Col>

                                        <Col xs={4} style={{cursor: album.photos[3] && 'pointer'}} onClick={() => album.photos[3] && handleLightBox(album.photos,3)}>
                                          <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[3]})` }} />
                                        </Col>
                                    </Row>
                                  </Col>
                                </Row>
                              </Col>
                              
                              <Col xs={1} className="d-flex">
                                <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                              </Col>
                          </Row>
                        </Col>

                        <Col xs={12} lg={7} className="d-flex">
                          <Row className="my-auto">
                              <Col xs={12} className="p-0">
                                <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{album.titre}</p>
                              </Col>

                              <Col xs={12} className="p-0 d-flex mb-4" onClick={() => album.photos && handleLightBox(album.photos,i)}>
                                <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeAlbum}</p>
                                <IoMdArrowForward color={"#00A7E2"} size={25} />
                              </Col>
                          </Row>
                        </Col>
                    </Row>
                  </Col>  
                  )}
                </Col>
                }
              </Desktop>
              
              <Tablet>
                {mediaAlbums
                &&
                <Col xs={12} className="mt-4 mb-3">
                  {mediaAlbums.map((album,i) => 
                  <Col xs={12} key={i * Math.random() * 5}>
                    <Row>
                        <Col xs={12} lg={5}>
                          <Row>
                              <Col xs={11} lg={10} className="mb-3 p-0">
                                <Row className="mb-2" style={{paddingRight: '30px'}}>
                                  <Col xs={12} style={{cursor: album.photos[0] && 'pointer'}} onClick={() => album.photos[0] && handleLightBox(album.photos,0)}>
                                    <div className="media-album-big-image" style={{ backgroundImage: `url(${album.photos[0]})` }} />
                                  </Col>
                                </Row>

                                <Row className="mb-2" style={{paddingRight: '30px'}}>
                                  <Col xs={12}>
                                    <Row>
                                        <Col xs={4} style={{cursor: album.photos[1] && 'pointer'}} onClick={() => album.photos[1] && handleLightBox(album.photos,1)}>
                                          <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[1]})`}} />
                                        </Col>

                                        <Col className="px-2" xs={4} style={{cursor: album.photos[2] && 'pointer'}} onClick={() => album.photos[2] && handleLightBox(album.photos,2)}>
                                          <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[2]})` }} />
                                        </Col>

                                        <Col xs={4} style={{cursor: album.photos[3] && 'pointer'}} onClick={() => album.photos[3] && handleLightBox(album.photos,3)}>
                                          <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[3]})` }} />
                                        </Col>
                                    </Row>
                                  </Col>
                                </Row>
                              </Col>
                              
                              <Col xs={1} className="d-flex">
                                <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                              </Col>
                          </Row>
                        </Col>

                        <Col xs={12} lg={7} className="d-flex">
                          <Row className="my-auto">
                              <Col xs={12} className="p-0">
                                <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{album.titre}</p>
                              </Col>

                              <Col xs={12} className="p-0 d-flex mb-4" onClick={() => album.photos && handleLightBox(album.photos,i)}>
                                <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeAlbum}</p>
                                <IoMdArrowForward color={"#00A7E2"} size={25} />
                              </Col>
                          </Row>
                        </Col>
                    </Row>
                  </Col>  
                  )}
                </Col>
                }
              </Tablet>
              
              
              <Mobile>
                {mediaAlbums.length > 0 
                &&
                <>
                <Container className="py-5 mb-4" fluid style={{backgroundColor: '#F4F6F9'}}>
                    <Row className="mb-5">
                      <Col xs={12}>
                        <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.titleAlbums}</p>
                      </Col>

                      <Col xs={12} className="mb-3">
                        <Image src={images.orange_divider} alt="afsac-divider"  />
                      </Col>
                    </Row>
                    
                    {mediaAlbums.map((album,i) => 
                        <Col xs={12} key={i * Math.random() * 5}>
                          <Row>
                              <Col xs={12} lg={5}>
                                <Row>
                                    <Col xs={12} className="mb-1">
                                      <Row className="mb-2" style={{paddingRight: '30px'}}>
                                        <Col xs={12} className="p-0" style={{cursor: album.photos[0] && 'pointer'}} onClick={() => album.photos[0] && handleLightBox(album.photos,0)}>
                                          <Image src={album.photos[0]} style={{width:"100%", maxHeight: "300px"}} />
                                        </Col>
                                      </Row>

                                      <Row className="mb-2" style={{paddingRight: '30px'}}>
                                        <Col xs={12}>
                                          <Row>
                                              <Col className="p-0" xs={4} style={{cursor: album.photos[1] && 'pointer'}} onClick={() => album.photos[1] && handleLightBox(album.photos,1)}>
                                                <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[1]})`}} />
                                              </Col>

                                              <Col className="px-2" xs={4} style={{cursor: album.photos[2] && 'pointer'}} onClick={() => album.photos[2] && handleLightBox(album.photos,2)}>
                                                <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[2]})` }} />
                                              </Col>

                                              <Col className="p-0" xs={4} style={{cursor: album.photos[3] && 'pointer'}} onClick={() => album.photos[3] && handleLightBox(album.photos,3)}>
                                                <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[3]})` }} />
                                              </Col>
                                          </Row>
                                        </Col>
                                      </Row>
                                    </Col>
                                </Row>
                              </Col>

                              <Col xs={12} lg={7} className="d-flex">
                                <Row className="my-auto">
                                    <Col xs={12} className="p-0">
                                      <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{album.titre}</p>
                                    </Col>

                                    <Col xs={12} className="p-0 d-flex mb-4" onClick={() => album.photos && handleLightBox(album.photos,i)}>
                                      <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeAlbum}</p>
                                      <IoMdArrowForward color={"#00A7E2"} size={25} />
                                    </Col>
                                </Row>
                              </Col>
                          </Row>
                        </Col>  
                    )}
                  
                    <Col xs={12} className="d-flex">
                      <Link to={"/all-media-albums"} className="mt-4 mb-5 mx-auto">
                        <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                      </Link>
                    </Col>
                </Container>
                </>
                }
              </Mobile>



              {/* LIGHT BOX FOR IMAGE GALLERY */}
              {isOpen && (
                <Lightbox
                  mainSrc={imagesLight[photoIndex]}
                  nextSrc={imagesLight[(photoIndex + 1) % imagesLight.length]}
                  prevSrc={imagesLight[(photoIndex + imagesLight.length - 1) % imagesLight.length]}
                  onCloseRequest={() =>  setIsOpen(false)}
                  onMovePrevRequest={() => setPhotoIndex(prevState => (prevState + imagesLight.length - 1) % imagesLight.length)}
                  onMoveNextRequest={() => setPhotoIndex(prevState => (prevState + 1) % imagesLight.length)}
                />
              )}
            </Row>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({
  siteLanguage : state.afsacR.siteLanguage,
  mediaAlbums: state.afsacR.mediaAlbums,
});

export default connect(mapStateToProps,{ getMediaNewsData })(AllMediaAlbums);