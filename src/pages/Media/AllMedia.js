import React, { useEffect, useState } from "react";
import {Container, Col, Row, Image, Breadcrumb} from "react-bootstrap";

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import moment from 'moment';
import 'moment/min/locales';

import { connect } from 'react-redux';
import { getMediaNewsData } from '../../redux/actions/AfsacActions';

import {Link} from "react-router-dom";
import { trans } from '../../constants/Translations';

import { IoMdArrowForward } from 'react-icons/io';

function AllMedia(props) {

  let {
    siteLanguage,
    mediaNews,
    mediaAlbums,
    mediaVideos,
    mediaCommuniques,

    // Functions
    getMediaNewsData,
  } = props;

  const getDay = (date) => {
    return moment(date).format("DD");
  };

  const getMonth = (date) => {
    var momentDate = moment(date);
    momentDate.locale(siteLanguage);
    return momentDate.format('MMM');;
  };

  const getYear = (date) => {
    return moment(date).format("YYYY");
  };

  useEffect(() => {
    getMediaNewsData(siteLanguage);
  },[]);

  return (
    <Container fluid>
      <Row className="justify-content-center my-5"> 
        <Col xs={12} xl={8}>
            <Row>
              <Col xs={12} className="d-flex">
                <h3 className="course-light-blue-text m-auto" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.news}</h3>
              </Col>

              <Col xs={12} className="d-flex">
                    <Image src={images.center_divider}  className="m-auto" alt="afsac-divider" />
                </Col>

              {mediaNews
              &&
              <Col xs={12} className="d-flex mt-3">
                <p className="m-auto course-gray-media-text">"{mediaNews.length}" {trans[siteLanguage].mediaNews.news}</p>
              </Col>
              }
              
              <Desktop>
                {mediaNews
                &&
                <Col xs={12} className="mt-4 mb-3">
                  {mediaNews.map((act, index) => 
                    <Link to={`/media/${act.id}/${act.titre}`}>
                      <Col xs={12} key={index * Math.random()}>
                        <Row>
                            <Col xs={12} lg={5}>
                              <Row>
                                  <Col xs={9} className="mb-3 p-0" style={{height: '300px', backgroundImage: `url(${act.image})`,width: '100%',backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover'}} />
                                  
                                  <Col xs={3} className="d-flex">
                                    <div className="m-auto flex-column d-flex"> 
                                        <p className="news-list-date-day mx-auto m-0">{getDay(act.date)}</p>
                                        <p className="my-0 news-list-date-month-year mx-auto">{getMonth(act.date)}</p>
                                        <p className="my-0 news-list-date-month-year mx-auto">{getYear(act.date)}</p>
                                    </div>
        
                                    <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                                  </Col>
                              </Row>
                            </Col>
        
                            <Col xs={12} lg={7} className="d-flex">
                              <Row className="my-auto">
                                  <Col xs={12} className="p-0">
                                    <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{act.titre}</p>
                                  </Col>
        
                                  {act.descritpion
                                  &&
                                  <Col xs={12} className="p-0">
                                    <p className="course-gray-media-text" style={{ fontSize: '18px', color: "black", fontWeight: "400", textAlign: "center"}}>{act.descritpion.substring(0,300) + "..."}</p>
                                  </Col>
                                  }
        
                                  <Col xs={12} className="p-0 d-flex mb-4">
                                    <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                                    <IoMdArrowForward color={"#00A7E2"} size={25} />
                                  </Col>
                              </Row>
                            </Col>
                        </Row>
                      </Col>  
                    </Link>
                  )}
                </Col>
                }
              </Desktop>
              
              <Tablet>
                {mediaNews
                &&
                <Col xs={12} className="mt-4 mb-3">
                  {mediaNews.map((act, index) => 
                    <Link to={`/media/${act.id}/${act.titre}`}>
                      <Col xs={12} key={index * Math.random()}>
                        <Row>
                            <Col xs={12} lg={5}>
                              <Row>
                                  <Col xs={9} className="mb-3 p-0" style={{height: '300px', backgroundImage: `url(${act.image})`,width: '100%',backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover'}} />
                                  
                                  <Col xs={3} className="d-flex">
                                    <div className="m-auto flex-column d-flex"> 
                                        <p className="news-list-date-day mx-auto m-0">{getDay(act.date)}</p>
                                        <p className="my-0 news-list-date-month-year mx-auto">{getMonth(act.date)}</p>
                                        <p className="my-0 news-list-date-month-year mx-auto">{getYear(act.date)}</p>
                                    </div>
        
                                    <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                                  </Col>
                              </Row>
                            </Col>
        
                            <Col xs={12} lg={7} className="d-flex">
                              <Row className="my-auto">
                                  <Col xs={12} className="p-0">
                                    <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{act.titre}</p>
                                  </Col>
        
                                  {act.descritpion
                                  &&
                                  <Col xs={12} className="p-0">
                                    <p className="course-gray-media-text" style={{ fontSize: '18px', color: "black", fontWeight: "400"}}>{act.descritpion.substring(0,300) + "..."}</p>
                                  </Col>
                                  }
        
                                  <Col xs={12} className="p-0 d-flex mb-4">
                                    <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                                    <IoMdArrowForward color={"#00A7E2"} size={25} />
                                  </Col>
                              </Row>
                            </Col>
                        </Row>
                      </Col>  
                    </Link>
                  )}
                </Col>
                }
              </Tablet>

              <Mobile>
                {mediaNews.map((act, index) => 
                  <Link to={`/media/${act.id}/${act.titre}`}>
                  <Col xs={12} key={index * Math.random()}>
                    <Row>
                        <Col xs={12} lg={5}>
                          <Row>
                              <Col xs={12} className="mb-1 p-1">
                                <Image src={act.image} style={{ minHeight: '300px', width: '100%'}} />
                              </Col>
                              
                              <Col xs={12} className="d-flex">
                                <div className="d-flex mr-auto">
                                    <p className="news-list-date-day my-auto mr-2" style={{fontSize: '50px'}}>{getDay(act.date)}</p>
                                    <p className="news-list-date-month-year my-auto mr-2">{getMonth(act.date)}</p>
                                    <p className="news-list-date-month-year my-auto mr-2">{getYear(act.date)}</p>
                                    <div className="my-auto" style={{height: '50px', borderLeft: '4px solid #00A7E2'}} />
                                </div>
                              </Col>
                          </Row>
                        </Col>
    
                        <Col xs={12} lg={7} className="d-flex">
                          <Row className="my-auto">
                              <Col xs={12} className="">
                                <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{act.titre}</p>
                              </Col>
    
                              {act.descritpion
                              &&
                              <Col xs={12} className="">
                                <p className="course-gray-media-text" style={{ fontSize: '18px', color: "black", fontWeight: "400"}}>{act.descritpion.substring(0,300) + "..."}</p>
                              </Col>
                              }
    
                              <Col xs={12} className=" d-flex mb-4">
                                <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                                <IoMdArrowForward color={"#00A7E2"} size={25} />
                              </Col>
                          </Row>
                        </Col>
                    </Row>
                  </Col>  
                </Link>
                )}
              </Mobile>


            </Row>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({
  siteLanguage : state.afsacR.siteLanguage,
  mediaNews: state.afsacR.mediaNews,
  mediaAlbums: state.afsacR.mediaAlbums,
  mediaVideos: state.afsacR.mediaVideos,
  mediaCommuniques: state.afsacR.mediaCommuniques,
});

export default connect(mapStateToProps,{ getMediaNewsData })(AllMedia);