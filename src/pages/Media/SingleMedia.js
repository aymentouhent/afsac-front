import React, { useEffect, useState } from "react";
import {Container, Col, Row, Image, Breadcrumb} from "react-bootstrap";

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import Layout from '../../components/Layout';
import Lightbox from 'react-image-lightbox';
import Slider from 'react-slick';
import ModalVideo from 'react-modal-video';

import { connect } from 'react-redux';
import { getMediaNewsData } from '../../redux/actions/AfsacActions';

import { Link, useParams, useHistory, useLocation } from "react-router-dom";
import { trans } from '../../constants/Translations';

function convertString(str)
{
    var ch = "";
    for (var i = 0; i < str.length; i++)
    {
      if(str.charAt(i) === '{')
        ch = ch + `<br>`;
      else
        ch = ch + str.charAt(i);
    }
    return ch;
}

function AllMedia(props) {

  let {
    id,
    title
  } = useParams();

  let history = useHistory();
  let location = useLocation();

  let {
    siteLanguage,
    mediaNews,
    // mediaAlbums,
    // mediaVideos,
    // mediaCommuniques,

    // Functions
    getMediaNewsData,
  } = props;

  const [choosenArticle,setChoosenArticle] = useState(null);
  const [fourArticles,setFourArticles] = useState([]);

  useEffect(() => {
    getMediaNewsData(siteLanguage);
  },[]);

  useEffect(() => {
    if(mediaNews) {
      setChoosenArticle(props.mediaNews.find((news) => news.id === parseInt(id)));
      setFourArticles(props.mediaNews.filter((news) => news.id !== parseInt(id)));

    };
    return () => {

    };
  },[props.mediaNews]);


  let new_string;

  return (

    <Container fluid>
      <Row className="justify-content-center my-5"> 
        <Col xs={12} xl={8}>
            <Row>
              <Col xs={12} className="d-flex">
                <h3 className="contact-title m-auto">{trans[siteLanguage].mediaNews.bigTitle}</h3>
              </Col>

              <Col xs={12} className="d-flex">
                    <Image src={images.center_divider}  className="m-auto" alt="afsac-divider" />
                </Col>

              <Col xs={12} className="d-flex mt-3">
                <p className="m-auto course-gray-media-text" style={{fontSize: '30px', textAlign: 'center'}}>{title}</p>
              </Col>
              
              {choosenArticle 
              &&
              <>
              <Col xs={12} className="d-flex mt-3">
                <p className="course-light-blue-text m-auto" style={{fontSize: '19px'}}>{choosenArticle.date}</p>
              </Col>

              <Col xs={12} className="d-flex mt-3">
                  <Image className="m-auto" fluid src={choosenArticle.image} alt={title} />
              </Col>

              <Col xs={12} className="d-flex mt-3">
                  <div
                      dangerouslySetInnerHTML={{__html: choosenArticle.descritpion}}
                      style={{fontSize: '25px',textAlign: 'justify-center'}}>
                  </div>
              </Col>

              
              {/* LAST 4 NEWS */}
              {fourArticles
              &&
              <Col xs={12} className="mt-5">
                <Row className="mb-4">
                  <Col xs={12} className="px-0">
                    <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.lastNews}</p>
                  </Col>

                  <Col xs={12} className="mb-3 px-0">
                    <Image src={images.orange_divider} alt="afsac-divider"  />
                  </Col>

                  {fourArticles.map((news,index) =>
                    index < 4
                    &&  
                    <Col key={index} lg={3} md={6} xs={12} className="pl-0 pr-3" onClick={() => window.location.reload() }>
                      <Link to={`/media/${news.id}/${news.titre}`}>
                        <Row className="justify-content-between">
                          <Col xs={12} className="">
                            <div style={{height: '160px', backgroundImage: `url(${news.image})`,backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover'}} />
                            {/* <Image src={news.image} fluid style={{ maxWidth: '100%' }}/> */}
                          </Col>

                          <Col xs={12} >
                                <p className="course-gray-date-text" style={{fontSize: '16px'}}>{news.titre.substring(0,37)}...</p>
                              </Col>

                          <Col xs={12} className="mt-1">
                            <p className="course-blue-text " style={{fontSize: '16px',textTransform:'uppercase',}}>{news.descritpion.substring(0,30)}...</p>
                          </Col>

                          <Col xs={12} className="mt-1">
                            <p className="course-light-blue-text " style={{fontSize: '14px'}}>{news.date}</p>
                          </Col>
        
                        </Row>
                      </Link>
                    </Col>
                  )} 

                </Row>
              </Col>
              }
              </>
              }
              
              {/* {mediaNews
              &&
              <Col xs={12} className="mt-4 mb-3">

                {mediaNews.map((news,index) =>
                    <Row key={index} className="mb-5">
                      <Col xs={12} md={5}>
                          <div style={{height: '317.39px', backgroundImage: `url(${news.image})`,backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover'}} />
                      </Col>

                      <Col xs={12} md={7}>
                        <Row>
                          <Col xs={12} className="mb-2">
                            <p className="m-auto course-gray-media-text" style={{fontSize: '27px'}}>{news.titre}</p>
                          </Col>
                          
                          {news.descritpion
                          &&
                          <Col xs={12}>
                            <p className="contact-title-media" style={{fontSize: '20px'}}>{news.descritpion.substring(0,220)}...</p>
                          </Col>
                          }
                          
                          <Col xs={12} className="mb-3">
                            <p className="course-light-blue-text" style={{fontSize: '18px'}}>{news.date}</p>
                          </Col>

                          <Col xs={12}>
                            <Link to={"#"}>
                              <span className="media-see-more-btn px-4 py-2">{trans[siteLanguage].courseCard.seeMoreButton} ></span>
                            </Link>
                          </Col>
                        </Row>

                      </Col>
                    </Row>
                )}

              </Col>

              } */}

            </Row>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state =>
(
    {
        siteLanguage : state.afsacR.siteLanguage,
        mediaNews: state.afsacR.mediaNews,
        mediaAlbums: state.afsacR.mediaAlbums,
        mediaVideos: state.afsacR.mediaVideos,
        mediaCommuniques: state.afsacR.mediaCommuniques,
    }
);

export default connect(mapStateToProps,{ getMediaNewsData })(AllMedia);