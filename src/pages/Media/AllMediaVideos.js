import React, { useEffect, useState } from "react";
import {Container, Col, Row, Image, Breadcrumb} from "react-bootstrap";

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import ModalVideo from 'react-modal-video';

import { connect } from 'react-redux';
import { getMediaNewsData } from '../../redux/actions/AfsacActions';

import {Link} from "react-router-dom";
import { trans } from '../../constants/Translations';

import { IoMdArrowForward } from 'react-icons/io';

function AllMediaVideos(props) {

  let {
    siteLanguage,
    mediaVideos,

    // Functions
    getMediaNewsData,
  } = props;

  const [isOpenVideo, setOpenVideo] = useState(false);
  const [videoToPlay, setVideoToPlay] = useState(null);
  
  const getYoutubeId = (url) => {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11) ? match[7] : false;
  };

  const getYoutubeImage = (url) => {

    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11) ? `https://img.youtube.com/vi/${match[7]}/hqdefault.jpg` : false;
    /*
    mqdefault
    hqdefault
    sddefault
    maxresdefault
    */
  };


  useEffect(() => {
    getMediaNewsData(siteLanguage);
  },[]);

  return (
    <Container fluid>
      <Row className="justify-content-center my-5"> 
        <Col xs={12} xl={8}>
            <Row>
              <Col xs={12} className="d-flex">
                <h3 className="course-light-blue-text m-auto" style={{fontFamily: 'SF Pro Display',fontSize:'25px'}}>
					{trans[siteLanguage].mediaNews.titleVideos}
				</h3>
              </Col>

              <Col xs={12} className="d-flex">
                    <Image src={images.center_divider}  className="m-auto" alt="afsac-divider" />
                </Col>

              {mediaVideos
              &&
              <Col xs={12} className="d-flex mt-3">
                <p className="m-auto course-gray-media-text" style={{fontSize: "20px"}}>"{mediaVideos.length}" {trans[siteLanguage].mediaNews.titleVideos}</p>
              </Col>
              }

              <ModalVideo channel='youtube' autoplay isOpen={isOpenVideo} videoId={videoToPlay} onClose={() => {setOpenVideo(false); setVideoToPlay(null);}} />

              <Desktop>
                {mediaVideos
                &&
                <Col xs={12} className="mt-4 mb-3">
                  {mediaVideos.map((video,i) => 
                    <Col xs={12} className="p-0" key={i * Math.random() * 6}>
                    <Row>
                        <Col xs={12} lg={5}>
                          <Row>
                              <Col xs={8} lg={10} className="mb-3 p-0">
                                <Image 
                                    src={getYoutubeImage(video.lien)}  
                                    style={{ width: '100%', cursor: 'pointer', height: '235px' }} 
                                    onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}
                                    alt={video.titre}
                                  />
                              </Col>
                              
                              <Col xs={1} className="d-flex">
                                <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                              </Col>
                          </Row>
                        </Col>

                        <Col xs={12} lg={7} className="d-flex">
                          <Row className="my-auto">
                              <Col xs={12} className="p-0">
                                <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{video.titre}</p>
                              </Col>

                              <Col xs={12} className="p-0 d-flex mb-4" onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}>
                                <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeVideo}</p>
                                <IoMdArrowForward color={"#00A7E2"} size={25} />
                              </Col>
                          </Row>
                        </Col>
                    </Row>
                  </Col>
                  )}

                </Col>
                }
              </Desktop>

              <Tablet>
                {mediaVideos.length > 0
                &&
                mediaVideos.map((video,i) => 
                  <Col xs={12} className="mt-3" key={i * Math.random() * 6}>
                    <Row>
                        <Col xs={12}>
                          <Image 
                              src={getYoutubeImage(video.lien)}  
                              style={{ width: '100%', cursor: 'pointer'}} 
                              className="m-auto"
                              onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}
                              alt={video.titre}
                          />
                        </Col>

                        <Col xs={12} lg={7} className="d-flex">
                          <Row className="my-auto">
                              <Col xs={12}>
                                <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{video.titre}</p>
                              </Col>

                              <Col xs={12} className="d-flex mb-4" onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}>
                                <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeVideo}</p>
                                <IoMdArrowForward color={"#00A7E2"} size={25} />
                              </Col>
                          </Row>
                        </Col>
                    </Row>
                  </Col>
                )}
              </Tablet>

              <Mobile>
                {mediaVideos.length > 0
                &&
                mediaVideos.map((video,i) => 
                  <Col xs={12} className="mt-3" key={i * Math.random() * 6}>
                    <Row>
                        <Col xs={12}>
                          <Image 
                              src={getYoutubeImage(video.lien)}  
                              style={{ width: '100%', cursor: 'pointer'}} 
                              className="m-auto"
                              onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}
                              alt={video.titre}
                          />
                        </Col>

                        <Col xs={12} lg={7} className="d-flex">
                          <Row className="my-auto">
                              <Col xs={12}>
                                <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{video.titre}</p>
                              </Col>

                              <Col xs={12} className="d-flex mb-4" onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}>
                                <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeVideo}</p>
                                <IoMdArrowForward color={"#00A7E2"} size={25} />
                              </Col>
                          </Row>
                        </Col>
                    </Row>
                  </Col>
                )}
              </Mobile>


            </Row>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({
  siteLanguage : state.afsacR.siteLanguage,
  mediaVideos: state.afsacR.mediaVideos,
});

export default connect(mapStateToProps,{ getMediaNewsData })(AllMediaVideos);