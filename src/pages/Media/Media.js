import React, { useEffect, useState } from "react";
import {Container, Col, Row, Image, Breadcrumb} from "react-bootstrap";
import "./media.css";
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import Layout from '../../components/Layout';
import Lightbox from 'react-image-lightbox';
import Slider from 'react-slick';
import ModalVideo from 'react-modal-video';
import moment from 'moment';
import 'moment/min/locales';

import { connect } from 'react-redux';
import { getMediaNewsData } from '../../redux/actions/AfsacActions';
import {Link} from "react-router-dom";

import { trans } from '../../constants/Translations';

import { IoMdArrowForward } from 'react-icons/io';


function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <>
    <Desktop>
      <Image
          src={images.next_slider}
          className={className}
          style={{ ...style, display: "block", height: '70px',width: '70px',marginRight: '-50px',}}
          onClick={onClick}
          alt="afsac-arrow"

      />
    </Desktop>
    
    <Tablet>
      <Image
          src={images.next_slider}
          className={className}
          style={{ ...style, display: "block", height: '40px',width: '40px',marginRight: '0px',}}
          onClick={onClick}
          alt="afsac-arrow"
      />
    </Tablet>
    
    <Mobile>
      <Image
          src={images.next_slider}
          className={className}
          style={{ ...style, display: "block", height: '35px',width: '35px',marginRight: '2px',}}
          onClick={onClick}
          alt="afsac-arrow"
      />
    </Mobile>

    </>
  );
};

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <>
    <Desktop>
      <Image
          src={images.prev_slider}
          className={className}
          style={{ ...style, display: "block", height: '70px',width: '70px',marginLeft: '-70px',filter:'grayscale(1)',}}
          onClick={onClick}
          alt="afsac-arrow"
      />
    </Desktop>

    <Tablet>
      <Image
          src={images.prev_slider}
          className={className}
          style={{ ...style, display: "block", height: '40px',width: '40px',marginLeft: '8px',filter:'grayscale(1)',}}
          onClick={onClick}
          alt="afsac-arrow"
      />
    </Tablet>

    <Mobile>
      <Image
          src={images.prev_slider}
          className={className}
          style={{ ...style, display: "block", height: '35px',width: '35px',marginLeft: '7px',filter:'grayscale(1)',}}
          onClick={onClick}
          alt="afsac-arrow"
      />
    </Mobile>
    </>
  );
};

function Media(props) {

  let {
    siteLanguage,
    mediaNews,
    mediaAlbums,
    mediaVideos,
    mediaCommuniques,

    // Functions
    getMediaNewsData,
  } = props;

  // STATE :

  const [photoIndex,setPhotoIndex] = useState(0);
  const [isOpen,setIsOpen] = useState(false);
  const [imagesLight, setImagesLight] = useState([]);
  const [isOpenVideo, setOpenVideo] = useState(false);
  const [videoToPlay, setVideoToPlay] = useState(null);
  const [zoomIndex, setZoomIndex] = useState(1);

  const settingsAlbumsSlider = {
    dots: true,
    infinite: true,
    swipe: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    dotsClass: "slider-pagination",
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                initialSlide: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        }
    ]
  };

  const handleLightBox = (photos,indexOfDeb) => {
    setImagesLight(photos);
    setPhotoIndex(indexOfDeb);
    setIsOpen(true);
  };

  const getYoutubeId = (url) => {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11) ? match[7] : false;
  };

  const getYoutubeImage = (url) => {

    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11) ? `https://img.youtube.com/vi/${match[7]}/hqdefault.jpg` : false;
    /*
    mqdefault
    hqdefault
    sddefault
    maxresdefault
    */
  };

  const getDay = (date) => {
    return moment(date).format("DD");
  };

  const getMonth = (date) => {
    var momentDate = moment(date);
    momentDate.locale(siteLanguage);
    return momentDate.format('MMM');;
  };

  const getYear = (date) => {
    return moment(date).format("YYYY");
  };

  useEffect(() => {
    getMediaNewsData(siteLanguage);

    // const interval = setInterval(() => {
    //   setZoomIndex(zoom => zoom > 3 ? 1 : zoom+=1);
    // }, 6000);
    // return () => clearInterval(interval);

  },[]);

  return (
    <>
      {/* Image Banner */}
      <Container fluid>
        <Row className="d-flex">
            <Image src={images.media_banner}  className="" alt="afsac-banner" style={{width:'100%'}} />
        </Row>
      </Container>

      {/* LIGHT BOX FOR IMAGE GALLERY */}
      {isOpen && (
        <Lightbox
          mainSrc={imagesLight[photoIndex]}
          nextSrc={imagesLight[(photoIndex + 1) % imagesLight.length]}
          prevSrc={imagesLight[(photoIndex + imagesLight.length - 1) % imagesLight.length]}
          onCloseRequest={() =>  setIsOpen(false)}
          onMovePrevRequest={() => setPhotoIndex(prevState => (prevState + imagesLight.length - 1) % imagesLight.length)}
          onMoveNextRequest={() => setPhotoIndex(prevState => (prevState + 1) % imagesLight.length)}
        />
      )}

      <Desktop>
        <Layout>
            {/* BreadCrumb */}
            <Row>
                <div className="Breadcrumb d-flex mb-3 px-0 ">
                    <div className="breadcrumb-item"> <Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>

                    <div className="breadcrumb-item active" >{trans[siteLanguage].mediaNews.bigTitle}</div>
                </div>
            </Row>

            {/* TITLE */}
            <Row className="my-5">
                <Col xs={12} className="d-flex">
                    <h3 className="contact-title m-auto">{trans[siteLanguage].mediaNews.bigTitle}</h3>
                </Col>

                <Col xs={12} className="d-flex">
                    <Image src={images.center_divider}  className="m-auto" alt="afsac-divider" />
                </Col>
            </Row>

            {/* BLOC 1 : ACTUALITES / NEWS */}
            {mediaNews && mediaNews.length > 0
            &&
            <>
           {/*  <Row className="mb-4">
              <Col xs={12} className="px-0">
                <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.news}</p>
              </Col>

              <Col xs={12} className="mb-3 px-0">
                <Image src={images.orange_divider} alt="afsac-divider"  />
              </Col>
            </Row> */}

            {mediaNews.map((act, index) => 
              index < 6
              &&
              <Link to={`/media/${act.id}/${act.titre}`}>
                <Col xs={12} className="p-0" key={index * Math.random()}>
                  <Row>
                      <Col xs={12} lg={5}>
                        <Row>
                            <Col xs={9} className="mb-3 p-0" style={{height: '300px', backgroundImage: `url(${act.image})`,width: '100%',backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover'}} />
                            
                            <Col xs={3} className="d-flex">
                              <div className="m-auto flex-column d-flex"> 
                                  <p className="news-list-date-day mx-auto m-0">{getDay(act.date)}</p> 
                                  <p className="my-0 news-list-date-month-year mx-auto">{getMonth(act.date)}</p>
                                  <p className="my-0 news-list-date-month-year mx-auto">{getYear(act.date)}</p>
                              </div>
  
                              <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                            </Col>
                        </Row>
                      </Col>
  
                      <Col xs={12} lg={7} className="d-flex">
                        <Row className="my-auto">
                            <Col xs={12} className="p-0">
                              <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold", textAlign: "center"}}>{act.titre}</p>
                            </Col>
  
                            {act.descritpion
                            &&
                            <Col xs={12} className="p-0">
                                <div
                                    dangerouslySetInnerHTML={{__html: act.descritpion.substring(0,300) + "..."}}
                                    className="course-gray-media-text"
                                    style={{ fontSize: '18px', color: "black", fontWeight: "400" , textAlign: "center"}}
                                >
                                </div>
                            </Col>
                            }
  
                            <Col xs={12} className="p-0 d-flex mb-4">
                              <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer" , textAlign: "center"}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                              <IoMdArrowForward color={"#00A7E2"} size={25} />
                            </Col>
                        </Row>
                      </Col>
                  </Row>
                </Col>  
              </Link>
            )}

          {/*   <Col xs={12} className="d-flex">
              <Link to={"/all-media-news"} className="mt-4 mb-5 mx-auto">
                <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
              </Link>
            </Col> */}
            </>
            }
        </Layout>

        {/* BLOC 2: ALBUMS */}
        {mediaAlbums.length > 0 
        &&
        <>
        <Container className="py-5 mb-4" fluid >
          <Layout>
            {/* <Row className="mb-5">
              <Col xs={12} className="px-0">
                <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.titleAlbums}</p>
              </Col>

              <Col xs={12} className="mb-3 px-0">
                <Image src={images.orange_divider} alt="afsac-divider"  />
              </Col>
            </Row>
             */}
            {mediaAlbums.map((album,i) => 
                i < 6
                &&
                <Col xs={12} className="p-0" key={i * Math.random() * 5}>
					<Row>
						<Col xs={12} lg={5}>
							<Row>
								<Col xs={8} md={11} lg={10} className="mb-3 p-0">
									<Row className="mb-2" style={{paddingRight: '30px'}}>
										<Col xs={12} className="p-0" style={{cursor: album.photos[0] && 'pointer'}} onClick={() => album.photos[0] && handleLightBox(album.photos,0)}>
											<div className="media-album-big-image" style={{ backgroundImage: `url(${album.photos[0]})` }} />
										</Col>
									</Row>
									<Row className="mb-2" style={{paddingRight: '30px'}}>
										<Col xs={12}>
											<Row>
												<Col className="p-0" xs={4} style={{cursor: album.photos[1] && 'pointer'}} onClick={() => album.photos[1] && handleLightBox(album.photos,1)}>
													<div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[1]})`}} />
												</Col>

												<Col className="px-2" xs={4} style={{cursor: album.photos[2] && 'pointer'}} onClick={() => album.photos[2] && handleLightBox(album.photos,2)}>
													<div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[2]})` }} />
												</Col>

												<Col className="p-0" xs={4} style={{cursor: album.photos[3] && 'pointer'}} onClick={() => album.photos[3] && handleLightBox(album.photos,3)}>
													<div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[3]})` }} />
												</Col>
											</Row>
										</Col>
									</Row>
								</Col>
								<Col xs={1} className="d-flex">
									<div className="m-auto flex-column d-flex"> 
										<p className="news-list-date-day mx-auto m-0">{getDay( album.created_at )}</p>
										<p className="my-0 news-list-date-month-year mx-auto">{getMonth( album.created_at )}</p>
										<p className="my-0 news-list-date-month-year mx-auto">{getYear( album.created_at )}</p>
									</div>
									<div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
								</Col>
							</Row>
						</Col>
						<Col xs={12} lg={7} className="d-flex">
							<Row className="my-auto">
								<Col xs={12} className="p-0">
									<center>
										<p className="contact-title-media" style={{textAlign: "center", fontSize: '25px', fontWeight: "bold"}}>{album.titre}</p>
									</center>
								</Col>

								<Col xs={12} className="p-0 d-flex mb-6" onClick={() => album.photos && handleLightBox(album.photos,i)}>
										<p className="course-light-blue-text my-auto mr-5" style={{fontSize: '18px', cursor: "pointer", paddingLeft: "1.5em" }}>{trans[siteLanguage].mediaNews.seeAlbum}<IoMdArrowForward color={"#00A7E2"} size={25} /></p>
										
								</Col>
							</Row>
						</Col>
					</Row>
                </Col>  
            )}
          
           {/*  <Col xs={12} className="d-flex">
              <Link to={"/all-media-albums"} className="mt-4 mb-5 mx-auto">
                <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
              </Link>
            </Col> */}
          </Layout>
        </Container>
        </>
        }
        
        
        {/* BLOC 3:  VIDEOS */}
        {mediaVideos.length > 0
        &&
        <Container fluid style={{background : '#fff'}} className="py-5 mb-4">
            <Layout>
              {/* TITLE */}
           {/*    <Row className="mb-4">
                <Col xs={12} className="px-0">
                  <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'26px'}}>{trans[siteLanguage].mediaNews.titleVideos}</p>
                </Col>

                <Col xs={12} className="mb-3 px-0">
                  <Image src={images.orange_divider} alt="afsac-divider"  />
                </Col>
              </Row> */}

              {mediaVideos.map((video,i) => 
                i < 6
                &&
                <Col xs={12} className="p-0" key={i * Math.random() * 6}>
                  <Row>
                      <Col xs={12} lg={5}>
                        <Row>
                            <Col xs={10} lg={10} className="mb-3 p-0">
								<Image 
									src={getYoutubeImage(video.lien)}  
									style={{ width: '100%', cursor: 'pointer', height: '235px' }} 
									onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}
									alt={video.titre}
                                />
                            </Col>
                            <Col xs={2} className="d-flex">
								<div className="m-auto flex-column d-flex"> 
									<p className="news-list-date-day mx-auto m-0">{getDay( video.created_at )}</p>
									<p className="my-0 news-list-date-month-year mx-auto">{getMonth( video.created_at )}</p>
									<p className="my-0 news-list-date-month-year mx-auto">{getYear( video.created_at )}</p>
								</div>
								<div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                            </Col>
							
                            
                        </Row>
                      </Col>

                      <Col xs={12} lg={7} className="d-flex">
                        <Row className="my-auto">
                            <Col xs={12} className="p-0">
                              <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold" ,textAlign: "center"}}>{video.titre}</p>
                            </Col>
                            <Col xs={12} className="p-0 d-flex mb-4" onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}>
                              <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer", paddingLeft: "1.5em" ,textAlign: "center"}}>
								{trans[siteLanguage].mediaNews.seeVideo}
								<IoMdArrowForward color={"#00A7E2"} size={25} />
							  </p>
                            </Col>
                        </Row>
                      </Col>
                  </Row>
                </Col>
              )}

           {/*    <Col xs={12} className="d-flex">
                <Link to={"/all-media-videos"} className="mt-4 mb-5 mx-auto">
                  <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                </Link>
              </Col>
 */}
              <ModalVideo channel='youtube' autoplay isOpen={isOpenVideo} videoId={videoToPlay} onClose={() => {setOpenVideo(false); setVideoToPlay(null);}} />
            </Layout>
        </Container>
        }
      </Desktop> 
      

      <Tablet>
        <Layout>
            {/* BreadCrumb */}
            <Row>
                <div className="Breadcrumb d-flex mb-3 px-0 ">
                    <div className="breadcrumb-item"> <Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>

                    <div className="breadcrumb-item active" >{trans[siteLanguage].mediaNews.bigTitle}</div>
                </div>
            </Row>

            {/* TITLE */}
            <Row className="my-5">
                <Col xs={12} className="d-flex">
                    <h3 className="contact-title m-auto">{trans[siteLanguage].mediaNews.bigTitle}</h3>
                </Col>

                <Col xs={12} className="d-flex">
                    <Image src={images.center_divider}  className="m-auto" alt="afsac-divider" />
                </Col>
            </Row>

            {/* BLOC 1 : ACTUALITES / NEWS */}
            {mediaNews && mediaNews.length > 0
            &&
            <>
            <Row className="mb-4">
              <Col xs={12} className="px-0">
                <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.news}</p>
              </Col>

              <Col xs={12} className="mb-3 px-0">
                <Image src={images.orange_divider} alt="afsac-divider"  />
              </Col>
            </Row>

            {mediaNews.map((act, index) => 
              index < 6
              &&
              <Link to={`/media/${act.id}/${act.titre}`}>
                <Col xs={12} className="p-0" key={index * Math.random()}>
                  <Row>
                      <Col xs={12} lg={5}>
                        <Row>
                            <Col xs={9} className="mb-3 p-0" style={{height: '300px', backgroundImage: `url(${act.image})`,width: '100%',backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover'}} />
                            
                            <Col xs={3} className="d-flex">
                              <div className="m-auto flex-column d-flex"> 
                                  <p className="news-list-date-day mx-auto m-0">{getDay(act.date)}</p>
                                  <p className="my-0 news-list-date-month-year mx-auto">{getMonth(act.date)}</p>
                                  <p className="my-0 news-list-date-month-year mx-auto">{getYear(act.date)}</p>
                              </div>
  
                              <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                            </Col>
                        </Row>
                      </Col>
  
                      <Col xs={12} lg={7} className="d-flex">
                        <Row className="my-auto">
                            <Col xs={12} className="p-0">
                              <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{act.titre}</p>
                            </Col>
  
                            {act.descritpion
                            &&
                            <Col xs={12} className="p-0">
                              <div
                                    dangerouslySetInnerHTML={{__html: act.descritpion.substring(0,300) + "..."}}
                                    className="course-gray-media-text"
                                    style={{ fontSize: '18px', color: "black", fontWeight: "400" , textAlign: "center"}}
                                >
                                </div>
                            </Col>
                            }
  
                            <Col xs={12} className="p-0 d-flex mb-4">
                              <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                              <IoMdArrowForward color={"#00A7E2"} size={25} />
                            </Col>
                        </Row>
                      </Col>
                  </Row>
                </Col>  
              </Link>
            )}

            <Col xs={12} className="d-flex">
              <Link to={"/all-media-news"} className="mt-4 mb-5 mx-auto">
                <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
              </Link>
            </Col>
            </>
            }
        </Layout>

        {/* BLOC 2: ALBUMS */}
        {mediaAlbums.length > 0 
        &&
        <>
        <Container className="py-5 mb-4" fluid style={{backgroundColor: '#F4F6F9'}}>
          <Layout>
            <Row className="mb-5">
              <Col xs={12} className="px-0">
                <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.titleAlbums}</p>
              </Col>

              <Col xs={12} className="mb-3 px-0">
                <Image src={images.orange_divider} alt="afsac-divider"  />
              </Col>
            </Row>
            
            {mediaAlbums.map((album,i) => 
                i < 6
                &&
                <Col xs={12} className="p-0" key={i * Math.random() * 5}>
                  <Row>
                      <Col xs={12} lg={5}>
                        <Row>
                            <Col xs={10} md={11} lg={10} className="mb-3 p-0">
                              <Row className="mb-2" style={{paddingRight: '30px'}}>
                                <Col xs={12} className="p-0" style={{cursor: album.photos[0] && 'pointer'}} onClick={() => album.photos[0] && handleLightBox(album.photos,0)}>
                                  <div className="media-album-big-image" style={{ backgroundImage: `url(${album.photos[0]})` }} />
                                </Col>
                              </Row>

                              <Row className="mb-2" style={{paddingRight: '30px'}}>
                                <Col xs={12}>
                                  <Row>
                                      <Col className="p-0" xs={4} style={{cursor: album.photos[1] && 'pointer'}} onClick={() => album.photos[1] && handleLightBox(album.photos,1)}>
                                        <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[1]})`}} />
                                      </Col>

                                      <Col className="px-2" xs={4} style={{cursor: album.photos[2] && 'pointer'}} onClick={() => album.photos[2] && handleLightBox(album.photos,2)}>
                                        <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[2]})` }} />
                                      </Col>

                                      <Col className="p-0" xs={4} style={{cursor: album.photos[3] && 'pointer'}} onClick={() => album.photos[3] && handleLightBox(album.photos,3)}>
                                        <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[3]})` }} />
                                      </Col>
                                  </Row>
                                </Col>
                              </Row>
                            </Col>
                            
                            <Col xs={1} className="d-flex">
                              <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                            </Col>
                        </Row>
                      </Col>

                      <Col xs={12} lg={7} className="d-flex">
                        <Row className="my-auto">
                            <Col xs={12} className="p-0">
                              <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{album.titre}</p>
                            </Col>

                            <Col xs={12} className="p-0 d-flex mb-4" onClick={() => album.photos && handleLightBox(album.photos,i)}>
                              <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeAlbum}</p>
                              <IoMdArrowForward color={"#00A7E2"} size={25} />
                            </Col>
                        </Row>
                      </Col>
                  </Row>
                </Col>  
            )}
          
            <Col xs={12} className="d-flex">
              <Link to={"/all-media-albums"} className="mt-4 mb-5 mx-auto">
                <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
              </Link>
            </Col>
          </Layout>
        </Container>
        </>
        }
        
        
        {/* BLOC 3:  VIDEOS */}
        {mediaVideos.length > 0
        &&
        <Container fluid style={{background : '#fff'}} className="py-5 mb-4">
            <Layout>
              {/* TITLE */}
              <Row className="mb-4">
                <Col xs={12} className="px-0">
                  <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'26px'}}>{trans[siteLanguage].mediaNews.titleVideos}</p>
                </Col>

                <Col xs={12} className="mb-3 px-0">
                  <Image src={images.orange_divider} alt="afsac-divider"  />
                </Col>
              </Row>

              {mediaVideos.map((video,i) => 
                i < 6
                &&
                <Col xs={12} className="p-0" key={i * Math.random() * 6}>
                  <Row>
                      <Col xs={12} lg={5}>
                        <Row>
                            <Col xs={11} lg={10} className="mb-3 p-0">
                              <Image 
                                  src={getYoutubeImage(video.lien)}  
                                  style={{ width: '100%', cursor: 'pointer'}} 
                                  onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}
                                  alt={video.titre}
                                />
                            </Col>
                            
                            <Col xs={1} className="d-flex">
                              <div className="m-auto" style={{height: '140px', borderLeft: '4px solid #00A7E2'}} />
                            </Col>
                        </Row>
                      </Col>

                      <Col xs={12} lg={7} className="d-flex">
                        <Row className="my-auto">
                            <Col xs={12} className="p-0">
                              <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{video.titre}</p>
                            </Col>

                            <Col xs={12} className="p-0 d-flex mb-4" onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}>
                              <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeVideo}</p>
                              <IoMdArrowForward color={"#00A7E2"} size={25} />
                            </Col>
                        </Row>
                      </Col>
                  </Row>
                </Col>
              )}

              <Col xs={12} className="d-flex">
                <Link to={"/all-media-videos"} className="mt-4 mb-5 mx-auto">
                  <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                </Link>
              </Col>

              <ModalVideo channel='youtube' autoplay isOpen={isOpenVideo} videoId={videoToPlay} onClose={() => {setOpenVideo(false); setVideoToPlay(null);}} />
            </Layout>
        </Container>
        }
      </Tablet> 
    
        
      <Mobile>
        <Layout>
            {/* BreadCrumb */}
            <Row>
                <div className="Breadcrumb d-flex mb-3">
                    <div className="breadcrumb-item"> <Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>

                    <div className="breadcrumb-item active" >{trans[siteLanguage].mediaNews.bigTitle}</div>
                </div>
            </Row>

            {/* TITLE */}
            <Row className="my-5">
                <Col xs={12} className="d-flex">
                    <h3 className="contact-title m-auto">{trans[siteLanguage].mediaNews.bigTitle}</h3>
                </Col>

                <Col xs={12} className="d-flex">
                    <Image src={images.center_divider}  className="m-auto" alt="afsac-divider" />
                </Col>
            </Row>

            {/* BLOC 1 : ACTUALITES / NEWS */}
            {mediaNews && mediaNews.length > 0
            &&
            <>
            <Row className="mb-4">
              <Col xs={12}>
                <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.news}</p>
              </Col>

              <Col xs={12} className="mb-3">
                <Image src={images.orange_divider} alt="afsac-divider"  />
              </Col>
            </Row>

            {mediaNews.map((act, index) => 
              index < 6
              &&
              <Link to={`/media/${act.id}/${act.titre}`}>
                <Col xs={12} className="p-0" key={index * Math.random()}>
                  <Row>
                      <Col xs={12} lg={5}>
                        <Row>
                            <Col xs={12} className="mb-1 p-1">
                              <Image src={act.image} style={{ minHeight: '300px', width: '100%'}} />
                            </Col>
                            
                            <Col xs={12} className="d-flex">
                              <div className="d-flex mr-auto">
                                  <p className="news-list-date-day my-auto mr-2" style={{fontSize: '50px'}}>{getDay(act.date)}</p>
                                  <p className="news-list-date-month-year my-auto mr-2">{getMonth(act.date)}</p>
                                  <p className="news-list-date-month-year my-auto mr-2">{getYear(act.date)}</p>
                                  <div className="my-auto" style={{height: '50px', borderLeft: '4px solid #00A7E2'}} />
                              </div>
                            </Col>
                        </Row>
                      </Col>
  
                      <Col xs={12} lg={7} className="d-flex">
                        <Row className="my-auto">
                            <Col xs={12} className="">
                              <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{act.titre}</p>
                            </Col>
  
                            {act.descritpion
                            &&
                            <Col xs={12} className="">
                              <div
                                    dangerouslySetInnerHTML={{__html: act.descritpion.substring(0,300) + "..."}}
                                    className="course-gray-media-text"
                                    style={{ fontSize: '18px', color: "black", fontWeight: "400" , textAlign: "center"}}
                                >
                                </div>
                            </Col>
                            }
  
                            <Col xs={12} className=" d-flex mb-4">
                              <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                              <IoMdArrowForward color={"#00A7E2"} size={25} />
                            </Col>
                        </Row>
                      </Col>
                  </Row>
                </Col>  
              </Link>
            )}

            <Col xs={12} className="d-flex">
              <Link to={"/all-media-news"} className="mt-4 mb-5 mx-auto">
                <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
              </Link>
            </Col>
            </>
            }
        </Layout>

        {/* BLOC 2: ALBUMS */}
        {mediaAlbums.length > 0 
        &&
        <>
        <Container className="py-5 mb-4" fluid style={{backgroundColor: '#F4F6F9'}}>
            <Row className="mb-5">
              <Col xs={12}>
                <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'30px'}}>{trans[siteLanguage].mediaNews.titleAlbums}</p>
              </Col>

              <Col xs={12} className="mb-3">
                <Image src={images.orange_divider} alt="afsac-divider"  />
              </Col>
            </Row>
            
            {mediaAlbums.map((album,i) => 
                i < 6
                &&
                <Col xs={12} key={i * Math.random() * 5}>
                  <Row>
                      <Col xs={12} lg={5}>
                        <Row>
                            <Col xs={12} className="mb-1">
                              <Row className="mb-2" style={{paddingRight: '30px'}}>
                                <Col xs={12} className="p-0" style={{cursor: album.photos[0] && 'pointer'}} onClick={() => album.photos[0] && handleLightBox(album.photos,0)}>
                                  <Image src={album.photos[0]} style={{width:"100%", maxHeight: "300px"}} />
                                </Col>
                              </Row>

                              <Row className="mb-2" style={{paddingRight: '30px'}}>
                                <Col xs={12}>
                                  <Row>
                                      <Col className="p-0" xs={4} style={{cursor: album.photos[1] && 'pointer'}} onClick={() => album.photos[1] && handleLightBox(album.photos,1)}>
                                        <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[1]})`}} />
                                      </Col>

                                      <Col className="px-2" xs={4} style={{cursor: album.photos[2] && 'pointer'}} onClick={() => album.photos[2] && handleLightBox(album.photos,2)}>
                                        <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[2]})` }} />
                                      </Col>

                                      <Col className="p-0" xs={4} style={{cursor: album.photos[3] && 'pointer'}} onClick={() => album.photos[3] && handleLightBox(album.photos,3)}>
                                        <div className="media-album-small-image" style={{ backgroundImage: `url(${album.photos[3]})` }} />
                                      </Col>
                                  </Row>
                                </Col>
                              </Row>
                            </Col>
                        </Row>
                      </Col>

                      <Col xs={12} lg={7} className="d-flex">
                        <Row className="my-auto">
                            <Col xs={12} className="p-0">
                              <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{album.titre}</p>
                            </Col>

                            <Col xs={12} className="p-0 d-flex mb-4" onClick={() => album.photos && handleLightBox(album.photos,i)}>
                              <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeAlbum}</p>
                              <IoMdArrowForward color={"#00A7E2"} size={25} />
                            </Col>
                        </Row>
                      </Col>
                  </Row>
                </Col>  
            )}
          
            <Col xs={12} className="d-flex">
              <Link to={"/all-media-albums"} className="mt-4 mb-5 mx-auto">
                <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
              </Link>
            </Col>
        </Container>
        </>
        }
        
        
        {/* BLOC 3:  VIDEOS */}
        {mediaVideos.length > 0
        &&
        <Container fluid style={{background : '#fff'}} className="py-5 mb-4">
            <Layout>
              {/* TITLE */}
              <Row className="mb-4">
                <Col xs={12} className="px-0">
                  <p className="course-light-blue-text" style={{fontFamily: 'SF Pro Display',fontSize:'26px'}}>{trans[siteLanguage].mediaNews.titleVideos}</p>
                </Col>

                <Col xs={12} className="mb-3 px-0">
                  <Image src={images.orange_divider} alt="afsac-divider"  />
                </Col>
              </Row>

              {mediaVideos.map((video,i) => 
                i < 6
                &&
                <Col xs={12} className="p-0" key={i * Math.random() * 6}>
                  <Row>
                      <Col xs={12} lg={5}>
                        <Row>
                            <Col xs={12} className="mb-3 p-0">
                              <Image 
                                  src={getYoutubeImage(video.lien)}  
                                  style={{ width: '100%', cursor: 'pointer'}} 
                                  onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}
                                  alt={video.titre}
                                />
                            </Col>
                        </Row>
                      </Col>

                      <Col xs={12} lg={7} className="d-flex">
                        <Row className="my-auto">
                            <Col xs={12} className="p-0">
                              <p className="contact-title-media" style={{fontSize: '25px', fontWeight: "bold"}}>{video.titre}</p>
                            </Col>

                            <Col xs={12} className="p-0 d-flex mb-4" onClick={() => {setVideoToPlay(getYoutubeId(video.lien)); setOpenVideo(true); }}>
                              <p className="course-light-blue-text my-auto mr-2" style={{fontSize: '18px', cursor: "pointer"}}>{trans[siteLanguage].mediaNews.seeVideo}</p>
                              <IoMdArrowForward color={"#00A7E2"} size={25} />
                            </Col>
                        </Row>
                      </Col>
                  </Row>
                </Col>
              )}

              <Col xs={12} className="d-flex">
                <Link to={"/all-media-videos"} className="mt-4 mb-5 mx-auto">
                  <p className="course-light-blue-text" style={{ fontSize: '18px', textDecoration: 'underline', cursor: 'pointer'}}>{trans[siteLanguage].mediaNews.seeMore}</p>
                </Link>
              </Col>

              <ModalVideo channel='youtube' autoplay isOpen={isOpenVideo} videoId={videoToPlay} onClose={() => {setOpenVideo(false); setVideoToPlay(null);}} />
            </Layout>
        </Container>
        }
      </Mobile> 
    </>
  );
};

const mapStateToProps = state => ({
  siteLanguage : state.afsacR.siteLanguage,
  mediaNews: state.afsacR.mediaNews,
  mediaAlbums: state.afsacR.mediaAlbums,
  mediaVideos: state.afsacR.mediaVideos,
  mediaCommuniques: state.afsacR.mediaCommuniques,
});

export default connect(mapStateToProps,{ getMediaNewsData })(Media);