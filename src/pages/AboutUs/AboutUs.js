import React, { useState, useEffect } from 'react'
import { Row, Col, Image, Container, Button, Modal } from 'react-bootstrap';
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import { trans } from '../../constants/Translations';
import { Link } from 'react-router-dom';
import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';
import { IoIosClose } from 'react-icons/io';
import { connect } from 'react-redux';
import { getAboutUsData, getAbout_us } from '../../redux/actions/AfsacActions';

function AboutUs(props) {

    let {
        siteLanguage,
        aboutUsData,
        about,

        //Functions
        getAboutUsData,
        getAbout_us
    } = props;

    const [pageLoading, setPageLoading] = useState(true);
    const [wordFromCeo, setWordFromCeo] = useState(false);
    const [embedId,] = useState('pob0Mpsy-KY')

    const loaderOpts = {
        loop: true,
        autoplay: true,
        animationData: loader,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    const getAccomodationDownloadLink = (data) => {
        console.log("Getting data", data);
        let link = "#";
        let found = [];
        if (siteLanguage === "fr") {
            found = data.filter((d) => d.titre === "Francais")

            if (typeof (found[0].accomodation) !== 'string') {
                link = found[0].accomodation[0].download_link;
            }

        } else if (siteLanguage === "en") {
            found = data.filter((d) => d.titre === "Anglais")
            if (typeof (found[0].accomodation) !== 'string') {
                link = found[0].accomodation[0].download_link;
            }

        } else {
            found = data.filter((d) => d.titre === "Arabe");

            if (typeof (found[0].accomodation) !== 'string') {
                link = found[0].accomodation[0].download_link;
            }
        };
        return link;
    };

    const renderWordCeoModal = () => {
        if (siteLanguage === "fr") {

            return (
                <Modal show={wordFromCeo} size="lg" centered>
                    <Modal.Body>
                        <Col xs={12}>
                            <Row>
                                <Col xs={12} className="d-flex">
                                    <p className="contact-title my-0" style={{ fontSize: 25 }}>{trans[siteLanguage].aboutUsPage.title1}</p>
                                    <span className="ml-auto" onClick={() => { setWordFromCeo(false); }}><IoIosClose color={"#707070"} size={30} style={{ cursor: 'pointer' }} /> </span>
                                </Col>

                                <Col xs={12} className="mt-3" style={{ maxHeight: '600px', overflowY: 'scroll' }}>
                                    <p className="registration-modal-content">Chers internautes / visiteurs,</p>
                                    <p className="registration-modal-content">L’AFSAC Centre Régional de Formation à la Sûreté de l’Aviation de l’OACI de Tunis – TUNISIE et TRAINAIR PLUS FULL MEMEBER GAT / ICAO vous souhaite la bienvenue sur son site web.</p>
                                    <p className="registration-modal-content">Notre plateforme numérique présente un lieu de rencontre et de convergence pour l’ensemble des acteurs et intervenants dans tous les secteurs de l’aviation civile.</p>
                                    <p className="registration-modal-content">Nous aspirons par ce moyen de communication nous rapprocher de notre public cible, se faire mieux connaître et surtout mieux le servir.</p>
                                    <p className="registration-modal-content">Le secteur de l’aviation civile dans le monde qui ne cesse de se développer fait face constamment aux défis majeurs qui sont la sécurité et la sûreté. Ainsi, il demeure indispensable de continuer à élaborer et à développer les réglementations, les procédures de supervision de la sécurité de l’aviation civile, de la sécurité aérienne et de la sûreté aéroportuaire et les référentiels techniques permettant de répondre aux attentes des instances de l’aviation civile internationale.</p>
                                    <p className="registration-modal-content">L’armature règlementaire, procédurale et technique doit nécessairement être associée à une politique optimale de formation et un personnel empreint d’expertise, d’efficacité et de rigueur.</p>
                                    <p className="registration-modal-content">C’est dans ce sens et dans le but de soutenir les Etats dans l’application des normes et des pratiques recommandées de l’Annexe 17 que l’OACI avait mis en place un réseau de trente-cinq centres de formation en sûreté de l’aviation (CRFSA) dans le monde, dont l’AFSAC, qui a pour objectif de développer la formation et la qualification du personnel des organismes intervenants dans le domaine de l’aviation civile en s’appuyant sur des supports pédagogiques élaborés et reconnus par l’OACI.</p>
                                    <p className="registration-modal-content">C’est ainsi que nous pourrons ensemble   participer à l’effort permettant d’atteindre l’objectif qui pourrait faire de l’aviation civile internationale, une aviation sûre.</p>
                                    <p className="registration-modal-content"><strong>Chers Internautes,</strong></p>
                                    <p className="registration-modal-content">Vos observations, suggestions et critiques constructives sont attendues en vue de l’amélioration de la qualité de cette plateforme</p>
                                    <p className="registration-modal-content">Bonne navigation !</p>
                                    <p className="registration-modal-content"><strong>Le Directeur Général</strong></p>
                                    <p className="registration-modal-content"><strong>Mr. Hassan SEDDIK</strong></p>
                                </Col>

                                <Col xs={12} className="d-flex mt-2">
                                    <div className="ml-auto registration-modal-btn d-flex px-4" onClick={() => { setWordFromCeo(false); }}>
                                        <p className="m-auto">{trans[siteLanguage].registration.modalCloseBtn}</p>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Modal.Body>
                </Modal>
            );
        } else if (siteLanguage === "en") {
            return (
                <Modal show={wordFromCeo} size="lg" centered>
                    <Modal.Body>
                        <Col xs={12}>
                            <Row>
                                <Col xs={12} className="d-flex">
                                    <p className="contact-title my-0" style={{ fontSize: 25 }}>{trans[siteLanguage].aboutUsPage.title1}</p>
                                    <span className="ml-auto" onClick={() => { setWordFromCeo(false); }}><IoIosClose color={"#707070"} size={30} style={{ cursor: 'pointer' }} /> </span>
                                </Col>

                                <Col xs={12} className="mt-3 " style={{ maxHeight: '600px', overflowY: 'scroll' }}>
                                    <p className="registration-modal-content">Dear Internet users / visitors,</p>
                                    <p className="registration-modal-content">AFSAC : The ICAO Regional Aviation Security Training Center of Tunis - TUNISIA and TRAINAIR PLUS FULL MEMEBER GAT / ICAO welcome you to its website.</p>
                                    <p className="registration-modal-content">Our digital platform presents a meeting place and convergence for all stakeholders in all sectors of civil aviation.</p>
                                    <p className="registration-modal-content">By this means of communication, we aspire to get closer to our target audience, to make ourselves better known and to serve them better, above all.</p>
                                    <p className="registration-modal-content">The growing civil aviation sector around the world constantly faces major security and safety challenges. Thus, it remains essential to continue elaborating and developing regulations, civil aviation safety supervision procedures, aviation safety and airport security, and the technical benchmarks to meet the expectations of the international civil aviation’s authorities.</p>
                                    <p className="registration-modal-content">The regulatory, procedural and technical framework must necessarily be associated with an optimal training policy and a staff imbued with expertise, efficiency and rigor.</p>
                                    <p className="registration-modal-content">It is in this sense and in order to support States in the application of the Standards and Recommended Practices of Annex 17 that the ICAO had set up a network of thirty-five Aviation Security Training Centers (CRFSA) around the world, including AFSAC, which aims to develop the training and qualification of personnel from organizations working in the field of civil aviation by relying on educational materials developed and recognized by ICAO.</p>
                                    <p className="registration-modal-content">This is how we can together participate in the effort to achieve the goal that could make international civil aviation safe and secure.</p>
                                    <p className="registration-modal-content"><strong>Dear Internet users,</strong></p>
                                    <p className="registration-modal-content">Your observations, suggestions and constructive criticism are awaited to improve the quality of this platform.</p>
                                    <p className="registration-modal-content">Good navigation !</p>
                                    <p className="registration-modal-content"><strong>CEO</strong></p>
                                    <p className="registration-modal-content"><strong>Mr. Hassan SEDDIK</strong></p>
                                </Col>

                                <Col xs={12} className="d-flex mt-2">
                                    <div className="ml-auto registration-modal-btn d-flex px-4" onClick={() => { setWordFromCeo(false); }}>
                                        <p className="m-auto">{trans[siteLanguage].registration.modalCloseBtn}</p>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Modal.Body>
                </Modal>
            );
        } else {
            return (
                <>
                </>
            );
        }
    };

    useEffect(() => {
        getAboutUsData(siteLanguage)
            .then(() => {
                setPageLoading(false);
            });

        getAbout_us(siteLanguage)
            .then(() => {
                setPageLoading(false);
            });
    }, [])

    return (
        <>
            {pageLoading
                ?
                <div className="preLoader">
                    <Lottie options={loaderOpts}
                        height={200}
                        width={200}
                        style={{ position: 'absolute', top: '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
                    />
                </div>
                :
                <>
                    {wordFromCeo && renderWordCeoModal()}

                    <Container className="my-5">

                        {/* BLOC 1 : AFSAC PRESENTATION */}
                        <Row className="mb-5">
                            <Col xs={12} lg={4} className="d-flex">
                                <Desktop> <Image src={about.image1} className="mr-auto" fluid style={{ maxWidth: '374px', maxHeight: '374px' }} alt={"afsac-logo"} /> </Desktop>
                                <Tablet> <Image src={about.image1} className="m-auto" fluid alt={"afsac-logo"} /> </Tablet>
                                <Mobile> <Image src={about.image1} className="m-auto" fluid alt={"afsac-logo"} /> </Mobile>
                            </Col>
                            <Col xs={12} lg={8} className="d-flex">
                                <p className="my-auto ml-auto afsac-presentation-text">
                                    {about.description}
                                </p>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} className="mb-3">
                                <p className="contact-title my-0">{trans[siteLanguage].aboutUsPage.title2}</p>
                                <Image src={images.icon_divider} alt="afsac-divider" />
                            </Col>
                        </Row>

                        <Row className="mb-5" className="justify-content-center">
                            <Col xs={12} className="mb-3">
                                <p className="course-gray-text text-bold" style={{ color: "black" }}>
                                    {about.text1}
                                </p>
                            </Col>

                            <Col xs={12} className="mt-3 mx-auto ">
                                <div className="card unit-7  d-block ">
                                    <a className="popup-youtube">
                                        <iframe width="100%" height="100%" src={`https://www.youtube.com/embed/${embedId}`} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                                    </a>
                                </div>
                            </Col>
                        </Row>

                    </Container>

                    <Container fluid className="py-5" style={{ background: '#F4F6F9' }}>

                        <Row className="mb-5">
                            <Col xs={12} className="d-flex">
                                <h3 className="contact-title m-auto">{trans[siteLanguage].aboutUsPage.title3}</h3>
                            </Col>

                            <Col xs={12} className="d-flex">
                                <Image src={images.center_divider} alt="afsac-divider" className="m-auto" />
                            </Col>
                        </Row>

                        {/* AVSEC + TRAINING PLUS */}
                        <Container className="justify-content-center">
                            <Row className="mb-5">
                                <Col xs={12}>
                                    <Row>
                                        <Col xs={12} lg={6}>
                                            <Row>
                                                <Col xs={12} className="mb-3">
                                                    <p className="course-light-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.avsecCourses}</p>
                                                    <Image src={images.orange_divider} alt="afsac-divider" />
                                                </Col>

                                                <Col xs={12} className="mb-3">
                                                    <Image src={images.about_2} fluid alt="afsac-about" />
                                                </Col>

                                                {/* COMPTEUR ENLEVE PAR NESRINE */}
                                                {/* <Col xs={12} className="mb-3">
                                        <Row>
                                            <Col xs={6}>
                                                <Row>
                                                    <Col xs={12} className="d-flex">
                                                        <p className="m-auto course-light-blue-text">5</p>
                                                    </Col>
                                                    <Col xs={12} className="d-flex">
                                                        <p className="m-auto course-light-blue-text">Modèles</p>
                                                    </Col>
                                                </Row>
                                            </Col>

                                            <Col xs={6}>
                                                <Row>
                                                    <Col xs={12} className="d-flex">
                                                        <p className="m-auto course-light-blue-text">5</p>
                                                    </Col>
                                                    <Col xs={12} className="d-flex">
                                                        <p className="m-auto course-light-blue-text">Workshops</p>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col> */}

                                                <Col xs={12} className="d-flex mb-2" >
                                                    <Link to={'/training-avsec'} className="program-blue-btn p-3 m-auto" style={{ width: '80%' }}>
                                                        <div className="d-flex justify-content-center">
                                                            <p className="my-auto mr-2">{trans[siteLanguage].navbar.y1}</p>
                                                        </div>
                                                    </Link>
                                                </Col>
                                                <Col xs={12} className="d-flex mb-5" >
                                                    <Link to={"/sessions-avsec"} className="program-blue-btn p-3 m-auto" style={{ background: '#00A7E2', border: '1px solid #00A7E2', width: '80%' }}>
                                                        <div className="d-flex justify-content-center">
                                                            <p className="my-auto mr-2">{trans[siteLanguage].aboutUsPage.checkUpcommingSessions}</p>
                                                        </div>
                                                    </Link>
                                                </Col>
                                            </Row>
                                        </Col>

                                        <Col xs={12} lg={6} >
                                            <Row>
                                                <Col xs={12} className="mb-3">
                                                    <p className="course-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.tppCourses}</p>
                                                    <Image src={images.orange_divider} alt="afsac-divider" />
                                                </Col>

                                                <Col xs={12} className="mb-3">
                                                    <Image src={images.about_3} fluid alt="afsac-about" />
                                                </Col>
                                                {/* COMPTEUR ENLEVE PAR NESRINE */}
                                                {/* <Col xs={12} className="mb-3">
                                            <Row>
                                                <Col xs={6}>
                                                    <Row>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-blue-text" style={{fontSize: '17px'}}>5</p>
                                                        </Col>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-blue-text" style={{fontSize: '17px'}}>Modèles</p>
                                                        </Col>
                                                    </Row>
                                                </Col>

                                                <Col xs={6}>
                                                    <Row>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-blue-text" style={{fontSize: '17px'}}>5</p>
                                                        </Col>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-blue-text" style={{fontSize: '17px'}}>Workshops</p>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Col> */}

                                                <Col xs={12} className="d-flex mb-2" >
                                                    <Link to={'/training-tpp'} className="program-blue-btn p-3 m-auto" style={{ width: '80%' }}>
                                                        <div className="d-flex justify-content-center">
                                                            <p className="my-auto mr-2">{trans[siteLanguage].navbar.y2}</p>
                                                        </div>
                                                    </Link>
                                                </Col>

                                                <Col xs={12} className="d-flex" >
                                                    <Link to={'/sessions-tpp'} className="program-blue-btn p-3 m-auto" style={{ background: '#00A7E2', border: '1px solid #00A7E2', width: '80%' }}>
                                                        <div className="d-flex justify-content-center">
                                                            <p className="my-auto mr-2">{trans[siteLanguage].aboutUsPage.checkUpcommingSessions}</p>
                                                        </div>
                                                    </Link>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>

                        {/* ONLINE COURSES + ICAO */}
                        <Container>
                            <Row className="justify-content-center mb-5">
                                <Col xs={12}  >
                                    <Row>
                                        <Col xs={12} lg={6} >
                                            <Row>
                                                <Col xs={12} className="mb-3">
                                                    <p className="course-light-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.onlineCourses}</p>
                                                    <Image src={images.orange_divider} alt="afsac-divider" />
                                                </Col>

                                                <Col xs={12} className="mb-3">
                                                    <Image src={images.about_4} fluid alt="afsac-about" />
                                                </Col>

                                                {/* COMPTEUR ENLEVE PAR NESRINE */}
                                                {/* <Col xs={12} className="mb-3">
                                            <Row>
                                                <Col xs={6}>
                                                    <Row>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-light-blue-text">5</p>
                                                        </Col>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-light-blue-text">Modèles</p>
                                                        </Col>
                                                    </Row>
                                                </Col>

                                                <Col xs={6}>
                                                    <Row>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-light-blue-text">5</p>
                                                        </Col>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-light-blue-text">Workshops</p>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Col> */}

                                                <Col xs={12} className="d-flex mb-2" >
                                                    <Link to={'/training-online-courses'} className="program-blue-btn p-3 m-auto" style={{ width: '80%' }}>
                                                        <div className="d-flex justify-content-center">
                                                            <p className="my-auto mr-2">{trans[siteLanguage].navbar.y3}</p>
                                                        </div>
                                                    </Link>
                                                </Col>

                                                <Col xs={12} className="d-flex">
                                                    <Link to={'/training-online-courses'} className="program-blue-btn p-3 m-auto" style={{ background: '#00A7E2', border: '1px solid #00A7E2', width: '80%' }}>
                                                        <div className="d-flex justify-content-center">
                                                            <p className="my-auto mr-2">{trans[siteLanguage].aboutUsPage.checkUpcommingSessions}</p>
                                                        </div>
                                                    </Link>
                                                </Col>
                                            </Row>
                                        </Col>

                                        <Col xs={12} lg={6} >
                                            <Row>
                                                <Col xs={12} className="mb-3 ">
                                                    <p className="course-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.virtualCourses}</p>
                                                    <Image src={images.orange_divider} alt="afsac-divider" />
                                                </Col>

                                                <Col xs={12} className="mb-3">
                                                    <Image src={images.about_5} fluid style={{ maxHeight: '371px' }} alt="afsac-about" />
                                                </Col>

                                                {/* COMPTEUR ENLEVE PAR NESRINE */}
                                                {/* <Col xs={12} className="mb-3">
                                            <Row>
                                                <Col xs={6}>
                                                    <Row>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-blue-text" style={{fontSize: '17px'}}>5</p>
                                                        </Col>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-blue-text" style={{fontSize: '17px'}}>Modèles</p>
                                                        </Col>
                                                    </Row>
                                                </Col>

                                                <Col xs={6}>
                                                    <Row>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-blue-text" style={{fontSize: '17px'}}>5</p>
                                                        </Col>
                                                        <Col xs={12} className="d-flex">
                                                            <p className="m-auto course-blue-text" style={{fontSize: '17px'}}>Workshops</p>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Col> */}

                                                <Col xs={12} className="d-flex mb-2" >
                                                    <Link to={"/training-virtual-classroom"} className="program-blue-btn p-3 m-auto" style={{ width: '80%' }}>
                                                        <div className="d-flex justify-content-center">
                                                            <p className="my-auto mr-2">{trans[siteLanguage].navbar.y4}</p>
                                                        </div>
                                                    </Link>
                                                </Col>

                                                <Col xs={12} className="d-flex" >
                                                    <Link to={'/sessions-virtual'} className="program-blue-btn p-3 m-auto" style={{ background: '#00A7E2', border: '1px solid #00A7E2', width: '80%' }}>
                                                        <div className="d-flex justify-content-center">
                                                            <p className="my-auto mr-2">{trans[siteLanguage].aboutUsPage.checkUpcommingSessions}</p>
                                                        </div>
                                                    </Link>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>

                    </Container>

                    {/* REFERENCES & ACCOMODATION */}
                    <Container>
                        {aboutUsData && aboutUsData.hasOwnProperty("Reference")
                            &&
                            <>
                                {/* TITLE */}
                                <Row className="mb-3 mt-5">
                                    <Col xs={12} >
                                        <h3 className="contact-title">{trans[siteLanguage].aboutUsPage.ourReferences}</h3>
                                    </Col>

                                    <Col xs={12}>
                                        <Image src={images.orange_divider} alt="afsac-divider" />
                                    </Col>
                                </Row>

                                {/* REFERENCES */}
                                <Row className="mb-5">
                                    <Col xs={12} className="p-4" style={{ background: '#F4F6F9', borderLeft: '5px solid #00A7E2', borderBottom: '5px solid #00A7E2' }}>
                                        <Row className="justify-content-center">
                                            {aboutUsData.Reference.map((ref, i) =>
                                                <Col md={3} key={i} className="d-flex">
                                                    <Image src={ref.image} fluid className="mr-auto mb-2" alt={ref.titre} style={{ border: '2px solid #BCBCBC' }} />
                                                </Col>
                                            )}
                                        </Row>
                                    </Col>
                                </Row>
                            </>
                        }

                        {/* ACCOMODATION */}
                        {aboutUsData && aboutUsData.hasOwnProperty("About")
                            &&
                            <Row className="mt-3 mb-5">
                                {getAccomodationDownloadLink(aboutUsData.About) !== "#"
                                    &&
                                    <Col xs={12} >
                                        <Row className="justify-content-center">
                                            <Col xs={12} lg={6} className="d-none d-lg-block" >
                                                <Image src={images.accomodation} fluid alt="afsac-accomodation" />
                                            </Col>
                                            <Col xs={12} lg={6} className="d-lg-none d-sm-block" >
                                                <Image src={images.accomodation1} fluid alt="afsac-accomodation" />
                                            </Col>
                                            <Col xs={12} lg={6}>

                                                <Row>
                                                    <Col xs={12} className="mb-3">
                                                        <p className="contact-title my-0">{trans[siteLanguage].aboutUsPage.title4}</p>
                                                        <Image src={images.icon_divider} alt="afsac-divider" />
                                                    </Col>
                                                    <Container>
                                                        <Col xs={12} className="mb-5">
                                                            <p className="course-gray-text" style={{ zIndex: 1, marginLeft: '-5%', color: "black" }}>
                                                                {trans[siteLanguage].aboutUsPage.accomodationDesc}
                                                            </p>
                                                        </Col>
                                                    </Container>
                                                    <Col xs={12}>

                                                        <a href={getAccomodationDownloadLink(aboutUsData.About)} id='contact-form-btn' className="program-blue-btn p-3 m-auto" style={{ background: '#00A7E2', border: '1px solid #00A7E2' }} >
                                                            {trans[siteLanguage].aboutUsPage.accomodationButton}
                                                        </a>
                                                    </Col>
                                                </Row>

                                            </Col>

                                        </Row>
                                    </Col>
                                }
                            </Row>
                        }

                    </Container>
                </>
            }
        </>
    )
};


const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
    aboutUsData: state.afsacR.aboutUsData,
    about: state.afsacR.about,
});

export default connect(mapStateToProps, { getAboutUsData, getAbout_us })(AboutUs);