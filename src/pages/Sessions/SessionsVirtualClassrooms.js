import React, { useState, useEffect } from 'react';

import AvsecCourses from '../../components/Avsec/AvsecCourses';

import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';

import { connect } from 'react-redux';
import { getUpcommingCourses, getTppCategories } from '../../redux/actions/AfsacActions'; 
import { trans } from '../../constants/Translations'; 

function TrainingVirtualClassrooms(props) {

  let {
    siteLanguage,
    sessionsVirtualCoursesData,
    TppCategories,


    // Functions
    getTppCategories,
    getUpcommingCourses,
  } = props;

  const loaderOpts = {
    loop: true,
    autoplay: true, 
    animationData: loader,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  const [pageLoading, setPageLoading] = useState(true);

  useEffect(() => {
    getTppCategories(siteLanguage)
    .then(() => {
      getUpcommingCourses(siteLanguage)
      .then(() => {
        setPageLoading(false);
      });
    });
  },[]);


    return (
        <>
        {pageLoading
        ?
        <div className="preLoader">
          <Lottie options={loaderOpts}
              height={200}
              width={200}
              style={{ position: 'absolute', top : '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
          />
        </div>
        :
        <AvsecCourses 
            courseTitle={trans[siteLanguage].avsecPages.upcommingVirtualTitle}
            courses={sessionsVirtualCoursesData}
            platform={"VirtualClassrooms"}
            categories={TppCategories}
        />  
        }
        </>
    )
}


const mapStateToProps = state => ({
    siteLanguage : state.afsacR.siteLanguage,
    TppCategories : state.afsacR.TppCategories,
    sessionsVirtualCoursesData : state.afsacR.sessionsVirtualCoursesData,
  });
  
  export default connect(mapStateToProps,{ getUpcommingCourses, getTppCategories })(TrainingVirtualClassrooms);
  
  