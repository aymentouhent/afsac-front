import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';
import { getUpcommingCourses, getTppCategories } from '../../redux/actions/AfsacActions'; 
import TppCourses from '../../components/Tpp/TppCourses';

import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';
import { trans } from '../../constants/Translations'; 


function SessionsTpp(props) {

    let {
        siteLanguage,
        sessionsTppCoursesData,
        TppCategories,
    
        // Functions
        getTppCategories,
        getUpcommingCourses,
    } = props;
    
    const loaderOpts = {
        loop: true,
        autoplay: true, 
        animationData: loader,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
    };

    const [pageLoading, setPageLoading] = useState(true);
    
    useEffect(() => {
        getTppCategories(siteLanguage)
        .then(() => {
            getUpcommingCourses(siteLanguage)
            .then(() => {
                setPageLoading(false)
            });
        });
    },[]);

    return (   
        <>
        {pageLoading
        ?
        <div className="preLoader">
            <Lottie options={loaderOpts}
                height={200}
                width={200}
                style={{ position: 'absolute', top : '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
            />
        </div>
        :
        <TppCourses 
            bannerTitle1={trans[siteLanguage].TppPages.bannerTitle1}
            bannerTitle2={trans[siteLanguage].TppPages.bannerTitle2}
            bannerDesc={trans[siteLanguage].TppPages.bannerDesc}
            courseTitle={trans[siteLanguage].TppPages.upcommingTppTitle}
            courseTitleDesc={trans[siteLanguage].TppPages.TppTitleDesc}
            courses={sessionsTppCoursesData}
            platform={"trainAirPlus"}
            categories={TppCategories}
        /> 
        }
        </>     
       
    )
};

const mapStateToProps = state => ({
    siteLanguage : state.afsacR.siteLanguage,
    TppCategories : state.afsacR.TppCategories,
    sessionsTppCoursesData : state.afsacR.sessionsTppCoursesData,
});
  
export default connect(mapStateToProps,{ getUpcommingCourses, getTppCategories })(SessionsTpp);
