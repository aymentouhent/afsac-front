import React, { useState } from 'react'
import '../../components/components.css';
import { NavLink, Navbar, FormControl, Container, Row, Col, Image } from 'react-bootstrap';
import { Link, useLocation, withRouter, useHistory } from 'react-router-dom';
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import { trans } from '../../constants/Translations';

import { connect } from 'react-redux';
import { setSiteLanguage, getUpcommingCourses, searchCourses } from '../../redux/actions/AfsacActions';

import OutsideClickHandler from 'react-outside-click-handler';
import Slider from "react-slick";

import { IoIosArrowDown, IoIosArrowUp, IoIosArrowForward, IoIosArrowBack } from 'react-icons/io';
import { AiOutlineMenu, AiOutlineSearch, AiOutlineClose } from 'react-icons/ai';

// import Layout from '../Layout';


function Session(props) {

    let {
        siteLanguage,
        sessionsTppCoursesData,

        setSiteLanguage,
        searchCourses,
        getUpcommingCourses,
    } = props;



    const [showMenu, setShowMenu] = useState(false);
    const [showSessionsMenu, setShowSessionsMenu] = useState(false);
    const [showMobileMenu, setShowMobileMenu] = useState(false);
    const [SubMobileMenu, setSubMobileMenu] = useState(null);
    const [SubMobileMenuSession, setSubMobileMenuSession] = useState(null);
    const [showMobileSearch, setShowMobileSearch] = useState(false);
    const [searchValue, setSearchValue] = useState('');
    const location = useLocation();
    const history = useHistory();




    return (
        <>
            <Desktop>
                {/* <Container fluid >
                    <OutsideClickHandler onOutsideClick={() => setShowSessionsMenu(false)}>
                        <Container className="p-5">
                            <Row>
                                <Col>
                                    <h2 className="header-dropdown-title">{trans[siteLanguage].navbar.sessions}</h2>

                                    <p className="header-dropdown-description">{trans[siteLanguage].navbar.sessionsDesc}</p>


                                </Col>

                                <Col className="d-flex flex-column">
                                    <Link style={{ textDecoration: "none" }} to="/sessions-avsec" className="mb-auto">
                                        <div className="d-flex" onClick={() => setShowSessionsMenu(false)}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.avsecCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/sessions-tpp" className="my-auto">
                                        <div className="d-flex" onClick={() => setShowSessionsMenu(false)}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.tppCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/sessions-virtual" className="my-auto">
                                        <div className="d-flex" onClick={() => setShowSessionsMenu(false)}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.virtualCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-online-courses" className="mt-auto">
                                        <div className="d-flex" onClick={() => setShowSessionsMenu(false)}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.onlineCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                </Col>



                            </Row>
                        </Container>

                    </OutsideClickHandler>

                </Container> */}
                <Container fluid className="py-5" style={{ background: '#F4F6F9' }}>

                    <Row className="mb-5">
                        <Col xs={12} className="d-flex">
                            <h1  className="contact-title m-auto">{trans[siteLanguage].navbar.sessions}</h1>
                        </Col>

                        <Col xs={12} className="d-flex">
                            <Image src={images.center_divider} alt="afsac-divider" className="m-auto" />
                        </Col>
                    </Row>

                    {/* AVSEC + TRAINING PLUS */}
                    <Container className="justify-content-center">
                        <Col style={{ marginLeft: -15 }}>

                            <p className="text-center mb-4 text-justify px-1 session-desc-home" id='contact-form-label' style={{ fontSize: "25px" }}>{trans[siteLanguage].navbar.sessionsDesc}</p>
                            {/* <Link style={{ textDecoration: "none" }}>
                <span className="header-dropdown-small-title">Discover Courses {">"} </span>
            </Link>*/}
                        </Col>
						<br />
                        <Row className="mb-5">
                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} lg={6}>
                                        <Row>
                                            <Col xs={12} className="mb-3">
                                                <p className="course-light-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.x1}</p>
                                                <Image src={images.orange_divider} alt="afsac-divider" />
                                            </Col>

                                            <Col xs={12} className="mb-3">
                                                {/* <Image src={about.image_cours_AVSEC_OACI} fluid alt="afsac-about" /> */}
                                                <Image src={images.about_2} fluid alt="afsac-about" />

                                            </Col>
                                            <Col xs={12} className="d-flex mb-2" >
                                                <Link to={"/sessions-avsec"} className="btn btn-primary" style={{ width: '80%', height: '50px' }}>
                                                    <div className="d-flex justify-content-center">
                                                        <p className="my-auto mr-2">{trans[siteLanguage].navbar.avsecCourses}</p>
                                                    </div>
                                                </Link>
                                            </Col>

                                        </Row>
                                    </Col>

                                    <Col xs={12} lg={6} >
                                        <Row>
                                            <Col xs={12} className="mb-3">
                                                <p className="course-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.x2}</p>
                                                <Image src={images.orange_divider} alt="afsac-divider" />
                                            </Col>

                                            <Col xs={12} className="mb-3">
                                                <Image src={images.about_3} fluid alt="afsac-about" />
                                            </Col>


                                            <Col xs={12} className="d-flex mb-2" >
                                                <Link to={'/sessions-tpp'} className="btn btn-primary" style={{ width: '80%', height: '50px' }}>
                                                    <div className="d-flex justify-content-center">
                                                        <p className="my-auto mr-2">{trans[siteLanguage].navbar.tppCourses}</p>
                                                    </div>
                                                </Link>
                                            </Col>

                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>

                    {/* ONLINE COURSES + ICAO */}
                    <Container>
                        <Row className="justify-content-center mb-5">
                            <Col xs={12}  >
                                <Row>
                                    <Col xs={12} lg={6} >
                                        <Row>
                                            <Col xs={12} className="mb-3">
                                                <p className="course-light-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.x3}</p>
                                                <Image src={images.orange_divider} alt="afsac-divider" />
                                            </Col>

                                            <Col xs={12} className="mb-3">
                                                <Image src={images.about_4} fluid alt="afsac-about" />
                                            </Col>

                                            <Col xs={12} className="d-flex mb-2" >
                                                <Link to={'/training-online-courses'} className="btn btn-primary" style={{ width: '80%', height: '50px' }}>
                                                    <div className="d-flex justify-content-center">
                                                        <p className="my-auto mr-2" style={{textAlign: 'center'}}>{trans[siteLanguage].navbar.onlineCourses}</p>
                                                    </div>
                                                </Link>
                                            </Col>

                                        </Row>
                                    </Col>

                                    <Col xs={12} lg={6} >
                                        <Row>
                                            <Col xs={12} className="mb-3 ">
                                                <p className="course-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.x4}</p>
                                                <Image src={images.orange_divider} alt="afsac-divider" />
                                            </Col>

                                            <Col xs={12} className="mb-3">
                                                <Image src={images.about_5} fluid style={{ maxHeight: '371px' }} alt="afsac-about" />
                                            </Col>

                                            <Col xs={12} className="d-flex mb-2" >
                                                <Link to={'/sessions-virtual'} className="btn btn-primary" style={{ width: '80%', height: '50px' }}>
                                                    <div className="d-flex justify-content-center">
                                                        <p className="my-auto mr-2">{trans[siteLanguage].navbar.virtualCourses}</p>
                                                    </div>
                                                </Link>
                                            </Col>

                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>

                </Container>
            </Desktop>




        </>
    );
};

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
    sessionsTppCoursesData: state.afsacR.sessionsTppCoursesData,
});

export default withRouter(connect(mapStateToProps, { setSiteLanguage, getUpcommingCourses, searchCourses })(Session));