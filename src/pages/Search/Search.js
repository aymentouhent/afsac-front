import React, { useEffect, useState } from "react";

// Components
import AvsecCourse from '../../components/Avsec/AvsecCourse';
import TppCourse from '../../components/Tpp/TppCourse';


import {Container, Col, Row, Image, Spinner } from "react-bootstrap";
import { images } from '../../constants/AppConfig';
import { connect } from 'react-redux';
import { searchCourses, getTppCategories } from '../../redux/actions/AfsacActions';
import { useParams, useHistory, Link, } from "react-router-dom";
import { trans } from '../../constants/Translations';

function Search(props) {

  let { searchValue } = useParams();
  let history = useHistory();

  let {
    siteLanguage,
    searchResults,
    TppCategories,

    // Functions
    searchCourses,
    getTppCategories,
  } = props;

  const [loading, setLoading] = useState(true);
  const [searchError, setSearchError] = useState(false);

  const pickCategoryColor = (cat_id) => {
    let found = TppCategories.find((cat) => cat.id === cat_id)
     
    if(found) {
     return found.color;
    } else {
        return false;
    }
 };

 const pickCategoryPhoto = (cat_id) => {
    let found = TppCategories.find((cat) => cat.id === cat_id)
 
    if(found) {
        return found.image;
    } else {
        return '';
    }
};

  useEffect(() => {
    if(searchValue) {
        getTppCategories(siteLanguage)
        .then(() => {
            searchCourses(searchValue)
            .then(() => {
                setLoading(false);
                setSearchError(false);
            })
            .catch(() => {
                setLoading(false);
                setSearchError(true);
            })
        });
    }
  },[searchValue]);

  return (
    <Container fluid>
      <Row className="justify-content-center my-5"> 
        <Col xs={12} xl={8}>
            <Row>
                <Col xs={12} className="d-flex">
                    <h3 className="contact-title m-auto">{trans[siteLanguage].search.title}</h3>
                </Col>

                <Col xs={12} className="d-flex">
                    <Image src={images.center_divider}  className="m-auto" alt="afsac-divider" />
                </Col>
                
                {searchError
                &&
                <div className="w-100 d-flex">
                    <div className="mx-auto d-flex pt-2" style={{paddingBottom: "250px"}}>
                        <p className="course-gray-media-text my-auto" style={{fontSize: '20px'}}>{trans[siteLanguage].search.error}</p>
                    </div>
                </div>
                }

                {loading 
                ?
                <div className="w-100 d-flex">
                    <div className="mx-auto d-flex pt-2" style={{paddingBottom: "250px"}}>
                        <p className="course-gray-media-text my-auto" style={{fontSize: '20px'}}>{trans[siteLanguage].search.loading}</p>
                        <Spinner animation="border" className="my-auto ml-2 spinner-blue" size="sm" />
                    </div>
                </div>
                :
                searchResults.length > 0
                ?
                <>
                {searchResults
                &&
                <Col xs={12} className="d-flex mt-3">
                    <p className="m-auto course-gray-media-text">"{searchResults.length}" {trans[siteLanguage].search.found} "{searchValue}"</p>
                </Col>
                }

                {searchResults
                &&
                <Col xs={12} className="mt-4 mb-3">
                    {searchResults.map((course,index) =>
                        course.plat === "trainAirPlus"
                        ?
                        <TppCourse 
                            key={index} 
                            border={pickCategoryColor(course.id_categorie)}  
                            id={course.id} 
                            title={course.titre}
                            description={course.description}
                            video={course.video}
                            // category={course.categorie}
                            // image={course.image}
                            image={pickCategoryPhoto(course.id_categorie)}
                            duration={course.duree}
                            delivery={course.livraison}
                            price={course.prix}
                            language={course.langue}
                            objectif={course.objectif}
                            inscription={course.inscription}
                            population={course.population}
                            place={course.place}
                            entry={course.entry}
                            image_detail={course.image_detail}
                            platform={course.plat}
                            date={course.hasOwnProperty('date') ? course.date : null}
                        />
                        :
                        <AvsecCourse
                            id={course.id}
                            title={course.titre}
                            description={course.description}
                            video={course.video}
                            category={course.categorie}
                            categories={TppCategories}
                            image={course.plat === "AVSEC" ? course.image : pickCategoryPhoto(parseInt(course.categorie))}
                            duration={course.duree}
                            delivery={course.livraison}
                            price={course.prix}
                            language={course.langue}
                            objectif={course.objectif}
                            inscription={course.inscription}
                            population={course.population}
                            place={course.place}
                            entry={course.entry}
                            image_detail={course.image_detail}
                            platform={course.plat}
                            date={course.hasOwnProperty('date') ? course.date : null}
                        />
                    )}
                </Col>
                }
                </>
                :
                <div className="w-100 d-flex">
                    <div className="mx-auto d-flex pt-2" style={{paddingBottom: "250px"}}>
                        <p className="course-gray-media-text my-auto" style={{fontSize: '20px'}}>{trans[siteLanguage].search.notFound}</p>
                    </div>
                </div>
                }
            </Row>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({
  siteLanguage : state.afsacR.siteLanguage,
  searchResults: state.afsacR.searchResults,
  TppCategories : state.afsacR.TppCategories,
  
});

export default connect(mapStateToProps,{ searchCourses, getTppCategories })(Search);