import React, { useState, useEffect } from 'react';

import AvsecCourses from '../../components/Avsec/AvsecCourses';

import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';

import { connect } from 'react-redux';
import { getAvsecCourses, } from '../../redux/actions/AfsacActions';
import { trans } from '../../constants/Translations';


function TrainingAvsec(props) {

  let {
    siteLanguage,
    AvsecCoursesData,

    // Functions
    getAvsecCourses,
  } = props;

  const loaderOpts = {
    loop: true,
    autoplay: true,
    animationData: loader,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  const [pageLoading, setPageLoading] = useState(true);

  useEffect(() => {
    getAvsecCourses(siteLanguage)
      .then(() => {
        setPageLoading(false);
      });
  }, []);


  return (
    <>
      {pageLoading
        ?
        <div className="preLoader">
          <Lottie options={loaderOpts}
            height={200}
            width={200}
            style={{ position: 'absolute', top: '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
          />
        </div>
        :
        <AvsecCourses
          courseTitle={trans[siteLanguage].navbar.avsecCourses}
          courses={AvsecCoursesData}
          platform={"AVSEC"}
          categories={null}
        />
      }
    </>
  )
}

const mapStateToProps = state => ({
  siteLanguage: state.afsacR.siteLanguage,
  AvsecCoursesData: state.afsacR.AvsecCoursesData,

});

export default connect(mapStateToProps, { getAvsecCourses })(TrainingAvsec);

