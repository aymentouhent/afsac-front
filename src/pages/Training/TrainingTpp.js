import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';
import { getTppCourses, getTppCategories } from '../../redux/actions/AfsacActions'; 

import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';

import TppCourses from '../../components/Tpp/TppCourses';
import { trans } from '../../constants/Translations'; 


function TrainingTpp(props) {

    let {
        siteLanguage,
        TppCoursesData,
        TppCategories,
    
        // Functions
        getTppCategories,
        getTppCourses,
    } = props;

    const loaderOpts = {
        loop: true,
        autoplay: true, 
        animationData: loader,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
    };


    const [pageLoading, setPageLoading] = useState(true);
    
    useEffect(() => {
        getTppCategories(siteLanguage)
        .then(() => {
            getTppCourses(siteLanguage)
            .then(() => {
                setPageLoading(false)
            });
        });
    },[]);

    return (   
        <>
        {pageLoading
        ?
        <div className="preLoader">
            <Lottie options={loaderOpts}
                height={200}
                width={200}
                style={{ position: 'absolute', top : '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
            />
        </div>
        :
        <TppCourses 
            bannerTitle1={trans[siteLanguage].TppPages.bannerTitle1}
            bannerTitle2={trans[siteLanguage].TppPages.bannerTitle2}
            bannerDesc={trans[siteLanguage].TppPages.bannerDesc}
            courseTitle={trans[siteLanguage].navbar.tppCourses}
            courseTitleDesc={trans[siteLanguage].TppPages.TppTitleDesc}
            courses={TppCoursesData}
            platform={"trainAirPlus"}
            categories={TppCategories}
        /> 
        }
        </>     
       
    )
};

const mapStateToProps = state => ({
    siteLanguage : state.afsacR.siteLanguage,
    TppCoursesData : state.afsacR.TppCoursesData,
    TppCategories : state.afsacR.TppCategories,
});
  
export default connect(mapStateToProps,{ getTppCourses, getTppCategories })(TrainingTpp);
