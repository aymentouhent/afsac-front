import React, { useState } from 'react'
import '../../components/components.css';
import { NavLink, Navbar, FormControl, Container, Row, Col, Image } from 'react-bootstrap';
import { Link, useLocation, withRouter, useHistory } from 'react-router-dom';
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import { trans } from '../../constants/Translations';

import { connect } from 'react-redux';
import { setSiteLanguage, getUpcommingCourses, searchCourses } from '../../redux/actions/AfsacActions';

// import Layout from '../Layout';


function Training(props) {

    let {
        siteLanguage,


        //Functions

    } = props;






    return (
        <>
            <Desktop>
                {/* <Container fluid style={{ background: "#FFF" }}>


                    <OutsideClickHandler >
                        <Container className="p-5">
                            <Row>
                                <Col>
                                    <h2 className="header-dropdown-title">Training</h2>
                                    <p className="header-dropdown-description">{trans[siteLanguage].navbar.trainingDesc}</p>
                                    {/* <Link style={{ textDecoration: "none" }}>
                                        <span className="header-dropdown-small-title">Discover Courses {">"} </span>
                                    </Link>
                                </Col>

                                <Col className="d-flex flex-column">
                                    <Link style={{ textDecoration: "none" }} to="/training-avsec" >
                                        <div className="d-flex" >
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.avsecCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />

                                    <Link style={{ textDecoration: "none" }} to="/training-tpp" className="my-auto">
                                        <div className="d-flex" >
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.tppCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-virtual-classroom" className="my-auto">
                                        <div className="d-flex" >
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.virtualCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-online-courses" className="mt-auto">
                                        <div className="d-flex" >
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.onlineCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                </Col>


                            </Row>
                        </Container>
                    </OutsideClickHandler>



                </Container> */}
                <Container fluid className="py-5" style={{ background: '#F4F6F9' }}>

                    <Row className="mb-5">
                        <Col xs={12} className="d-flex">
                            <h3 className="contact-title m-auto">{trans[siteLanguage].aboutUsPage.title3}</h3>
                        </Col>

                        <Col xs={12} className="d-flex">
                            <Image src={images.center_divider} alt="afsac-divider" className="m-auto" />
                        </Col>
                    </Row>



                    {/* AVSEC + TRAINING PLUS */}
					
                    <Container className="justify-content-center">

                        <Col style={{ marginLeft: -15 }}>
                            <h5 style = {{color:"orange"}} className="text-center mb-4 text-justify px-1 session-desc-training1">
                                {trans[siteLanguage].navbar.trainingDesc}
                            </h5>
                        </Col>
						<br />
                        <br />
                        <Row className="mb-5">
                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} lg={6}>
                                        <Row>
                                            <Col xs={12} className="mb-3">
                                                <p className="course-light-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.x1}</p>
                                                <Image src={images.orange_divider} alt="afsac-divider" />
                                            </Col>

                                            <Col xs={12} className="mb-3">
                                                {/* <Image src={about.image_cours_AVSEC_OACI} fluid alt="afsac-about" /> */}
                                                <Image src={images.about_2} fluid alt="afsac-about" />

                                            </Col>


                                            <Col xs={12} className="d-flex mb-2" >
                                                <Link to={'/training-avsec'} className="btn btn-primary" style={{ width: '80%', height: '50px' }}>
                                                    <div className="d-flex justify-content-center">
                                                        <p className="my-auto mr-2">{trans[siteLanguage].navbar.avsecCourses}</p>
                                                    </div>
                                                </Link>
                                            </Col>

                                        </Row>
                                    </Col>

                                    <Col xs={12} lg={6} >
                                        <Row>
                                            <Col xs={12} className="mb-3">
                                                <p className="course-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.x2}</p>
                                                <Image src={images.orange_divider} alt="afsac-divider" />
                                            </Col>

                                            <Col xs={12} className="mb-3">
                                                <Image src={images.about_3} fluid alt="afsac-about" />
                                            </Col>


                                            <Col xs={12} className="d-flex mb-2" >
                                                <Link to={'/training-tpp'} className="btn btn-primary" style={{ width: '80%', height: '50px' }}>
                                                    <div className="d-flex justify-content-center">
                                                        <p className="my-auto mr-2">{trans[siteLanguage].navbar.tppCourses}</p>
                                                    </div>
                                                </Link>
                                            </Col>


                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>

                    {/* ONLINE COURSES + ICAO */}
                    <Container>
                        <Row className="justify-content-center mb-5">
                            <Col xs={12}  >
                                <Row>
                                    <Col xs={12} lg={6} >
                                        <Row>
                                            <Col xs={12} className="mb-3">
                                                <p className="course-light-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.x3}</p>
                                                <Image src={images.orange_divider} alt="afsac-divider" />
                                            </Col>

                                            <Col xs={12} className="mb-3">
                                                <Image src={images.about_4} fluid alt="afsac-about" />
                                            </Col>

                                            <Col xs={12} className="d-flex mb-2" >
                                                <Link to={'/training-online-courses'} className="btn btn-primary" style={{ width: '80%', height: '50px' }}>
                                                    <div className="d-flex justify-content-center">
                                                        <p className="my-auto mr-2">{trans[siteLanguage].navbar.onlineCourses}</p>
                                                    </div>
                                                </Link>
                                            </Col>

                                        </Row>
                                    </Col>

                                    <Col xs={12} lg={6} >
                                        <Row>
                                            <Col xs={12} className="mb-3 ">
                                                <p className="course-blue-text" style={{ fontSize: '20px' }}>{trans[siteLanguage].navbar.x4}</p>
                                                <Image src={images.orange_divider} alt="afsac-divider" />
                                            </Col>

                                            <Col xs={12} className="mb-3">
                                                <Image src={images.about_5} fluid style={{ maxHeight: '371px' }} alt="afsac-about" />
                                            </Col>

                                            <Col xs={12} className="d-flex mb-2" >
                                                <Link to={"/training-virtual-classroom"} className="btn btn-primary" style={{ width: '80%', height: '50px' }}>
                                                    <div className="d-flex justify-content-center">
                                                        <p className="my-auto mr-2">{trans[siteLanguage].navbar.virtualCourses}</p>
                                                    </div>
                                                </Link>
                                            </Col>


                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>

                </Container>
            </Desktop>


        </>
    );
};

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
    sessionsTppCoursesData: state.afsacR.sessionsTppCoursesData,

    aboutUsData: state.afsacR.aboutUsData,
    about: state.afsacR.about,
});

export default withRouter(connect(mapStateToProps, { setSiteLanguage, getUpcommingCourses, searchCourses })(Training));