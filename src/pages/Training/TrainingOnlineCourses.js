import React, { useState, useEffect } from 'react';

import AvsecCourses from '../../components/Avsec/AvsecCourses';

import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';

import { connect } from 'react-redux';
import { getOnlineCourses, getTppCategories  } from '../../redux/actions/AfsacActions'; 
import { trans } from '../../constants/Translations'; 

function TrainingOnlineCourses(props) {

  let {
    siteLanguage,
    OnlineCoursesData,
    TppCategories,
    // Functions
    getTppCategories,
    getOnlineCourses,
  } = props;

  const loaderOpts = {
    loop: true,
    autoplay: true, 
    animationData: loader,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  const [pageLoading, setPageLoading] = useState(true);

  useEffect(() => {
    getTppCategories(siteLanguage)
    .then(() => {
      getOnlineCourses(siteLanguage)
      .then(() => {
        setPageLoading(false);
      });
    });
  },[]);


    return (
        <>
        {pageLoading
        ?
        <div className="preLoader">
          <Lottie options={loaderOpts}
              height={200}
              width={200}
              style={{ position: 'absolute', top : '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
          />
        </div>
        :
        <AvsecCourses 
            courseTitle={trans[siteLanguage].navbar.onlineCourses}
            courses={OnlineCoursesData}
            platform={"OnlineCourses"}
            categories={TppCategories}
        />  
        }
        </>
    )
};

const mapStateToProps = state => ({
    siteLanguage : state.afsacR.siteLanguage,
    TppCategories : state.afsacR.TppCategories,
    OnlineCoursesData : state.afsacR.OnlineCoursesData,
});
  
export default connect(mapStateToProps,{ getOnlineCourses,  getTppCategories})(TrainingOnlineCourses);
  
  