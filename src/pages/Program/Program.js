import React from 'react';

import {
    Container,
    Col,
    Row,
    Carousel,
    Image,
} from 'react-bootstrap';
import Slider from 'react-slick';

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import { FiArrowRightCircle } from 'react-icons/fi';
import { IoIosArrowRoundForward } from 'react-icons/io';

export default function Program() {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dotsClass: "slider-pagination", 
    };

    const programs = [                
        { title: 'AERODROMES', background : '#29337C' },
        { title: 'AIR TRANSPORT', background : '#8C99A1' },
        { title: 'TRAINING COMPETENCY DEVELOPMENT', background : '#9D050C' },
        { title: 'ENVIRONEMENT', background : '#118834' },
        { title: 'AIR NAVIGATION SERVICES', background : '#39464E' },
        { title: 'FLIGHT SAFETY AND SAFETY MANAGEMENT', background : '#FCA728' },
        { title: 'AVIATION MANAGEMENT', background : '#612687' },
        { title: 'AVIATION LAW', background : '#1690CF' },
        { title: 'SECURITY AND FACILITATION', background : '#F56E20' },
    ];

    return (
        <>
        <Carousel className="mb-5">
            <Carousel.Item>
                <Image
                    className="d-block w-100"
                    src={images.program_banner}
                    alt="First slide"
                />

                <Carousel.Caption>
                    <Row>
                        <Col xs={12}>
                            <h3 className="carousel-title">Spécialiste en Maintenance des Stations VSAT Aeronautiques </h3>
                        </Col>
                    </Row>
                    <Desktop>
                        <Row>
                            <Col xs={4}>
                                <button className="program-orange-btn p-3 w-100">
                                    <div className="d-flex justify-content-center">
                                        <p className="my-auto mr-3">Upcoming courses</p>
                                        <Image src={images.program_plane} fluid className="my-auto" style={{maxHeight: '25px'}} />
                                    </div>
                                </button>
                            </Col>

                            <Col xs={4}>
                                <button className="program-blue-btn p-3 w-100">
                                    <div className="d-flex justify-content-center">
                                        <p className="my-auto mr-2">Demander une formation</p>
                                        <FiArrowRightCircle size={25} className="my-auto" />
                                    </div>
                                </button>
                            </Col>
                        </Row>
                    </Desktop>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>

        <Container>
            {/* BTNS RESPONSIVE */}
            <Tablet>
                <Row>
                    <Col xs={12} className="mb-3">
                        <button className="program-orange-btn p-3 w-100">
                            <div className="d-flex justify-content-center">
                                <p className="my-auto mr-3">Upcoming courses</p>
                                <Image src={images.program_plane} fluid className="my-auto" style={{maxHeight: '25px'}} />
                            </div>
                        </button>
                    </Col>

                    <Col xs={12} className="mb-4">
                        <button className="program-blue-btn p-3 w-100">
                            <div className="d-flex justify-content-center">
                                <p className="my-auto mr-2">Demander une formation</p>
                                <FiArrowRightCircle size={25} className="my-auto" />
                            </div>
                        </button>
                    </Col>
                </Row>
            </Tablet>

            {/* BTNS RESPONSIVE */}
            <Mobile>
                <Row>
                    <Col xs={12} className="mb-3">
                        <button className="program-orange-btn p-3 w-100">
                            <div className="d-flex justify-content-center">
                                <p className="my-auto mr-3">Upcoming courses</p>
                                <Image src={images.program_plane} fluid className="my-auto" style={{maxHeight: '25px'}} />
                            </div>
                        </button>
                    </Col>

                    <Col xs={12} className="mb-4">
                        <button className="program-blue-btn p-3 w-100">
                            <div className="d-flex justify-content-center">
                                <p className="my-auto mr-2">Demander une formation</p>
                                <FiArrowRightCircle size={25} className="my-auto" />
                            </div>
                        </button>
                    </Col>
                </Row>
            </Mobile>

            <Row className="mb-4">
                <Col xs={12} className="d-flex">
                    <h3 className="contact-title m-auto">TRAINAIR PLUS PROGRAM </h3>
                </Col>

                <Col xs={12} className="d-flex">
                    <Image src={images.center_divider} fluid className="m-auto" />
                </Col>
            </Row>

            {/* Programs */}
            <Row className="mb-4">
                <Col xs={12} className="d-flex">
                    <Row className="d-flex m-auto justify-content-center">
                            {
                            programs.map((program,i) => 
                                <Col lg={2} className="p-0 mt-3 d-flex align-items-center flex-column mr-2" >
                                    <Image fluid className="w-100" src={require(`../../assets/images/Home/${i+1}.png`)} style={{minHeight: '209px'}} />
                                    <button className="d-flex w-100 align-items-center justify-content-center home-program-btn" style={{ background: program.background }}>
                                        {program.title}
                                    </button>
                                </Col>
                            )
                            }
                            <Desktop>
                                <Col lg={2} className="p-0 mt-3 d-flex align-items-center flex-column mr-2" style={{background : "#EAEAEA"}} >
                                        <Image fluid className="m-auto" src={images.big_search_icon} />
                                        <button className="d-flex w-100 align-items-center justify-content-center home-program-btn" style={{background : "#EAEAEA"}}>
                                            <div className="d-flex justify-content-center home-see-more-bloc">
                                                <p className="m-auto">View more</p>
                                                <IoIosArrowRoundForward size={30} className="m-auto" />
                                            </div>
                                        </button>
                                </Col>
                            </Desktop>

                            <Tablet>
                                <Col lg={2} className="p-3 mt-3 d-flex align-items-center flex-column mr-2" style={{background : "#EAEAEA"}} >
                                        <Image fluid className="m-auto" src={images.big_search_icon} />
                                        <button className="d-flex w-100 align-items-center justify-content-center home-program-btn" style={{background : "#EAEAEA"}}>
                                            <div className="d-flex justify-content-center home-see-more-bloc">
                                                <p className="m-auto">View more</p>
                                                <IoIosArrowRoundForward size={30} className="m-auto" />
                                            </div>
                                        </button>
                                </Col>
                            </Tablet>

                            <Mobile>
                                <Col lg={2} className="p-3 mt-3 d-flex align-items-center flex-column mr-2" style={{background : "#EAEAEA"}} >
                                        <Image fluid className="m-auto" src={images.big_search_icon} />
                                        <button className="d-flex w-100 align-items-center justify-content-center home-program-btn" style={{background : "#EAEAEA"}}>
                                            <div className="d-flex justify-content-center home-see-more-bloc">
                                                <p className="m-auto">View more</p>
                                                <IoIosArrowRoundForward size={30} className="m-auto" />
                                            </div>
                                        </button>
                                </Col>
                            </Mobile>

                    </Row>
                </Col>
            </Row>
            

        </Container>
        {/* AVSEC & 2 carousels */}
        <Container fluid className="mb-4" style={{background: '#F4F6F9'}}>
            <Row className="mb-4">
                <Col xs={12} className="d-flex">
                    <h3 className="contact-title m-auto">AVSEC</h3>
                </Col> 
                
                <Col xs={12} className="d-flex">
                    <Image src={images.center_divider} fluid className="m-auto" />
                </Col>            
            </Row>
            
            {/* TWO SLIDERS ROW */}
            <Desktop>
                <Row className="justify-content-center mb-5">
                    <Col xs={8}>
                        <Row>
                            <Col xs={6}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-blue-text">Training Packages (ASTPs)</p>
                                    </Col>

                                    <Col xs={12}>
                                    <div>
                                        <Slider {...settings}>
                                            {Array.from({length: 5}, (_,i) =>  
                                                <Image src={images.program_train} fluid />
                                            )} 
                                        </Slider>
                                        </div>
                                    </Col>

                                    <Col xs={12} className="mb-3">
                                        <button className="program-blue-btn p-3 w-100">
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Consulter le calendrier</p>
                                                <FiArrowRightCircle size={25} className="my-auto" />
                                            </div>
                                        </button>
                                    </Col>

                                    <Col xs={12}>
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={6}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>Aviation Security Workshops</p>
                                    </Col>

                                    <Col xs={12}>
                                    <div>
                                        <Slider {...settings}>
                                            {Array.from({length: 5}, (_,i) =>  
                                                <Image src={images.program_sec} fluid />
                                            )} 
                                        </Slider>
                                        </div>
                                    </Col>

                                    <Col xs={12} className="mb-3">
                                        <button className="program-blue-btn p-3 w-100">
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Consulter le calendrier</p>
                                                <FiArrowRightCircle size={25} className="my-auto" />
                                            </div>
                                        </button>
                                    </Col>

                                    <Col xs={12}>
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Desktop>

            {/* TWO SLIDERS ROW RESPONSIVE */}
            <Tablet>
                <Row className="justify-content-center mb-5">
                    <Col xs={8}>
                        <Row>
                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-blue-text">Training Packages (ASTPs)</p>
                                    </Col>

                                    <Col xs={12}>
                                    <div>
                                        <Slider {...settings}>
                                            {Array.from({length: 5}, (_,i) =>  
                                                <Image src={images.program_train} fluid />
                                            )} 
                                        </Slider>
                                        </div>
                                    </Col>

                                    <Col xs={12} className="mb-3">
                                        <button className="program-blue-btn p-3 w-100">
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Consulter le calendrier</p>
                                                <FiArrowRightCircle size={25} className="my-auto" />
                                            </div>
                                        </button>
                                    </Col>

                                    <Col xs={12} className="mb-5">
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>Aviation Security Workshops</p>
                                    </Col>

                                    <Col xs={12}>
                                    <div>
                                        <Slider {...settings}>
                                            {Array.from({length: 5}, (_,i) =>  
                                                <Image src={images.program_sec} fluid />
                                            )} 
                                        </Slider>
                                        </div>
                                    </Col>

                                    <Col xs={12} className="mb-3">
                                        <button className="program-blue-btn p-3 w-100">
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Consulter le calendrier</p>
                                                <FiArrowRightCircle size={25} className="my-auto" />
                                            </div>
                                        </button>
                                    </Col>

                                    <Col xs={12}>
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Tablet>
            
            <Mobile>
                <Row className="justify-content-center mb-5">
                    <Col xs={8}>
                        <Row>
                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-blue-text">Training Packages (ASTPs)</p>
                                    </Col>

                                    <Col xs={12}>
                                    <div>
                                        <Slider {...settings}>
                                            {Array.from({length: 5}, (_,i) =>  
                                                <Image src={images.program_train} fluid />
                                            )} 
                                        </Slider>
                                        </div>
                                    </Col>

                                    <Col xs={12} className="mb-3">
                                        <button className="program-blue-btn p-3 w-100">
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Consulter le calendrier</p>
                                                <FiArrowRightCircle size={25} className="my-auto" />
                                            </div>
                                        </button>
                                    </Col>

                                    <Col xs={12} className="mb-5">
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>Aviation Security Workshops</p>
                                    </Col>

                                    <Col xs={12}>
                                    <div>
                                        <Slider {...settings}>
                                            {Array.from({length: 5}, (_,i) =>  
                                                <Image src={images.program_sec} fluid />
                                            )} 
                                        </Slider>
                                        </div>
                                    </Col>

                                    <Col xs={12} className="mb-3">
                                        <button className="program-blue-btn p-3 w-100">
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Consulter le calendrier</p>
                                                <FiArrowRightCircle size={25} className="my-auto" />
                                            </div>
                                        </button>
                                    </Col>

                                    <Col xs={12}>
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Mobile>



            {/*  VIRTUAL & ONLINE */}
            <Desktop>
                <Row className="justify-content-center mb-4">
                    <Col xs={8}>
                        <Row>
                            <Col xs={6}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>VIRTUAL CLASSROOM</p>
                                    </Col>

                                    <Col xs={12}>
                                        <Image src={images.program_classroom} fluid />
                                    </Col>

                                    <Col xs={12} className="mb-3 d-flex">
                                        <div className="d-flex justify-content-center ml-auto course-light-blue-text" style={{ zIndex: 1, marginTop: '-40px'}}>
                                            <p className="my-auto mr-1 course-light-blue-text" style={{fontSize: '16px'}}>Consulter le calendrier</p>
                                            <IoIosArrowRoundForward size={40} className="my-auto" />
                                        </div>
                                    </Col>

                                    <Col xs={12}>
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2',  border: '1px solid #00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={6}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>ONLINE COURSES</p>
                                    </Col>

                                    <Col xs={12}>
                                        <Image src={images.program_online} fluid />                    
                                    </Col>

                                    <Col xs={12} className="mb-3 d-flex">
                                        <div className="d-flex justify-content-center ml-auto course-light-blue-text" style={{ zIndex: 1, marginTop: '-40px'}}>
                                            <p className="my-auto mr-1 course-light-blue-text" style={{fontSize: '16px'}}>Consulter le calendrier</p>
                                            <IoIosArrowRoundForward size={40} className="my-auto" />
                                        </div>
                                    </Col>

                                    <Col xs={12}>
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2', border: '1px solid #00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Desktop>


            <Tablet>
                <Row className="justify-content-center mb-4">
                    <Col xs={8}>
                        <Row>
                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>VIRTUAL CLASSROOM</p>
                                    </Col>

                                    <Col xs={12}>
                                        <Image src={images.program_classroom} fluid />
                                    </Col>

                                    <Col xs={12} className="mb-3 d-flex">
                                        <div className="d-flex justify-content-center ml-auto mr-4 course-light-blue-text" style={{ zIndex: 1, marginTop: '-55px'}}>
                                            <p className="my-auto mr-2 course-light-blue-text" style={{fontSize: '20px'}}>Consulter le calendrier</p>
                                            <IoIosArrowRoundForward size={40} className="my-auto" />
                                        </div>
                                    </Col>

                                    <Col xs={12} className="mb-5">
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2',  border: '1px solid #00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>ONLINE COURSES</p>
                                    </Col>

                                    <Col xs={12}>
                                        <Image src={images.program_online} fluid />                    
                                    </Col>

                                    <Col xs={12} className="mb-3 d-flex">
                                        <div className="d-flex justify-content-center ml-auto mr-4 course-light-blue-text" style={{ zIndex: 1, marginTop: '-55px'}}>
                                            <p className="my-auto mr-2 course-light-blue-text" style={{fontSize: '20px'}}>Consulter le calendrier</p>
                                            <IoIosArrowRoundForward size={40} className="my-auto" />
                                        </div>
                                    </Col>

                                    <Col xs={12}>
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2', border: '1px solid #00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Tablet>

            <Mobile>
                <Row className="justify-content-center mb-4">
                    <Col xs={8}>
                        <Row>
                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>VIRTUAL CLASSROOM</p>
                                    </Col>

                                    <Col xs={12}>
                                        <Image src={images.program_classroom} fluid />
                                    </Col>

                                    <Col xs={12} className="mb-3 d-flex">
                                        <div className="d-flex justify-content-center ml-auto course-light-blue-text" style={{ zIndex: 1, marginTop: '-20px'}}>
                                            <p className="my-auto mr-1 course-light-blue-text" style={{fontSize: '10px'}}>Consulter le calendrier</p>
                                            <IoIosArrowRoundForward size={18} className="my-auto" />
                                        </div>
                                    </Col>

                                    <Col xs={12} className="mb-5">
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2',  border: '1px solid #00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} className="d-flex mb-3">
                                        <p className="m-auto course-light-blue-text" style={{fontSize: '20px'}}>ONLINE COURSES</p>
                                    </Col>

                                    <Col xs={12}>
                                        <Image src={images.program_online} fluid />                    
                                    </Col>

                                    <Col xs={12} className="mb-3 d-flex">
                                    <   div className="d-flex justify-content-center ml-auto course-light-blue-text" style={{ zIndex: 1, marginTop: '-20px'}}>
                                            <p className="my-auto mr-1 course-light-blue-text" style={{fontSize: '10px'}}>Consulter le calendrier</p>
                                            <IoIosArrowRoundForward size={18} className="my-auto" />
                                        </div>
                                    </Col>

                                    <Col xs={12}>
                                        <button className="program-blue-btn p-3 w-100" style={{background: '#00A7E2', border: '1px solid #00A7E2'}}>
                                            <div className="d-flex justify-content-center">
                                                <p className="my-auto mr-2">Voir plus</p>
                                            </div>
                                        </button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Mobile>
        </Container>
        </>
    )
}
