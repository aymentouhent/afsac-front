import React, { useEffect } from 'react';

import {
    Col,
    Row,
    Image,
    Container, Breadcrumb,
} from 'react-bootstrap';

import Layout from '../../components/Layout'

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import { IoIosCheckmark } from 'react-icons/io';

import { connect } from 'react-redux';
import { getAboutUsData, getAccreditations } from '../../redux/actions/AfsacActions';
import { Link } from "react-router-dom";
import { trans } from '../../constants/Translations';

function Accreditation(props) {

    let {
        siteLanguage,
        getAboutUsData,
        aboutUsData,
        accreditations
    } = props;

    useEffect(() => {
        getAccreditations(siteLanguage);
        getAboutUsData();
    }, [])
    // {trans[siteLanguage].accredationsPage.recognitionTitle}
    return (
        <>
            <Container fluid style={{ backgroundColor: '#F7F7F7' }}>

                <Row className="d-flex">
                    <Image src={images.accreditations_banner} alt="afsac-banner" style={{ width: '100%' }} />
                </Row>

                {/* Title */}
                <section>
                    <Row style={{ backgroundColor: '#F7F7F7' }}>

                        <Container>
                            <Col xs={12}  >
                                <div className=" Breadcrumb d-flex mb-3 px-0 " style={{ backgroundColor: '#F7F7F7' }}>
                                    <div className="breadcrumb-item" ><Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>
                                    <div className="breadcrumb-item active" >{trans[siteLanguage].accredationsPage.title1}</div>
                                </div>
                            </Col>
                        </Container>

                        <Col xs={12} className="d-flex flex-column my-4">

                            <h3 className="authorization-title m-auto">{trans[siteLanguage].accredationsPage.title1}</h3>
                            <Image src={images.center_divider} className="mx-auto my-2" alt="afsac-divider" />
                        </Col>

                        <Col xs={11} lg={6} className="m-auto">
                            <p className="authorization-desc mx-auto ">{trans[siteLanguage].accredationsPage.desc1}</p>
                            <p className="authorization-desc mx-auto mb-3">{trans[siteLanguage].accredationsPage.desc2}</p>
                            <p className="authorization-desc mx-auto mt-5">{trans[siteLanguage].accredationsPage.desc3}</p>
                            <p className="authorization-desc mx-auto mt-5">{trans[siteLanguage].accredationsPage.desc4}</p>
                            <p className="authorization-desc mx-auto mt-5">{trans[siteLanguage].accredationsPage.desc5}</p>
                        </Col>
                    </Row>
                </section>

                {/* Recogintions */}
                <section>
                    <Row className="justify-content-center" style={{ backgroundColor: '#F7F7F7' }}>
                        <Col lg={12} className="d-flex my-4">
                            <p className="authorization-desc mx-auto">{trans[siteLanguage].accredationsPage.recognitionTitle}</p>
                        </Col>
                        <Container>
                            <Col lg={12}>
                                <Row className=" mb-5 justify-content-center">
                                    <Col xs={12} md={10} lg={7} style={{ borderLeft: '5px solid orange' }}>
                                        {trans[siteLanguage].accredationsPage.recognitionElements.map((elm, index) =>
                                            <Row key={index} className="d-flex">
                                                <IoIosCheckmark color={"#00A7E2"} size={40} className="my-auto" />
                                                <p className="authorization-recognition-text my-auto">{elm}</p>
                                            </Row>
                                        )}
                                    </Col>
                                </Row>
                            </Col>
                        </Container>
                    </Row>
                </section>

                {/* Security Training */}
                <section className="bg-white">
                    <Row>
                        <Col xs={12} className="d-flex flex-column my-4">
                            <h3 className="authorization-title mt-5" style={{ color: "#00A7E2", fontSize: '26px', }}>{trans[siteLanguage].accredationsPage.securityTitle}</h3>
                            <Image src={images.orange_divider} className="mx-auto my-2" alt="afsac-divider" />
                        </Col>

                        <Container>
                            <Desktop>
                                <Row>
                                    <Col xs={12} lg={6} className="d-flex">
                                        <Image src={images.security_train} className="mr-auto my-auto" fluid alt="afsac-security" />
                                    </Col>

                                    <Col xs={12} lg={6} className="d-flex">
                                        <p className="authorization-desc ml-auto my-auto text-justify px-2" style={{ lineHeight: '2', fontWeight: 'normal', color: "black" }}>
                                            {trans[siteLanguage].accredationsPage.securityDesc}
                                        </p>
                                    </Col>
                                </Row>
                            </Desktop>


                            <Tablet>
                                <Row>
                                    <Col xs={12} lg={6} className="d-flex">
                                        <Image src={images.security_train} className="m-auto" fluid alt="afsac-security" />
                                    </Col>

                                    <Col xs={12} lg={6} className="d-flex">
                                        <p className="authorization-desc m-auto text-justify px-2" style={{ lineHeight: '2', fontWeight: 'normal', color: "black" }}>
                                            {trans[siteLanguage].accredationsPage.securityDesc}
                                        </p>
                                    </Col>
                                </Row>
                            </Tablet>

                            <Mobile>
                                <Row>
                                    <Col xs={12} lg={6} className="d-flex">
                                        <Image src={images.security_train} className="m-auto" fluid alt="afsac-security" />
                                    </Col>

                                    <Col xs={12} lg={6} className="d-flex">
                                        <p className="authorization-desc m-auto text-justify px-2" style={{ lineHeight: '2', fontWeight: 'normal', fontSize: '16px', color: "black" }}>
                                            {trans[siteLanguage].accredationsPage.securityDesc}
                                        </p>
                                    </Col>
                                </Row>
                            </Mobile>
                        </Container>

                    </Row>
                </section>

                {/* Full Membership Program */}
                <section className="bg-white">
                    <Row>
                        <Col xs={12} className="d-flex flex-column my-4">
                            <h3 className="authorization-title mt-5 text-center" style={{ color: "#00A7E2", fontSize: '26px', }}>{trans[siteLanguage].accredationsPage.membershipTitle}</h3>
                            <Image src={images.orange_divider} className="mx-auto my-2" alt="afsac-divider" />
                        </Col>

                        <Container>
                            <Desktop>
                                <Row>
                                    <Col xs={12} lg={6} className="d-flex">
                                        <p className="authorization-desc  m-auto text-justify px-2" style={{ lineHeight: '2', fontWeight: 'normal', color: "black" }}>
                                            {trans[siteLanguage].accredationsPage.membershipDesc}
                                        </p>
                                    </Col>

                                    <Col xs={12} lg={6} className="d-flex">
                                        <Image src={images.full_membership} className="ml-auto my-auto" fluid alt="afsac-membership" />
                                    </Col>
                                </Row>
                            </Desktop>

                            <Tablet>
                                <Row>
                                    <Col xs={12} lg={6} className="d-flex">
                                        <Image src={images.full_membership} className="m-auto" fluid alt="afsac-membership" />
                                    </Col>

                                    <Col xs={12} lg={6} className="d-flex">
                                        <p className="authorization-desc m-auto text-justify m-3 px-2" style={{ lineHeight: '2', fontWeight: 'normal', color: "black" }}>
                                            {trans[siteLanguage].accredationsPage.membershipDesc}
                                        </p>
                                    </Col>

                                </Row>
                            </Tablet>

                            <Mobile>
                                <Row>
                                    <Col xs={12} lg={6} className="d-flex">
                                        <Image src={images.full_membership} className="m-auto" fluid alt="afsac-membership" />
                                    </Col>

                                    <Col xs={12} lg={6} className="d-flex">
                                        <p className="authorization-desc m-auto text-justify m-3 px-2" style={{ lineHeight: '2', fontSize: '16px', fontWeight: 'normal', color: "black" }}>
                                            {trans[siteLanguage].accredationsPage.membershipDesc}
                                        </p>
                                    </Col>
                                </Row>
                            </Mobile>

                        </Container>
                    </Row>
                </section>

                {/* SLOGAN */}
                <section>
                    <Container fluid>
                        <Desktop>
                            <Row
                                className="justify-content-center py-5 mb-5"
                                style={{ backgroundImage: `url(${images.slogan_banner})`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover', height: '600px' }}
                            >
                                <Col lg={10} className="d-flex flex-column" style={{ background: 'rgba(0,86,161,0.5)', height: '440px' }}>
                                    <Row className="justify-content-center">
                                        <Col lg={12} className="d-flex flex-column my-5">
                                            {/*  <p className="mx-auto authorization-slogan">{trans[siteLanguage].accredationsPage.slogan}</p>
                                            <Image src={images.center_divider} className="mx-auto" alt="afsac-divider" />  */}
                                        </Col>

                                        <Col lg={10} className="d-flex flex-column">
                                            <h3 className="mx-auto authorization-big-slogan">{trans[siteLanguage].accredationsPage.sloganDesc1}</h3>
                                            <h3 className="mx-auto authorization-big-slogan">{trans[siteLanguage].accredationsPage.sloganDesc2}</h3>
                                        </Col>

                                        <Col lg={12} className="mb-5" />
                                        <Col lg={12} className="mb-5" />

                                    </Row>
                                </Col>
                            </Row>
                        </Desktop>

                        <Tablet>
                            <Row
                                className="justify-content-center py-5 my-5"
                                style={{ backgroundImage: `url(${images.slogan_banner})`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover', }}
                            >
                                <Col md={11} className="d-flex flex-column" style={{ background: 'rgba(0,86,161,0.5)' }}>
                                    <Row className="justify-content-center">
                                        <Col md={12} className="d-flex flex-column my-5">
                                            <p className="mx-auto authorization-slogan">{trans[siteLanguage].accredationsPage.slogan}</p>
                                            <Image src={images.center_divider} className="mx-auto" alt="afsac-divider" />
                                        </Col>

                                        <Col md={10} className="d-flex flex-column">
                                            <h3 className="mx-auto authorization-big-slogan">{trans[siteLanguage].accredationsPage.sloganDesc1}</h3>
                                            <h3 className="mx-auto authorization-big-slogan">{trans[siteLanguage].accredationsPage.sloganDesc2}</h3>
                                        </Col>

                                        <Col md={12} className="mb-5" />
                                        <Col md={12} className="mb-5" />
                                    </Row>
                                </Col>
                            </Row>
                        </Tablet>

                        <Mobile>
                            <Row
                                className="justify-content-center py-5 my-5"
                                style={{ backgroundImage: `url(${images.slogan_banner_responsive})`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover', }}
                            >
                                <Col xs={11} className="d-flex flex-column" style={{ background: 'rgba(0,86,161,0.5)' }}>
                                    <Row className="justify-content-center">
                                        <Col lg={12} className="d-flex flex-column my-5">
                                            <p className="mx-auto authorization-slogan">{trans[siteLanguage].accredationsPage.slogan}</p>
                                            <Image src={images.center_divider} className="mx-auto" alt="afsac-divider" />
                                        </Col>

                                        <Col lg={10} className="d-flex flex-column">
                                            <h3 className="mx-auto authorization-big-slogan">{trans[siteLanguage].accredationsPage.sloganDesc1}</h3>
                                            <h3 className="mx-auto authorization-big-slogan">{trans[siteLanguage].accredationsPage.sloganDesc2}</h3>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Mobile>
                    </Container>
                </section>

                {/* Vision */}
                <section>
                    <Layout>
                        <Desktop>

                            <Row className="justify-content-center mb-5">
                                <Col lg={8} className="p-0 m-0 authorization-vision-mission-container">
                                    <Row className="d-flex">
                                        <Col lg={4} className="d-flex">
                                            <Image src={images.vision} fluid className="img-vision  m-auto" alt="afsac-vision" />
                                        </Col>

                                        <Col lg={8} className="d-flex flex-column">
                                            <div className="m-auto">
                                                <Image src={images.vision_icon} fluid className="" style={{ maxWidth: '40px', maxHeight: '40px' }} alt="afsac-vision" />
                                                <h4 className="authorization-vision-mission-title mt-2">{trans[siteLanguage].accredationsPage.visonTitle}</h4>
                                                <p className="authorization-recognition-our pr-2">
                                                    {trans[siteLanguage].accredationsPage.visionDesc}
                                                </p>
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Desktop>
                    </Layout>

                    <Container>
                        <Tablet>
                            <Row className="justify-content-center mb-5">
                                <Col md={12} className="p-0 m-0 authorization-vision-mission-container">
                                    <Row className="d-flex">
                                        <Col md={4} className="d-flex">
                                            <Image src={images.vision} fluid className="img-vision m-auto" alt="afsac-vision" />
                                        </Col>

                                        <Col md={8} className="d-flex flex-column ">
                                            <div className="m-auto ">
                                                <Image src={images.vision_icon} fluid className="mb-3" style={{ maxWidth: '30px' }} alt="afsac-vision" />
                                                <h4 className="authorization-vision-mission-title mt-3">{trans[siteLanguage].accredationsPage.visonTitle}</h4>
                                                <p className="authorization-recognition-text pr-2">
                                                    {trans[siteLanguage].accredationsPage.visionDesc}
                                                </p>
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Tablet>

                        <Mobile>
                            <Row className="justify-content-center mb-5">
                                <Col xs={8} className="p-0 m-0 authorization-vision-mission-container">
                                    <Row className="d-flex">
                                        <Col xs={12} className="d-flex">
                                            <Image src={images.vision} fluid className="img-vision m-auto" alt="afsac-vision" />
                                        </Col>

                                        <Col xs={12} className="d-flex flex-column mb-5">
                                            <div className="m-auto pl-2">
                                                <Image src={images.vision_icon} fluid className="mt-4" style={{ maxWidth: '30px' }} alt="afsac-vision" />
                                                <h4 className="authorization-vision-mission-title mb-3">{trans[siteLanguage].accredationsPage.visonTitle}</h4>
                                                <p className="authorization-recognition-text pr-2">
                                                    {trans[siteLanguage].accredationsPage.visionDesc}
                                                </p>
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Mobile>
                    </Container>
                </section>

                {/* Mission */}
                <section>
                    <Layout>
                        <Desktop>
                            <Row className="justify-content-center mb-5">
                                <Col lg={8} className="p-0 m-0 authorization-vision-mission-container">
                                    <Row className="d-flex">

                                        <Col lg={8} className="d-flex flex-column">
                                            <div className="m-auto pl-3">
                                                <Image src={images.mission_icon} fluid className="" style={{ maxWidth: '40px', maxHeight: '40px' }} alt="afsac-mission" />
                                                <h4 className="authorization-vision-mission-title mt-2" style={{ color: '#00A7E2' }} >{trans[siteLanguage].accredationsPage.missionTitle}</h4>
                                                <p className="authorization-recognition-our">
                                                    {trans[siteLanguage].accredationsPage.missionDesc}
                                                </p>
                                            </div>
                                        </Col>

                                        <Col lg={4} className="d-flex">
                                            <Image src={images.mission} fluid className="img-mission m-auto" alt="afsac-mission" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Desktop>
                    </Layout>

                    <Container>
                        <Tablet>
                            <Row className="justify-content-center mb-3">
                                <Col md={12} className="p-0 m-0 authorization-vision-mission-container">
                                    <Row className="d-flex">
                                        <Col md={8} className="d-flex flex-column">
                                            <div className="m-auto pl-3">
                                                <Image src={images.mission_icon} fluid className="" style={{ maxWidth: '30px' }} alt="afsac-mission" />
                                                <h4 className="authorization-vision-mission-title mb-3" style={{ color: '#00A7E2' }}>{trans[siteLanguage].accredationsPage.missionTitle}</h4>
                                                <p className="authorization-recognition-text pr-2">
                                                    {trans[siteLanguage].accredationsPage.missionDesc}
                                                </p>
                                            </div>
                                        </Col>

                                        <Col md={4} className="d-flex">
                                            <Image src={images.mission} fluid className="img-mission m-auto" alt="afsac-mission" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Tablet>

                        <Mobile>
                            <Row className="justify-content-center mb-5">
                                <Col xs={8} className="p-0 m-0 authorization-vision-mission-container">
                                    <Row className="d-flex">
                                        <Col xs={12} className="d-flex">
                                            <Image src={images.mission} fluid className=" img-mission m-auto" alt="afsac-mission" />
                                        </Col>

                                        <Col xs={12} className="d-flex flex-column mb-5">
                                            <div className="m-auto pl-2">
                                                <Image src={images.mission_icon} fluid className="mt-4" style={{ maxWidth: '30px' }} alt="afsac-mission" />
                                                <h4 className="authorization-vision-mission-title mb-3" style={{ color: '#00A7E2' }} >{trans[siteLanguage].accredationsPage.missionTitle}</h4>
                                                <p className="authorization-recognition-text pr-2">
                                                    {trans[siteLanguage].accredationsPage.missionDesc}
                                                </p>
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Mobile>
                    </Container>
                </section>

                {/* References Only Desktop */}
                <section>
                    {aboutUsData && aboutUsData.Reference.length > 0 &&
                        <Container fluid>
                            <Desktop>
                                <Row className="justify-content-center mt-5" style={{ backgroundColor: '#F7F7F7' }}>
                                    <Col xs={12} className="d-flex flex-column my-4">
                                        <h3 className="authorization-title mx-auto text-center" style={{ fontSize: '40px', textTransform: 'uppercase' }} >{trans[siteLanguage].references.referencesTitle}</h3>
                                        <Image src={images.orange_divider} className="mx-auto text-center" alt="afsac-divider" />
                                    </Col>

                                    <Col xs={6} className="d-flex mb-4">
                                        <p className="m-auto text-center authorization-desc">
                                        </p>
                                    </Col>

                                    <Col xs={10} className="mb-4">
                                        <Row className="justify-content-center">
                                            {aboutUsData.Reference.map((ref, i) =>
                                                <Col md={3} key={i} className="d-flex">
                                                    <Image src={ref.image} fluid className="mr-auto mb-2" alt={ref.titre} style={{ border: '2px solid #BCBCBC' }} />
                                                </Col>
                                            )}
                                        </Row>
                                    </Col>
                                </Row>
                            </Desktop>

                            <Tablet>
                                <Row className="justify-content-center" style={{ backgroundColor: '#F7F7F7' }}>
                                    <Col xs={12} className="d-flex flex-column my-4">
                                        <h3 className="authorization-title m-auto">{trans[siteLanguage].references.referencesTitle}</h3>
                                        <Image src={images.orange_divider} className="mx-auto my-2" alt="afsac-divider" />
                                    </Col>

                                    <Col xs={12} className="d-flex mb-4">
                                        <p className="m-auto text-center authorization-desc">
                                        </p>
                                    </Col>

                                    <Col xs={10} className="mb-4">
                                        <Row className="justify-content-center">
                                            {aboutUsData.Reference.map((ref, i) =>
                                                <Col xs={3} md={6} lg={3} key={i} className="d-flex">
                                                    <Image src={ref.image} fluid className="mr-auto mb-2" alt={ref.titre} style={{ border: '2px solid #BCBCBC' }} />
                                                </Col>
                                            )}
                                        </Row>
                                    </Col>

                                </Row>
                            </Tablet>
                        </Container>
                    }
                </section>
            </Container>
        </>
    )
}

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
    aboutUsData: state.afsacR.aboutUsData,
    accreditations: state.afsacR.accreditations,
});

export default connect(mapStateToProps, { getAboutUsData, getAccreditations })(Accreditation);
