import React, { useEffect,useState } from "react";

import {
  Image,
  Row,
  Col,
  Button, Breadcrumb,
} from 'react-bootstrap';
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import { trans } from '../../constants/Translations'; // {trans[siteLanguage].contactPage.requestButton}
import Layout from '../../components/Layout';

import { Link } from 'react-router-dom';
import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';

import { connect } from 'react-redux';
import { getContactData } from '../../redux/actions/AfsacActions';

function Contact(props) {

  let {
    siteLanguage,
    contactData,

    // Functions:
    getContactData,
  } = props;

  const loaderOpts = {
    loop: true,
    autoplay: true, 
    animationData: loader,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  const [pageLoading, setPageLoading] = useState(true);

  useEffect(() => {
    getContactData(siteLanguage)
        .then(() => {
          setTimeout(() => {
            setPageLoading(false)
          },2000);
        });
  },[]);

  return (
      <>
        {pageLoading
            ?
            <div className="preLoader">
              <Lottie options={loaderOpts}
                  height={200}
                  width={200}
                  style={{ position: 'absolute', top : '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
              />
            </div>
            :
            <>
              <Image src={images.contact_ban} style={{ width: "100%", Height: '300px' ,marginBottom: "20px"}} alt="afsac-banner"  />

              {contactData
              &&
              <Layout  >
                {/* Title */}

                <Row className="d-flex">

                  <Col xs={12} className="d-flex flex-column">
                    <div className=" Breadcrumb d-flex mb-3 px-0 ">
                      <div className="breadcrumb-item"> <Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>
                      <div className="breadcrumb-item active" >{trans[siteLanguage].navbar.contact}</div>
                    </div>

                    <h3 className="contact-title m-auto mt-5">{contactData.titre}</h3>
                    <Image src={images.orange_divider} fluid className="mx-auto mt-2" style={{ maxWidth: '200px'}} alt="afsac-divider"  />
                    
                    <p className="mt-3 mx-auto px-5 contact-request-desc text-center" style={{ fontSize: '23px' }}>
                      {trans[siteLanguage].contactPage.contactDesc}
                    </p> 
                  </Col>
                </Row>

                {/* Visit Us */}
                <Row className="mt-5">
                  <Col xs={12} lg={6}>
                    <Desktop>
                      <Image src={images.afsac_building} fluid style={{ maxWidth: '550px', maxHeight: '486px', }} alt="afsac-building"  />
                    </Desktop>

                    <Tablet>
                      <Image src={images.afsac_building} fluid alt="afsac-building"  />
                    </Tablet>

                    <Mobile>
                      <Image src={images.afsac_building} fluid alt="afsac-building"  />
                    </Mobile>
                  </Col>

                  <Col xs={12} lg={6} className="p-0">
                    <Row className="d-flex h-100">
                      <Desktop>
                        <div className="d-flex flex-column py-5 px-5 mr-auto my-auto contact-light-right-section" style={{ width: '100%' }}>
                          <h3>{trans[siteLanguage].contactPage.visitUs}</h3>
                          <p>7 Avenue Taha Hussein Montfleury</p>
                          <p> - 1008 Tunisie</p>
                        </div>
                      </Desktop>

                      <Tablet>
                        <div className="d-flex flex-column py-5 px-5 mx-auto contact-light-right-section">
                          <h3 className="d-flex mx-auto mb-3">{trans[siteLanguage].contactPage.visitUs}</h3>
                          <p className="d-flex m-auto">7 Avenue Taha Hussein Montfleury</p>
                          <p className="d-flex m-auto"> - 1008 Tunisie</p>
                        </div>
                      </Tablet>

                      <Mobile>
                        <div className="d-flex flex-column py-5 px-5  text-center mx-auto contact-light-right-section">
                          <h3 className="d-flex mx-auto mb-3">{trans[siteLanguage].contactPage.visitUs}</h3>
                          <p className="d-flex m-auto">7 Avenue Taha Hussein Montfleury</p>
                          <p className="d-flex m-auto"> - 1008 Tunisie</p>
                        </div>
                      </Mobile>
                    </Row>
                  </Col>
                </Row>

                {/* Call us */}
                <Row className="mt-5">
                  <Desktop>
                    <Col xs={12} lg={6} className="p-0">
                      <Row className="d-flex h-100">
                        <div className="d-flex flex-column py-5 px-5 ml-auto my-auto contact-light-left-section" style={{ width: '387px'}}>
                          <h3>{trans[siteLanguage].contactPage.CallUs}</h3>
                          <p>{contactData.phone1}</p>
                          <p>{contactData.phone2}</p>
                          <p>{contactData.phone3}</p>
                        </div>
                      </Row>
                    </Col>

                    <Col xs={12} lg={6}>
                      <Image src={images.phone} fluid style={{maxWidth: '570px'}} alt="afsac-phone"  />
                    </Col>
                  </Desktop>

                  <Tablet>
                    <Col xs={12} lg={6}>
                      <Image src={images.phone} fluid alt="afsac-phone"  />
                    </Col>

                    <Col xs={12} lg={6} className="p-0">
                      <Row className="d-flex h-100">
                        <div className="d-flex flex-column py-5 px-5 mx-auto contact-light-right-section">
                          <h3 className="d-flex mx-auto mb-3">{trans[siteLanguage].contactPage.CallUs}</h3>
                          <p className="d-flex mx-auto">{contactData.phone1}</p>
                          <p className="d-flex mx-auto">{contactData.phone2}</p>
                          <p className="d-flex mx-auto">{contactData.phone3}</p>
                        </div>
                      </Row>
                    </Col>
                  </Tablet>

                  <Mobile>
                    <Col xs={12} lg={6}>
                      <Image src={images.phone} fluid alt="afsac-phone" />
                    </Col>

                    <Col xs={12} lg={6} className="p-0">
                      <Row className="d-flex h-100">
                        <div className="d-flex flex-column py-5 px-5 mx-auto contact-light-right-section">
                          <h3 className="d-flex mx-auto mb-3">{trans[siteLanguage].contactPage.CallUs}</h3>
                          <p className="d-flex mx-auto">{contactData.phone1}</p>
                          <p className="d-flex mx-auto">{contactData.phone2}</p>
                          <p className="d-flex mx-auto">{contactData.phone3}</p>
                        </div>
                      </Row>
                    </Col>
                  </Mobile>

                </Row>

                {/* Email Us */}
                <Row className="mt-5">
                  <Col xs={12} lg={6}>
                    <Desktop>
                      <Image src={images.laptop} fluid style={{ maxWidth: '570px' }} alt="afsac-laptop" />
                    </Desktop>

                    <Tablet>
                      <Image src={images.laptop} fluid alt="afsac-laptop" />
                    </Tablet>

                    <Mobile>
                      <Image src={images.laptop} fluid alt="afsac-laptop" />
                    </Mobile>
                  </Col>

                  <Col xs={12} lg={6} className="p-0">
                    <Row className="d-flex h-100">
                      <Desktop>
                        <div className="d-flex flex-column py-5 px-5 mr-auto my-auto contact-light-right-section" style={{ width: '454px', marginLeft: '' }}>
                          <h3>{trans[siteLanguage].contactPage.EmailUs}</h3>
                          <p>{contactData.email}</p>
                        </div>
                      </Desktop>

                      <Tablet>
                        <div className="d-flex flex-column py-5 px-5 mx-auto contact-light-right-section">
                          <h3 className="d-flex mx-auto mb-3">{trans[siteLanguage].contactPage.EmailUs}</h3>
                          <p className="d-flex m-auto">{contactData.email}</p>
                        </div>
                      </Tablet>

                      <Mobile>
                        <div className="d-flex flex-column py-5 px-5  text-center mx-auto contact-light-right-section">
                          <h3 className="d-flex mx-auto mb-3">{trans[siteLanguage].contactPage.EmailUs}</h3>
                          <p className="d-flex m-auto">{contactData.email}</p>
                        </div>
                      </Mobile>
                    </Row>
                  </Col>
                </Row>

                {/* Social Media */}
                <Row className="mt-5">
                  <Desktop>
                    <Col xs={12} lg={6} className="p-0">
                      <Row className="d-flex h-100">
                        <div className="d-flex flex-column py-5 px-5 ml-auto my-auto contact-light-left-section" style={{ width : '387px' }}>
                          <h3>{trans[siteLanguage].contactPage.SocialMedia}</h3>
                          <a href={contactData.facebook} target='_blank'><p>Facebook</p></a>
                          <a href={contactData.linkedin} target='_blank'><p>Linkedin</p></a>
                          <a href={contactData.youtube} target='_blank'><p>Youtube</p></a>
                        </div>
                      </Row>
                    </Col>

                    <Col xs={12} lg={6}>
                      <Image src={images.tablet} fluid alt="afsac-tablet" />
                    </Col>
                  </Desktop>

                  <Tablet>
                    <Col xs={12} lg={6}>
                      <Image src={images.tablet} fluid alt="afsac-tablet" />
                    </Col>

                    <Col xs={12} lg={6} className="p-0">
                      <Row className="d-flex h-100">
                        <div className="d-flex flex-column py-5 px-5 mx-auto contact-light-right-section">
                          <h3 className="d-flex mx-auto mb-3">{trans[siteLanguage].contactPage.SocialMedia}</h3>
                          <a href={contactData.facebook} target='_blank'><p className="d-flex mx-auto">Facebook</p></a>
                          <a href={contactData.linkedin} target='_blank'><p className="d-flex mx-auto">Linkedin</p></a>
                          <a href={contactData.youtube} target='_blank'><p className="d-flex mx-auto">Youtube</p></a>
                        </div>
                      </Row>
                    </Col>
                  </Tablet>

                  <Mobile>
                    <Col xs={12} lg={6}>
                      <Image src={images.tablet} fluid alt="afsac-tablet" />
                    </Col>

                    <Col xs={12} lg={6} className="p-0">
                      <Row className="d-flex h-100">
                        <div className="d-flex flex-column py-5 px-5 mx-auto contact-light-right-section">
                          <h3 className="d-flex mx-auto mb-3">{trans[siteLanguage].contactPage.SocialMedia}</h3>
                          <a href={contactData.facebook} target='_blank'><p className="d-flex mx-auto">Facebook</p></a>
                          <a href={contactData.linkedin} target='_blank'><p className="d-flex mx-auto">Linkedin</p></a>
                          <a href={contactData.youtube} target='_blank'><p className="d-flex mx-auto">Youtube</p></a>
                        </div>
                      </Row>
                    </Col>
                  </Mobile>
                </Row>

                <Row className="d-flex mt-5 text-center">
                  <Col xs={12} className="d-flex flex-column m-auto">
                    <p className="mx-auto contact-request-desc" style={{ margin:0, fontSize: '26px'}}>{trans[siteLanguage].contactPage.requestDesc}</p>
                    <p className="mx-auto mb-3 contact-request-desc" style={{ fontSize: '26px'}}>{trans[siteLanguage].contactPage.requestDesc1}</p>
                    <Link 
                      to={{ pathname : "/", state : { scrollTo: true },  hash: '#contact-us'}} 
                      id="contact-form-btn" 
                      className="mx-auto mb-5 text-center" 
                      style={{ width: "34%", height: '70px'}}
                      >
                      {trans[siteLanguage].contactPage.requestButton}
                      </Link>
                  </Col>
                </Row>
              </Layout>
              }
            </>

        }


      </>
  );
};

const mapStateToProps = state => ({
  siteLanguage : state.afsacR.siteLanguage,
  contactData : state.afsacR.contactData,
});

export default connect(mapStateToProps,{ getContactData })(Contact);
