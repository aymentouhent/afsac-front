import React, { useState, useEffect } from 'react';

import {
    Container,
    Row,
    Col,
    Image,
    Form,
    InputGroup,
    Button,
    Modal
} from 'react-bootstrap';
import { Link, useParams, useHistory } from "react-router-dom";

import { connect } from 'react-redux';
import { sendRegistration } from '../../redux/actions/AfsacActions';

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import { trans } from '../../constants/Translations';

import CheckBox from "../../components/CheckBox";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import ReactFlagsSelect from 'react-flags-select';
import PhoneInput from 'react-phone-input-2';
import { IoIosArrowBack, IoIosClose } from 'react-icons/io';



function Registration(props) {

    let { title, plat, id } = useParams();

    let {
        siteLanguage,

        // Functions:
        sendRegistration,
    } = props;

    const history = useHistory();

    const [titrePerso, setTitrePerso] = useState('Mr');
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [job, setJob] = useState('');
    const [org, setOrg] = useState('');
    const [typeOrg, setTypeOrg] = useState('');
    const [supervisor, setSupervisor] = useState('');
    const [supervisorJob, setSupervisorJob] = useState('');
    const [supervisorEmail, setSupervisorEmail] = useState('');
    const [supervisorPhone, setSupervisorPhone] = useState('');
    const [adress, setAdress] = useState('');
    const [ville, setVille] = useState('');
    const [etat, setEtat] = useState('');
    const [postalCode, setPostalCode] = useState('');
    const [pays, setPays] = useState("TN");
    const [phone, setPhone] = useState('');
    const [phone2, setPhone2] = useState('');
    const [facturation, setFacturation] = useState('');
    const [adresseFacturation, setAdresseFacturation] = useState('');
    const [accept, setAccept] = useState(false);
    const [showPrivacyModal, setShowPrivacyModal] = useState(false);

    const popup = (msg) => Swal.fire({ position: 'center', icon: 'error', title: msg, showConfirmButton: false, timer: 2000 });

    const convertToSlug = (title) => {
        return title
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-');
    };

    const convertToSlugTo = (title) => {
        var titleNew = title.toUpperCase()
            .replace(/-/g, ' ');

        return setTitleFormation(titleNew);
    };
    useEffect(() => {

        convertToSlugTo(title)
        /* .then(() => {
            setTitleFormation(title);
        }); */
    }, []);
    const [titleFormation, setTitleFormation] = useState('');

    const isValidEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };

    const resetForm = () => {
        setTitrePerso('Mr')
        setName('');
        setLastName('');
        setEmail('');
        setJob('');
        setOrg('');
        setTypeOrg('');
        setSupervisor('');
        setSupervisorJob('');
        setSupervisorEmail('');
        setSupervisorPhone('');
        setAdress('');
        setVille('');
        setEtat('');
        setPostalCode('');
        setPays('');
        setPhone('');
        setPhone2('');
        setFacturation('');
        setAdresseFacturation('');
        setAccept(false);
    };

    const validateMiracleForm = () => {

        if (name.length < 3) { popup(trans[siteLanguage].registration.firstNameErr) }
        else if (lastName.length < 3) { popup(trans[siteLanguage].registration.lastNameErr) }
        else if (!isValidEmail(email)) { popup(trans[siteLanguage].registration.emailErr) }
        else if (job.length < 2) { popup(trans[siteLanguage].registration.jobTitleErr) }
        else if (org.length < 2) { popup(trans[siteLanguage].registration.organizationErr) }
        else if (typeOrg.length === 0) { popup(trans[siteLanguage].registration.organizationTypeErr) }
        else if (supervisor.length < 2) { popup(trans[siteLanguage].registration.supervisorNameErr) }
        else if (supervisorJob.length < 2) { popup(trans[siteLanguage].registration.supervisorJobErr) }
        else if (!isValidEmail(supervisorEmail)) { popup(trans[siteLanguage].registration.supervisorEmailErr) }
        else if (supervisorPhone.length < 8) { popup(trans[siteLanguage].registration.supervisorPhoneErr) }
        else if (adress.length < 2) { popup(trans[siteLanguage].registration.addressErr) }
        else if (ville.length < 2) { popup(trans[siteLanguage].registration.cityErr) }
        else if (etat.length < 2) { popup(trans[siteLanguage].registration.stateErr) }
        else if (postalCode.length < 4) { popup(trans[siteLanguage].registration.postalCodeErr) }
        else if (pays.length < 1) { popup(trans[siteLanguage].registration.countryErr) }
        else if (phone.length < 8) { popup(trans[siteLanguage].registration.phoneErr) }
        else if (facturation.length < 2) { popup(trans[siteLanguage].registration.invoicingNameErr) }
        else if (adresseFacturation.length < 2) { popup(trans[siteLanguage].registration.invoicingAdresseErr) }
        else {

            // Accepted agreements:
            if (accept) {
                let objectToSend = {
                    titre_personnel: titrePerso,
                    nom: name,
                    prenom: lastName,
                    email: email,
                    profession: job,
                    organisation: org,
                    type_organisation: typeOrg,
                    nom_du_superviseur: supervisor,
                    profession_du_superviseur: supervisorJob,
                    email_du_superviseur: supervisorEmail,
                    telephone_du_superviseur: supervisorPhone,
                    adresse: adress,
                    ville: ville,
                    etat: etat,
                    code_postal: postalCode,
                    pays: pays,
                    telephone: phone,
                    second_telephone: phone2,
                    facturation: facturation,
                    adresse_de_facturation: adresseFacturation,
                    titre_formation: title,
                    id_formation: id,
                    type_formation: plat,
                };

                sendRegistration(objectToSend)
                    .then(response => {
                        console.log("sendRegistration form result ==>", response.data);

                        if (response.data) {
                            resetForm();
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: trans[siteLanguage].registration.formSuccess,
                                showConfirmButton: false,
                                timer: 3000
                            });
                            history.push(`/solo-avsec/${plat}/${id}/${convertToSlug(title)}`);
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: trans[siteLanguage].registration.formError1,
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }

                    })
                    .catch(error => {
                        console.log("sendRegistration form Error ==>", error);

                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: trans[siteLanguage].registration.formConnexionError,
                            showConfirmButton: false,
                            timer: 2000
                        });
                    });
            }
            else {
                popup(trans[siteLanguage].registration.checkboxErr);
            }
        }

    };

    const renderPolicyModal = () => (
        <Modal show={showPrivacyModal} size="lg" centered>
            <Modal.Body>
                <Col xs={12}>
                    <Row>
                        <Col xs={12} className="d-flex">
                            {
                                plat === "AVSEC"
                                    ?
                                    trans[siteLanguage].registration.modalTitleAvsec
                                    :
                                    trans[siteLanguage].registration.modalTitleTpp
                            }
                            <span className="ml-auto" onClick={() => { setShowPrivacyModal(false); setAccept(false); }}><IoIosClose color={"black"} size={30} style={{ cursor: 'pointer' }} /> </span>
                        </Col>

                        <Col xs={12} className="">
                            {
                                plat === "AVSEC"
                                    ?
                                    trans[siteLanguage].registration.modalContentAvsec
                                    :
                                    trans[siteLanguage].registration.modalContentTpp
                            }
                        </Col>

                        <Col xs={12} className="d-flex my-4">
                            <div className="ml-auto registration-modal-btn d-flex px-4" onClick={() => { setShowPrivacyModal(false); setAccept(true); }}>
                                <p className="m-auto">{trans[siteLanguage].registration.modalBtn}</p>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Modal.Body>
        </Modal>
    );

    return (
        <>
            {/* Privacy policy modal */}
            {showPrivacyModal && renderPolicyModal()}

            {/* Image Banner */}
            <Container fluid>
                <Row className="d-flex">
                    <Image src={images.courses_banner} fluid style={{ height: '300px' }} alt="afsac-banner" />
                </Row>
            </Container>

            <Container>
                {/* Title */}
                <div className="Breadcrumb d-flex mb-3 px-0 ">
                    <div className="breadcrumb-item"><Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>
                    <div className="breadcrumb-item"><Link to={'#'}>{trans[siteLanguage].registration.breadCrumbTitle}</Link></div>
                    <div className="breadcrumb-item active">
                        <Link to={plat === "trainAirPlus" ? `/solo-tpp/${plat}/${id}/${convertToSlug(title)}` : `/solo-avsec/${plat}/${id}/${convertToSlug(title)}`}>
                            {title}
                        </Link>
                    </div>
                </div>
                <Row className="my-4">
                    <Col lg={12} className="d-flex pl-0 mb-3">
                        <Link to={plat === "trainAirPlus" ? `/solo-tpp/${plat}/${id}/${convertToSlug(title)}` : `/solo-avsec/${plat}/${id}/${convertToSlug(title)}`} className="d-flex">
                            <IoIosArrowBack size={18} color={"#707070"} className="my-auto" style={{ cursor: 'pointer', fontWeight: 'bold' }} />
                            <h6 className="registration-small-gray-text my-auto">{trans[siteLanguage].registration.backButton}</h6>
                        </Link>
                    </Col>

                    <Col lg={12} className="d-flex flex-column">
                        <h3 className="contact-title mr-auto" style={{ fontSize: '36px' }}>{trans[siteLanguage].registration.registrationTitle.toUpperCase()}: {titleFormation}</h3>
                    </Col>

                    <Col lg={12}>
                        <hr style={{ height: '2px', borderColor: '#BCBCBC' }} />
                    </Col>
                </Row>
                <Container>
                    <Form style={{ width: '100%' }}>
                        <Row lg={12} className="d-flex flex-column my-4">
                            <h3 className="contact-title mr-auto" style={{ fontSize: '28px' }} >{trans[siteLanguage].registration.personalInfo}</h3>
                        </Row>

                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={2} md={6} >
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.personalTitle}</Form.Label>
                                        <Form.Control as="select" defaultValue={titrePerso} className="mr-sm-2" id="registration-input" onChange={(e) => setTitrePerso(e.target.value)} custom>
                                            <option value="Mr">{trans[siteLanguage].registration.mrValue}</option>
                                            <option value="Mrs">{trans[siteLanguage].registration.mrsValue}</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                            </Form.Row>
                        </Row>

                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group>
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.firstName}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setName(e.target.value)} /* placeholder={trans[siteLanguage].registration.firstNamePlaceholder}*/ />
                                    </Form.Group>
                                </Col>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.lastName}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setLastName(e.target.value)} /* placeholder={trans[siteLanguage].registration.lastNamePlaceholder}*/ />
                                    </Form.Group >
                                </Col>
                            </Form.Row>

                        </Row>

                        <Row className="d-flex flex-column mt-1 ">
                            <Form.Row>
                                <Col lg={6} md={12} >
                                    <Form.Group  >
                                        <Form.Label className="registration-input-label mr-auto" >{trans[siteLanguage].registration.email}</Form.Label>
                                        <Form.Control type="email" id="registration-input" onChange={(e) => setEmail(e.target.value)} /* placeholder={trans[siteLanguage].registration.emailPlaceholder}*/ />
                                        <Form.Text className="text-muted">
                                            {trans[siteLanguage].registration.emailCaption}
                                        </Form.Text>
                                    </Form.Group>
                                </Col>
                            </Form.Row>
                        </Row>
                    </Form>

                </Container>

                <Col lg={12}>
                    <hr style={{ height: '2px', borderColor: '#BCBCBC' }} />
                </Col>

                <Container>
                    <Form style={{ width: '100%' }}>
                        <Row lg={12} className="d-flex flex-column my-4">
                            <h3 className="contact-title mr-auto" style={{ fontSize: '28px' }} > {trans[siteLanguage].registration.EmploymentInfo}</h3>
                        </Row>


                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group  >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.jobTitle}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setJob(e.target.value)} />
                                    </Form.Group>
                                </Col>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.organization}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setOrg(e.target.value)} />
                                    </Form.Group >
                                </Col>
                            </Form.Row>

                        </Row>
                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} >
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.organizationType}</Form.Label>

                                        <Form.Control className="mr-sm-2" id="registration-input" onChange={(e) => setTypeOrg(e.target.value)} />
                                        {/* 
                                    <Form.Control as="select" className="mr-sm-2" id="registration-input" onChange={(e) => setTypeOrg(e.target.value)}  custom>
                                        <option value=""></option>
                                        {trans[siteLanguage].registration.organizationsOptions.map((org,index) => 
                                            <option key={index} value={org}>{org}</option>
                                        )}
                                    </Form.Control> 
                                    */}
                                    </Form.Group>

                                </Col>
                            </Form.Row>
                        </Row>

                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group  >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.supervisorName}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setSupervisor(e.target.value)} />
                                    </Form.Group>
                                </Col>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.supervisorJob}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setSupervisorJob(e.target.value)} />
                                    </Form.Group >
                                </Col>
                            </Form.Row>

                        </Row>
                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group  >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.supervisorEmail}</Form.Label>
                                        <Form.Control type="email" id="registration-input" onChange={(e) => setSupervisorEmail(e.target.value)} />
                                    </Form.Group>
                                </Col>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.supervisorPhone}</Form.Label>
                                        {/* <Form.Control type="tel" maxLength={8} id="registration-input" onChange={(e) => setSupervisorPhone(e.target.value)} /> */}
                                        <PhoneInput country={'tn'} value={phone} onChange={phone => setSupervisorPhone(phone)} placeholder="" containerStyle={{ height: '50px' }} inputClass={"registration-phone-input"} />
                                    </Form.Group >
                                </Col>
                            </Form.Row>

                        </Row>
                    </Form>

                </Container>


                <Col lg={12}>
                    <hr style={{ height: '2px', borderColor: '#BCBCBC' }} />
                </Col>
                <Container>
                    <Form style={{ width: '100%' }}>
                        <Row lg={12} className="d-flex flex-column my-4">
                            <h3 className="contact-title mr-auto" style={{ fontSize: '28px' }} >{trans[siteLanguage].registration.contactInfo}</h3>
                        </Row>

                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} >
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.address}</Form.Label>

                                        <Form.Control as="textarea" rows={3} id="registration-input" onChange={(e) => setAdress(e.target.value)} />
                                    </Form.Group>

                                </Col>
                            </Form.Row>
                        </Row>
                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group  >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.city}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setVille(e.target.value)} />
                                    </Form.Group>
                                </Col>
                                <Col lg={3} md={6} xs={12}>
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.state}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setEtat(e.target.value)} />
                                    </Form.Group >
                                </Col>
                                <Col lg={3} md={6} xs={12}>
                                    <Form.Group  >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.postalCode}</Form.Label>
                                        <Form.Control type="text" /* maxLength="4" */ id="registration-input" onChange={(e) => setPostalCode(e.target.value)} />
                                    </Form.Group>
                                </Col>
                            </Form.Row>

                        </Row>

                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group>
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.country}</Form.Label>
                                        {/* <Form.Control type="text" className="mr-sm-2" id="registration-input" onChange={(e) => setPays(e.target.value)}  /> */}
                                        <ReactFlagsSelect onSelect={(code) => setPays(code)} defaultCountry="TN" className="registration-menu-flags" />
                                    </Form.Group>
                                </Col>
                            </Form.Row>
                        </Row>

                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group  >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.phone}</Form.Label>

                                        <InputGroup className="mb-2">
                                            <PhoneInput country={'tn'} value={phone} onChange={phone => setPhone(phone)} placeholder="" containerStyle={{ height: '50px' }} inputClass={"registration-phone-input"} />
                                        </InputGroup>
                                    </Form.Group>
                                </Col>
                                <Col lg={6} md={6} xs={12}>
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.secondPhone}</Form.Label>
                                        <InputGroup className="mb-2" >
                                            <PhoneInput country={'tn'} value={phone2} onChange={phone => setPhone2(phone)} placeholder="" containerStyle={{ height: '50px' }} inputClass={"registration-phone-input"} />
                                        </InputGroup>
                                    </Form.Group >
                                </Col>
                            </Form.Row>

                        </Row>
                    </Form>

                </Container>
                <Col lg={12}>
                    <hr style={{ height: '2px', borderColor: '#BCBCBC' }} />
                </Col>
                <Container>
                    <Form style={{ width: '100%' }}>
                        <Row lg={12} className="d-flex flex-column my-4">
                            <h3 className="contact-title mr-auto" style={{ fontSize: '28px' }} >{trans[siteLanguage].registration.invoicingInfo}</h3>
                        </Row>

                        <Row lg={12} className="d-flex flex-column my-4">
                            <Form.Row>
                                <Col lg={6} md={6} >
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.invoicingName}</Form.Label>
                                        <Form.Control type="text" id="registration-input" onChange={(e) => setFacturation(e.target.value)} />
                                    </Form.Group>
                                </Col>

                                <Col lg={6} md={6} >
                                    <Form.Group >
                                        <Form.Label className="registration-input-label mr-auto">{trans[siteLanguage].registration.invoicingAdresse}</Form.Label>
                                        <Form.Control as="textarea" rows={3} id="registration-input" onChange={(e) => setAdresseFacturation(e.target.value)} />
                                    </Form.Group>
                                </Col>
                            </Form.Row>
                        </Row>
                    </Form>
                </Container>

                <Col lg={12}>
                    <hr style={{ height: '2px', borderColor: '#BCBCBC' }} />
                </Col>

                <Desktop>
                    <Container>

                        <Row lg={12} xs={12} className="mt-5 justify-content-lg-center " >
                            <Form.Row controlId="check-register" id="check-register">
                                <div onClick={() => { setAccept(prev => prev ? !accept : setShowPrivacyModal(true)); }}>
                                    <CheckBox className="check-register" label={trans[siteLanguage].registration.checkboxLabel} checked={accept} style={{ cursor: 'pointer' }} />
                                </div>
                            </Form.Row>
                        </Row>

                        <Row lg={12} className="m-5 justify-content-lg-center ">
                            <Col lg={6} xs={12}>
                                <Button onClick={() => validateMiracleForm()} className="register-btn" size="lg" block style={{ height: '70px', background: '#0056A1', borderRadius: '0', color: 'white', fontSize: '22px', fontWeight: 'bold' }}>
                                    {trans[siteLanguage].registration.buttonSubmit}
                                </Button>
                            </Col>
                        </Row>

                    </Container>
                </Desktop>

                <Tablet>
                    <Container>
                        <Row lg={12} xs={12} className="mt-5 justify-content-center" >
                            <Form.Row controlId="check-register" id="check-register">
                                <div onClick={() => { setAccept(prev => prev ? !accept : setShowPrivacyModal(true)); }}>
                                    <CheckBox className="check-register" label={trans[siteLanguage].registration.checkboxLabel} checked={accept} />
                                </div>
                            </Form.Row>
                        </Row>
                        <Row lg={12} className="m-5 justify-content-center ">

                            <Col md={8} >
                                <Button className="register-btn" size="lg" block style={{ height: '70px', background: '#0056A1', borderRadius: '0', color: 'white', fontSize: '18px', fontWeight: 'bold' }}>
                                    {trans[siteLanguage].registration.buttonSubmit}
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                </Tablet>

                <Mobile>
                    <Row lg={12} xs={12} className="mt-5 justify-content-center" >
                        <Form.Row controlId="check-register" id="check-register">
                            <div onClick={() => { setAccept(prev => prev ? !accept : setShowPrivacyModal(true)); }}>
                                <CheckBox className="check-register" label={trans[siteLanguage].registration.checkboxLabel} checked={accept} />
                            </div>
                        </Form.Row>
                    </Row>

                    <Row lg={12} className="mt-4 mb-5 justify-content-center">
                        <Col sm={12} >
                            <Button className="register-btn" size="lg" block style={{ height: '70px', background: '#0056A1', borderRadius: '0', color: 'white', fontSize: '18px', fontWeight: 'bold' }} >
                                {trans[siteLanguage].registration.buttonSubmit}
                            </Button>
                        </Col>
                    </Row>
                </Mobile>
            </Container>
        </>
    );
};

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
});

export default connect(mapStateToProps, { sendRegistration })(Registration);

