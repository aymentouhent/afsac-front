import React, { useEffect, useState } from 'react'
import { Card, Carousel, Row, Col, Image, Container, Button } from 'react-bootstrap';
import Slider from 'react-slick';

import Layout from '../../components/Layout';

import SessionBox from '../../components/Home/SessionBox';

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import { Link, useHistory } from 'react-router-dom';

import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';

import { connect } from 'react-redux';
import { getHomeData, getUpcommingCourses } from '../../redux/actions/AfsacActions';
import { IoIosArrowRoundForward } from 'react-icons/io';

import { trans } from '../../constants/Translations';


function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <Image
            src={images.next_slider}
            className={className}
            style={{ ...style, display: "block", height: '70px', width: '70px', marginRight: '-50px', }}
            onClick={onClick}
            alt="afsac-arrow"

        />
    );
};

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <Image
            src={images.prev_slider}
            className={className}
            style={{ ...style, display: 'block', height: '70px', width: '70px', marginLeft: '-50px', filter: 'grayscale(1)', }}
            onClick={onClick}
            alt="afsac-arrow"
        />
    );
};


function Home(props) {

    let {
        siteLanguage,
        homeData,
        sessionsTppCoursesData,

        // Functions:
        getHomeData,
        getUpcommingCourses,
    } = props;

    const history = useHistory();

    const loaderOpts = {
        loop: true,
        autoplay: true,
        animationData: loader,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    const settingsSessions = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        dotsClass: "slider-pagination",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    const PartnersSlider = {
        dots: false,
        infinite: true,
        //speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        autoplay: true,
        speed: 500,
        autoplaySpeed: 3000,
        cssEase: "linear",
        // nextArrow: <SampleNextArrow />,
        // prevArrow: <SamplePrevArrow />,
        dotsClass: "slider-pagination",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    const [pageLoading, setPageLoading] = useState(true);

    const isValidHttpUrl = (string) => {
        let url;

        try {
            url = new URL(string);
        } catch (_) {
            return false;
        }

        return url.protocol === "http:" || url.protocol === "https:";
    }


    useEffect(() => {
        getHomeData(siteLanguage)
            .then(() => {
                getUpcommingCourses(siteLanguage);
                setPageLoading(false);
            });
    }, []);

    return (
        <>
            {pageLoading
                ?
                <div className="preLoader">
                    <Lottie options={loaderOpts}
                        height={200}
                        width={200}
                        style={{ position: 'absolute', top: '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
                    />
                </div>
                :
                <>
                    {homeData && homeData.carousel.length > 0 &&
                        <Carousel>
                            {homeData.carousel.map((carousel, index) =>
                                <Carousel.Item>
                                    <Desktop>
                                        <div key={index} className="home-carousel-image" style={{ backgroundImage: `url(${carousel.image})` }} />
                                    </Desktop>

                                    <Tablet>
                                        <Image
                                            className="d-block w-100"
                                            src={carousel.image}
                                            alt={carousel.titre}
                                        />
                                    </Tablet>

                                    <Mobile>
                                        <Image
                                            className="d-block w-100"
                                            src={carousel.image}
                                            alt={carousel.titre}
                                            style={{ height: "240px" }}
                                        />
                                    </Mobile>

                                    <Carousel.Caption style={{ bottom: '21% !important' }}>
                                        {carousel.titre &&
                                            <h3 className="carousel-title">{carousel.titre}</h3>
                                        }
                                        {carousel.button_titre && isValidHttpUrl(carousel.button_link)
                                            &&
                                            <span className="home-blue-btn mt-5" style={{ cursor: 'pointer' }} onClick={() => window.location.href = carousel.button_link}>{carousel.button_titre}</span>
                                        }
                                    </Carousel.Caption>
                                </Carousel.Item>
                            )}
                        </Carousel>
                    }

                    {/* Second Carousel */}
                    <Row style={{ background: "#F4F6F9" }} className="justify-content-center text-center px-4">
                        <Col xs={10} md={10} lg={6} xl={6}>

                            <div>
                                <p
                                    className="text-center mb-4 text-justify px-1 session-desc-slogon mt-1"
                                    style={{color: "gray"}}
                                >
                                    {trans[siteLanguage].homePage.sloganHome}
                                </p>
                                {/* <p  className="slider-caption">- ICAO</p> */}
                            </div>

                        </Col>
                    </Row>

                    {/* Mot du DG */}
                    <Layout>
                        <Row className="my-5">
                            <Col xs={12}>
                                <Row>
                                    <Col xs={12} lg={6}>
                                        <Image src={images.about_1} fluid alt="afsac-about" />
                                    </Col>

                                    <Col xs={12} lg={6}>
                                        <Row>
                                            <Col xs={12} className="mb-3">
                                                <p className="contact-title my-0">{trans[siteLanguage].aboutUsPage.title1}</p>
                                                <Image src={images.icon_divider} alt="afsac-divider" />
                                            </Col>

                                            <Desktop>
                                                <Col xs={12} className="mb-3" style={{ maxHeight: '190px', overflowY: 'scroll' }}>
                                                    {trans[siteLanguage].aboutUsPage.motDG}
                                                </Col>
                                            </Desktop>

                                            <Tablet>
                                                <Col xs={12} className="mb-3">
                                                    {trans[siteLanguage].aboutUsPage.motDG}
                                                </Col>
                                            </Tablet>

                                            <Mobile>
                                                <Col xs={12} className="mb-3">
                                                    {trans[siteLanguage].aboutUsPage.motDG}
                                                </Col>
                                            </Mobile>

                                            <Col xs={12}>
                                                <Link to={'/our-accreditations-and-recognitions'}>
                                                    <Button id='contact-form-btn' >{trans[siteLanguage].navbar.accredations}</Button>
                                                </Link>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>

                        {/* <Row className="my-5">
                            <Col xs={12} lg={4} className="d-flex">
                                <Desktop> <Image src={images.blue_logo} className="mr-auto" fluid style={{maxWidth: '374px', maxHeight: '374px'}} alt={"afsac-logo"} /> </Desktop>
                                <Tablet> <Image src={images.blue_logo} className="m-auto" fluid alt={"afsac-logo"} /> </Tablet>
                                <Mobile> <Image src={images.blue_logo} className="m-auto" fluid alt={"afsac-logo"} /> </Mobile>
                            </Col>
                            <Col xs={12} lg={8} className="d-flex">
                                <p className="my-auto ml-auto afsac-presentation-text">
                                    {trans[siteLanguage].homePage.afsacPresentation}
                                </p>
                            </Col>
                        </Row> */}
                    </Layout>

                    {/* Afsac Categories */}
                    {homeData && homeData.TrainairCategorys &&
                        <Layout>
                            <Row className="mt-3 mb-5" >
                                <Col lg={8}>
                                    <h4 className="home-blue-title">{trans[siteLanguage].homePage.programTitle}</h4>
                                    <Image src={images.orange_divider} style={{ maxWidth: '200px' }} alt={"afsac-divider"} />

                                    <Row className="px-0">
                                        {homeData.TrainairCategorys.map((train, index) =>
                                            // index < 8 &&
                                            <Col xs={10} lg={4} key={index} className="mt-3">
                                                <Desktop>
                                                    <Link to={{ pathname: '/training-tpp', state: { category_id: train.id } }}>
                                                        <Card>
                                                            <div className="home-program-image mr-auto" style={{ backgroundImage: `url(${train.image})` }} />

                                                            <div className="d-flex home-program-btn mr-auto text-white" style={{ background: train.color, width: '100%' }}>
                                                                <span className="m-auto">{train.nom}</span>
                                                            </div>
                                                        </Card>
                                                    </Link>
                                                </Desktop>

                                                <Tablet>
                                                    <Link to={{ pathname: '/training-tpp', state: { category_id: train.id } }}>
                                                        <Card>
                                                            <Card.Img variant="top" src={`${train.image}`} fluid />
                                                            <button className="d-flex align-items-center justify-content-center w-100 text-white home-program-btn" style={{ background: train.color }}>
                                                                {train.nom}
                                                            </button>
                                                        </Card>
                                                    </Link>
                                                </Tablet>

                                                <Mobile>
                                                    <Link to={{ pathname: '/training-tpp', state: { category_id: train.id } }}>
                                                        <Card>
                                                            <Card.Img variant="top" src={`${train.image}`} fluid />
                                                            <button className="d-flex align-items-center justify-content-center w-100 home-program-btn text-white" style={{ background: train.color, color: 'white' }}>
                                                                {train.nom}
                                                            </button>
                                                        </Card>
                                                    </Link>
                                                </Mobile>
                                            </Col>
                                        )}
                                    </Row>
                                </Col>

                                <Col lg={4}>
                                    <h4 className="home-blue-title mt-5 mt-md-2 mt-lg-0 mt-xl-0 ">{trans[siteLanguage].homePage.securityTitle}</h4>
                                    <Image src={images.orange_divider} style={{ maxWidth: '200px' }} alt={"afsac-divider"} />

                                    <Row className="px-0">
                                        <Col lg={12} className="d-flex flex-column">
                                            <Desktop>
                                                <Row>
                                                    <Col lg={12}>
                                                        {/* <Link to={{pathname: '/training-avsec', state: { category_id: 8 } }}> */}
                                                        <div className="home-program-security-image mr-auto mt-3" style={{ backgroundImage: `url(${images.security_img})`, height: '673px' }} />
                                                        {/* </Link> */}

                                                        <Link to={"/training-avsec"} className="d-flex home-see-more-bloc">
                                                            <Image src={images.big_search_icon} style={{ height: '80.26px', width: '80.26px' }} className="my-auto ml-3" alt={"afsac-search"} />
                                                            <div to={"/training-avsec"} className="my-auto ml-5 d-flex">
                                                                <p className="my-auto">{trans[siteLanguage].homePage.securityButton}</p>
                                                                <IoIosArrowRoundForward size={35} className="my-auto" />
                                                            </div>
                                                        </Link>
                                                    </Col>
                                                </Row>
                                            </Desktop>
                                        </Col>
                                    </Row>

                                    <Col lg={12} className="p-0">
                                        <Tablet>
                                            <Col className="p-0 d-flex mb-auto mt-3">
                                                <Image src={images.security_img} fluid className="mx-auto" alt={"afsac-trainairplus"} />
                                            </Col>

                                            <Col className="mx-auto" />

                                            <Col onClick={() => history.push('/training-avsec')} className="px-0 py-2 d-flex flex-row " style={{ backgroundColor: '#EAEAEA', cursor: 'pointer' }}>

                                                <div className="d-flex my-auto ml-5">
                                                    <Image src={images.big_search_icon} className="m-auto" alt={"afsac-search"} />
                                                </div>
                                                <div className="d-flex my-auto m-auto home-see-more-bloc">
                                                    <p className="m-auto">{trans[siteLanguage].homePage.securityButton}</p>
                                                    <IoIosArrowRoundForward size={35} className="m-auto" />
                                                </div>

                                            </Col>
                                        </Tablet>

                                        <Mobile>
                                            <Col className="p-0 d-flex mb-auto mt-3">
                                                <Image src={images.security_img} fluid className="m-auto" alt={"afsac-trainairplus"} />
                                            </Col>

                                            <Col onClick={() => history.push('/training-avsec')} className="px-0 py-2 d-flex flex-row mt-3" style={{ backgroundColor: '#EAEAEA' }}>
                                                <div className="d-flex my-auto ml-5">
                                                    <Image src={images.big_search_icon} style={{ width: '50px' }} className="m-auto" alt="afsac-search" />
                                                </div>
                                                <div className="d-flex my-auto m-auto home-see-more-bloc">
                                                    <p className="m-auto">{trans[siteLanguage].homePage.securityButton}</p>
                                                    <IoIosArrowRoundForward size={35} className="my-auto ml-3" />
                                                </div>
                                            </Col>
                                        </Mobile>

                                    </Col>
                                </Col>
                            </Row>
                        </Layout>
                    }
                    {/* Sessions */}
                    {sessionsTppCoursesData && sessionsTppCoursesData.length > 0 &&
                        <Layout className="Sessions">
                            <div className="text-center mb-4 mt-2">
                                <h3 className="home-blue-title">{trans[siteLanguage].navbar.sessions}</h3>
                                <Image src={images.center_divider} alt="afsac-divider" />
                            </div>
                            <Container>
                                <p className="text-center mb-4 text-justify px-1 session-desc-home" id='contact-form-label' style={{ fontSize: "25px" }}>{trans[siteLanguage].navbar.sessionsDesc}</p>
                            </Container>

                            <div className="mb-5">
                                <Slider {...settingsSessions}>
                                    {sessionsTppCoursesData.map((course, i) =>
                                        <SessionBox
                                            key={i}
                                            id={course.id}
                                            title={course.titre}
                                            image={course.image}
                                            image_detail={course.image_detail}
                                            categoryId={course.id_categorie}
                                            categories={homeData.TrainairCategorys}
                                            platform={"trainAirPlus"}
                                            date={course.hasOwnProperty('date') ? course.date : null}
                                        />
                                    )}
                                </Slider>
                            </div>
                        </Layout>
                    }

                    {/* Partners */}
                    {homeData && homeData.Partner.length > 0 &&
                        <div style={{ background: "#F4F6F9" }}>
                            <Layout>
                                <div className="text-center mt-4">
                                    <h3 className="home-blue-title">{trans[siteLanguage].partners.title}</h3>
                                    <Image src={images.center_divider} alt="afsac-divider" />
                                </div>
                                <br />
                                <Slider {...PartnersSlider}>
                                    {homeData.Partner.map((partner, index) =>
                                        <div key={index} className="row d-flex w-100 py-3">
                                            <Image src={partner.image} className="m-auto" style={{ width: '90%', border: '2px solid #BCBCBC' }} alt={partner.titre} />
                                        </div>
                                    )}
                                </Slider>
                            </Layout>
                        </div>
                    }
                </>
            }
        </>
    );
}

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
    homeData: state.afsacR.homeData,
    sessionsTppCoursesData: state.afsacR.sessionsTppCoursesData,
});

export default connect(mapStateToProps, { getHomeData, getUpcommingCourses })(Home);