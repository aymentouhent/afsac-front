import React, { useState, useEffect, useRef } from 'react';

import {
    Container,
    Row,
    Col,
    Image,
    Form,
    Button
} from 'react-bootstrap';
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import { trans } from '../../constants/Translations';
import { useLocation, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUpcommingSessionsData, sendBrochureToUser } from '../../redux/actions/AfsacActions';
import TppCourse from './TppCourse';
import Swal from 'sweetalert2/dist/sweetalert2.js';


function TppCourses(props) {

    let {
        siteLanguage,
        bannerTitle1,
        bannerTitle2,
        bannerDesc,
        courseTitle,
        courseTitleDesc,
        courses,
        categories,
        platform,

        sendBrochureToUser,
    } = props;

    const location = useLocation();
    const searchBoxRef = useRef();

    // const [seeMore,setSeeMore] = useState(5);
    // const [catalogueEmail, setCatalogueEmail] = useState('');

    const [searchEnabled, setSearchEnabled] = useState(false);
    const [filteredCourses, setFiltredCourses] = useState([]);
    const [filtredCategories, setFiltredCategories] = useState([]);
    const [categorySelected, setCategorySelected] = useState('');
    const [courseKeyword, setCourseKeyword] = useState('');

    const pickCategoryColor = (cat_id) => {
        let found = categories.find((cat) => cat.id === cat_id)
        console.log("eeee" + found)
        if (found) {
            return found.color;
        } else {
            return false;
        }
    };

    const handleCategorySelected = (e) => {
        setCategorySelected(e.target.value);

        if (e.target.value === "all") {
            setFiltredCourses([]);
            setSearchEnabled(false);
            // setSeeMore(5);
        } else {
            setCourseKeyword("");
            let filteredCourses = courses.filter(course => course.id_categorie == e.target.value && course.titre !== null);
            // console.log("Result tpp's =>", filteredCourses);

            setFiltredCourses(filteredCourses);
            setSearchEnabled(true);
            // setSeeMore(5);
        }
    };

    const handleCategorySelectedFromHome = (id) => {
        setCategorySelected(id);
        let filteredCourses = courses.filter(course => course.id_categorie == id && course.titre !== null);
        // console.log("Result tpp's from home =>", filteredCourses);

        setFiltredCourses(filteredCourses);
        searchBoxRef.current.scrollIntoView();
    };

    const handleSearchKeyword = (e) => {
        setCategorySelected('all');
        setFiltredCourses([]);
        setCourseKeyword(e.target.value);

        if (e.target.value === "") {
            setFiltredCourses([]);
            setSearchEnabled(false);
        } else {
            // console.log("Let search by", e.target.value);
            let filtered = [];
            courses.map(course => {
                if (course.titre) {
                    let courseLowercase = (course.titre).toLowerCase();
                    let searchCourseLowercase = e.target.value.toLowerCase();
                    if (courseLowercase.indexOf(searchCourseLowercase) > -1) {
                        filtered.push(course);
                    };
                }
            });
            setFiltredCourses(filtered);
            setSearchEnabled(true);
        }
    };

    const pickCategoryPhoto = (cat_id) => {
        let found = categories.find((cat) => cat.id === cat_id)

        if (found) {
            return found.image;
        } else {
            return '';
        }
    };

    useEffect(() => {
        if (location.state) {
            handleCategorySelectedFromHome(location.state.category_id);
        }
    }, []);

    // Filtrage categories enlévé par nesrine :
    // useEffect(() => {
    //     if(courses) {
    //         let filtredCategories = [];

    //         categories.map(category => {
    //             for(let i=0; i < courses.length; i++) {
    //                 if(courses[i].id_categorie === category.id) {
    //                     filtredCategories.push(category);
    //                     break;
    //                 } 
    //             };
    //         });
    //         // console.log("Filtrd !", filtredCategories);
    //         setFiltredCategories(filtredCategories);
    //     }
    //     return () => {

    //     };
    // },[courses]);

    return (
        <>
            {/* Image Banner */}
            <div className="banner" >
                <Container>
                    <p className="session-banner-title">{bannerTitle1}</p>
                    <p className="session-banner-title">{bannerTitle2}</p>
                    <p className="session-banner-title" style={{ fontSize: '16px', marginTop: '2rem', fontWeight: 'normal' }}>
                        {bannerDesc}
                    </p>
                </Container>
            </div>

            <Container>
                {/* Title */}
                <div className=" Breadcrumb d-flex mb-3 px-0 ">
                    <div className="breadcrumb-item"> <Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>

                    <div className="breadcrumb-item active" >{courseTitle}</div>
                </div>
                <Row className="my-4">
                    <Col xs={12} className="d-flex flex-column">
                        <h3 className="contact-title mr-auto" style={{ fontSize: '36px' }}>{courseTitle}</h3>
                        <Image src={images.orange_divider} className="mr-auto" alt="afsac-divider" />
                    </Col>
                    <Col xs={12} lg={8} md={8} className="d-flex flex-column">
                        <p className="mt-4 mr-auto contact-desc">{courseTitleDesc}</p>
                    </Col>
                </Row>
                <hr />
                {/* Search Box */}
                <Row className="my-4  mx-0 search-box-courses" ref={searchBoxRef}>
                    <Col lg={12} className="mb-3 mt-2">
                        <Row>
                            {filtredCategories.length > 0
                                ?
                                <Col xs={12} md={6} lg={4}>
                                    <p className="search-input-label">{trans[siteLanguage].coursesSearchBox.categoriesLabel}</p>
                                    <Form.Group controlId="exampleForm.SelectCustom">
                                        <Form.Control as="select" value={categorySelected} onChange={handleCategorySelected} id="search-input-courses" custom style={{ height: '50px' }}>
                                            <option value={"all"}>{trans[siteLanguage].coursesSearchBox.categoriesPlaceholder}</option>
                                            {filtredCategories.map((cat, i) =>
                                                <option key={i} value={`${cat.id}`}>{cat.nom}</option>
                                            )}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                :
                                categories
                                &&
                                <Col xs={12} md={6} lg={4}>
                                    <p className="search-input-label">{trans[siteLanguage].coursesSearchBox.categoriesLabel}</p>
                                    <Form.Group controlId="exampleForm.SelectCustom">
                                        <Form.Control as="select" value={categorySelected} onChange={handleCategorySelected} id="search-input-courses" custom style={{ height: '50px' }}>
                                            <option value={"all"}>{trans[siteLanguage].coursesSearchBox.categoriesPlaceholder}</option>
                                            {categories.map((cat, i) =>
                                                <option key={i} value={`${cat.id}`}>{cat.nom}</option>
                                            )}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                            }

                            <Col xs={12} md={6} lg={3}>
                                <p className="search-input-label">{trans[siteLanguage].coursesSearchBox.keywordLabel}</p>
                                <Form.Control type={"text"} id="search-input-courses" className="p-2 w-100" placeholder={trans[siteLanguage].coursesSearchBox.keywordPlaceholder} style={{ height: '50px' }} value={courseKeyword} onChange={handleSearchKeyword} />
                            </Col>

                            <Col xs={12} md={6} lg={3} className="">
                                <p style={{ color: '#F4F4F4' }}>cl</p>
                                <button className="m-auto p-2 w-100 search-courses-btn" style={{ height: '50px' }}>{trans[siteLanguage].coursesSearchBox.searchButton}</button>
                            </Col>

                            <Col xs={12} md={6} lg={2}>
                                <p style={{ color: '#F4F4F4' }}>cl</p>
                                <p className="mr-auto my-auto search-courses-clear-btn py-2">{trans[siteLanguage].coursesSearchBox.clearButton}</p>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                {
                    filteredCourses.length === 0 && searchEnabled
                        ?
                        <>
                            <Row className="my-4">
                                <Col xs={12} className="d-flex">
                                    <h3 className="contact-title m-auto " style={{ fontSize: '20px' }}>{trans[siteLanguage].coursesSearchBox.noResultsText}</h3>
                                </Col>
                            </Row>
                            <div style={{ height: '150px' }} />
                        </>
                        // <p>NO RESULTS</p>
                        :
                        filteredCourses.length > 0
                            ?
                            filteredCourses.map((course, i) =>
                                // i < seeMore &&
                                course.titre !== null
                                &&
                                <TppCourse
                                    key={i}
                                    border={pickCategoryColor(course.id_categorie)}
                                    id={course.id}
                                    title={course.titre}
                                    description={course.description}
                                    video={course.video}
                                    // category={course.categorie}
                                    // image={course.image}
                                    image={pickCategoryPhoto(course.id_categorie)}
                                    duration={course.duree}
                                    delivery={course.livraison}
                                    price={course.prix}
                                    language={course.langue}
                                    objectif={course.objectif}
                                    inscription={course.inscription}
                                    population={course.population}
                                    place={course.place}
                                    entry={course.entry}
                                    image_detail={course.image_detail}
                                    platform={platform}
                                    date={course.hasOwnProperty('date') ? course.date : null}
                                />
                            )
                            :
                            courses.length > 0 &&
                            courses.map((course, i) =>
                                // i < seeMore &&
                                course.titre !== null
                                &&
                                <TppCourse
                                    key={i}
                                    border={pickCategoryColor(course.id_categorie)}
                                    id={course.id}
                                    title={course.titre}
                                    description={course.description}
                                    video={course.video}
                                    // category={course.categorie}
                                    // image={course.image}
                                    image={pickCategoryPhoto(course.id_categorie)}
                                    duration={course.duree}
                                    delivery={course.livraison}
                                    price={course.prix}
                                    language={course.langue}
                                    objectif={course.objectif}
                                    inscription={course.inscription}
                                    population={course.population}
                                    place={course.place}
                                    entry={course.entry}
                                    image_detail={course.image_detail}
                                    platform={platform}
                                    date={course.hasOwnProperty('date') ? course.date : null}
                                />
                            )}

                {/* <Row>
            <Col xs={12} className="d-flex mt-5 mb-4">
                <p className="m-auto course-light-blue-text" style={{ textDecoration: 'underline', cursor: 'pointer',fontSize:'20px' }} onClick={() => setSeeMore(prevState => prevState+=5)}>{trans[siteLanguage].courseCard.seeMoreButton}</p>
            </Col>
        </Row> */}

            </Container>
        </>
    )
};

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
});

export default connect(mapStateToProps, { getUpcommingSessionsData, sendBrochureToUser })(TppCourses);
