import React, { useEffect, useState } from 'react';


import {
    Container,
    Row,
    Col,
    Image,
    Button,
} from 'react-bootstrap';
import { PDFDownloadLink } from '@react-pdf/renderer';
import useHideOnScrolled from "../useHideOnScrolled";
import TppPdf from '../Tpp/TppPdf';

import { Link, useParams } from 'react-router-dom';
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';

import { connect } from 'react-redux';
import { getSoloTppCourse, getTppCategories, getBrochure } from '../../redux/actions/AfsacActions'; 

import { trans } from '../../constants/Translations';

function SoloTpp(props) {
    
    let { plat,id } = useParams();
    
    let { 
        siteLanguage,
        SoloTppCourseData,
        SoloTppCourseSimilarData,
        TppCategories,
        
        // Functions:
        getSoloTppCourse,
        getTppCategories,
    } = props;

    const loaderOpts = {
        loop: true,
        autoplay: true, 
        animationData: loader,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
    };

    const hidden = useHideOnScrolled();
    const [pageLoading, setPageLoading] = useState(true);

    const convertToSlug = (title) => {
        return title
        .replace(/\//g,'')
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
    };

    const pickDeliveryByLang = (del) => {

        if(del == "virtual classroom") {
           return trans[siteLanguage].courseCard.virtualClassroom
        // } else if (del.toLowerCase() == "classroom" ) {
        } else {
            return trans[siteLanguage].courseCard.classroom
        }
    }

    useEffect(() => {
        getTppCategories(siteLanguage)
        .then(() =>
        {
            getSoloTppCourse(plat,id,siteLanguage)
            .then(() =>
            {
                //getSoloTppCourse(plat,id,siteLanguage)
                setPageLoading(false);
            })
        });
    },[]);

    const pickCategoryName = (cat_id) => {
        let found = TppCategories.find((cat) => cat.id === cat_id)
     
        if(found) {
            return found.nom
        } else {
            return '';
        }
    } 

    const pickCategoryColor = (cat_id) => {
        let found = TppCategories.find((cat) => cat.id === cat_id)
     
        if(found) {
            return found.color;
        } else {
            return '';
        }
    }; 

    const pickCategoryPhoto = (cat_id) => {
        let found = TppCategories.find((cat) => cat.id === cat_id)
     
        if(found) {
            return found.image;
        } else {
            return '';
        }
    }; 

    return (
        <>
        {pageLoading
        ?
        <div className="preLoader">
            <Lottie options={loaderOpts}
                height={200}
                width={200}
                style={{ position: 'absolute', top : '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
            />
        </div>
        :
        <Container fluid>

        <Row className="d-flex">
            <Image src={images.solo_plane} fluid style={{ height: '300px'}} alt="afsac-plane" />
        </Row>
        
        <Container>
            <div className=" Breadcrumb d-flex mb-3 px-0 ">
                <div className="breadcrumb-item" ><Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>
                {/* <div className="breadcrumb-item" ><Link to={'/sessions-avsec'}> TPP courses  </Link></div> */}
                <div className="breadcrumb-item active" > {SoloTppCourseData.titre}</div>
            </div>
        </Container>

        <Container>
            <Desktop>
                <Row className="mt-5">
                    <Col lg={4}>
                        <Row>
                            <Col lg={12} className="d-flex mb-3">
                                <Link to={`/registration/${convertToSlug(SoloTppCourseData.titre)}/${plat}/${id}`} className="w-100">
                                    {SoloTppCourseData.date 
                                    ?
                                    <Button id="register-now-btn-orange" className="w-100 py-3">
                                        {trans[siteLanguage].soloPage.subscribeButton}
                                    </Button>
                                    :
                                    <Button id="register-now-btn-orange" className="w-100 py-3">
                                        {trans[siteLanguage].soloPage.trainingButton}
                                    </Button>
                                    }
                                </Link>
                            </Col>

                            <Col lg={12} className="d-flex mb-3">
                                {SoloTppCourseData && siteLanguage
                                &&
                                <span id="register-now-btn-white" className="w-100 py-3 text-center">
                                    <PDFDownloadLink document={<TppPdf data={SoloTppCourseData} siteLanguage={siteLanguage} categoryName={pickCategoryName(SoloTppCourseData.id_categorie)} categoryColor={pickCategoryColor(SoloTppCourseData.id_categorie)} />} fileName={`${SoloTppCourseData.titre}.pdf`} style={{color: "#F47820", padding:'20px'}}>
                                            {trans[siteLanguage].soloPage.brochureButton}
                                    </PDFDownloadLink>
                                </span>
                                }
                            </Col>
                        </Row>

                        <Row>
                            <Col lg={12} className="d-flex">
                                <div className="course-big-image m-auto" style={{ backgroundImage: `url(${pickCategoryPhoto(SoloTppCourseData.id_categorie)})` }} />
                            </Col>
                        </Row>
                    </Col>

                    <Col lg={1} className="d-flex p-0">
                        <div className="my-auto solo-session-orange-rectangle" style={{ backgroundColor: pickCategoryColor(SoloTppCourseData.id_categorie) }} /> 
                    </Col>
                    
                    {/* RIGHT BLOC */}
                    <Col lg={7} id="solo">
                        <Row className="mb-3">
                            <Col lg={12} className="p-0">
                                <p className="course-blue-text" style={{fontSize:'34px'}}>{SoloTppCourseData.titre} </p>
                            </Col>
                        </Row>

                        <Row className="my-4">
                            <Col lg={12} className="p-0">
                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.category}</p>
                            </Col>
                        </Row>

                        <Row className="my-4">
                            <Col lg={12} className="p-0 d-flex">
                                <div className="mr-auto d-flex">
                                    <div className="solo-session-circle-orange mr-3" style={{ backgroundColor: pickCategoryColor(SoloTppCourseData.id_categorie) }} />
                                    <p className="course-light-blue-text-solo my-auto" style={{ fontSize: '18px',fontWeight:'600', color:  pickCategoryColor(SoloTppCourseData.id_categorie)}}>{pickCategoryName(SoloTppCourseData.id_categorie)}</p>
                                </div>
                            </Col>
                        </Row>


                        {SoloTppCourseData.objectif.length > 0
                        &&
                        <>
                        <Row className="my-4">
                            <Col lg={12} className="p-0">
                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.objectives}</p>
                            </Col>
                        </Row>

                        <Row  className="mt-4 mb-5">
                            <Col lg={12} className="p-0">
                                <p className="course-small-gray-text my-auto" style={{ textAlign: 'justify', color: 'black' }}>{SoloTppCourseData.objectif[0]}</p>
                                <ul>
                                    {SoloTppCourseData.objectif.map((obj,i) => 
                                        i > 0
                                        &&
                                        <li className="course-small-gray-text" style={{ textAlign: 'justify', color: 'black' }}>{obj}</li>
                                    )}
                                </ul>
                            </Col>
                        </Row>
                        </>
                        }

                        {SoloTppCourseData.description.length > 0
                        &&
                        <Row className="mt-3 mb-5">
                            <Col lg={12} className="mb-3 p-0">
                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.content}</p>
                            </Col>

                            <Col lg={12} className="p-0">    
                                {/* <ul> */}
                                    {SoloTppCourseData.description.map((desc,i) => 
                                        // <li key={i} className="course-small-gray-text my-auto" style={{ textAlign: 'justify', color: 'black' }}>{desc}</li>
                                        <p className="course-small-gray-text my-auto" style={{ textAlign: 'justify', color: 'black' }}>{desc}</p>
                                    )}
                                {/* </ul> */}
                            </Col>
                        </Row>
                        }


                        {SoloTppCourseData.population 
                        && 
                        <Row className="mt-3 mb-5">
                            <Col lg={12} className="mb-3 p-0">
                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.population}</p>
                            </Col>

                            <Col lg={12} className="p-0">
                                <p className="course-small-gray-text my-auto" style={{ textAlign: 'justify', color: 'black' }}>{SoloTppCourseData.population}</p>
                            </Col>
                        </Row>
                        }
                        
                        <Row className="d-flex justify-content-between my-3">
                            <Col lg={12} className="p-0">
                                <Row className="justify-content-between">

                                    {SoloTppCourseData.duree &&
                                    <Col lg={3}>
                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.duration}</p>
                                                <p className="course-small-gray-text" style={{ color: 'black' }}>{SoloTppCourseData.duree}</p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    }

                                    {SoloTppCourseData.prix &&
                                    <Col lg={3}>
                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.price}</p>
                                                <p className="course-small-gray-text" style={{ color: 'black' }}>{SoloTppCourseData.prix}</p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    }

                                    {SoloTppCourseData.langue &&
                                    <Col lg={3}>
                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.language}</p>
                                                <p className="course-small-gray-text" style={{ color: 'black' }}>{SoloTppCourseData.langue}</p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    }
                                    
                                    {SoloTppCourseData.livraison &&
                                    <Col lg={3}>
                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.delivery}</p>
                                                <p className="course-small-gray-text" style={{ color: 'black' }}>{pickDeliveryByLang(SoloTppCourseData.livraison)}</p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    }
                                </Row>
                            </Col>
                        </Row>

                    </Col>
                </Row>

                {/* FORMATIONS SIMILAIRES */}
                <Row className="mt-5 mb-5">
                    <Col lg={12}>
                        <Row >
                        {SoloTppCourseSimilarData && SoloTppCourseSimilarData.length > 0 &&
                        SoloTppCourseSimilarData.map((similar,i) =>
                            <Col lg={6} key={i} className="px-4">
                                <Row style={{background: '#F4F4F4',width:'100%',padding:'1rem'}}>
                                <Col lg={4} className=" p-2">
                                        <a href={`/solo-tpp/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                            <Image src={similar.image}  style={{width:'160px'}} alt={similar.titre && similar.titre}  />
                                        </a>
                                    </Col>

                                    <Col lg={8} className="p-2">
                                        <Row className="mb-2 ml-3">
                                            <Col lg={12} className="d-flex">
                                                <a href={`/solo-tpp/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                    <p className="course-blue-text" style={{cursor: 'pointer',fontSize:'23px'}}>
                                                        {similar.titre}
                                                    </p>
                                                </a>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-gray-text ml-4 mt-3">
                                                    {similar.description.length > 0 && similar.description[0].substring(0,52)}...
                                                </p>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        )}
                        </Row>
                    </Col>
                </Row>
            </Desktop>

            <Tablet>
                <Row className="mt-5">
                    <Col md={4}>
                        <Row>
                            <Col md={12} className="d-flex mb-3">
                                <Link to={`/registration/${convertToSlug(SoloTppCourseData.titre)}/${plat}/${id}`} className="w-100">
                                    {SoloTppCourseData.date 
                                    ?
                                    <Button id="register-now-btn-orange" className="w-100 py-3">
                                        {trans[siteLanguage].soloPage.subscribeButton}
                                    </Button>
                                    :
                                    <Button id="register-now-btn-orange" className="w-100 py-3">
                                        {trans[siteLanguage].soloPage.trainingButton}
                                    </Button>
                                    }
                                </Link>
                            </Col>

                            <Col md={12} className="d-flex mb-3">
                                {SoloTppCourseData && siteLanguage
                                &&
                                <span id="register-now-btn-white" className="w-100 py-3 text-center">
                                    <PDFDownloadLink document={<TppPdf data={SoloTppCourseData} siteLanguage={siteLanguage} categoryName={pickCategoryName(SoloTppCourseData.id_categorie)} categoryColor={pickCategoryColor(SoloTppCourseData.id_categorie)} />} fileName={`${SoloTppCourseData.titre}.pdf`} style={{color: "#F47820", padding:'20px'}}>
                                        {trans[siteLanguage].soloPage.brochureButton}
                                    </PDFDownloadLink>
                                </span>
                                }
                            </Col>
                        </Row>

                        <Row>
                            <Col md={12} className="d-flex">
                                {/* <Image src={images.solo_session_img} fluid className="m-auto" /> */}
                                <div className="course-big-image m-auto" style={{ backgroundImage: `url(${pickCategoryPhoto(SoloTppCourseData.id_categorie)})` }} />
                            </Col>
                        </Row>
                    </Col>

                    <Col md={7} id="solo">
                        <Row className="mb-3">
                            <Col md={12}>
                                <p className="course-blue-text">{SoloTppCourseData.titre}</p>
                            </Col>
                        </Row>

                        <Row className="my-4">
                            <Col md={12}>
                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.category}</p>
                            </Col>
                        </Row>

                        <Row className="my-4">
                            <Col md={2}>
                                <div className="solo-session-circle-orange" style={{backgroundColor: pickCategoryColor(SoloTppCourseData.id_categorie)}} />
                            </Col>

                            <Col md={10} className="d-flex px-0">
                                <p className="course-light-blue-text my-auto" style={{ fontSize: '15px', color: pickCategoryColor(SoloTppCourseData.id_categorie)}}>
                                    {pickCategoryName(SoloTppCourseData.id_categorie)}
                                </p>
                            </Col>
                        </Row>

                        {SoloTppCourseData.objectif.length > 0
                        &&
                        <>
                        <Row className="my-4">
                            <Col md={12} className="mb-2">
                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.objectives}</p>
                            </Col>
                            
                            <Col md={12}>
                                <ul>
                                    {SoloTppCourseData.objectif.map((obj,i) => 
                                        <li className="course-small-gray-text" key={i}>{obj}</li>
                                    )}
                                </ul>
                            </Col>
                        </Row>
                        </>
                        }
                       
                       {SoloTppCourseData.description.length > 0
                        &&
                        <Row className="mt-3 mb-5">
                            <Col md={12} className="mb-2">
                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.content}</p>
                            </Col>

                            <Col lg={12}>  
                                <ul>
                                    {SoloTppCourseData.description.map((desc,i) => 
                                        <li key={i} className="course-small-gray-text">{desc}</li>
                                    )}
                                </ul>                          
                            </Col>
                        </Row>
                        }


                        {SoloTppCourseData.population 
                        && 
                        <Row className="mt-3 mb-5">
                            <Col md={10} className="mb-3">
                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.population}</p>
                            </Col>

                            <Col md={10}>
                                <p className="course-small-gray-text my-auto">{SoloTppCourseData.population}</p>
                            </Col>
                        </Row>
                        }

                        <Row className="d-flex justify-content-between my-3">
                        <Col md={12}>
                            <Row className="justify-content-between">

                                {SoloTppCourseData.duree &&
                                <Col md={3}>
                                    <Row>
                                        <Col md={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.duration}</p>
                                            <p className="course-small-gray-text">{SoloTppCourseData.duree}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                {SoloTppCourseData.livraison &&
                                <Col md={3}>
                                    <Row>
                                        <Col md={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.delivery}</p>
                                            <p className="course-small-gray-text">{pickDeliveryByLang(SoloTppCourseData.livraison)}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                {SoloTppCourseData.prix &&
                                <Col md={3}>
                                    <Row>
                                        <Col md={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.price}</p>
                                            <p className="course-small-gray-text">{SoloTppCourseData.prix}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }

                                {SoloTppCourseData.langue &&
                                <Col md={3}>
                                    <Row>
                                        <Col md={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.language}</p>
                                            <p className="course-small-gray-text">{SoloTppCourseData.langue}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                

                            </Row>
                        </Col>
                        </Row>
                    </Col>
                </Row>
                
                {/* FORMATIONS SIMILAR */}
                <Row className="mt-5 mb-5">
                    <Col md={12}>
                        <Row >
                            {SoloTppCourseSimilarData && SoloTppCourseSimilarData.length > 0 &&
                            SoloTppCourseSimilarData.map((similar,i) => 
                                <Col md={6} key={i} className="px-4">
                                    <Row style={{background: '#F4F4F4',}}>
                                        <Col md={4} className="d-flex p-3">
                                            <a href={`/solo-tpp/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                <Image src={similar.image} alt={similar.titre && similar.titre} className="m-auto" style={{maxHeight: '82px', maxWidth: '82px'}} />
                                            </a>
                                        </Col>

                                        <Col md={8} className="p-3">
                                            <Row className="mb-2">
                                                <Col md={12} className="d-flex">
                                                    <a href={`/solo-tpp/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                        <p className="course-blue-text" style={{cursor: 'pointer'}}>
                                                            {similar.titre.length > 30
                                                            ?
                                                            similar.titre.substring(0,30) + "..."
                                                            :
                                                            similar.titre
                                                            }
                                                            
                                                        </p>
                                                    </a>
                                                </Col>
                                            </Row>
                            
                                            <Row>
                                                <Col md={10}>
                                                    <p className="course-small-gray-text" style={{fontSize:'13px'}}>
                                                        {similar.description.length > 0 && similar.description[0].substring(0,25)}...
                                                    </p>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            )}
                        </Row>
                    </Col>
                </Row>
            </Tablet>
        
            <Mobile>
                <div id="solo">
                    <Row className="mb-2">
                        <Col xs={12} className="d-flex">
                            <p className="course-blue-text mr-auto">{SoloTppCourseData.titre}</p>
                        </Col>
                    </Row>

                    <Row className="mb-2">
                        <Col xs={12} className="d-flex">
                            <p className="course-light-blue-text mr-auto">{trans[siteLanguage].soloPage.category}</p>
                        </Col>
                    </Row>

                    <Row className="mb-4">
                        <Col xs={2}>
                            <div className="solo-session-circle-orange" style={{backgroundColor: pickCategoryColor(SoloTppCourseData.id_categorie)}} />
                        </Col>

                        <Col xs={10} className="d-flex px-0">
                            <p className="course-light-blue-text my-auto" style={{ fontSize: '14px', color: pickCategoryColor(SoloTppCourseData.id_categorie)}}>
                                {pickCategoryName(SoloTppCourseData.id_categorie)}
                            </p>
                        </Col>
                    </Row>

                    <Row className="mb-2">
                        <Col xs={12} className="d-flex mb-3">
                            <Link to={`/registration/${convertToSlug(SoloTppCourseData.titre)}/${plat}/${id}`} className="w-100">
                                {SoloTppCourseData.date 
                                ?
                                <Button id="register-now-btn-orange" className="w-100 py-3">
                                    {trans[siteLanguage].soloPage.subscribeButton}
                                </Button>
                                :
                                <Button id="register-now-btn-orange" className="w-100 py-3">
                                    {trans[siteLanguage].soloPage.trainingButton}
                                </Button>
                                }
                            </Link>
                        </Col>

                        <Col xs={12} className="d-flex mb-3">
                            {SoloTppCourseData && siteLanguage
                            &&
                            <span id="register-now-btn-white" className="w-100 py-3 text-center">
                                <PDFDownloadLink document={<TppPdf data={SoloTppCourseData} siteLanguage={siteLanguage} categoryName={pickCategoryName(SoloTppCourseData.id_categorie)} categoryColor={pickCategoryColor(SoloTppCourseData.id_categorie)} />} fileName={`${SoloTppCourseData.titre}.pdf`} style={{color: "#F47820", padding:'20px'}}>    
                                    {trans[siteLanguage].soloPage.brochureButton}
                                </PDFDownloadLink>
                            </span>
                            }
                        </Col>
                    </Row>
                    
                    <Row>
                        <Col xs={11} className="d-flex">
                            <div className="course-big-image mr-auto" style={{ backgroundImage: `url(${pickCategoryPhoto(SoloTppCourseData.id_categorie)})` }} />
                        </Col>

                        <Col xs={1} className="d-flex p-0">
                        <   div className="my-auto mr-auto solo-session-orange-rectangle" style={{ backgroundColor: pickCategoryColor(SoloTppCourseData.id_categorie) }} />                                        
                        </Col>
                    </Row>

                    {SoloTppCourseData.objectif.length > 0
                    &&
                    <>
                    <Row className="my-4">
                        <Col xs={12} className="mb-2">
                            <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.objectives}</p>
                        </Col>
                        
                        <Col xs={12}>
                            <ul>
                                {SoloTppCourseData.objectif.map((obj,i) => 
                                    <li key={i} className="course-small-gray-text-solo">{obj}</li>
                                )}      
                            </ul>
                        </Col>
                    </Row>
                    </>
                    }
                    
                    {SoloTppCourseData.description.length > 0
                    &&
                    <Row className="mt-3 mb-5">
                        <Col xs={12} className="mb-2">
                            <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.content}</p>
                        </Col>

                        <Col xs={12}>     
                            <ul>
                                {SoloTppCourseData.description.map((desc,i) => 
                                    <li key={i} className="course-small-gray-text-solo">{desc}</li>
                                )}
                            </ul>                       
                        </Col>
                    </Row>
                    }


                    {SoloTppCourseData.population 
                    && 
                    <Row className="mt-3 mb-5">
                        <Col xs={12} className="mb-3">
                            <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.population}</p>
                        </Col>

                        <Col xs={12}>
                            <p className="course-small-gray-text-solo my-auto">{SoloTppCourseData.population}</p>
                        </Col>
                    </Row>
                    }

                    <Row className="d-flex justify-content-between my-3">
                        <Col xs={12}>
                            <Row className="justify-content-between">

                                {SoloTppCourseData.duree &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.duration}</p>
                                            <p className="course-small-gray-text-solo">{SoloTppCourseData.duree}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                {SoloTppCourseData.livraison &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.delivery}</p>
                                            <p className="course-small-gray-text-solo">{pickDeliveryByLang(SoloTppCourseData.livraison)}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                {SoloTppCourseData.prix &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.price}</p>
                                            <p className="course-small-gray-text-solo">{SoloTppCourseData.prix}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }

                                {SoloTppCourseData.langue &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.language}</p>
                                            <p className="course-small-gray-text-solo">{SoloTppCourseData.langue}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                
                            </Row>
                        </Col>
                    </Row>
                </div>
                
                {/* FORMATIONS SIMILAR */}
                <Row className="mt-5 mb-5 ">
                    <Col xs={12}>
                        <Row>
                            {SoloTppCourseSimilarData && SoloTppCourseSimilarData.length > 0 &&
                            SoloTppCourseSimilarData.map((similar,i) =>
                                <Col xs={12} key={i} className="pl-4 mb-3">
                                    <Row style={{background: '#F4F4F4'}}>
                                        <Col xs={6} className="d-flex p-3">
                                            <a href={`/solo-tpp/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                <Image src={similar.image} alt={similar.titre && similar.titre} className="m-auto" style={{width:'120px'}}/>
                                            </a>
                                        </Col>

                                        <Col xs={12} className="p-3">
                                            <Row className="mb-2">
                                                <Col xs={12} className="d-flex">
                                                    <a href={`/solo-tpp/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                        <p className="course-blue-text" style={{cursor: 'pointer'}}>
                                                            {similar.titre.length > 30
                                                                ?
                                                                similar.titre.substring(0,30) + "..."
                                                                :
                                                                similar.titre
                                                            }

                                                        </p>
                                                    </a>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col xs={12}>
                                                    <p className="course-small-gray-text">
                                                        {similar.description.length > 0 && similar.description[0].substring(0,25)}...
                                                    </p>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            )}
                        </Row>
                    </Col>
                </Row>
            </Mobile>
        </Container>

        {/* BLOC S'affiche onScroll */}
        {hidden 
        &&
        <div className="solo-page-sticky-btns">
            <Row className="d-flex mx-0">
                <Col md={6} className="d-flex py-3">
                    <Link to={`/registration/${convertToSlug(SoloTppCourseData.titre)}/${plat}/${id}`} className="ml-auto">
                        {SoloTppCourseData.date 
                        ?
                        <Button id="register-now-btn-orange" className="p-3">
                            {trans[siteLanguage].soloPage.subscribeButton}
                        </Button>
                        :
                        <Button id="register-now-btn-orange" className="p-3">
                            {trans[siteLanguage].soloPage.trainingButton}
                        </Button>
                        }
                    </Link>
                </Col>

                <Col md={6} className="d-flex py-3">
                    {SoloTppCourseData && siteLanguage
                    &&
                    <span id="register-now-btn-white" className="p-3 text-center mr-auto">
                        <PDFDownloadLink document={<TppPdf data={SoloTppCourseData} siteLanguage={siteLanguage} categoryName={pickCategoryName(SoloTppCourseData.id_categorie)} categoryColor={pickCategoryColor(SoloTppCourseData.id_categorie)} />} fileName={`${SoloTppCourseData.titre}.pdf`} style={{color: "#F47820", padding:'20px'}}>    
                            {trans[siteLanguage].soloPage.brochureButton}
                        </PDFDownloadLink>
                    </span>
                    }
                </Col>
            </Row>
        </div>
        }

        </Container>
        }


        </>
    );
}


const mapStateToProps = state => ({
    siteLanguage : state.afsacR.siteLanguage,
    SoloTppCourseData : state.afsacR.SoloTppCourseData,
    SoloTppCourseSimilarData : state.afsacR.SoloTppCourseSimilarData,
    TppCategories : state.afsacR.TppCategories,
    brochure : state.afsacR.brochure,
});
  
export default connect(mapStateToProps,{ getSoloTppCourse, getTppCategories,getBrochure })(SoloTpp);
