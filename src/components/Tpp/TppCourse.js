import React, { useEffect } from 'react';

import {
    Row,
    Col,
    Image
} from 'react-bootstrap';

import {
    Link
} from 'react-router-dom';

import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import { connect } from 'react-redux';
import { trans } from '../../constants/Translations'; // {trans[siteLanguage].courseCard.requestButton}


function TppCourse(props) {

    let {
        siteLanguage,
        id,
        title,
        border,
        image,
        objectif,
        duration,
        delivery,
        price,
        language,
        date,
        // video,
         description,
        // category,
        // inscription,
        // population,
        // place,
        // entry,
        // image_detail,
        platform,
    } = props;

    const convertToSlug = (title) => {
        return title
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
    };

    const pickDeliveryByLang = (del) => {
        
        if(del == "virtual classroom") {
           return trans[siteLanguage].courseCard.virtualClassroom
        // } else if (del.toLowerCase() == "classroom" ) {
        } else {
            return trans[siteLanguage].courseCard.classroom
        }
    }

    return (
        <>
        <Desktop>
            <Row key={id} className="courses-item mb-4 py-4 mx-auto" style={{background : '#F4F4F4', borderLeft: `5px solid ${border}`}}>
                <Col lg={3} className="d-flex">
                    <div className="course-small-image" style={{backgroundImage: `url(${image})`, height:'216px'}} />
                </Col>

                <Col lg={9} className=" flex-column px-0">
                    <Row className="justify-content-between d-flex mt-auto" >
                        <Col lg={9}>
                            <Link to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`}>
                                <p className="course-blue-text" style={{color: border }}>
                                    {title.length > 45
                                    ?
                                    title.substring(0,43) + "..."
                                    :
                                    title
                                    }
                                </p>
                            </Link>
                        </Col>
                        <Col lg={2} className="d-flex mr-3">
                             <p className="course-gray-date-text my-auto ml-auto">{date}</p>
                        </Col>
                    </Row>
                    
                    

                    <Row className="justify-content-between d-flex " >
                        <Col lg={12}>
                            <p className="course-light-blue-text" style={{ fontSize: '18px',textTransform:"none"}}>{trans[siteLanguage].courseCard.objectives}</p>
                        </Col>
                    </Row>

                    <Row  className="mt-3">
                        <Col lg={12}>
                            {description &&
                            <p className="course-gray-text">{description.substring(0, description.length/2 + (description.length/2) /2 )}...</p>

                            }
                        </Col>
                    </Row>
                    <Row className="d-flex  mt-auto">
                        <Col lg={12}>
                            <Row >

                                {duration &&
                                <Col lg={2}>
                                    <Row>
                                        <Col lg={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.duration}</p>
                                            <p className="course-small-gray-text">{duration}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                {delivery &&
                                <Col lg={3}>
                                    <Row>
                                        <Col lg={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.delivery}</p>
                                            <p className="course-small-gray-text">{pickDeliveryByLang(delivery)}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                {price &&
                                <Col lg={1}>
                                    <Row>
                                        <Col lg={14}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.price}</p>
                                            <p className="course-small-gray-text">{price}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                {language &&
                                <Col lg={3}>
                                    <Row>
                                        <Col lg={12}>
                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.language}</p>
                                            <p className="course-small-gray-text">{language}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                <Col lg={3}>
                                    <div className="py-2 mr-2">
                                        <Link className="d-flex" to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`}>
                                            <button className="courses-btn py-2">{trans[siteLanguage].courseCard.seeMoreButton}</button>
                                        </Link>
                                    </div>
                                </Col>

                            
                            </Row>
                        </Col>


                    </Row>
                </Col>
            </Row>
            </Desktop>

        <Tablet>
            <Row key={id} className="courses-item mb-3 px-2 py-3 mx-auto" style={{background : '#F4F4F4', borderTop: `5px solid ${border}`}}>

                <Col xs={12} className="d-flex px-0 mb-2">
                    {/* <Image src={image} fluid className="mr-auto" />  */}
                    <div className="course-small-image" style={{backgroundImage: `url(${image})`}} /> 
                </Col>

                <Col xs={12} className="d-flex flex-column px-0">
                   
                    <Row className="justify-content-between mb-2">
                        <Col xs={12}>
                            <Link to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`}>
                                <p className="course-light-blue-text" style={{color: border }}>{title}</p>
                            </Link>
                        </Col>
                        
                        <Col xs={12}>
                            <p className="course-gray-date-text">{date}</p>
                        </Col>
                    </Row>

                    <Row className="mb-2">
                        <Col xs={12}>
                            <p className="course-light-blue-text">{trans[siteLanguage].courseCard.objectives}</p>
                        </Col>
                    </Row>
                    
                    <Row  className="mb-2">
                        <Col xs={12}>
                            {objectif[0] &&
                            <p className="course-gray-text">{objectif[0].substring(0,76)}...</p>
                            }
                        </Col>
                    </Row>

                    <Row className="d-flex justify-content-between mt-auto">
                        <Col lg={8} className="mb-2">
                            <Row className="justify-content-between">
                                
                                {duration &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.duration}</p>
                                            <p className="course-small-gray-text">{duration}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                
                                {price &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.price}</p>
                                            <p className="course-small-gray-text">{price}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                
                                {language &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.language}</p>
                                            <p className="course-small-gray-text">{language}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                
                                {delivery &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.delivery}</p>
                                            <p className="course-small-gray-text">{pickDeliveryByLang(delivery)}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                            </Row>
                        </Col>

                        <Col lg={2} className="d-flex mr-2 mb-2">
                            <Link className="m-auto" to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`}>
                                    <p className="course-light-blue-text">{trans[siteLanguage].courseCard.seeMoreButton}</p>
                            </Link>
                            {/* <p className="ml-auto mt-auto course-light-blue-text">Voir details</p> */}
                        </Col>
                    </Row>
                </Col>
            </Row>
            </Tablet>
            
        <Mobile>
            <Row key={id} className="courses-item mb-3 px-2 py-3 mx-auto" style={{background : '#F4F4F4', borderTop: `5px solid ${border}`}}>
            
                <Col xs={12} className="d-flex px-0 mb-2">
                    <div className="course-small-image" style={{backgroundImage: `url(${image})`}} /> 
                </Col>

                <Col xs={12} className="d-flex flex-column px-0">
                   
                    <Row className="justify-content-between mb-2 " >
                        <Col xs={12}>
                            <Link to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`}>
                                <p className="course-light-blue-text" style={{color: border }}>{title}</p>
                            </Link>
                        </Col>          
                                                
                        <Col xs={12} className="d-flex mr-2">
                            <p className="course-gray-date-text">{date}</p>
                        </Col>
                    </Row>

                    <Row className="mb-2">
                        <Col xs={12}>
                            <p className="course-light-blue-text">{trans[siteLanguage].courseCard.objectives}</p>
                        </Col>
                    </Row>
                    
                    <Row  className="mb-2">
                        <Col xs={12}>
                            {objectif[0] &&
                            <p className="course-gray-text">{objectif[0].substring(0,76)}...</p>
                            }
                        </Col>
                    </Row>

                    <Row className="d-flex justify-content-between mt-auto">
                        <Col lg={8} className="mb-2">
                            <Row className="justify-content-between">

                                {duration &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.duration}</p>
                                            <p className="course-small-gray-text">{duration}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                
                                {price &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.price}</p>
                                            <p className="course-small-gray-text">{price}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }

                                {language &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.language}</p>
                                            <p className="course-small-gray-text">{language}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                                
                                {delivery &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.delivery}</p>
                                            <p className="course-small-gray-text">{pickDeliveryByLang(delivery)}</p>
                                        </Col>
                                    </Row>
                                </Col>
                                }
                            
                            </Row>
                        </Col>

                        <Col lg={2} className="d-flex mr-2 mb-2">
                            <Link to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`} className="m-auto">
                                <p className="course-light-blue-text">{trans[siteLanguage].courseCard.seeMoreButton}</p>
                            </Link>
                        </Col>
                    </Row>
                </Col>

                </Row>
            </Mobile>
        </>
    )
};

const mapStateToProps = state => ({
    siteLanguage : state.afsacR.siteLanguage,
});
  
export default connect(mapStateToProps,{})(TppCourse);
  

