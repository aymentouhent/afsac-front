import React from 'react';

import { Document, Page, Text, View,Image, StyleSheet, Font, Link } from '@react-pdf/renderer';
import sfBold from "../../assets/fonts/SFProDisplay-Bold.ttf";
import sfSemiBold from '../../assets/fonts/SFProDisplay-Semibold.ttf';
import { trans } from '../../constants/Translations';

const PDF_HEADER = require('../../assets/images/SoloSession/tpp-header-pdf.png');



function TppPdf(props) {

    let { siteLanguage, data, categoryName, categoryColor } = props;

    if(data && siteLanguage && categoryName && categoryColor) {
        console.log(data);
        return (
            <Document>
                {data && siteLanguage && categoryName && categoryColor
                &&
                <Page size="A4">

                    {PDF_HEADER
                    &&
                    <View style={styles.headerContainer}>
                        <Image
                            style={styles.headerImg}
                            src={PDF_HEADER}
                        />
                    </View>
                    }

                    <View style={styles.contentContainer}>
                        <Text style={styles.BlueTitre}>{data.titre}</Text>

                        {data.id_categorie
                        &&
                        <>
                            <Text style={styles.lightBlueTitre}>{trans[siteLanguage].soloPage.category}</Text>

                            <View style={styles.dotsCategoryView}>
                                <View style={[styles.categoryDot,{ backgroundColor: categoryColor }]} />
                                <Text style={[styles.grayText, { color: categoryColor }]}>{categoryName}</Text>
                            </View>
                        </>
                        }

                        {data.objectif.length > 0
                        &&
                        <>
                            <Text style={styles.lightBlueTitre}>{trans[siteLanguage].soloPage.objectives}</Text>

                            {data.objectif.map((obj,i) =>
                                <View style={styles.dotsView}>
                                    <View style={styles.dot} />
                                    <Text key={i} style={styles.grayText}>{obj}</Text>
                                </View>
                            )}
                        </>
                        }

                        {data.description.length > 0
                        &&
                        <>
                            <Text style={styles.lightBlueTitre}>{trans[siteLanguage].soloPage.content}</Text>

                            {data.description.map((desc,i) =>
                                <View style={styles.dotsView}>
                                    <View style={styles.dot} />
                                    <Text key={i} style={styles.grayText}>{desc}</Text>
                                </View>
                            )}
                        </>
                        }

                        {data.population
                        &&
                        <>
                            <Text style={styles.lightBlueTitre}>{trans[siteLanguage].soloPage.population}</Text>
                            <Text style={styles.grayText}>{data.population}</Text>
                        </>
                        }

                        {data.entry.length > 0
                        &&
                        <>
                            <Text style={styles.lightBlueTitre}>{trans[siteLanguage].soloPage.entry}</Text>

                            {data.entry.map((entry,i) =>
                                <View style={styles.dotsView}>
                                    <View style={styles.dot} />
                                    <Text key={i} style={styles.grayText}>{entry}</Text>
                                </View>
                            )}
                        </>
                        }

                        {data.place
                        &&
                        <>
                            <Text style={styles.lightBlueTitre}>{trans[siteLanguage].soloPage.place}</Text>
                            <Text style={styles.grayText}>{data.place}</Text>
                        </>
                        }

                        <View style={styles.fourRow}>
                            {data.duree
                            &&
                            <View style={styles.rowElement}>
                                <Text style={styles.lightBlueSmallTitre}>{trans[siteLanguage].soloPage.duration}</Text>
                                <Text style={styles.grayText}>{data.duree}</Text>
                            </View>
                            }

                            {data.prix
                            &&
                            <View style={styles.rowElement}>
                                <Text style={styles.lightBlueSmallTitre}>{trans[siteLanguage].soloPage.price}</Text>
                                <Text style={styles.grayText}>{data.prix}</Text>
                            </View>
                            }

                            {data.langue
                            &&
                            <View style={styles.rowElement}>
                                <Text style={styles.lightBlueSmallTitre}>{trans[siteLanguage].soloPage.language}</Text>
                                <Text style={styles.grayText}>{data.langue}</Text>
                            </View>
                            }

                            {data.livraison
                            &&
                            <View style={styles.rowElement}>
                                <Text style={styles.lightBlueSmallTitre}>{trans[siteLanguage].soloPage.delivery}</Text>
                                <Text style={styles.grayText}>{data.livraison}</Text>
                            </View>
                            }
                        </View>
                    </View>

                    <View fixed style={styles.footerContainer}>
                        <View style={{marginTop: 8}}>
                            <Text style={styles.blackBoldText}>N'hésitez pas à nous contacter.</Text>
                            <Text style={styles.blackText}>Notre équipe est toujours à votre disposition pour vous renseigner et répondre à toutes vos questions.</Text>

                            <Text style={styles.blackBoldText}>For Immediate Support.</Text>
                            <Text style={styles.blackText}>Our support team is available to provide any guidance requested.</Text>

                            <Text style={styles.blackText}>00216 98 333 330 / 00216 20 199 950</Text>
                            <Text style={styles.blackText}>Email : training@afsactunisie.com</Text>
                            <Link src={"https://www.afsac-tunisie.com"} style={styles.linkText}>www.afsac-tunisie.com</Link>
                        </View>

                        {/* <View style={{marginTop: 8}}>
                            <Text style={styles.blackBoldText}>N'hésitez pas à nous contacter.</Text>
                            <Text style={styles.blackText}>Notre équipe est toujours à votre disposition pour vous renseigner et répondre à toutes vos questions.</Text>
                            <Text style={styles.blackText}>Email : training@afsactunisie.com</Text>
                            <Text style={styles.blackText}>00216 98 333 330 / 00216 20 199 950</Text>
                            <Link src={"https://www.afsac-tunisie.com"} style={styles.linkText}>www.afsac-tunisie.com</Link>
                        </View>

                        <View style={{marginTop: 8}}>
                            <Text style={styles.blackBoldText}>For Immediate Support.</Text>
                            <Text style={styles.blackText}>Our support team is available to provide any guidance requested.</Text>
                            <Text style={styles.blackText}>To contact us, send an email to training@afsactunisie.com</Text>
                            <Text style={styles.blackText}>00216 98 333 330 / 00216 20 199 950</Text>
                            <Link src={"https://www.afsac-tunisie.com"} style={styles.linkText}>www.afsac-tunisie.com</Link>
                        </View> */}
                    </View>

                </Page>
                }
            </Document>
        )
    } else {
        return <Document><Page></Page></Document>
    }
};


Font.register({
    family: 'sf-bold',
    src: sfBold,
});


Font.register({
    family: 'sf-semibold',
    src: sfSemiBold
});

// Create styles
const styles = StyleSheet.create({

    headerContainer: {
        width: "100%",
        paddingHorizontal: 5,
        paddingBottom: 0,
        flex: 0.5,
    },
    headerImg: {
        width: "100%",
        marginBottom: 8
    },

    contentContainer: {
        paddingLeft: 10,
        flex: 30,
    },

    footerContainer: {
        flex: 0.5,
        justifyContent: "center",
        textAlign: 'center',
    },

    BlueTitre : {
        color: "#0056A1",
        fontFamily: 'sf-bold',
        margin: 0,
    },

    lightBlueTitre : {
        color: "#00A7E2",
        fontFamily: 'sf-semibold',
        fontSize: 13,
        marginTop: 5,
        marginBottom: 0.5,
    },

    lightBlueSmallTitre : {
        color: "#00A7E2",
        fontFamily: 'sf-semibold',
        fontSize: 11,
        marginVertical: 5,
    },

    dotsView: {
        flexDirection: 'row',
    },
    dotsCategoryView: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    dot : {
        height: 4,
        width: 4,
        marginRight: 5,
        marginTop: 7.5,
        borderRadius: 10,
        backgroundColor: 'black',
    },

    categoryDot : {
        height: 10,
        width: 10,
        marginRight: 5,
        // marginTop: 7.5,
        borderRadius: 10,
    },

    grayText : {
        color: "#707070",
        fontFamily: 'sf-semibold',
        fontSize: 11,
        marginVertical: 3,
        textAlign: 'justify',
        marginRight: 40,
    },

    fourRow: {
        flexDirection: 'row',
        // justifyContent: 'space-between',
        marginTop: 3,
    },

    rowElement: {
        marginRight: 15,
    },

    blackBoldText: {
        fontFamily: 'sf-bold',
        fontSize: 9,
        color: 'black',
    },

    blackText: {
        fontFamily: 'sf-semibold',
        fontSize: 9,
        color: 'black',
    },

    linkText: {
        fontFamily: 'sf-semibold',
        fontSize: 9,
        // color: 'black',
    },

});
export default TppPdf;
