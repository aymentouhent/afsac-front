import React from 'react';

import {
    Image,
    Col,
    Row
} from 'react-bootstrap';

import moment from 'moment';

import { Link } from 'react-router-dom';

const SESSION_IMG = require('../../assets/images/Home/session.png');

function SessionBox(props) {

    let {
        key, 
        id,
        title,
        image,
        image_detail,
        categoryId,  
        categories,
        platform,
        date,
    } = props;

    const pickCategoryColor = (cat_id) => {
        let found = categories.find((cat) => cat.id === cat_id)
         
        if(found) {
         return found.color;
        } else {
            return false;
        }
    };

    const pickCategoryName = (cat_id) => {
        let found = categories.find((cat) => cat.id == cat_id)
     
        if(found) {
            return found.nom
        } else {
            return '';
        }
    };

    const pickCategoryPhoto = (cat_id) => {
        let found = categories.find((cat) => cat.id === cat_id)
     
        if(found) {
            return found.image;
        } else {
            return SESSION_IMG;
        }
    }; 

    const convertToSlug = (title) => {
        return title
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
    };

    
    return (
        <Col className="d-flex flex-column p-0" style={{ width: "90%", margin: 'auto'}}>
            
            <div  style={{width: '100%'}}>
                <Link to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`}>
                    <Image src={pickCategoryPhoto(categoryId)}  className="m-auto w-100" alt={pickCategoryName(categoryId)}  />
                </Link>
            </div>


            <Row className="d-flex p-0 m-0">
                <Col xs={4} className="d-flex flex-column" style={{backgroundColor: '#fff'}}>
                    <span className="m-auto home-session-day">{moment(date).format("DD")}</span>
                    <span className="m-auto home-session-month">{moment(date).format("MMMM")}</span>
                </Col>

                
                    <Col xs={8} className="d-flex flex-column p-3 m-0" style={{ background: "#F4F6F9", height: '104px' }}>
                        <Link to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`}>
                            <span className="mr-auto home-session-title">{pickCategoryName(categoryId)}</span>
                        </Link>

                        <Link to={`/solo-tpp/${platform}/${id}/${convertToSlug(title)}`}>
                            <span className="mr-auto home-session-desc" style={{ color: "#0056A1", fontWeight: "normal" }}>{title.length > 30 ? title.substring(0,30) + "..." : title}</span>
                        </Link>
                    </Col>
              
            </Row>
        </Col>
    );
}

export default SessionBox;