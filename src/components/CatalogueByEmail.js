import React, { useState, useEffect, useRef } from 'react';
import './components.css';
// import axios from 'axios';

import { connect } from 'react-redux';
import { trans } from '../constants/Translations'; // {trans[siteLanguage].contactForm.requestButton}
import { sendBrochureToUser } from '../redux/actions/AfsacActions';
import { images, Desktop, Mobile, Tablet, config } from '../constants/AppConfig';
import { Form, Col, Button, Image, Row, Container } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';

import Swal from 'sweetalert2/dist/sweetalert2.js';

// import ReactFlagsSelect from 'react-flags-select';
// import PhoneInput from 'react-phone-input-2'

function CatalogueByEmail(props) {

    let {
        siteLanguage,
        sendBrochureToUser,
    } = props;

    const [catalogueEmail, setCatalogueEmail] = useState('');

    const validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };

    const sendCatalogueByMail = () => {

        if (!validateEmail(catalogueEmail)) {
            alert("verif email please");
        } else {
            sendBrochureToUser(catalogueEmail, siteLanguage)
                .then(response => {
                    console.log("sendRegistration form result ==>", response.data);

                    // if(response.data) {
                    //   Swal.fire({
                    //     position: 'center',
                    //     icon: 'error',
                    //     title: trans[siteLanguage].TppPages.brochureFormError1,
                    //     showConfirmButton: false,
                    //     timer: 2000
                    //   });
                    // } else {
                    setCatalogueEmail("");

                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: trans[siteLanguage].TppPages.brochureFormSuccess,
                        showConfirmButton: false,
                        timer: 3000
                    });
                    // }

                })
                .catch(error => {
                    console.log("sendRegistration form Error ==>", error);

                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: trans[siteLanguage].TppPages.brochureFormConnexionError,
                        showConfirmButton: false,
                        timer: 2000
                    });
                });
        }
    };


    return (
        <Container fluid style={{ background: '#00A7E2' }}>
            <Row className="justify-content-center mt-5">
                <Col xs={11} lg={9} md={12}>
                    <Row>
                        <Col xs={12} lg={6} md={6} className="mt-2">
                            <Image src={images.sessions_catalogue} className="session-catalogue-img center" alt={trans[siteLanguage].TppPages.downloadBrochure} />
                        </Col>

                        <Col xs={12} lg={6} md={6} >
                            <Row className="m-auto ">
                                <Col xs={12} className="d-flex mb-4 mt-5">
                                    <h3 className="m-auto session-catalogue-title">{trans[siteLanguage].TppPages.downloadBrochure}</h3>
                                </Col>

                                <Col xs={12} className="d-flex mb-5">
                                    <p className="m-auto session-catalogue-desc">{trans[siteLanguage].TppPages.downloadBrochureDesc}</p>
                                </Col>

                                <Col xs={12}>
                                    <Row className="justify-content-center">
                                        <Col xs={12} lg={10}>
                                            <Row>
                                                <Col xs={12} lg={6} className="px-2 d-flex">
                                                    <Form.Control type="text" id="session-catalogue-input" className="mx-auto mb-2" onChange={(e) => setCatalogueEmail(e.target.value)} value={catalogueEmail} placeholder={trans[siteLanguage].TppPages.downloadEmailPlaceholder} />
                                                </Col>

                                                <Col xs={12} lg={6} className="px-2 d-flex">
                                                    <Button className="mx-auto mb-3" id="session-catalogue-btn" onClick={() => sendCatalogueByMail()} >{trans[siteLanguage].TppPages.downloadBrochureButton}</Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );
};

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
});

export default connect(mapStateToProps, { sendBrochureToUser })(CatalogueByEmail);

