import React, { useEffect, useState } from 'react';

import {
    Container,
    Row,
    Col,
    Image,
    Button, Card,
} from 'react-bootstrap';
import { PDFDownloadLink } from '@react-pdf/renderer';

import { connect } from 'react-redux';
import { getSoloAvsecCourse, getBrochure } from '../../redux/actions/AfsacActions';

import { trans } from '../../constants/Translations';

import useHideOnScrolled from "../useHideOnScrolled";
import AvsecPdf from './AvsecPdf';

import { Link, useParams } from 'react-router-dom';
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import Lottie from 'react-lottie';
import loader from '../../assets/json/loader.json';

const PDF_HEADER = require('../../assets/images/SoloSession/avsec-header-pdf.png');

function SoloAvsec(props) {
    let { plat, id } = useParams();

    let {
        siteLanguage,
        SoloAvsecCourseData,
        SoloAvsecCourseSimilarData,

        // Functions:
        getSoloAvsecCourse,
    } = props;

    const loaderOpts = {
        loop: true,
        autoplay: true,
        animationData: loader,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    const hidden = useHideOnScrolled();
    const [pageLoading, setPageLoading] = useState(true);
    const [headerPdf, setHeaderPdf] = useState('none');

    const convertToSlug = (title) => {
        return title
            .replace(/\//g, '')
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-');
    };

    const pickDeliveryByLang = (del) => {

        if (del == "virtual classroom") {
            return trans[siteLanguage].courseCard.virtualClassroom
            // } else if (del.toLowerCase() == "classroom" ) {
        } else {
            return trans[siteLanguage].courseCard.classroom
        }
    }

    useEffect(() => {
        getSoloAvsecCourse(plat, id, siteLanguage)
            .then(() => {
                // getBrochure(siteLanguage);
                setPageLoading(false);
            });
    }, []);

    return (
        <>
            {pageLoading
                ?
                <div className="preLoader">
                    <Lottie
                        options={loaderOpts}
                        height={200}
                        width={200}
                        style={{ position: 'absolute', top: '45%', left: '45%', marginTop: '-13px', marginLeft: '-13px', }}
                    />
                </div>
                :
                <Container fluid>

                    <Row className="d-flex">
                        <Image src={images.solo_plane} fluid style={{ height: '300px' }} alt="afsac-plane" />
                    </Row>

                    <Container>
                        <div className=" Breadcrumb d-flex mb-3 px-0 ">
                            <div className="breadcrumb-item"><Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>
                            {/* <div className="breadcrumb-item" ><Link to={'/sessions-avsec'}> AVSEC courses </Link></div> */}
                            <div className="breadcrumb-item active">{SoloAvsecCourseData.titre}</div>
                        </div>

                        <Desktop>
                            <Row className="mt-5">
                                <Col lg={4}>
                                    <Row>
                                        <Col lg={12} className="d-flex mb-3">
                                            <Link to={`/registration/${convertToSlug(SoloAvsecCourseData.titre)}/${plat}/${id}`} className="w-100">
                                                {SoloAvsecCourseData.date ?
                                                    <Button id="register-now-btn-orange" className="w-100 py-3">
                                                        {trans[siteLanguage].soloPage.subscribeButton}
                                                    </Button>
                                                    :
                                                    <Button id="register-now-btn-orange" className="w-100 py-3">
                                                        {trans[siteLanguage].soloPage.trainingButton}
                                                    </Button>
                                                }
                                            </Link>
                                        </Col>

                                        <Col lg={12} className="d-flex mb-3">
                                            {SoloAvsecCourseData && siteLanguage
                                                &&
                                                <span id="register-now-btn-white" className="w-100 py-3 text-center">
                                                    <PDFDownloadLink document={<AvsecPdf data={SoloAvsecCourseData} siteLanguage={siteLanguage} />} fileName={`${SoloAvsecCourseData.titre}.pdf`} style={{ color: "#F47820", padding: '20px' }}>
                                                        {trans[siteLanguage].soloPage.brochureButton}
                                                    </PDFDownloadLink>
                                                </span>
                                            }
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col lg={12} className="d-flex">
                                            <div className="course-big-image m-auto" style={{ backgroundImage: `url(${SoloAvsecCourseData.image_detail})` }} />
                                        </Col>
                                    </Row>
                                </Col>

                                {/* Right Bloc */}
                                <Col lg={7} className="ml-5" id="solo-avsec">
                                    <Row className="mb-3">
                                        <Col lg={12} className="p-0">
                                            <p className="course-blue-text" style={{ fontSize: '34px' }}>{SoloAvsecCourseData.titre}</p>
                                        </Col>
                                    </Row>

                                    {SoloAvsecCourseData.objectif.length > 0
                                        &&
                                        <>
                                            <Row className="my-4">
                                                <Col lg={12} className="p-0">
                                                    <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.objectives}</p>
                                                </Col>
                                            </Row>

                                            <Row className="mt-4 mb-5">
                                                <Col lg={12} className="p-0">
                                                    <p className="course-small-gray-text my-auto" style={{ textAlign: 'justify', color: 'black' }}>{SoloAvsecCourseData.objectif[0]}</p>
                                                    <ul>
                                                        {SoloAvsecCourseData.objectif.map((obj, i) =>
                                                            i > 0
                                                            &&
                                                            <li key={i} className="course-small-gray-text" style={{ textAlign: 'justify', color: 'black' }}>{obj}</li>
                                                        )}
                                                    </ul>
                                                </Col>
                                            </Row>
                                        </>
                                    }

                                    {SoloAvsecCourseData.description.length > 0
                                        &&
                                        <Row className="mt-3 mb-5">
                                            <Col lg={12} className="mb-3 p-0">
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.content}</p>
                                            </Col>

                                            <Col lg={12} className="p-0">
                                                {/* <ul> */}
                                                {SoloAvsecCourseData.description.map((desc, i) =>
                                                    // <li key={i} className="course-small-gray-text-solo" style={{textAlign: 'justify', color: 'black'}}>{desc}</li>
                                                    <p className="course-small-gray-text my-auto" style={{ textAlign: 'justify', color: 'black' }}>{desc}</p>
                                                )}
                                                {/* </ul> */}
                                            </Col>
                                        </Row>
                                    }

                                    {SoloAvsecCourseData.population
                                        &&
                                        <Row className="mt-3 mb-5">
                                            <Col lg={12} className="mb-3 p-0">
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.population}</p>
                                            </Col>

                                            <Col lg={12} className="p-0">
                                                <p className="course-small-gray-text-solo my-auto" style={{ textAlign: 'justify', color: 'black' }}>{SoloAvsecCourseData.population}</p>
                                            </Col>
                                        </Row>
                                    }

                                    {SoloAvsecCourseData.entry.length > 0 &&
                                        <Row className="mt-3 mb-5">
                                            <Col lg={12} className="mb-3 p-0">
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.entry}</p>
                                            </Col>

                                            <Col lg={12} className="p-0">
                                                <ul>
                                                    {SoloAvsecCourseData.entry.map((entry, i) =>
                                                        <li key={i} className="course-small-gray-text-solo" style={{ textAlign: 'justify', color: 'black' }}>{entry}</li>
                                                    )}
                                                </ul>
                                            </Col>
                                        </Row>
                                    }

                                    {SoloAvsecCourseData.place
                                        &&
                                        <>
                                            <Row className="my-4">
                                                <Col lg={12} className="p-0">
                                                    <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.place}</p>
                                                </Col>
                                            </Row>

                                            <Row className="mt-4 mb-5">
                                                <Col lg={12} className="p-0">
                                                    <p className="course-small-gray-text-solo" style={{ textAlign: 'justify', color: 'black' }}>
                                                        {SoloAvsecCourseData.place}
                                                    </p>
                                                </Col>
                                            </Row>
                                        </>
                                    }

                                    <Row className="d-flex justify-content-between my-3">
                                        <Col lg={12} className="p-0">
                                            <Row className="justify-content-between">

                                                {SoloAvsecCourseData.duree &&
                                                    <Col lg={3}>
                                                        <Row>
                                                            <Col lg={12}>
                                                                <p className="course-small-light-blue-text mb-2" >{trans[siteLanguage].soloPage.duration}</p>

                                                                <p className="course-small-gray-text-solo" style={{ color: 'black' }}>{SoloAvsecCourseData.duree}</p>



                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                }

                                                {SoloAvsecCourseData &&
                                                    <Col lg={3}>
                                                        <Row>
                                                            <Col lg={12}>
                                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.price}</p>
                                                                <p className="course-small-gray-text-solo" style={{ color: 'black' }}>{SoloAvsecCourseData.prix}</p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                }

                                                {SoloAvsecCourseData.langue &&
                                                    <Col lg={3}>
                                                        <Row>
                                                            <Col lg={12}>
                                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.language}</p>
                                                                <p className="course-small-gray-text-solo" style={{ color: 'black' }}>{SoloAvsecCourseData.langue}</p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                }

                                                {SoloAvsecCourseData.livraison &&
                                                    <Col lg={3}>
                                                        <Row>
                                                            <Col lg={12}>
                                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.delivery}</p>
                                                                <p className="course-small-gray-text-solo" style={{ color: 'black' }}>{pickDeliveryByLang(SoloAvsecCourseData.livraison)}</p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                }
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>

                            {/* FORMATIONS SIMILAIRES */}
                            <Row className="mt-5 mb-5">
                                <Col lg={12}>
                                    <Row >
                                        {SoloAvsecCourseSimilarData && SoloAvsecCourseSimilarData.length > 0 &&
                                            SoloAvsecCourseSimilarData.map((similar, i) =>
                                                <Col lg={6} key={i}  >

                                                    <Row style={{ background: '#F4F4F4', width: '100%', padding: '1rem' }}>
                                                        <Col lg={4} className=" p-2">
                                                            <a href={`/solo-avsec/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                                <Card.Img src={similar.image} style={{ width: '160px' }} />
                                                            </a>
                                                        </Col>

                                                        <Col lg={8} className="p-2 ">
                                                            <Row className="mb-2 ml-3">
                                                                <Col lg={12} className="d-flex">
                                                                    <a href={`/solo-avsec/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                                        <p className="course-blue-text" style={{ cursor: 'pointer', fontSize: '25px' }}>
                                                                            {similar.titre && similar.titre}
                                                                        </p>
                                                                    </a>
                                                                </Col>
                                                            </Row>

                                                            <Row>
                                                                <Col lg={12}>
                                                                    <p className="course-small-gray-text ml-4 mt-3">
                                                                        {similar.description[0] && similar.description[0].substring(0, 52)}...
                                                                    </p>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </Row>


                                                </Col>
                                            )}
                                    </Row>
                                </Col>
                            </Row>
                        </Desktop>

                        <Tablet>
                            <Row className="mt-5">
                                <Col md={4}>
                                    <Row>
                                        <Col md={12} className="d-flex mb-3">
                                            <Link to={`/registration/${convertToSlug(SoloAvsecCourseData.titre)}/${plat}/${id}`} className="w-100">
                                                {SoloAvsecCourseData.date
                                                    ?
                                                    <Button id="register-now-btn-orange" className="w-100 py-3">
                                                        {trans[siteLanguage].soloPage.subscribeButton}
                                                    </Button>
                                                    :
                                                    <Button id="register-now-btn-orange" className="w-100 py-3">
                                                        {trans[siteLanguage].soloPage.trainingButton}
                                                    </Button>
                                                }
                                            </Link>
                                        </Col>

                                        <Col md={12} className="d-flex mb-3">
                                            {SoloAvsecCourseData && siteLanguage
                                                &&
                                                <span id="register-now-btn-white" className="w-100 py-3 text-center">
                                                    <PDFDownloadLink document={<AvsecPdf data={SoloAvsecCourseData} siteLanguage={siteLanguage} />} fileName={`${SoloAvsecCourseData.titre}.pdf`} style={{ color: "#F47820", padding: '20px' }}>
                                                        {trans[siteLanguage].soloPage.brochureButton}
                                                    </PDFDownloadLink>
                                                </span>
                                            }
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={12} className="d-flex">
                                            <Image src={SoloAvsecCourseData.image_detail} fluid className="m-auto w-100" alt={SoloAvsecCourseData.titre} />
                                        </Col>
                                    </Row>
                                </Col>

                                <Col md={7} id="solo-avsec">
                                    <Row className="mb-3">
                                        <Col md={12}>
                                            <p className="course-blue-text-solo">{SoloAvsecCourseData.titre}</p>
                                        </Col>
                                    </Row>

                                    {SoloAvsecCourseData.objectif.length > 0
                                        &&
                                        <Row className="mt-3 mb-5">
                                            <Col md={12}>
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.objectives}</p>
                                            </Col>

                                            <Col md={12}>
                                                <ul>
                                                    {SoloAvsecCourseData.objectif.map((objectif, i) =>
                                                        <li key={i} className="course-small-gray-text">{objectif}</li>
                                                    )}
                                                </ul>
                                            </Col>
                                        </Row>
                                    }

                                    {SoloAvsecCourseData.description.length > 0
                                        &&
                                        <Row className="mt-3 mb-5">
                                            <Col md={12}>
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.content}</p>
                                            </Col>

                                            <Col md={12}>
                                                <ul>
                                                    {SoloAvsecCourseData.description.map((desc, i) =>
                                                        <li className="course-small-gray-text my-auto">{desc}</li>
                                                    )}
                                                </ul>
                                            </Col>
                                        </Row>
                                    }

                                    {SoloAvsecCourseData.population
                                        &&
                                        <>
                                            <Row className="my-2">
                                                <Col md={12}>
                                                    <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.population}</p>
                                                </Col>
                                            </Row>

                                            <Row className="mb-3">
                                                <Col md={12}>
                                                    <p className="course-small-gray-text">{SoloAvsecCourseData.population}</p>
                                                </Col>
                                            </Row>
                                        </>
                                    }

                                    {SoloAvsecCourseData.entry.length > 0
                                        &&
                                        <Row className="mt-3 mb-5">
                                            <Col md={12}>
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.entry}</p>
                                            </Col>

                                            <Col md={12}>
                                                <ul>
                                                    {SoloAvsecCourseData.entry.map((entry, i) =>
                                                        <p key={i} className="course-small-gray-text">{entry}</p>
                                                    )}
                                                </ul>
                                            </Col>
                                        </Row>
                                    }

                                    {SoloAvsecCourseData.place
                                        &&
                                        <>
                                            <Row className="my-2">
                                                <Col md={12}>
                                                    <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.place}</p>
                                                </Col>
                                            </Row>

                                            <Row className="mb-3">
                                                <Col md={12}>
                                                    <p className="course-small-gray-text-solo">{SoloAvsecCourseData.place}</p>
                                                </Col>
                                            </Row>
                                        </>
                                    }


                                    <Row className="d-flex justify-content-between my-3">
                                        <Col md={12}>
                                            <Row className="justify-content-between">

                                                {SoloAvsecCourseData.duree &&
                                                    <Col md={3}>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.duration}</p>
                                                                <p className="course-small-gray-text">{SoloAvsecCourseData.duree}</p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                }

                                                {SoloAvsecCourseData.prix &&
                                                    <Col md={3}>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.price}</p>
                                                                <p className="course-small-gray-text">{SoloAvsecCourseData.prix}</p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                }

                                                {SoloAvsecCourseData.langue &&
                                                    <Col md={3}>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.language}</p>
                                                                <p className="course-small-gray-text">{SoloAvsecCourseData.langue}</p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                }

                                                {SoloAvsecCourseData.livraison &&
                                                    <Col md={3}>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.delivery}</p>
                                                                <p className="course-small-gray-text">{pickDeliveryByLang(SoloAvsecCourseData.livraison)}</p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                }
                                            </Row>
                                        </Col>
                                    </Row>

                                </Col>
                            </Row>

                            {/* Similar courses */}
                            <Row className="mt-5 mb-5">
                                <Col md={12}>
                                    <Row className="justify-content-between">
                                        {SoloAvsecCourseSimilarData && SoloAvsecCourseSimilarData.length > 0
                                            &&
                                            SoloAvsecCourseSimilarData.map((similar, i) =>
                                                <Col md={6} className="px-4">
                                                    <Row style={{ background: '#F4F4F4' }}>
                                                        <Col md={4} className="d-flex p-3">
                                                            <a href={`/solo-avsec/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                                <Image src={similar.image} fluid className="m-auto" alt={similar.titre} />
                                                            </a>
                                                        </Col>

                                                        <Col md={8} className="p-3">
                                                            <Row className="mb-2">
                                                                <Col md={12} className="d-flex">
                                                                    <a href={`/solo-avsec/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                                        <p className="course-blue-text">
                                                                            {similar.titre && similar.titre.substring(0, 30)}...
                                                                        </p>
                                                                    </a>
                                                                </Col>
                                                            </Row>

                                                            <Row>
                                                                <Col md={12}>
                                                                    {similar.description[0]
                                                                        &&
                                                                        <p className="course-small-gray-text">{similar.description[0].substring(0, 20)}...</p>
                                                                    }
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            )}
                                    </Row>
                                </Col>
                            </Row>
                        </Tablet>

                        <Mobile>
                            <div id="solo-avsec">
                                <Row className="my-2">
                                    <Col xs={12} className="d-flex">
                                        <p className="course-blue-text mr-auto">{SoloAvsecCourseData.titre}</p>
                                    </Col>
                                </Row>

                                <Row className="mb-2">
                                    <Col xs={12} className="d-flex mb-3">
                                        <Link to={`/registration/${convertToSlug(SoloAvsecCourseData.titre)}/${plat}/${id}`} className="w-100">
                                            {SoloAvsecCourseData.date
                                                ?
                                                <Button id="register-now-btn-orange" className="w-100 py-3">
                                                    {trans[siteLanguage].soloPage.subscribeButton}
                                                </Button>
                                                :
                                                <Button id="register-now-btn-orange" className="w-100 py-3">
                                                    {trans[siteLanguage].soloPage.trainingButton}
                                                </Button>
                                            }
                                        </Link>
                                    </Col>

                                    <Col xs={12} className="d-flex mb-3">
                                        {SoloAvsecCourseData && siteLanguage
                                            &&
                                            <span id="register-now-btn-white" className="w-100 py-3 text-center">
                                                <PDFDownloadLink document={<AvsecPdf data={SoloAvsecCourseData} siteLanguage={siteLanguage} />} fileName={`${SoloAvsecCourseData.titre}.pdf`} style={{ color: "#F47820", padding: '20px' }}>
                                                    {trans[siteLanguage].soloPage.brochureButton}
                                                </PDFDownloadLink>
                                            </span>
                                        }
                                    </Col>
                                </Row>

                                <Row>
                                    <Col xs={12} className="d-flex">
                                        <Image src={SoloAvsecCourseData.image_detail} className="m-auto w-100" style={{ height: '400px' }} alt={SoloAvsecCourseData.titre} />
                                    </Col>
                                </Row>

                                {SoloAvsecCourseData.objectif.length > 0
                                    &&
                                    <>
                                        <Row className="my-4">
                                            <Col xs={12}>
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.objectives}</p>
                                            </Col>
                                        </Row>

                                        <Row className="mt-4 mb-5">
                                            <ul>
                                                {SoloAvsecCourseData.objectif.map((obj, i) =>
                                                    <li key={i} className="course-small-gray-text">{obj}</li>
                                                )}
                                            </ul>
                                        </Row>
                                    </>
                                }

                                {SoloAvsecCourseData.description.length > 0
                                    &&
                                    <>
                                        <Row className="my-4">
                                            <Col xs={12}>
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.content}</p>
                                            </Col>
                                        </Row>

                                        <Row className="mt-4 mb-5">
                                            {SoloAvsecCourseData.description.map((desc, i) =>
                                                <Col xs={12} key={i} className="mb-1 d-flex">
                                                    <p className="course-small-gray-text mb-auto">{desc}</p>
                                                </Col>
                                            )}
                                        </Row>
                                    </>
                                }

                                {SoloAvsecCourseData.population
                                    &&
                                    <>
                                        <Row className="my-4">
                                            <Col xs={12}>
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.population}</p>
                                            </Col>
                                        </Row>

                                        <Row className="mt-4 mb-5">
                                            <Col xs={12}>
                                                <p className="course-small-gray-text">{SoloAvsecCourseData.population}</p>
                                            </Col>
                                        </Row>
                                    </>
                                }

                                {SoloAvsecCourseData.entry.length > 0
                                    &&
                                    <>
                                        <Row className="my-4">
                                            <Col xs={12}>
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.entry}</p>
                                            </Col>
                                        </Row>

                                        <Row className="mt-4 mb-5">
                                            {SoloAvsecCourseData.entry.map((entry, i) =>
                                                <Col xs={12} key={i} className="mb-1 d-flex">
                                                    <p className="course-small-gray-text mb-auto">{entry}</p>
                                                </Col>
                                            )}
                                        </Row>
                                    </>
                                }

                                {SoloAvsecCourseData.place
                                    &&
                                    <>
                                        <Row className="my-4">
                                            <Col xs={12}>
                                                <p className="course-light-blue-text-solo">{trans[siteLanguage].soloPage.place}</p>
                                            </Col>
                                        </Row>

                                        <Row className="mt-4 mb-5">
                                            <Col xs={12}>
                                                <p className="course-small-gray-text">{SoloAvsecCourseData.place}</p>
                                            </Col>
                                        </Row>
                                    </>
                                }

                                <Row className="d-flex justify-content-between my-3">
                                    <Col xs={12}>
                                        <Row className="justify-content-between">

                                            {SoloAvsecCourseData.duree &&
                                                <Col xs={3}>
                                                    <Row>
                                                        <Col xs={12}>
                                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.duration}</p>
                                                            <p className="course-small-gray-text">{SoloAvsecCourseData.duree}</p>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            }

                                            {SoloAvsecCourseData.price &&
                                                <Col xs={3}>
                                                    <Row>
                                                        <Col xs={12}>
                                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.price}</p>
                                                            <p className="course-small-gray-text">{SoloAvsecCourseData.price}</p>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            }

                                            {SoloAvsecCourseData.langue &&
                                                <Col xs={3}>
                                                    <Row>
                                                        <Col xs={12}>
                                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.language}</p>
                                                            <p className="course-small-gray-text">{SoloAvsecCourseData.langue} </p>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            }

                                            {SoloAvsecCourseData.livraison &&
                                                <Col xs={3}>
                                                    <Row>
                                                        <Col xs={12}>
                                                            <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.delivery}</p>
                                                            <p className="course-small-gray-text">{pickDeliveryByLang(SoloAvsecCourseData.livraison)}</p>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            }
                                        </Row>
                                    </Col>
                                </Row>
                            </div>
                            {/* SIMILAR COURSES */}
                            {SoloAvsecCourseSimilarData && SoloAvsecCourseSimilarData.length > 0
                                &&
                                <Row className="mt-5 mb-5">
                                    <Col xs={12}>
                                        <Row>
                                            {SoloAvsecCourseSimilarData.map((similar, i) =>
                                                <Col xs={12} key={i} className="mb-3 pl-4">
                                                    <Row style={{ background: '#F4F4F4' }}>
                                                        <Col xs={6} className="d-flex p-3">
                                                            <a href={`/solo-avsec/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                                <Image src={similar.image} fluid className="m-auto" style={{ width: '120px' }} alt={similar.titre && similar.titre} />
                                                            </a>
                                                        </Col>

                                                        <Col xs={12} className="p-3">
                                                            <Row className="mb-2">
                                                                <Col xs={12} className="d-flex">
                                                                    <a href={`/solo-avsec/${plat}/${similar.id}/${convertToSlug(similar.titre)}`}>
                                                                        <p className="course-blue-text">
                                                                            {similar.titre && similar.titre}
                                                                        </p>
                                                                    </a>
                                                                </Col>
                                                            </Row>

                                                            <Row>
                                                                <Col xs={12}>
                                                                    {similar.description[0] &&
                                                                        <p className="course-small-gray-text">
                                                                            {similar.description[0].substring(0, 53)}...
                                                                        </p>
                                                                    }
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            )}
                                        </Row>
                                    </Col>
                                </Row>
                            }
                        </Mobile>

                    </Container>

                    {/* BLOC SAFFICHE ON SCROLL */}
                    {hidden
                        &&
                        <div className="solo-page-sticky-btns">
                            <Row className="d-flex mx-0">
                                <Col md={6} className="d-flex py-3">
                                    <Link to={`/registration/${convertToSlug(SoloAvsecCourseData.titre)}/${plat}/${id}`} className="ml-auto">
                                        {SoloAvsecCourseData.date
                                            ?
                                            <Button id="register-now-btn-orange" className="p-3">
                                                {trans[siteLanguage].soloPage.subscribeButton}
                                            </Button>
                                            :
                                            <Button id="register-now-btn-orange" className="p-3">
                                                {trans[siteLanguage].soloPage.trainingButton}
                                            </Button>
                                        }
                                    </Link>
                                </Col>

                                <Col md={6} className="d-flex py-3">
                                    {SoloAvsecCourseData && siteLanguage
                                        &&
                                        <span id="register-now-btn-white" className="p-3 text-center mr-auto">
                                            <PDFDownloadLink document={<AvsecPdf data={SoloAvsecCourseData} siteLanguage={siteLanguage} />} fileName={`${SoloAvsecCourseData.titre}.pdf`} style={{ color: "#F47820", padding: '20px' }}>
                                                {trans[siteLanguage].soloPage.brochureButton}
                                            </PDFDownloadLink>
                                        </span>
                                    }
                                </Col>
                            </Row>
                        </div>
                    }

                    <div id="header-avsec" style={{ display: headerPdf }}>
                        <Image src={PDF_HEADER} />
                    </div>

                    <Row className="mb-3" id="pdf-title" style={{ display: headerPdf }}>
                        <Col lg={12}>
                            <p className="course-blue-text" style={{ fontSize: '34px' }}>{SoloAvsecCourseData.titre}</p>
                        </Col>
                    </Row>

                </Container>
            }
        </>
    );
}


const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
    SoloAvsecCourseData: state.afsacR.SoloAvsecCourseData,
    SoloAvsecCourseSimilarData: state.afsacR.SoloAvsecCourseSimilarData,
    brochure: state.afsacR.brochure,
});

export default connect(mapStateToProps, { getSoloAvsecCourse, getBrochure })(SoloAvsec);
