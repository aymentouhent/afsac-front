import React, { useState, useEffect } from 'react';

import {
    Container,
    Row,
    Col,
    Image,
    Form,
} from 'react-bootstrap';

import { images } from '../../constants/AppConfig';

// Checkbox
import CheckBox from '../CheckBox';
import AvsecCourse from './AvsecCourse';
import { Link } from "react-router-dom";

import { connect } from 'react-redux';
import { trans } from '../../constants/Translations';


function Courses(props) {

    let {
        siteLanguage,
        courseTitle,
        categories,
        courses,
        platform,
    } = props;

    // Search:
    // const [seeMore,setSeeMore] = useState(5);
    const [searchCourse, setSearchCourse] = useState(false);
    const [searchWorkShop, setSearchWorkShop] = useState(false);
    const [courseKeyword, setCourseKeyword] = useState('');
    const [categorySelected, setCategorySelected] = useState('');
    const [filteredCourses, setFiltredCourses] = useState([]);
    const [filtredCategories, setFiltredCategories] = useState([]);
    const [noResults, setNoResults] = useState(false);

    const handleSearchCheckBox = (searchBy) => {
        setCourseKeyword("");

        if (searchBy === "Course") {
            setSearchWorkShop(false);

            if (searchCourse) {
                setSearchCourse(false);
                setFiltredCourses([]);
            } else {
                setSearchCourse(true);
                // Find type === course
                let filteredCourses = courses.filter(course => course.categorie === "courses");
                // console.log("Result courses =>", filteredCourses);
                setFiltredCourses(filteredCourses);
            }
        } else {
            setSearchCourse(false);

            if (searchWorkShop) {
                setSearchWorkShop(false);
                setFiltredCourses([]);
            } else {
                setSearchWorkShop(true);
                let filteredCourses = courses.filter(course => course.categorie === "workshop");
                // console.log("Result workshoop =>", filteredCourses);
                setFiltredCourses(filteredCourses)
            }
        }
    };

    const handleCategorySelected = (e) => {
        setCategorySelected(e.target.value);
        setNoResults(false);

        if (e.target.value === "all") {
            setFiltredCourses([]);
            setNoResults(false);
            // setSeeMore(5);
        } else {
            setCourseKeyword("");
            let filteredCourses = courses.filter(course => course.categorie == e.target.value && course.titre !== null);
            console.log("Result avsec cats =>", filteredCourses);
            setFiltredCourses(filteredCourses);
            if (filteredCourses.length === 0) setNoResults(true);
            // setSeeMore(5);
        }
    };

    const handleSearchKeyword = (e) => {
        setSearchWorkShop(false);
        setSearchCourse(false);
        setFiltredCourses([]);

        setCourseKeyword(e.target.value);
        if (e.target.value === "") {
            setFiltredCourses([]);
            setNoResults(false);
        } else {
            // console.log("Let search by", e.target.value);
            let filtered = [];
            courses.map(course => {
                if (course.titre) {
                    let courseLowercase = (course.titre).toLowerCase();
                    let searchCourseLowercase = e.target.value.toLowerCase();
                    if (courseLowercase.indexOf(searchCourseLowercase) > -1) {
                        filtered.push(course);
                    };
                }
            });
            setFiltredCourses(filtered);

            if (filtered.length === 0) {
                setNoResults(true);
            }
        }
    };

    const clearFilters = () => {
        setSearchCourse(false);
        setSearchWorkShop(false);
        setCourseKeyword('');
        setCategorySelected('all');
    };

    const pickCategoryPhoto = (cat_id) => {
        let found = categories.find((cat) => cat.id === cat_id)

        if (found) {
            return found.image;
        } else {
            return '';
        }
    };

    // Filtrage de categories enlévé par nesrine !
    // useEffect(() => {
    //     if(categories) {
    //         // console.log("cats", categories);
    //         // console.log("courses", courses);
    //         let filtredCategories = [];

    //         categories.map(category => {
    //             for(let i=0; i < courses.length; i++) {
    //                 if(parseInt(courses[i].categorie) === category.id) {
    //                     // console.log("Exists! ", category.nom);
    //                     filtredCategories.push(category);
    //                     break;
    //                 } 
    //             };
    //         });
    //         // console.log("Filtrd !", filtredCategories);

    //         setFiltredCategories(filtredCategories);
    //     }
    // },[categories])

    return (
        <>
            {/* Image Banner */}
            <Container fluid>
                <Row className="d-flex">
                    <Image src={images.courses_banner} style={{ width: '100%' }} alt="afsac-banner" />
                </Row>
            </Container>

            <Container>
                {/* Title */}
                <div className=" Breadcrumb d-flex mb-3 px-0 ">
                    <div className="breadcrumb-item"> <Link to={'/'}>{trans[siteLanguage].breadcrumb.home}</Link></div>

                    <div className="breadcrumb-item active" >{courseTitle} </div>
                </div>

                <Row className="my-4">
                    <Col xs={12} lg={12} className="d-flex flex-column">
                        <h3 className="contact-title mr-auto ">{courseTitle}</h3>
                        <Image src={images.orange_divider} className="mr-auto" alt="afsac-divider" />
                    </Col>
                </Row>

                {/* Search Box */}
                <Row className="my-4 mt-5 p-4 mx-auto search-box-courses">

                    {!categories
                        &&
                        <Col xs={12} className="d-flex m-3">
                            <div onClick={() => handleSearchCheckBox('Course')}>
                                <CheckBox checked={searchCourse} label={trans[siteLanguage].coursesSearchBox.checkboxCours} />
                            </div>

                            <div onClick={() => handleSearchCheckBox('Workshop')}>
                                <CheckBox checked={searchWorkShop} label={trans[siteLanguage].coursesSearchBox.checkboxWorkshop}  />
                            </div>
                        </Col>
                    }

                    <Col lg={12} className="mb-3 mt-2">
                        <Row className="justify-content-between">
                            {filtredCategories.length > 0
                                ?
                                <Col xs={12} md={6} lg={4}>
                                    <p className="search-input-label">{trans[siteLanguage].coursesSearchBox.categoriesLabel}</p>
                                    <Form.Group controlId="exampleForm.SelectCustom">
                                        <Form.Control as="select" id="search-input-courses" value={categorySelected} onChange={handleCategorySelected} custom style={{ height: '50px' }}>
                                            <option value={"all"}>{trans[siteLanguage].coursesSearchBox.categoriesPlaceholder}</option>
                                            {filtredCategories.map((cat, i) =>
                                                <option key={i} value={`${cat.id}`}>{cat.nom}</option>
                                            )}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                                :
                                categories
                                &&
                                <Col xs={12} md={6} lg={3}>
                                    <p className="search-input-label">{trans[siteLanguage].coursesSearchBox.categoriesLabel}</p>
                                    <Form.Group controlId="exampleForm.SelectCustom">
                                        <Form.Control as="select" id="search-input-courses" value={categorySelected} onChange={handleCategorySelected} custom style={{ height: '50px' }}>
                                            <option value={"all"}>{trans[siteLanguage].coursesSearchBox.categoriesPlaceholder}</option>
                                            {categories.map((cat, i) =>
                                                <option key={i} value={`${cat.id}`}>{cat.nom}</option>
                                            )}
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                            }

                            <Col xs={12} md={6} lg={3}>
                                <p className="search-input-label">{trans[siteLanguage].coursesSearchBox.keywordLabel}</p>
                                <Form.Control type={"text"} id="search-input-courses" className="p-2 w-100" placeholder={trans[siteLanguage].coursesSearchBox.keywordPlaceholder} style={{ height: '50px' }} value={courseKeyword} onChange={handleSearchKeyword} />
                            </Col>

                            <Col xs={12} md={6} lg={3} >
                                <p style={{ color: '#F4F4F4' }}>cl</p>
                                <button className="m-auto p-2 w-100 search-courses-btn" style={{ height: '50px' }}>{trans[siteLanguage].coursesSearchBox.searchButton}</button>
                            </Col>

                            <Col xs={12} md={6} lg={3}>
                                <p style={{ color: '#F4F4F4' }}>cl</p>
                                <p className="text-center search-courses-clear-btn py-3" onClick={clearFilters}>{trans[siteLanguage].coursesSearchBox.clearButton}</p>
                            </Col>
                        </Row>
                    </Col>
                </Row>

                {/* {noResults 
                &&
                <Row className="my-4">
                    <Col xs={12} className="d-flex">
                        <h3 className="contact-title m-auto " style={{fontSize: '20px'}}>{trans[siteLanguage].coursesSearchBox.noResultsText}</h3>
                    </Col>
                </Row>
                } */}

                {
                    filteredCourses.length === 0 && noResults
                        ?
                        <>
                            <Row className="my-4">
                                <Col xs={12} className="d-flex">
                                    <h3 className="contact-title m-auto " style={{ fontSize: '20px' }}>{trans[siteLanguage].coursesSearchBox.noResultsText}</h3>
                                </Col>
                            </Row>
                            <div style={{ height: '150px' }} />
                        </>
                        :
                        filteredCourses.length > 0
                            ?
                            filteredCourses.map((course, i) =>
                                // i < seeMore &&
                                course.titre !== null &&
                                <AvsecCourse
                                    id={course.id}
                                    title={course.titre}
                                    description={course.description}
                                    video={course.video}
                                    category={course.categorie}
                                    categories={categories}
                                    // image={course.image}
                                    image={categories ? pickCategoryPhoto(parseInt(course.categorie)) : images.security_img /*course.image*/}
                                    duration={course.duree}
                                    delivery={course.livraison}
                                    price={course.prix}
                                    language={course.langue}
                                    objectif={course.objectif}
                                    inscription={course.inscription}
                                    population={course.population}
                                    place={course.place}
                                    entry={course.entry}
                                    image_detail={course.image_detail}
                                    platform={platform}
                                    date={course.hasOwnProperty('date') ? course.date : null}
                                />
                            )
                            :
                            courses.length > 0
                            &&
                            courses.map((course, i) =>
                                // i < seeMore &&
                                course.titre !== null &&
                                <AvsecCourse
                                    id={course.id}
                                    title={course.titre}
                                    description={course.description}
                                    video={course.video}
                                    category={course.categorie}
                                    categories={categories}
                                    // image={course.image}
                                    image={categories ? pickCategoryPhoto(parseInt(course.categorie)) : images.security_img /*course.image*/}
                                    duration={course.duree}
                                    delivery={course.livraison}
                                    price={course.prix}
                                    language={course.langue}
                                    objectif={course.objectif}
                                    inscription={course.inscription}
                                    population={course.population}
                                    place={course.place}
                                    entry={course.entry}
                                    image_detail={course.image_detail}
                                    platform={platform}
                                    date={course.hasOwnProperty('date') ? course.date : null}
                                />
                            )
                }

                {/* <Row>
                    <Col xs={12} className="d-flex mt-5 mb-4">
                        <p className="m-auto course-light-blue-text" style={{ textDecoration: 'underline', cursor: 'pointer',fontSize:'20px' }} onClick={() => setSeeMore(prevState => prevState+=5)}>SEE MORE</p>
                    </Col>
                </Row> */}
            </Container>
        </>
    )
};

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
});

export default connect(mapStateToProps, {})(Courses);

