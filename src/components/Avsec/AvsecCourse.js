import React from 'react';

import {
    Row,
    Col,
    Image
} from 'react-bootstrap';

import {
    Link
} from 'react-router-dom';

import { Desktop, Tablet, Mobile } from '../../constants/AppConfig';

import { connect } from 'react-redux';
import { trans } from '../../constants/Translations';


function Course(props) {

    let {
        siteLanguage,
        id,
        title,
        description,
        category,
        categories,
        image,
        duration,
        delivery,
        price,
        language,
        date,
        // video,
        // objectif,
        // inscription,
        // population,
        // place,
        // entry,
        // image_detail,

        // For get Solo Page Api
        platform,
    } = props;

    const pickCategoryName = (cat_id) => {
        let found = categories.find((cat) => cat.id == cat_id)

        if (found) {
            return found.nom
        } else {
            return '';
        }
    };

    const pickCategoryByLang = (cat) => {
        if (cat.toLowerCase() == "workshop") {
            return trans[siteLanguage].courseCard.workshop
        } else {
            return trans[siteLanguage].courseCard.course
        }
    };

    const pickDeliveryByLang = (del) =>
    {
        switch (del)
        {
            case "virtual classroom" : return trans[siteLanguage].courseCard.virtualClassroom;
            break;
            case "online" : return trans[siteLanguage].courseCard.online
            break;
            default : return trans[siteLanguage].courseCard.classroom
            break;
        }
        /*
        if (del == "virtual classroom")
        {
            return trans[siteLanguage].courseCard.virtualClassroom
        }

        else {
            return trans[siteLanguage].courseCard.classroom
        } */
    }

    const convertToSlug = (title) => {
        return title
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-');
    };

    return (
        <Row key={id} className="courses-item mb-3 px-2 py-3 mx-auto" style={{ background: '#F4F6F9' }}>
            <Desktop>
                <Col lg={3} className="align-self-center">
                    {/* <Image src={images.course_img} fluid className="m-auto" />  */}
                    <div className="course-small-image" style={{ backgroundImage: `url(${image})` }} />
                </Col>

                <Col lg={9} className="d-flex flex-column  px-0">
                    <Row className="justify-content-between d-flex mt-auto" >
                        <Col lg={7}>
                            <p className="course-light-blue-text">{categories ? pickCategoryName(category) : pickCategoryByLang(category)}</p>
                        </Col>

                        <Col lg={3} className="d-flex mr-3">
                            <p className="course-gray-date-text ml-auto">{date}</p>
                        </Col>
                    </Row>

                    <Row className="mt-auto">
                        <Col lg={10}>
                            <Link to={`/solo-avsec/${platform}/${id}/${convertToSlug(title)}`}>
                                <p className="course-blue-text">{title && title}</p>

                            </Link>
                        </Col>
                    </Row>

                    <Row className="mt-auto">
                        <Col lg={10}>
                            <p className="course-gray-text" style={{ color: "black" }}>{description && description.substring(0, (description.length)/2 + ((description.length)/2)/2 )}...</p>
                        </Col>
                    </Row>

                    <Row className="d-flex justify-content-between mt-1">
                        <Col lg={8} md={12}>
                            <Row >
                                {duration &&
                                    <Col lg={3}>
                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.duration}</p>
                                                <p className="course-small-gray-text">{duration}</p>
                                            </Col>
                                        </Row>
                                    </Col>
                                }

                                {delivery &&
                                    <Col lg={4}>
                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.delivery}</p>
                                                <p className="course-small-gray-text">{pickDeliveryByLang(delivery)}</p>
                                            </Col>
                                        </Row>
                                    </Col>
                                }

                                {price &&
                                    <Col lg={2}>
                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.price}</p>
                                                <p className="course-small-gray-text">{price}</p>
                                            </Col>
                                        </Row>
                                    </Col>
                                }
                                {language &&
                                    <Col lg={2}>
                                        <Row>
                                            <Col lg={12}>
                                                <p className="course-small-light-blue-text mb-2">{trans[siteLanguage].soloPage.language}</p>
                                                <p className="course-small-gray-text">{language}</p>
                                            </Col>
                                        </Row>
                                    </Col>
                                }
                            </Row>
                        </Col>

                        <Col lg={4} md={12} >
                            <Link to={`/solo-avsec/${platform}/${id}/${convertToSlug(title)}`}>
                                <button className="search-plus-btn py-2" style={{ cursor: 'pointer' }}>{trans[siteLanguage].courseCard.seeMoreButton}</button>
                            </Link>
                        </Col>
                    </Row>
                </Col>
            </Desktop>

            <Tablet>
                <Row>
                    <Col xs={12} className="d-flex mb-2">
                        {/* <Image src={images.course_img} fluid className="mr-auto" />  */}
                        <div className="course-small-image" style={{ backgroundImage: `url(${image})` }} />
                    </Col>

                    <Col xs={12} className="d-flex mb-2">
                        <p className="course-light-blue-text">{categories ? pickCategoryName(category) : pickCategoryByLang(category)}</p>
                    </Col>

                    <Col xs={12} className="d-flex mb-2">
                        <Link to={`/solo-avsec/${platform}/${id}/${convertToSlug(title)}`}>
                            <p className="course-blue-text">{title}</p>
                        </Link>
                    </Col>

                    <Col xs={12} className="mb-2">
                        <p className="course-gray-date-text">{date}</p>
                    </Col>

                    <Col xs={12}>
                        <p className="course-gray-text">{description && description.substring(0, 40)}...</p>
                    </Col>

                    <Col xs={12}>
                        <Row className="justify-content-between">
                            {duration &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.duration}</p>
                                            <p className="course-small-gray-text">{duration}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            }

                            {delivery &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.delivery}</p>
                                            <p className="course-small-gray-text">{pickDeliveryByLang(delivery)}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            }

                            {price &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.price}</p>
                                            <p className="course-small-gray-text">{price}</p>

                                        </Col>
                                    </Row>
                                </Col>
                            }

                            {language &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.language}</p>
                                            <p className="course-small-gray-text">{language}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            }


                        </Row>
                    </Col>

                    <Col xs={12} className="d-flex mt-4  justify-content-end">
                        <Link to={`/solo-avsec/${platform}/${id}/${convertToSlug(title)}`} >
                            <button className="search-plus-btn py-2 px-3" style={{ cursor: 'pointer', width: '350px' }}>{trans[siteLanguage].courseCard.seeMoreButton}</button>
                        </Link>
                    </Col>
                </Row>
            </Tablet>

            <Mobile>
                <Row>
                    <Col xs={12} className="d-flex mb-2">
                        <div className="course-small-image" style={{ backgroundImage: `url(${image})` }} />
                    </Col>

                    <Col xs={12} className="d-flex mb-2">
                        <p className="course-light-blue-text">{categories ? pickCategoryName(category) : pickCategoryByLang(category)}</p>
                    </Col>

                    <Col xs={12} className="d-flex mb-2">
                        <Link to={`/solo-avsec/${platform}/${id}/${convertToSlug(title)}`}>
                            <p className="course-blue-text">{title}</p>
                        </Link>
                    </Col>

                    <Col xs={12} className="mb-2">
                        <p className="course-gray-date-text">{date}</p>
                    </Col>

                    <Col xs={12}>
                        <p className="course-gray-text">{description && description.substring(0, 40)}...</p>
                    </Col>

                    <Col xs={12}>
                        <Row className="justify-content-between">

                            {duration &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.duration}</p>
                                            <p className="course-small-gray-text">{duration}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            }

                            {delivery &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.delivery}</p>
                                            <p className="course-small-gray-text">{pickDeliveryByLang(delivery)}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            }

                            {price &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.price}</p>
                                            <p className="course-small-gray-text">{price}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            }

                            {language &&
                                <Col xs={3}>
                                    <Row>
                                        <Col xs={12}>
                                            <p className="course-small-light-blue-text mb-1">{trans[siteLanguage].soloPage.language}</p>
                                            <p className="course-small-gray-text">{language}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            }


                        </Row>
                        <Row>
                            <Col lg={12} className="d-flex justify-content-end mt-4">
                                <Link to={`/solo-avsec/${platform}/${id}/${convertToSlug(title)}`}>
                                    <button className="search-plus-btn py-2 px-3" style={{ cursor: 'pointer', width: '250px' }}>{trans[siteLanguage].courseCard.seeMoreButton}</button>
                                </Link>
                            </Col>
                        </Row>
                    </Col>

                </Row>
            </Mobile>
        </Row>
    );
};

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
});

export default connect(mapStateToProps, {})(Course);


