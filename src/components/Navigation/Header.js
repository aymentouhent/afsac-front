import React, { useState } from 'react'
import '../components.css';
import { NavLink, Navbar, FormControl, Container, Row, Col, Image } from 'react-bootstrap';
import { Link, useLocation, withRouter, useHistory } from 'react-router-dom';
import { images, Desktop, Tablet, Mobile } from '../../constants/AppConfig';
import { trans } from '../../constants/Translations';

import { connect } from 'react-redux';
import { setSiteLanguage,setSiteLanguageEn,setSiteLanguageFr, getUpcommingCourses, searchCourses } from '../../redux/actions/AfsacActions';

import OutsideClickHandler from 'react-outside-click-handler';
import Slider from "react-slick";

import { IoIosArrowDown, IoIosArrowUp, IoIosArrowForward, IoIosArrowBack } from 'react-icons/io';
import { AiOutlineMenu, AiOutlineSearch, AiOutlineClose } from 'react-icons/ai';

// import Layout from '../Layout';


function Header(props) {

    let {
        siteLanguage,
        sessionsTppCoursesData,

        setSiteLanguage,
        searchCourses,
        getUpcommingCourses,
    } = props;

    const promoSettings = {
        dots: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        speed: 500,
        autoplaySpeed: 5000,
        cssEase: "linear",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    const [showMenu, setShowMenu] = useState(false);
    const [showSessionsMenu, setShowSessionsMenu] = useState(false);
    const [showMobileMenu, setShowMobileMenu] = useState(false);
    const [SubMobileMenu, setSubMobileMenu] = useState(null);
    const [SubMobileMenuSession, setSubMobileMenuSession] = useState(null);
    const [showMobileSearch, setShowMobileSearch] = useState(false);
    const [searchValue, setSearchValue] = useState('');
    const location = useLocation();
    const history = useHistory();

    // LANGUAGE :
    const [showLanguage, setShowLanguage] = useState(false);
    const languages = ['en', 'fr', /*'ar'*/];

    // Check if link is active
    const activateLink = (link) => location.pathname === link;

    const convertToSlug = (title) => {
        return title
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-');
    };


    const resetMenu = () => {
        setShowSessionsMenu(false);
        setShowMobileMenu(false);
        setSubMobileMenu(false);
        setSubMobileMenuSession(false);
        setShowMobileSearch(false);
    };

    const navigateTo = (link) => {
        setShowMenu(false);
        setShowSessionsMenu(false);
        props.history.push(link);
    };

    const getLanguageName = (lang) => {

        if (lang === "en") {
            return "English"
        } else if (lang === "fr") {
            return "Français"
        } else {
            return "العربية"
        };
    };

    const redirectToSearch = (value) => {
        if (history.location.pathname.includes("/search")) {
            history.push(`/search/${value}`);
            window.location.reload();
        } else {
            history.push(`/search/${value}`);
        }
    }

    return (
        <>
            <Desktop>
                <Container fluid style={{ background: "#FFF" }}>
                    <Col lg={12} className="d-flex flex-column pt-1 pb-0 px-0" >
                        <Link to="/">
                            {siteLanguage === "en"
                                ?
                                <Image src={images.logo} className="logo" alt="afsac-logo" />
                                :
                                <Image src={images.logo_fr} className="logo" alt="afsac-logo" />
                            }
                        </Link>
                        <Row className="d-flex justify-content-center">
                            <Col lg={{ span: 10, offset: 2 }}>
                                <Row className="justify-content-end d-flex">
                                    <Link to="/contact" className="my-auto mr-4"><p className="header-contact-text my-auto">{trans[siteLanguage].navbar.hasOwnProperty("contact") && trans[siteLanguage].navbar.contact}</p></Link>

                                    {/* Social icons */}
                                    <Row className="d-flex mr-4">
                                        <a href='https://twitter.com/astcicao' target="_blank" >
                                            <Image src={images.twitter_logo} className="header-social-icon my-auto mr-3" style={{ maxWidth: '20px', maxHeight: '20px' }} alt="afsac-social-icon" />
                                        </a>

                                        <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >
                                            <Image src={images.fb_icon} className="header-social-icon my-auto mr-3" style={{ maxWidth: '20px', maxHeight: '20px' }} alt="afsac-social-icon" />
                                        </a>

                                        <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" >
                                            <Image src={images.lkin_logo} className="header-social-icon my-auto mr-3" style={{ maxWidth: '20px', maxHeight: '20px' }} alt="afsac-social-icon" />
                                        </a>

                                        <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank" >
                                            <Image src={images.youtube_logo} className="header-social-icon my-auto mr-3" style={{ width: '28.44px', maxHeight: '20px' }} alt="afsac-social-icon" />
                                        </a>
                                    </Row>

                                    <Row className="d-flex mr-4" style={{ border: "2px solid #BCBCBC", borderRadius: '4px' }}>
                                        <Col lg={10} className="d-flex p-0 my-auto">
                                            <FormControl aria-describedby="search-bar" className="m-auto" id="search-input" value={searchValue} onChange={(e) => setSearchValue(e.target.value)} onKeyDown={(e) => e.key === "Enter" && redirectToSearch(e.target.value)} />
                                        </Col>

                                        <Col lg={2} className="d-flex p-0 my-auto">
                                            <Image style={{ maxWidth: 18, cursor: 'pointer' }} src={images.search_icon} alt="afsac-search-icon" className="m-auto" onClick={(e) => e.key === "Enter" && redirectToSearch(e.target.value)} />
                                        </Col>
                                    </Row>

                                    {/* LANGUAGE SELECTION */}
                                    <Row className="d-flex justify-content-start">
                                        <Col xs={4} className="d-flex">
                                            <Image src={images.en_flag} className="my-auto" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("en", history)} />
                                        </Col>
                                        <Col xs={4} className="d-flex">
                                            <Image src={images.fr_flag} className="my-auto mr-auto px-0" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("fr", history)} />
                                        </Col>
                                        {/* <div className="header-language-select d-flex" onClick={() => setShowLanguage(!showLanguage)}>
                                        <p className="my-auto ml-3">{getLanguageName(siteLanguage)}</p>
                                        <IoIosArrowForward className="my-auto ml-2" size={16} />
                                    </div>

                                    {showLanguage &&
                                    <div className="header-language-select d-flex flex-column" style={{ position: 'absolute', zIndex: 999, top: '32%', height: '50px' }}>
                                        {languages.map((lang, i) =>
                                            lang !== siteLanguage
                                            &&
                                            <p key={i} className="ml-3 my-auto" onClick={() => setSiteLanguage(lang, history)}>{getLanguageName(lang)}</p>
                                        )}
                                    </div>
                                    } */}
                                    </Row>
                                </Row>

                                {/* TWO AFSAC LOGOS */}
                                <Row className="d-flex justify-content-end mt-2" >
                                    <Image src={images.logo_3} alt="afsac-logo1" style={{ maxWidth: '135px', maxHeight: '53px' }} className="mr-5" />
                                    <Image src={images.logo_2} style={{ maxWidth: '135px', maxHeight: '73px' }} alt="afsac-logo2" className="mr-3" />
                                </Row>

                            </Col>
                        </Row>

                        {/* Menu items */}
                        <Row className="header  d-flex" style={{ backgroundColor: '#00A7E2' }} >
                            {/* <Col lg={3} xl={0} /> */}
                            <Col lg={{ span: 10, offset: 2 }} >
                                <Navbar className="row d-flex " >


                                    <NavLink className={activateLink("/about-us") ? "header-nav-item-active d-flex" : "header-nav-item d-flex"} onClick={() => navigateTo('/about-us')}>
                                        {trans[siteLanguage].navbar.aboutUs}
                                    </NavLink>


                                    <div className="divi"></div>

                                    <NavLink className={activateLink("/training") ? "header-nav-item-active d-flex" : "header-nav-item d-flex"} onClick={() => navigateTo('/training')}>
                                        {trans[siteLanguage].navbar.training}
                                    </NavLink>
                                    {/* <NavLink className={showMenu ? "header-nav-item-active d-flex px-2" : "header-nav-item d-flex px-2"} onClick={() => { setShowSessionsMenu(false); setShowMenu(!showMenu); }}>
                                        <Link to="#" className="mr-auto my-auto">
                                            {trans[siteLanguage].navbar.training}
                                        </Link>
                                        {showMenu
                                            ?
                                            <IoIosArrowUp className="ml-2 my-auto header-nav-item-icon" size={20} />
                                            :
                                            <IoIosArrowDown className="ml-2 my-auto header-nav-item-icon" size={20} />
                                        }
                                    </NavLink> */}
                                    <div className="divi"></div>
                                    <NavLink className={activateLink("/session") ? "header-nav-item-active d-flex" : "header-nav-item d-flex"} onClick={() => navigateTo('/session')}>
                                        {trans[siteLanguage].navbar.sessions}
                                    </NavLink>
               {/*                      <NavLink className={showSessionsMenu ? "header-nav-item-active d-flex" : "header-nav-item d-flex"} onClick={() => { setShowMenu(false); setShowSessionsMenu(!showSessionsMenu); }}>
                                        <Link to="#" className="mr-auto my-auto">
                                            {trans[siteLanguage].navbar.sessions}
                                        </Link>
                                        {showSessionsMenu
                                            ?
                                            <IoIosArrowUp className="ml-2 my-auto header-nav-item-icon" size={20} />
                                            :
                                            <IoIosArrowDown className="ml-2 my-auto header-nav-item-icon" size={20} />
                                        }
                                    </NavLink> */}

                                    <div className="divi"></div>
                                    <NavLink className={activateLink("/our-accreditations-and-recognitions") ? "header-nav-item-active d-flex" : "header-nav-item d-flex"} onClick={() => navigateTo('/our-accreditations-and-recognitions')}>
                                        {trans[siteLanguage].navbar.accredations}
                                    </NavLink>

                                    <div className="divi"></div>

                                    <NavLink className={activateLink("/media-news") ? "header-nav-item-active d-flex" : "header-nav-item d-flex"} onClick={() => navigateTo('/media-news')}>
                                        {trans[siteLanguage].navbar.news}
                                    </NavLink>
                                </Navbar>
                            </Col>

                        </Row>

                    </Col>

                    {showMenu &&
                        <OutsideClickHandler onOutsideClick={() => { setShowMenu(false); }}>
                            <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "#F4F6F9", zIndex: 999 }} onClick={() => setShowSessionsMenu(false)}>
                                <Container className="p-5">
                                    <Row>
                                        <Col>
                                            <h2 className="header-dropdown-title">Training</h2>
                                            <p  className="header-dropdown-description">{trans[siteLanguage].navbar.trainingDesc}</p>
                                            {/* <Link style={{ textDecoration: "none" }}>
                                        <span className="header-dropdown-small-title">Discover Courses {">"} </span>
                                    </Link> */}
                                        </Col>

                                        <Col className="d-flex flex-column">
                                            <Link style={{ textDecoration: "none" }} to="/training-avsec" >
                                                <div className="d-flex" onClick={() => setShowMenu(false)}>
                                                    <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.avsecCourses}</p>
                                                    <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                                </div>
                                            </Link>
                                            <hr className="lineheader" />

                                            <Link style={{ textDecoration: "none" }} to="/training-tpp" className="my-auto">
                                                <div className="d-flex" onClick={() => setShowMenu(false)}>
                                                    <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.tppCourses}</p>
                                                    <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                                </div>
                                            </Link>
                                            <hr className="lineheader" />
                                            <Link style={{ textDecoration: "none" }} to="/training-virtual-classroom" className="my-auto">
                                                <div className="d-flex" onClick={() => setShowMenu(false)}>
                                                    <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.virtualCourses}</p>
                                                    <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                                </div>
                                            </Link>
                                            <hr className="lineheader" />
                                            <Link style={{ textDecoration: "none" }} to="/training-online-courses" className="mt-auto">
                                                <div className="d-flex" onClick={() => setShowMenu(false)}>
                                                    <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.onlineCourses}</p>
                                                    <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                                </div>
                                            </Link>
                                            <hr className="lineheader" />
                                        </Col>

                                        <Col>
                                            <Image src={images.session_img} style={{ width: "100%" }} alt="afsac-session-img" />
                                        </Col>
                                    </Row>
                                </Container>
                            </div>
                        </OutsideClickHandler>
                    }

                    {showSessionsMenu &&
                        <OutsideClickHandler onOutsideClick={() => setShowSessionsMenu(false)}>
                            <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "#F4F6F9", zIndex: 999 }} onClick={() => setShowMenu(false)}>
                                <Container className="p-5">
                                    <Row>
                                        <Col>
                                            <h2 className="header-dropdown-title">{trans[siteLanguage].navbar.sessions}</h2>

                                            <p className="header-dropdown-description">{trans[siteLanguage].navbar.sessionsDesc}</p>

                                            {/* <Link style={{ textDecoration: "none" }}>
                                        <span className="header-dropdown-small-title">Discover Courses {">"} </span>
                                    </Link> */}
                                        </Col>

                                        <Col className="d-flex flex-column">
                                            <Link style={{ textDecoration: "none" }} to="/sessions-avsec" className="mb-auto">
                                                <div className="d-flex" onClick={() => setShowSessionsMenu(false)}>
                                                    <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.avsecCourses}</p>
                                                    <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                                </div>
                                            </Link>
                                            <hr className="lineheader" />
                                            <Link style={{ textDecoration: "none" }} to="/sessions-tpp" className="my-auto">
                                                <div className="d-flex" onClick={() => setShowSessionsMenu(false)}>
                                                    <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.tppCourses}</p>
                                                    <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                                </div>
                                            </Link>
                                            <hr className="lineheader" />
                                            <Link style={{ textDecoration: "none" }} to="/sessions-virtual" className="my-auto">
                                                <div className="d-flex" onClick={() => setShowSessionsMenu(false)}>
                                                    <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.virtualCourses}</p>
                                                    <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                                </div>
                                            </Link>
                                            <hr className="lineheader" />
                                            <Link style={{ textDecoration: "none" }} to="/training-online-courses" className="mt-auto">
                                                <div className="d-flex" onClick={() => setShowSessionsMenu(false)}>
                                                    <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.onlineCourses}</p>
                                                    <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                                </div>
                                            </Link>
                                            <hr className="lineheader" />
                                        </Col>

                                        <Col>
                                            <Image src={images.sessions_header_img} style={{ width: "100%" }} alt="afsac-session-img" />
                                        </Col>
                                    </Row>
                                </Container>
                            </div>
                        </OutsideClickHandler>
                    }
                </Container>
            </Desktop>

            <Tablet>
                <Container fluid style={{ backgroundColor: '#FFF' }}>
                    <Col xs={12} className="d-flex px-0 py-3 justify-content-between">
                        <div className="d-flex">
                            {SubMobileMenu || SubMobileMenuSession
                                ?
                                <IoIosArrowBack size={35} color={"#0056A1"} className="m-auto" style={{ cursor: 'pointer' }} onClick={() => { setSubMobileMenu(null); setSubMobileMenuSession(null); setShowMobileMenu(true); }} />
                                :
                                !showMobileMenu
                                    ?
                                    <AiOutlineMenu size={35} color={"#0056A1"} className="m-auto" style={{ cursor: 'pointer' }} onClick={() => { setShowMobileSearch(false); setShowMobileMenu(prevState => !prevState) }} />
                                    :
                                    <AiOutlineClose size={35} color={"#0056A1"} className="m-auto" style={{ cursor: 'pointer' }} onClick={() => { setShowMobileMenu(false) }} />
                            }
                        </div>

                        <div className="d-flex">
                            <Link to="/">
                                {siteLanguage === "en"
                                    ?
                                    <Image src={images.logo} fluid style={{ maxWidth: "150px" }} className="m-auto" alt="afsac-logo" />
                                    :
                                    <Image src={images.logo_fr} fluid style={{ maxWidth: "150px" }} className="m-auto" alt="afsac-logo" />
                                }
                            </Link>
                        </div>

                        <div className="d-flex">
                            <AiOutlineSearch size={35} color={"#0056A1"} className="m-auto" style={{ cursor: 'pointer' }} onClick={() => { setShowMobileMenu(false); setShowMobileSearch(prevState => !prevState) }} />
                        </div>
                    </Col>

                    {/* Mobile Search */}
                    {showMobileSearch &&
                        <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "white", zIndex: 999 }}>
                            <Row className="p-3">
                                <Col xs={3} />

                                <Col xs={6} className="d-flex">
                                    <input type="search" placeholder={trans[siteLanguage].navbar.searchPlaceholder} className="header-mobile-search-input w-100 p-3 m-auto" value={searchValue} onChange={(e) => setSearchValue(e.target.value)} onKeyDown={(e) => e.key === "Enter" && redirectToSearch(e.target.value)} />
                                </Col>

                                <Col xs={3} />
                            </Row>
                        </div>
                    }

                    {showMobileMenu &&
                        <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "white", zIndex: 999, height: "100vh" }}>
                            <Row className="py-3 px-1" style={{ backgroundColor: '#BCBCBC' }}>
                                <Col xs={12} className="d-flex flex-column mx-3">

                                    <div className="header-nav-item d-flex mb-4" onClick={() => resetMenu()}>
                                        <Link to="/about-us">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.aboutUs}</p>
                                        </Link>
                                    </div>

                                    <div onClick={() => { setSubMobileMenu(trans[siteLanguage].navbar.training); setShowMobileMenu(false); }} className="header-nav-item d-flex mb-4">
                                        <Row className="m-0">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.training}</p>
                                            <IoIosArrowForward className="ml-2 my-auto header-nav-item-icon" size={20} />
                                        </Row>
                                    </div>

                                    <div onClick={() => { setSubMobileMenuSession(trans[siteLanguage].navbar.sessions); setShowMobileMenu(false); }} className="header-nav-item d-flex mb-4">
                                        <Row className="m-0">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.sessions}</p>
                                            <IoIosArrowForward className="ml-2 my-auto header-nav-item-icon" size={20} />
                                        </Row>
                                    </div>

                                    <div className="header-nav-item d-flex mb-4" onClick={() => resetMenu()}>
                                        <Link to="/our-accreditations-and-recognitions">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.accredations}</p>
                                        </Link>
                                    </div>

                                    <div className="header-nav-item" onClick={() => resetMenu()}>
                                        <Link to="/media-news">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.news}</p>
                                        </Link>
                                    </div>

                                </Col>
                            </Row>

                            <Container fluid className="box">
                                <Row className="d-flex p-3" >


                                    <Col xs={6} className=" justify-content-center d-flex px-3">
                                        <Image src={images.logo_3} alt="afsac-logo1" style={{ maxWidth: 150 }} className="" />

                                    </Col>

                                    <Col xs={6} className=" justify-content-center d-flex px-3" ><Image src={images.logo_2} style={{ maxWidth: 150 }} className="" alt="afsac-logo2" /></Col>

                                </Row>

                                <Row className=" d-flex p-3">
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://twitter.com/astcicao' target="_blank" >
                                            <Image src={images.twitter_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >
                                            <Image src={images.fb_icon} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" >
                                            <Image src={images.lkin_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank">
                                            <Image src={images.youtube_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>

                                </Row>


                            </Container>

                            <Row className="mx-0">
                                <Col xs={12}>
                                    {/* LANGUAGE SELECTION */}
                                    <Row className="d-flex p-3">
                                        <Image src={images.en_flag} className="my-auto mr-3" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("en", history)} />
                                        <Image src={images.fr_flag} className="my-auto" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("fr", history)} />
                                    </Row>

                                    {/* <Row className="d-flex mx-0">
                                <div className="header-language-select d-flex" onClick={() => setShowLanguage(!showLanguage)}>
                                    <p className="my-auto ml-3">{getLanguageName(siteLanguage)}</p>
                                    <IoIosArrowForward className="my-auto ml-2" size={16} />
                                </div>

                                {showLanguage &&
                                <div className="header-language-select d-flex flex-column" style={{ position: 'absolute', zIndex: 999, top: '93%', height: '50px' }}>
                                    {languages.map((lang, i) =>
                                        lang !== siteLanguage
                                        &&
                                        <p key={i} className="ml-3 my-auto" onClick={() => setSiteLanguage(lang, history)}>{getLanguageName(lang)}</p>
                                    )}
                                </div>
                                }
                            </Row> */}
                                </Col>
                            </Row>

                        </div>
                    }

                    {SubMobileMenu &&
                        <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "#F4F6F9", zIndex: 999 }}>
                            <Row className="header-mobile-active-item py-4 d-flex">
                                <p className="header-nav-item my-auto ml-3 pl-3">{SubMobileMenu}</p>
                                <IoIosArrowDown className="ml-2 my-auto header-nav-item-icon" color={'#fff'} size={20} />
                            </Row>

                            <Row className="d-flex py-3">
                                <Col xs={12} className="d-flex flex-column ml-4 pl-4">

                                    <Link style={{ textDecoration: "none" }} to="/training-avsec" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.avsecCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-tpp" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.tppCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-virtual-classroom" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.virtualCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-online-courses" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.onlineCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />

                                    <div className="header-nav-item text-center">
                                        <Image src={images.session_img} style={{ width: "25%" }} alt="afsac-session-img" />
                                    </div>
                                </Col>
                            </Row>


                            <Container fluid className="box" style={{ marginTop: "2%" }}>
                                <Row className="d-flex p-3" >


                                    <Col xs={6} className=" justify-content-center d-flex px-3">
                                        <Image src={images.logo_3} alt="afsac-logo1" style={{ maxWidth: 150 }} className="" />

                                    </Col>

                                    <Col xs={6} className=" justify-content-center d-flex px-3" ><Image src={images.logo_2} style={{ maxWidth: 150 }} className="" alt="" /></Col>

                                </Row>

                                <Row className=" d-flex p-3">
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://twitter.com/astcicao' target="_blank" >
                                            <Image src={images.twitter_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >
                                            <Image src={images.fb_icon} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" >
                                            <Image src={images.lkin_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank">
                                            <Image src={images.youtube_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>

                                </Row>

                                <Row className="mx-auto">
                                    <Col xs={12}>
                                        {/* LANGUAGE SELECTION */}
                                        <Row className="d-flex p-4">
                                            <Image src={images.en_flag} className="my-auto mr-3" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("en", history)} />
                                            <Image src={images.fr_flag} className="my-auto" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("fr", history)} />
                                        </Row>

                                        {/* <Row className="d-flex mx-0">
                                <div className="header-language-select d-flex" onClick={() => setShowLanguage(!showLanguage)}>
                                    <p className="my-auto ml-3">{getLanguageName(siteLanguage)}</p>
                                    <IoIosArrowForward className="my-auto ml-2" size={16} />
                                </div>

                                {showLanguage &&
                                <div className="header-language-select d-flex flex-column" style={{ position: 'absolute', zIndex: 999, top: '93%', height: '50px' }}>
                                    {languages.map((lang, i) =>
                                        lang !== siteLanguage
                                        &&
                                        <p key={i} className="ml-3 my-auto" onClick={() => setSiteLanguage(lang, history)}>{getLanguageName(lang)}</p>
                                    )}
                                </div>
                                }
                            </Row> */}
                                    </Col>
                                </Row>

                            </Container>
                        </div>
                    }

                    {/* UPCOMMING SESSIONS SUB MENU */}
                    {SubMobileMenuSession &&
                        <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "#F4F6F9", zIndex: 999 }}>
                            <Row className="header-mobile-active-item py-4 d-flex">
                                <p className="header-nav-item my-auto ml-3 pl-3">{SubMobileMenuSession}</p>
                                <IoIosArrowDown className="ml-2 my-auto header-nav-item-icon" color={'#fff'} size={20} />
                            </Row>

                            <Row className="d-flex py-3">
                                <Col xs={12} className="d-flex flex-column ml-4 pl-4">

                                    <Link style={{ textDecoration: "none" }} to="/sessions-avsec" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.avsecCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/sessions-tpp" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.tppCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/sessions-virtual" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.virtualCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-online-courses" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.onlineCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <div className="header-nav-item text-center">

                                        <Image src={images.sessions_header_img} style={{ width: "25%" }} alt="afsac-session-img" />

                                    </div>
                                </Col>
                            </Row>


                            <Container fluid className="box" style={{ marginTop: "2%" }}>
                                <Row className="d-flex p-3" >


                                    <Col xs={6} className=" justify-content-center d-flex px-3">
                                        <Image src={images.logo_3} alt="afsac-logo1" style={{ maxWidth: 150 }} className="" />

                                    </Col>

                                    <Col xs={6} className=" justify-content-center d-flex px-3" ><Image src={images.logo_2} style={{ maxWidth: 150 }} className="" alt="afsac-logo2" /></Col>

                                </Row>

                                <Row className=" d-flex p-3">
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://twitter.com/astcicao' target="_blank" >
                                            <Image src={images.twitter_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >
                                            <Image src={images.fb_icon} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" >
                                            <Image src={images.lkin_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank">
                                            <Image src={images.youtube_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>

                                </Row>
                            </Container>
                            <Row className="mx-auto">
                                <Col xs={12}>
                                    {/* LANGUAGE SELECTION */}
                                    <Row className="d-flex p-4">
                                        <Image src={images.en_flag} className="my-auto mr-3" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguageEn("en", history)} />
                                        <Image src={images.fr_flag} className="my-auto" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguageFr("fr", history)} />
                                    </Row>

                                    {/* <Row className="d-flex mx-0">
                                <div className="header-language-select d-flex" onClick={() => setShowLanguage(!showLanguage)}>
                                    <p className="my-auto ml-3">{getLanguageName(siteLanguage)}</p>
                                    <IoIosArrowForward className="my-auto ml-2" size={16} />
                                </div>

                                {showLanguage &&
                                <div className="header-language-select d-flex flex-column" style={{ position: 'absolute', zIndex: 999, top: '93%', height: '50px' }}>
                                    {languages.map((lang, i) =>
                                        lang !== siteLanguage
                                        &&
                                        <p key={i} className="ml-3 my-auto" onClick={() => setSiteLanguage(lang, history)}>{getLanguageName(lang)}</p>
                                    )}
                                </div>
                                }
                            </Row> */}
                                </Col>
                            </Row>

                        </div>
                    }
                </Container>
            </Tablet>

            <Mobile>
                <Container fluid style={{ backgroundColor: '#FFF' }}>
                    <Col xs={12} className="d-flex px-0  justify-content-between">
                        <div className="d-flex">
                            {SubMobileMenu || SubMobileMenuSession
                                ?
                                <IoIosArrowBack size={35} color={"#0056A1"} className="m-auto" style={{ cursor: 'pointer' }} onClick={() => { setSubMobileMenu(null); setSubMobileMenuSession(null); setShowMobileMenu(true); }} />
                                :
                                !showMobileMenu
                                    ?
                                    <AiOutlineMenu size={35} color={"#0056A1"} className="m-auto" style={{ cursor: 'pointer' }} onClick={() => { setShowMobileSearch(false); setShowMobileMenu(prevState => !prevState) }} />
                                    :
                                    <AiOutlineClose size={35} color={"#0056A1"} className="m-auto" style={{ cursor: 'pointer' }} onClick={() => { setShowMobileMenu(false) }} />
                            }
                        </div>

                        <div className="d-flex">
                            <Link to="/">
                                {siteLanguage === "en"
                                    ?
                                    <Image src={images.logo} fluid style={{ maxWidth: "100px" }} className="m-auto" alt="afsac-logo" />
                                    :
                                    <Image src={images.logo_fr} fluid style={{ maxWidth: "100px" }} className="m-auto" alt="afsac-logo" />
                                }
                            </Link>
                        </div>

                        <div className="d-flex">
                            <AiOutlineSearch size={35} color={"#0056A1"} className="m-auto" style={{ cursor: 'pointer' }} onClick={() => { setShowMobileMenu(false); setShowMobileSearch(prevState => !prevState) }} />
                        </div>
                    </Col>

                    {/* Mobile Search */}
                    {showMobileSearch &&
                        <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "white", zIndex: 999 }}>
                            <Row className="p-3">
                                <Col xs={2} />

                                <Col xs={8} className="d-flex">
                                    <input type="search" placeholder={trans[siteLanguage].navbar.searchPlaceholder} className="header-mobile-search-input w-100 p-3 m-auto" value={searchValue} onChange={(e) => setSearchValue(e.target.value)} onKeyDown={(e) => e.key === "Enter" && redirectToSearch(e.target.value)} />
                                </Col>

                                <Col xs={2} />
                            </Row>
                        </div>
                    }

                    {showMobileMenu &&
                        <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "white", zIndex: 999, height: "100%" }}>
                            <Row className="py-3 px-1" style={{ backgroundColor: '#BCBCBC', width: "100%" }}>
                                <Col xs={12} className="d-flex flex-column mx-3">
                                    <div className="header-nav-item d-flex mb-4" onClick={() => resetMenu()}>
                                        <Link to="/about-us">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.aboutUs}</p>
                                        </Link>
                                    </div>

                                    <div onClick={() => { setSubMobileMenu(trans[siteLanguage].navbar.training); setShowMobileMenu(false); }} className="header-nav-item d-flex mb-4">
                                        <Row className="m-0">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.training}</p>
                                            <IoIosArrowForward className="ml-2 my-auto header-nav-item-icon" size={20} />
                                        </Row>
                                    </div>

                                    <div onClick={() => { setSubMobileMenuSession(trans[siteLanguage].navbar.sessions); setShowMobileMenu(false); }} className="header-nav-item d-flex mb-4">
                                        <Row className="m-0">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.sessions}</p>
                                            <IoIosArrowForward className="ml-2 my-auto header-nav-item-icon" size={20} />
                                        </Row>
                                    </div>

                                    <div className="header-nav-item d-flex mb-4" onClick={() => resetMenu()}>
                                        <Link to="/our-accreditations-and-recognitions">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.accredations}</p>
                                        </Link>
                                    </div>

                                    <div className="header-nav-item" onClick={() => resetMenu()}>
                                        <Link to="/media-news">
                                            <p className="mr-auto my-auto">{trans[siteLanguage].navbar.news}</p>
                                        </Link>
                                    </div>
                                </Col>
                            </Row>
                            <Container fluid className="box">
                                <Row className="d-flex p-3" >


                                    <Col xs={6} className=" justify-content-center d-flex px-3">
                                        <Image src={images.logo_3} alt="afsac-logo1" style={{ maxWidth: 150 }} className="" />

                                    </Col>
                                    <Col xs={6} className=" justify-content-center d-flex px-3" ><Image src={images.logo_2} style={{ maxWidth: 150 }} className="" alt="afsac-logo2" /></Col>

                                </Row>

                                <Row className=" d-flex p-3">
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://twitter.com/astcicao' target="_blank" >
                                            <Image src={images.twitter_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >
                                            <Image src={images.fb_icon} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" >
                                            <Image src={images.lkin_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank">
                                            <Image src={images.youtube_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>

                                </Row>


                                <Row className="mx-0">
                                    <Col xs={12}>
                                        {/* LANGUAGE SELECTION */}
                                        <Row className="d-flex p-3">
                                            <Image src={images.en_flag} className="my-auto mr-3" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("en", history)} />
                                            <Image src={images.fr_flag} className="my-auto" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("fr", history)} />
                                        </Row>

                                        {/* <Row className="d-flex mx-0">
                                <div className="header-language-select d-flex" onClick={() => setShowLanguage(!showLanguage)}>
                                    <p className="my-auto ml-3">{getLanguageName(siteLanguage)}</p>
                                    <IoIosArrowForward className="my-auto ml-2" size={16} />
                                </div>

                                {showLanguage &&
                                <div className="header-language-select d-flex flex-column" style={{ position: 'absolute', zIndex: 999, top: '93%', height: '50px' }}>
                                    {languages.map((lang, i) =>
                                        lang !== siteLanguage
                                        &&
                                        <p key={i} className="ml-3 my-auto" onClick={() => setSiteLanguage(lang, history)}>{getLanguageName(lang)}</p>
                                    )}
                                </div>
                                }
                            </Row> */}
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                    }

                    {/* AVSEC */}
                    {SubMobileMenu &&
                        <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "#F4F6F9", zIndex: 999, height: "100%" }}>
                            <Row className="header-mobile-active-item py-4 d-flex">
                                <p className="header-nav-item my-auto ml-3 pl-3">{SubMobileMenu}</p>
                                <IoIosArrowDown className="ml-2 my-auto header-nav-item-icon" color={'#fff'} size={20} />
                            </Row>

                            <Row className="d-flex py-3">
                                <Col xs={12} className="d-flex flex-column ml-4 pl-4">

                                    <Link style={{ textDecoration: "none" }} to="/training-avsec" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.avsecCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-tpp" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.tppCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-virtual-classroom" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.virtualCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-online-courses" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.onlineCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <div className="header-nav-item text-center">
                                        <Image src={images.session_img} style={{ width: "30%" }} alt="afsac-session-img" />
                                    </div>
                                </Col>
                            </Row>


                            <Container fluid className="box" style={{ marginTop: "1%" }} >
                                <Row className="d-flex p-3" >


                                    <Col xs={6} className=" justify-content-center d-flex px-3">
                                        <Image src={images.logo_3} alt="afsac-logo1" style={{ maxWidth: 150 }} className="" />

                                    </Col>

                                    <Col xs={6} className=" justify-content-center d-flex px-3" ><Image src={images.logo_2} style={{ maxWidth: 150 }} className="" alt="afsac-logo2" /></Col>

                                </Row>

                                <Row className=" d-flex p-3">
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://twitter.com/astcicao' target="_blank" >
                                            <Image src={images.twitter_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >
                                            <Image src={images.fb_icon} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" >
                                            <Image src={images.lkin_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank">
                                            <Image src={images.youtube_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>

                                </Row>

                                <Row className="mx-auto">
                                    <Col xs={12}>
                                        {/* LANGUAGE SELECTION */}
                                        {/* LANGUAGE SELECTION */}
                                        <Row className="d-flex p-3">
                                            <Image src={images.en_flag} className="my-auto mr-3" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("en", history)} />
                                            <Image src={images.fr_flag} className="my-auto" style={{ height: '30px', width: "30px", cursor: "pointer" }} onClick={() => setSiteLanguage("fr", history)} />
                                        </Row>
                                        <Row className="d-flex mx-0">
                                            <div className="header-language-select d-flex" onClick={() => setShowLanguage(!showLanguage)}>
                                                <p className="my-auto ml-3">{getLanguageName(siteLanguage)}</p>
                                                <IoIosArrowForward className="my-auto ml-2" size={16} />
                                            </div>

                                            {showLanguage &&
                                                <div className="header-language-select d-flex flex-column" style={{ position: 'absolute', zIndex: 999, top: '93%', height: '50px' }}>
                                                    {languages.map((lang, i) =>
                                                        lang !== siteLanguage
                                                        &&
                                                        <p key={i} className="ml-3 my-auto" onClick={() => setSiteLanguage(lang, history)}>{getLanguageName(lang)}</p>
                                                    )}
                                                    {/* <p className="ml-3 my-auto">العربية</p> */}
                                                </div>
                                            }
                                        </Row>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                    }

                    {/* TPP */}
                    {SubMobileMenuSession &&
                        <div style={{ position: "absolute", width: "100%", left: 0, right: 0, background: "#F4F6F9", zIndex: 999, height: "100vh" }}>
                            <Row className="header-mobile-active-item py-4 d-flex">
                                <p className="header-nav-item my-auto ml-3 pl-3">{SubMobileMenuSession}</p>
                                <IoIosArrowDown className="ml-2 my-auto header-nav-item-icon" color={'#fff'} size={20} />
                            </Row>

                            <Row className="d-flex py-3">
                                <Col xs={12} className="d-flex flex-column ml-4 pl-4">

                                    <Link style={{ textDecoration: "none" }} to="/sessions-avsec" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.avsecCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/sessions-tpp" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.tppCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="sessions-virtual" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.virtualCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <Link style={{ textDecoration: "none" }} to="/training-online-courses" className="">
                                        <div className="d-flex" onClick={() => resetMenu()}>
                                            <p className="header-dropdown-item-text my-auto">{trans[siteLanguage].navbar.onlineCourses}</p>
                                            <IoIosArrowForward className="ml-2 my-auto" color={"#00A7E2"} size={22} />
                                        </div>
                                    </Link>
                                    <hr className="lineheader" />
                                    <div className="header-nav-item text-center">

                                        <Image src={images.sessions_header_img} style={{ width: "30%" }} alt="afsac-session-img" />

                                    </div>
                                </Col>
                            </Row>


                            <Container fluid className="box" style={{ marginTop: "1%" }}>
                                <Row className="d-flex p-3" >


                                    <Col xs={6} className=" justify-content-center d-flex px-3">
                                        <Image src={images.logo_3} alt="afsac-logo1" style={{ maxWidth: 150 }} className="" />

                                    </Col>

                                    <Col xs={6} className=" justify-content-center d-flex px-3" ><Image src={images.logo_2} style={{ maxWidth: 150 }} className="" alt="afsac-logo2" /></Col>

                                </Row>

                                <Row className=" d-flex p-3">
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://twitter.com/astcicao' target="_blank" >
                                            <Image src={images.twitter_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >
                                            <Image src={images.fb_icon} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" >
                                            <Image src={images.lkin_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>
                                    <Col xs={3} className="align-self-center">
                                        <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank">
                                            <Image src={images.youtube_logo} style={{ width: '35px', height: 'auto' }} className="my-auto mr-3" alt="afsac-social-icon" />
                                        </a>
                                    </Col>

                                </Row>

                                <Row className="mx-auto">
                                    <Col xs={12}>
                                        {/* LANGUAGE SELECTION */}
                                        <Row className="d-flex mx-0">
                                            <div className="header-language-select d-flex" onClick={() => setShowLanguage(!showLanguage)}>
                                                <p className="my-auto ml-3">{getLanguageName(siteLanguage)}</p>
                                                <IoIosArrowForward className="my-auto ml-2" size={16} />
                                            </div>

                                            {showLanguage &&
                                                <div className="header-language-select d-flex flex-column" style={{ position: 'absolute', zIndex: 999, top: '93%', height: '50px' }}>
                                                    {languages.map((lang, i) =>
                                                        lang !== siteLanguage
                                                        &&
                                                        <p key={i} className="ml-3 my-auto" onClick={() => setSiteLanguage(lang, history)}>{getLanguageName(lang)}</p>
                                                    )}
                                                    {/* <p className="ml-3 my-auto">العربية</p> */}
                                                </div>
                                            }
                                        </Row>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                    }
                </Container>
            </Mobile>

            {/* PROMO BANNER */}
            {location.pathname === '/'
                &&
                <>
                    <Container fluid className="px-0 py-2" style={{ background: '#0056A1' }}>
                        <Row>
                            <Col xs={12}>
                                <Row className="justify-content-center ">
                                    <Col xs={9} lg={6} xl={5} className="px-0">
                                        <Slider {...promoSettings}>
                                            {sessionsTppCoursesData
                                                &&
                                                sessionsTppCoursesData.map((session, i) =>
                                                    session.titre !== null &&
                                                    <Col key={i} className="d-flex px-0 ">
                                                        <p className="ml-auto my-auto mr-2 header-promo-banner-title">
                                                            {session.titre}
                                                        </p>
                                                        <Link className="mr-auto my-auto" to={`/solo-tpp/trainAirPlus/${session.id}/${convertToSlug(session.titre)}`}>
                                                            <div className="header-promo-button text-center d-flex">
                                                                <span className="m-auto">{trans[siteLanguage].promoBanner.seeMoreButton}</span>
                                                            </div>
                                                        </Link>
                                                    </Col>
                                                )}
                                        </Slider>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </>
            }
        </>
    );
};

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
    sessionsTppCoursesData: state.afsacR.sessionsTppCoursesData,
});

export default withRouter(connect(mapStateToProps, { setSiteLanguage, getUpcommingCourses, searchCourses })(Header));