import React, { useState, useEffect } from 'react'
import '../components.css';

import { Form, Row, Col, Button, Image,Container,Spinner } from 'react-bootstrap';
import Slider from "react-slick";
import { Link, withRouter } from 'react-router-dom';
import { images,Desktop,Mobile,Tablet } from '../../constants/AppConfig';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import ContactUs from '../ContactUs';
import CatalogueByEmail from '../CatalogueByEmail';

import { connect } from 'react-redux';
import { getHomeData, sendNewsletterForm } from '../../redux/actions/AfsacActions'; 

import { trans } from '../../constants/Translations';


function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <Image
        src={images.next_slider}
        className={className}
        style={{ ...style, display: "block", height: '70px',width: '70px',marginRight: '-50px',}}
        onClick={onClick}
        alt={"afsac-arrow"}
      />
    );
};
  
function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <Image
        src={images.prev_slider}
        className={className}
        style={{ ...style, display: "block", height: '70px',width: '70px', marginLeft: '-50px',}}
        onClick={onClick}
        alt={"afsac-arrow"}
      />
    );
};

function Footer(props) {

    let {
        siteLanguage,
        homeData,

        // Functions :
        getHomeData,
        sendNewsletterForm
    } = props;

    // Popup Config:
    // const Swal = Swal.mixin({
    //     customClass: { confirmButton: 'newsletter-btn' },
    //     buttonsStyling: false,
    // });

    const settingsSessions = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        cssEase: "linear",
        dotsClass: "slider-pagination", 
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                initialSlide: 1
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    };

    const [newsletterName,setNewsletterName] = useState('');
    const [newsletterEmail,setNewsletterEmail] = useState('');
    const [newsletterLoading,setNewsletterLoading] = useState(false);

    const validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };

    const sendNewsletter = () => {
       
        if(newsletterName === "") {
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: trans[siteLanguage].newsletter.newsNameErr,
                showConfirmButton: false,
                timer: 2000
            })
        } else if (!validateEmail(newsletterEmail)) {
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: trans[siteLanguage].newsletter.newsEmailErr,
                showConfirmButton: false,
                timer: 2000
            })

        } else {
            sendNewsletterForm(newsletterName,newsletterEmail)
            .then(response => {
                console.log("Sending newsletter data result =>", response.data);
                setNewsletterLoading(false);

                if(response.data)
                {
                  Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: trans[siteLanguage].newsletter.newsEmailExist,
                    showConfirmButton: false,
                    timer: 3000
                  });
                } else {
                  Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: trans[siteLanguage].newsletter.newsSuccess,
                    showConfirmButton: false,
                    timer: 3000
                  });
                  setNewsletterName("");
                  setNewsletterEmail("");
                }
            
            })
            .catch(error => {
                console.log("Sending newsletter data Error =>", error);

                setNewsletterLoading(false);
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: trans[siteLanguage].newsletter.newsConexErr,
                  showConfirmButton: false,
                  timer: 2000
                })
            });
        }
    };

    useEffect(() => {
        if(homeData === null) {
            getHomeData(siteLanguage);
        }
    },[]);

    return (
        <div className="footer" style={{ background: "#F4F6F9" }}>
            {
            props.location.pathname === "/" 
            &&
            // Newsletter
            <Row className="d-flex" style={{ background: "#0056A1"}}>
                <Container>
                    <Col xs={12} className="p-5">
                        <Row className="d-flex">
                            <Col xs={12} lg={5} className="d-flex">
                                <p className="m-auto newsletter-text" style={{ color: "#fff" }}>{trans[siteLanguage].newsletter.hasOwnProperty("newsletterDesc") && trans[siteLanguage].newsletter.newsletterDesc }</p>
                            </Col>
                            <Col xs={12} lg={7} className="d-flex p-0">
                                <Row className="d-flex justify-content-between m-auto">
                                    <Col xs={12} lg={4} className="d-flex m-auto mb-3">
                                        <Form.Control type="text" className="m-auto w-100" id="newsletter-input"  onChange={(e) => setNewsletterName(e.target.value)} value={newsletterName} placeholder={trans[siteLanguage].newsletter.namePlaceholder} />
                                    </Col>

                                    <Col xs={12} lg={4} className="d-flex my-auto mb-3">
                                        <Form.Control type="text" className="m-auto w-100" id="newsletter-input" onChange={(e) => setNewsletterEmail(e.target.value)} value={newsletterEmail} placeholder={trans[siteLanguage].newsletter.emailPlaceholder} />
                                    </Col>

                                    <Col xs={12} lg={4} className="d-flex">
                                        {newsletterLoading 
                                        ?
                                        <Button className="my-auto" id="newsletter-btn">
                                            <Spinner animation="grow" size="sm" variant="light" />
                                        </Button>
                                        :
                                        <Button className="my-auto" id="newsletter-btn" onClick={sendNewsletter}>{trans[siteLanguage].newsletter.buttonText}</Button>
                                        }

                                    </Col>
                                </Row>

                            </Col>
                        </Row>
                    </Col>
                </Container>
            </Row>
            }

            {props.location.pathname !== '/'
            ?
            // Partners & Catalogue
            <>
            <div className="catalogueRow">
                <CatalogueByEmail />
            </div>

            {homeData && homeData.Partner.length > 0 &&
            <div style={{ background: "#F4F6F9" }}>
                <div className={props.location.pathname === "/training-tpp" ? "container p-5"  : "container p-5 mt-5"}>
                    <div className="text-center">
                        <h3 className="contact-title">{trans[siteLanguage].partners.title}</h3>
                        <Image src={images.center_divider} alt="afsac-divider" />
                    </div>

                    <br />
                    
                    <Slider {...settingsSessions}>
                        {homeData.Partner.map((partner,index) => 
                            <div key={index} className="d-flex w-100">
                                <Image src={partner.image} className="m-auto" style={{ width: '90%',border: '2px solid #BCBCBC'}}  alt={partner.titre} />
                            </div>
                        )}
                    </Slider>
                </div>
            </div>
            }
            </>
            :
            // Contact form
            <ContactUs />
            }

            {/* Footer */}
            <div style={{ background: "#0056A1" }} className="py-3 px-1">
                <Desktop>
                    <Row>
                        <Col md className="mb-4 d-flex flex-column align-items-center">
                            <div>
                                {siteLanguage === "en"
                                ?
                                <Image src={images.white_logo} alt="afsac-logo" />
                                :
                                <Image src={images.white_logo_fr} alt="afsac-logo" />
                                }
                            </div>
                            <div>
                                <a href='https://twitter.com/astcicao' target="_blank" >
                                    <Image src={images.white_twitter} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                </a>

                                <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >  
                                    <Image src={images.white_fb} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                </a>

                                <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" > 
                                    <Image src={images.white_lkin} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                </a>

                                <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank"> 
                                    <Image src={images.white_youtube} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                </a>
                            </div>
                        </Col>
                        <Col md className="mt-2">
                            <p className="footer-title">{trans[siteLanguage].footer.sitemap}</p>
                            <Link to="/about-us" className="footer-link"><p>{trans[siteLanguage].navbar.aboutUs}</p></Link>
                            <Link to="/training" className="footer-link"><p>{trans[siteLanguage].navbar.training}</p></Link>
                            <Link to="/session" className="footer-link"><p>{trans[siteLanguage].navbar.sessions}</p></Link>
                            <Link to="/media-news" className="footer-link"><p>{trans[siteLanguage].navbar.news}</p></Link>
                            <Link to="/contact" className="footer-link"><p>{trans[siteLanguage].navbar.hasOwnProperty("contact") && trans[siteLanguage].navbar.contact}</p></Link>
                        </Col>
                        <Col md className="mt-2">
                            <p className="footer-title">{trans[siteLanguage].footer.courses}</p>
                            <Link to="/training-avsec" className="footer-link"><p>{trans[siteLanguage].navbar.avsecCourses}</p></Link>
                            <Link to="/training-tpp" className="footer-link"><p>{trans[siteLanguage].navbar.tppCourses}</p></Link >
                            <Link to="/training-online-courses" className="footer-link"><p>{trans[siteLanguage].navbar.onlineCourses}</p></Link >
                            <Link to="/training-virtual-classroom" className="footer-link"><p>{trans[siteLanguage].navbar.virtualCourses}</p></Link >
                        </Col>
                    </Row>
                    <hr style={{ background: "#fff", height: 1 }} />
                    <div className="d-flex justify-content-between px-3 flex-md-row flex-sm-column flex-xm-column">
                        <span className="footer-copyright">Copyright © {new Date().getFullYear()} AFSAC-Tunisie</span>
                        <span className="footer-copyright">Powered by Switch.tn</span>
                    </div>
                </Desktop>

                <Tablet>
                    <Container>
                        <Col xs={12}>
                            <Row className="justify-content-between">
                                <Col md={6} className="mt-2 d-flex">
                                    <div className="mx-auto">
                                        <p className="footer-title">{trans[siteLanguage].footer.sitemap}</p>
                                        <Link to="/about-us" className="footer-link"><p>{trans[siteLanguage].navbar.aboutUs}</p></Link>
                                        <Link to="/training" className="footer-link"><p>{trans[siteLanguage].navbar.training}</p></Link>
                                        <Link to="/session" className="footer-link"><p>{trans[siteLanguage].navbar.sessions}</p></Link>
                                        <Link to="/media-news" className="footer-link"><p>{trans[siteLanguage].navbar.news}</p></Link>
                                        <Link to="/contact" className="footer-link"><p>{trans[siteLanguage].navbar.hasOwnProperty("contact") && trans[siteLanguage].navbar.contact}</p></Link>
                                    </div>
                                </Col>
                                <Col md={6} className="mt-2 d-flex">
                                    <div className="mx-auto">
                                        <Link to="#" className="footer-title"><p>{trans[siteLanguage].footer.courses}</p></Link>
                                        <Link to="/training-avsec" className="footer-link"><p>{trans[siteLanguage].navbar.avsecCourses}</p></Link>
                                        <Link to="/training-tpp" className="footer-link"><p>{trans[siteLanguage].navbar.tppCourses}</p></Link >
                                        <Link to="/training-online-courses" className="footer-link"><p>{trans[siteLanguage].navbar.onlineCourses}</p></Link >
                                        <Link to="/training-virtual-classroom" className="footer-link"><p>{trans[siteLanguage].navbar.virtualCourses}</p></Link >
                                    </div>
                                </Col>
                            </Row>
                        </Col>

                        <Row className="justify-content-center">
                            <Col md={4} className="mx-auto justify-content-center d-flex">
                                {siteLanguage === "en"
                                ?
                                <Image src={images.white_logo}  className="m-auto" alt="afsac-logo"  style={{ width:"135px"}}/>
                                :
                                <Image src={images.white_logo_fr}  className="m-auto" alt="afsac-logo"  style={{ width:"135px"}}/>
                                }
                            </Col>
                        </Row>

                        
                        <Row className="justify-content-center">
                            <Col md={5} >
                                <Row className="justify-content-between mx-auto">
                                    <a href='https://twitter.com/astcicao' target="_blank" >
                                        <Image src={images.white_twitter} alt="" className=" mt-4" alt="afsac-social-icon" />
                                    </a>

                                    <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >  
                                        <Image src={images.white_fb} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                    </a>

                                    <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" > 
                                        <Image src={images.white_lkin} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                    </a>

                                    <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank"> 
                                        <Image src={images.white_youtube} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                    </a>
                                </Row>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} className="p-0">
                                <hr style={{ background: "#fff", height: 1 }} />    
                            </Col>
                        </Row>

                        <Row className="justify-content-center">
                            <Col xs={8} className="d-flex">
                                <p className="m-auto footer-copyright">Copyright © {new Date().getFullYear()} AFSAC-Tunisie</p>
                            </Col>
                        </Row>

                        <Row className="justify-content-center">
                            <Col xs={8} className="d-flex">
                                <p className="m-auto footer-copyright">Powered by Switch.tn</p>
                            </Col>
                        </Row>
                    </Container>
                </Tablet>

                <Mobile>
                    <Container>
                        <Col xs={12}>
                            <Row className="justify-content-between">
                                <Col xs={6} className="d-flex">
                                    <div className="mx-auto">
                                        <p className="footer-title">{trans[siteLanguage].footer.sitemap}</p>
                                        <Link to="/about-us" className="footer-link"><p>{trans[siteLanguage].navbar.aboutUs}</p></Link>
                                        <Link to="/training" className="footer-link"><p>{trans[siteLanguage].navbar.training}</p></Link>
                                        <Link to="/session" className="footer-link"><p>{trans[siteLanguage].navbar.sessions}</p></Link>
                                        <Link to="/media-news" className="footer-link"><p>{trans[siteLanguage].navbar.news}</p></Link>
                                        <Link to="/contact" className="footer-link"><p>{trans[siteLanguage].navbar.hasOwnProperty("contact") && trans[siteLanguage].navbar.contact}</p></Link>
                                    </div>
                                </Col>
                                <Col xs={6} className="d-flex">
                                    <div className="mx-auto">
                                        <p className="footer-title">{trans[siteLanguage].footer.courses}</p>
                                        <Link to="/training-avsec" className="footer-link"><p>{trans[siteLanguage].navbar.avsecCourses}</p></Link>
                                        <Link to="/training-tpp" className="footer-link"><p>{trans[siteLanguage].navbar.tppCourses}</p></Link >
                                        <Link to="/training-online-courses" className="footer-link"><p>{trans[siteLanguage].navbar.onlineCourses}</p></Link >
                                        <Link to="/training-virtual-classroom" className="footer-link"><p>{trans[siteLanguage].navbar.virtualCourses}</p></Link >
                                    </div>
                                </Col>
                            </Row>
                        </Col>

                        <Row className="justify-content-center">
                            <Col xs={12} className="d-flex justify-content-center" >
                                {siteLanguage === "en"
                                ?
                                <Image src={images.white_logo} className="mx-auto" alt="afsac-logo" style={{ width:"135px"}} />
                                :
                                <Image src={images.white_logo_fr} className="mx-auto" alt="afsac-logo" style={{ width:"135px"}} />
                                }
                            </Col>
                        </Row>

                        
                        <Row className="justify-content-center">
                            <Col xs={12} className="d-flex">
                                <Row className="justify-content-between m-auto">
                                    <a href='https://twitter.com/astcicao' target="_blank" >
                                        <Image src={images.white_twitter} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                    </a>

                                    <a href='https://www.facebook.com/AFSACTunisia/' target="_blank" >  
                                        <Image src={images.white_fb} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                    </a>

                                    <a href='https://www.linkedin.com/company/afsactunisia' target="_blank" > 
                                        <Image src={images.white_lkin} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                    </a>

                                    <a href='https://www.youtube.com/channel/UCApngEQYcqj0xSm3AzZfh0Q' target="_blank"> 
                                        <Image src={images.white_youtube} alt="" className="m-2 mt-4" alt="afsac-social-icon" />
                                    </a>
                                </Row>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} className="p-0">
                                <hr style={{ background: "#fff", height: 1 }} />    
                            </Col>
                        </Row>

                        <Row className="justify-content-center">
                            <Col xs={12} className="d-flex">
                                <p className="m-auto footer-copyright">Copyright © {new Date().getFullYear()} AFSAC-Tunisie</p>
                            </Col>
                        </Row>

                        <Row className="justify-content-center">
                            <Col xs={12} className="d-flex">
                                <p className="m-auto footer-copyright">Powered by Switch.tn</p>
                            </Col>
                        </Row>
                    </Container>
                </Mobile>
            </div>

        </div>
    )
}

const mapStateToProps = state => ({
    siteLanguage : state.afsacR.siteLanguage,
    homeData : state.afsacR.homeData,
});

export default withRouter(connect(mapStateToProps,{ getHomeData, sendNewsletterForm })(Footer));
