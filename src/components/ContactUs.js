import React, { useState, useEffect, useRef } from 'react';
import './components.css';

import axios from 'axios';

import { connect } from 'react-redux';
import { trans } from '../constants/Translations'; // {trans[siteLanguage].contactForm.requestButton}
import { images,Desktop,Mobile,Tablet, config } from '../constants/AppConfig';
import { Form, Col, Button, Image,} from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import ReactFlagsSelect from 'react-flags-select';
import PhoneInput from 'react-phone-input-2';

function ContactUs(props) {

    let {
        siteLanguage
    } = props;

    const [name,setName] = useState('');
    const [fname,setFname] = useState('');
    const [email,setEmail] = useState('');
    const [phone,setPhone] = useState('');
    const [pays,setPays] = useState("TN");
    const [organisme,setOrganisme] = useState('');
    const [objet,setObjet] = useState('');
    const [cv,setCv] = useState(null);
    const [letter,setLetter] = useState(null);
    const [diploma,setDiploma] = useState([]);
    const [site,setSite] = useState('');
    const [message,setMessage] = useState('');

    const location = useLocation();
    const contactFormRef = useRef();

    const resetForm = () => {
        setName(name);
        setFname(fname);
        setEmail(email);
        setPhone(phone);
        setPays(pays);
        setOrganisme(organisme);
        setObjet(objet);
        setCv(cv); //setCv(null);
        setLetter(letter); //setLetter(null);
        setDiploma(diploma); //setDiploma(null);
        setSite(site);
        setMessage(message);
    };

    const validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const popup = (type,message) => {
        if(type === "success") {
            return Swal.fire({
                position: 'center',
                icon: 'success',
                title: message,
                showConfirmButton: false,
                timer: 3000
            })


        }
        else {
            return Swal.fire({
                position: 'center',
                icon: 'error',
                title: message,
                showConfirmButton: false,
                timer: 2000
            });
        }
    };

   const sendContactForm = () => {
        if(name === "") {
            popup('error', 'Please Verify your name !');
        } else if(fname === "") {
            popup('error', 'Please Verify your first name !');
        } else if(email === "" || !validateEmail(email)) {
            popup('error', 'Please Verify your email !');
        } else if(phone === "") {
            popup('error', 'Please Verify your phone number !');
        } else if(pays === "") {
            popup('error', 'Please Verify your country name !');
        } else if(organisme === "") {
            popup('error', 'Please Verify your organism name !');
        }else if(objet === "") {
            popup('error', 'Please Choose an object !');
        }
        //else if(site === "") {
          //  popup('error', 'Please Verify your website adress !');
        //}
        else if(objet === "Donner des cours à l’AFSAC" && !cv) {
            popup('error', 'Please Upload your CV !') 
        }  else if(objet === "Donner des cours à l’AFSAC" && !letter) { 
            popup('error', 'Please Upload your Letter of motivation !') 
        } else if(objet === "Donner des cours à l’AFSAC" && !diploma) { 
            popup('error', 'Please Upload your Certifs/Diploma !')
        }
        //else if (message === "") {
           // popup('error', 'Please Verify your message');
        //}
        else {
            // ALL DONE
 
            let data = new FormData();
            data.append('nom', name);
            data.append('prenom', fname);
            data.append('email', email);
            data.append('phone', phone);
            data.append('pays', pays);
            data.append('organisme', organisme);
            data.append('objet', objet);
            data.append('site_web', site);
            data.append('message', message);
            // FILES
            if(cv != null)
                data.append('cv', cv);
            else
                data.append('cv',null);
            if(letter != null)
                data.append('letter', letter);
            else
                data.append('letter', null);

            if(diploma != null)
            {
                for (let i = 0; i < diploma.length; i++)
                {
                    data.append('diploma[]',diploma[i]);
                }
            }
            else
                data.append('diploma[]', null);

            const options = { 
                headers: {'Content-Type': 'multipart/form-data'}
            };

            axios.post(`${config.url}contact`,data,options)
            .then(response => {
              console.log("Sending contact form result ==>", response.data);

              Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: trans[siteLanguage].contactForm.formSuccess,
                  showConfirmButton: false,
                  timer: 2000
              });
                window.location.reload(false);
              resetForm();

            })
            .catch(error => {
              console.log("Sending contact form Error ==>", error);
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: trans[siteLanguage].contactForm.formError,
                showConfirmButton: false,
                timer: 2000,
                  function()
                  {
                      location.reload();
                  }
              })
            });
        }
    };

    useEffect(() => {
        if(location.state) {
            contactFormRef.current.scrollIntoView();
        }
    },[]);
      
    return (
        <section id="contact-us" ref={contactFormRef}>
        <div className="footer-contact p-5">
            <div className="text-center">
                <h3 className="home-blue-title">{trans[siteLanguage].contactForm.title}</h3>
                <Image src={images.center_divider} alt="afsac-divider" />
            </div>

            <div style={{ background: "#fff", boxShadow: "0 0 10px lightgray", maxWidth: 900, margin: "auto" }} className="py-5 px-2 p-md-5  p-lg-5 p-xl-5 mt-4">
                <Form>
                    <Form.Row>
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.lastName}</Form.Label>
                            <Form.Control id="contact-us-input" onChange={(e) => setName(e.target.value)}/>
                        </Col>
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.firstName}</Form.Label>
                            <Form.Control id="contact-us-input" onChange={(e) => setFname(e.target.value)}/>
                        </Col>
                    </Form.Row>
                    <br />
                    <Form.Row>
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.email}</Form.Label>
                            <Form.Control id="contact-us-input" onChange={(e) => setEmail(e.target.value)}/>
                        </Col>
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.phone}</Form.Label>
                            {/* <Form.Control id="contact-us-input" type="text" maxLength="8" onChange={(e) => setPhone(e.target.value)}/> */}
                            <PhoneInput country={'tn'} value={phone} onChange={phone => setPhone(phone)} placeholder="" inputClass="contact-phone-input" />
                        </Col>
                    </Form.Row>
                    <br />
                    <Form.Row>
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.country}</Form.Label>
                            {/* <Form.Control id="contact-us-input" onChange={(e) => setPays(e.target.value)}/> */}
                            <ReactFlagsSelect onSelect={(code) => setPays(code)} defaultCountry="TN" className="menu-flags"  />
                        </Col>
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.organism}</Form.Label>
                            <Form.Control id="contact-us-input" type="text" onChange={(e) => setOrganisme(e.target.value)}/>
                        </Col>
                    </Form.Row>
                    <br />
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridState">
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.object}</Form.Label>
                            <Form.Control id="contact-us-input"  as="select" onChange={(e) => setObjet(e.target.value)}>
                                <option value="">{trans[siteLanguage].contactForm.objectPlaceholder}</option>
                                <option value={trans[siteLanguage].contactForm.object1}>{trans[siteLanguage].contactForm.object1}</option>
                                <option value={trans[siteLanguage].contactForm.object2}>{trans[siteLanguage].contactForm.object2}</option>
                                <option value={trans[siteLanguage].contactForm.object3}>{trans[siteLanguage].contactForm.object3}</option>
                                <option value={trans[siteLanguage].contactForm.object4}>{trans[siteLanguage].contactForm.object4}</option>
                            </Form.Control>
                        </Form.Group>
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.website}</Form.Label>
                            <Form.Control id="contact-us-input" onChange={(e) => setSite(e.target.value)} />
                        </Col>
                    </Form.Row>
                    <br />
                    {objet === trans[siteLanguage].contactForm.object2
                    &&
                    <>
                    <Form.Row>
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.cv}</Form.Label>
                            <label for="upload-cv" className="d-flex">
                                <div id="contact-us-input-file" className="form-control d-flex">
                                    <p className="mr-auto">
                                        {cv 
                                        ?
                                        cv.name
                                        :
                                        trans[siteLanguage].contactForm.browsePlaceholder
                                        }
                                    </p>
                                </div>
                            </label>
                            <input type="file" name="cv" id="upload-cv" onChange={(e) => setCv(e.target.files[0])}  accept=".docx, image/jpeg, image/png, application/pdf" />
                        </Col>
                        
                        <Col md>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.letter}</Form.Label>
                            <label for="upload-letter" className="d-flex">
                                <div id="contact-us-input-file" className="form-control d-flex">
                                    <p className="mr-auto">
                                        {letter 
                                        ?
                                        letter.name
                                        :
                                        trans[siteLanguage].contactForm.browsePlaceholder
                                        }
                                    </p>
                                </div>
                            </label>
                            <input type="file" name="letter" id="upload-letter" onChange={ (e) => setLetter(e.target.files[0])}  accept=".docx, image/jpeg, image/png, application/pdf"  />
                        </Col>
                    </Form.Row>
                    <br />
                    <Form.Row>
                        <Col xs={12} md={6}>
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.diploma}</Form.Label>
                            <label for="upload-diploma" className="d-flex">
                                <div id="contact-us-input-file" className="form-control d-flex">
                                    <p className="mr-auto">
                                        {diploma 
                                        ?
                                        diploma.name
                                        :
                                        trans[siteLanguage].contactForm.browsePlaceholder
                                        }
                                    </p>
                                </div>
                            </label>
                            <input type="file" name="diploma" id="upload-diploma" onChange={ (e) => setDiploma(e.target.files)}  accept=".docx, image/jpeg, image/png, application/pdf" multiple />
                        </Col>
                    </Form.Row>
                    </>
                    }
                    <br />
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridState">
                            <Form.Label id="contact-us-input-label">{trans[siteLanguage].contactForm.message}</Form.Label>
                            <Form.Control id="contact-us-input" as="textarea" onChange={(e) => setMessage(e.target.value)} />
                        </Form.Group>
                    </Form.Row>
                    <div className="text-center">
                        <Button className="text-center" id="contact-form-btn" onClick={sendContactForm}>{trans[siteLanguage].contactForm.submitButton}</Button>
                    </div>
                </Form>
            </div>
        </div>
        </section>
    );
};

const mapStateToProps = state => ({
    siteLanguage : state.afsacR.siteLanguage,
});
  
export default connect(mapStateToProps,{})(ContactUs);
  
