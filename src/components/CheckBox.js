import React from 'react'
import './components.css';

import {
    Row,
    Col
} from 'react-bootstrap';

export default function CheckBox(props) {
    return (
        <Row className="d-flex mr-5" style={props.style}>
           {props.checked
            ?
            <div onClick={() => props.onClick} className="m-auto d-flex search-radio-btn-active">
               <div className="m-auto search-circle-radio-btn" />
            </div>
            :
            <div className="m-auto d-flex search-radio-btn"/>
            }

            <div className="my-auto ml-2 search-radio-label">
                {props.label}
            </div>
        </Row>
    )
}
