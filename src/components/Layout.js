import React from 'react';

import { Row, Col, Container } from 'react-bootstrap';


export default function Layout({children}) {
    return (    
        <Container  className="my-2"  > {/*      className="my-5"  */}
            <Row>
                <Col xs={12}>
                    <Row className="justify-content-center">
                        <Col xs={12} lg={12}>
                            {children}
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    )
}
