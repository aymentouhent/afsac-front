import React from "react";
import { Container, Row, Table, Col } from "react-bootstrap";
import bao from "../assets/images/bao.png";
import Button from "../components/Button";
function Passenger() {
  return (
    <>
      <div>
        <div className="w-100">
          <img className="img-fluid " src={bao} />
        </div>
      </div>

      <Container className="mt-5">
        <span className="font-weight-bold text-secondary">&lt; Back </span>

        <p className="text-blue mt-3 mb-3">Training Compentency Development</p>
        <h1 className="h1 font-weight-bold title">
          Airport Passenger Boarding Bridge (PBB) Operations
        </h1>

        <hr />
        <Table striped bordered hover>
          <tbody>
            <tr>
              <th className="text-secondary">Course</th>
              <td>Formation De Base Du Personnel De Sureté D’aéroport</td>
            </tr>
            <tr>
              <th className="text-secondary">Course language</th>
              <td>English</td>
            </tr>
            <tr>
              <th className="text-secondary">Provider</th>
              <td>ICAO AVSEC</td>
            </tr>
            <tr>
              <th className="text-secondary">Trainer</th>
              <td>ICAO AVSEC Instructor</td>
            </tr>

            <tr>
              <th className="text-secondary">City</th>
              <td>Tunis</td>
            </tr>

            <tr>
              <th className="text-secondary">From date</th>
              <td>22 - 08 - 2020</td>
            </tr>

            <tr>
              <th className="text-secondary">To date</th>
              <td>22 - 08 - 2020</td>
            </tr>

            <tr>
              <th className="text-secondary">Duration</th>
              <td>10 days</td>
            </tr>
          </tbody>
        </Table>

        <div>
          <h3 className=" blueTitle mt-4 mb-4">Cost details</h3>
        </div>

        <Row className=" d-flex justify-content-between text-center">
          <Col className="text-secondary font-weight-bold">Nb of seats</Col>
          <Col className="text-secondary font-weight-bold">Currency</Col>
          <Col className="text-secondary font-weight-bold">Cost</Col>
        </Row>
        <Row
          className="   d-flex justify-content-between text-center py-3 mt-1 "
          style={{
            background: "#0056A1",
          }}
        >
          <Col className="text-light font-weight-bold">15</Col>
          <Col className="text-light font-weight-bold">EUR</Col>
          <Col className="text-light font-weight-bold">12000</Col>
        </Row>

        <div className="mt-5 d-flex justify-content-center font-weight-bold">
          <button
            className=" text-light p-2 px-4  "
            style={{
              background: "#F47820",
              borderColor: "#F47820",
            }}
          >
            Apply for this plan
          </button>
        </div>
        <div className="text-center font-weight-bold mt-3">
          <p
            style={{
              color: "#F47820",
            }}
          >
            Contact us for more informations
          </p>
        </div>

        <hr />

        <h3 className="blueTitle">Related courses</h3>
        <Row className="mb-5">
          <Col className="py-5 bg-light px-3 mr-5">
            <Row className="bg-light">
              <Col md="4">
                <img
                  style={{
                    height: "160px",
                    width: "160px",
                  }}
                  className="img-fluid"
                  src={require("../assets/images/engin.png")}
                />
              </Col>
              <Col>
                <div className="d-flex flex-column ">
                  <p className="title h4">
                    Airport Passenger Boarding Bridge (PBB) Operations
                  </p>

                  <p>Lorem ipsum dolor sit amet, consetetur sadip</p>
                </div>
              </Col>
            </Row>
          </Col>
          <Col className="py-5 bg-light px-3 mr-5">
            <Row className="bg-light">
              <Col md="4">
                <img
                  style={{
                    height: "160px",
                    width: "160px",
                  }}
                  className="img-fluid"
                  src={require("../assets/images/engin.png")}
                />
              </Col>
              <Col>
                <div className="d-flex flex-column ">
                  <p className="title h4">
                    Airport Passenger Boarding Bridge (PBB) Operations
                  </p>

                  <p>Lorem ipsum dolor sit amet, consetetur sadip</p>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Passenger;
