import React from "react";
import "../assets/css/button.css";

function Button({ text, color = "primary", svg, ...rest })
{
  return (
    <button
      className={`px-3 py-2 ${
        color === "primary"
          ? "button-primary"
          : color === "secondary"
          ? "button-secondary"
          : "button-info"
      }  d-flex flex-row  justify-content-center align-content-center `}
      {...rest}
    >
      <span>{text}</span>
      {svg && (
        <span className=" ml-2 d-flex  justify-content-center align-item-center">
          <img src={svg} />
        </span>
      )}
    </button>
  );
}

export default Button;