import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

function Course() {
    return (
        <div className="p-5 mt-4 mb-4" style={{ background: "#F4F6F9" }}>
            <Row>
                <Col md={2}>
                    <img src={require('../assets/1.png')} alt="" style={{ width: "100%" }} />
                </Col>
                <Col md={8} className="pl-4 pr-4">
                    <h4 className="lightBlueTitle">ICAO AVSEC</h4>
                    <h2 className="blueTitle">Airport Passenger Boarding Bridge (PBB) Operations</h2>
                    <p className="sessionP">and operation.  Participate in the organization’s SSP/SMS implementation provided in ICAO Safety Management SARPs and guidance material;  Develop SSP/SMS implementation plans based on the framworks management;  Apply the fundamental concepts and principles of aviation safety will be able to:  At the end of the Safety Management Online courses, trainees </p>

                </Col>
                <Col md={2} className="col-md-2 d-flex flex-column justify-content-between">
                    <span style={{ color: "#707070", fontWeight: "bold", fontSize: 17 }}>22 August 2020</span>
                    <Link to="/System"><button style={{
                        background: "transparent",
                        border: "2px solid #00A7E2",
                        color: "#00A7E2",
                        width: "100%",
                        padding: "10px 0",
                        fontWeight: "bold"
                    }}>Voir details</button></Link>
                </Col>
            </Row>
        </div>
    )
}

export default Course
