import React from 'react'
import bao from "../assets/images/bao.png";
import { Form } from 'react-bootstrap'
import Course from '../components/Course';

function Courses() {
    return (
        <div>
            <div className="w-100">
                <img className="img-fluid " src={bao} />
            </div>
            <div className="container p-5">

                <h1 className="h1 title text-left">AVSEC courses</h1>
                <img src={require('../assets/dividerOrange.png')} alt="" />
            </div>
            <div className="container mb-5">
                <hr />
                <Form className="pl-5 pr-5 pt-4 pb-4" style={{ background: "#F4F4F4" }}>
                    <div class="row">
                        <div class="col-md">
                            <Form.Label>Categorie</Form.Label>
                            <select required placeholder="Choose Category..." class="mt-2 form-control" >
                                <option value="Choose Category..." selected disabled>Choose Category...</option>
                                <option value="Aerodromes">Aerodromes</option>
                            </select>

                        </div>
                        <div class="col-md">
                            <Form.Label>Keyword</Form.Label>
                            <input required placeholder="Course name" class="mt-2 form-control" />
                        </div>
                        <div class="col-md">
                            <Form.Label><span style={{ visibility: "hidden" }}>test</span></Form.Label>
                            <br />
                            <div>
                                <button type="submit" class="mt-2 mr-2 ml-2 btn btn-primary pl-5 pr-5" style={{ background: "#00A7E2" }} >Rechercher</button>
                                <button type="reset" class="mt-2 mr-2 ml-2 btn" style={{ color: "#707070" }}>clear</button>
                            </div>
                        </div>

                    </div>
                </Form>

            </div>
            <div className="mt-5 mb-5 container">
                <Course />
            </div>
        </div>
    )
}

export default Courses
