import React from 'react'
import { Row, Col, } from 'react-bootstrap'
import Button from '../components/Button'
function Apropos()
{
    return (
        <div>
            <img src={require('../assets/aproposban.png')} alt="" style={{ width: "100%" }} />
            <div className="container p-5">
                <Row>
                    <Col md>
                        <img src={require("../assets/apropos1.png")} style={{ width: "100%" }} />
                    </Col>

                    <Col md>
                        <h2 className="blueTitle">Mot du DG</h2>
                        <img src={require('../assets/dividerIcon.png')} alt="" />
                        <br />
                        <br />
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et…</p>
                        <Button text={"Read More"} />

                    </Col>
                </Row>
            </div>
            <div className="container p-5">
                <div>
                    <h2 className="blueTitle">Historique</h2>
                    <img src={require('../assets/dividerOrange.png')} alt="" />
                </div>
                <img src={require('../assets/historique.png')} alt="" style={{ width: "100%" }} />
            </div>
            <section style={{ background: "#F4F6F9" }}>
                <div className="container p-5">
                    <div className="text-center">
                        <h2 className="blueTitle">Nos formations OACI</h2>
                        <img src={require('../assets/centerDivider.png')} alt="" />
                    </div>
                    <Row className="mt-5">
                        <Col md>
                            <h2 className="lightBlueTitle">AVSEC</h2>
                            <img src={require('../assets/dividerOrange.png')} alt="" />
                            <div className="mt-4">
                                <img src={require('../assets/apropos2.png')} style={{ width: "100%" }} alt="" />
                                <div className="d-flex justify-content-around text-center m-4" style={{ color: "#00A7E2", fontWeight: "bold" }}>
                                    <span>
                                        5
                                        <br />
                                        Modéles
                                    </span>

                                    <span>
                                        6
                                        <br />
                                        Workshops
                                    </span>
                                </div>
                                <div className="text-center m-4">
                                    <button style={{ backgroundColor: "#00A7E2" }} className="bluBtn" >Check upcoming courses</button>
                                </div>
                            </div><br /><br />
                            <h2 className="lightBlueTitle">Online courses</h2>
                            <img src={require('../assets/dividerOrange.png')} alt="" />
                            <div className="mt-5">
                                <img src={require('../assets/apropos4.png')} style={{ width: "100%" }} alt="" />
                                <div className="d-flex justify-content-around text-center m-4" style={{ color: "#00A7E2", fontWeight: "bold" }}>
                                    <span>
                                        5
                                        <br />
                                        Modéles
                                    </span>

                                    <span>
                                        6
                                        <br />
                                        Workshops
                                    </span>
                                </div>
                                <div className="text-center m-4">
                                    <button style={{ backgroundColor: "#00A7E2" }} className="bluBtn" >Check upcoming courses</button>
                                </div>
                            </div>
                        </Col>
                        <Col md>
                            <h2 className="blueTitle">Trainair plus</h2>
                            <img src={require('../assets/dividerOrange.png')} alt="" />
                            <div className="mt-4">
                                <img src={require('../assets/apropos3.png')} style={{ width: "100%" }} alt="" />
                                <div className="d-flex justify-content-around text-center m-4" style={{ color: "#0056A1", fontWeight: "bold" }}>
                                    <span>
                                        324
                                        <br />
                                        Mattes
                                    </span>

                                    <span>
                                        9
                                        <br />
                                        Thémes
                                    </span>
                                </div>
                                <div className="text-center m-4">
                                    <button style={{ backgroundColor: "#0056A1" }} className="bluBtn" >All courses</button>
                                </div>
                            </div><br /><br />
                            <h2 className="blueTitle">Icao virtual classroom</h2>
                            <img src={require('../assets/dividerOrange.png')} alt="" />
                            <div className="mt-4">
                                <img src={require('../assets/apropos5.png')} style={{ width: "100%" }} alt="" />
                                <div className="d-flex justify-content-around text-center m-4" style={{ color: "#0056A1", fontWeight: "bold" }}>
                                    <span>
                                        324
                                        <br />
                                        Mattes
                                    </span>

                                    <span>
                                        9
                                        <br />
                                        Thémes
                                    </span>
                                </div>
                                <div className="text-center m-4">
                                    <button style={{ backgroundColor: "#0056A1" }} className="bluBtn" >All courses</button>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            <section className="container p-5">
                <Row>
                    <Col className="d-flex flex-column align-items-center justify-content-between text-center p-4 m-3" sm style={{ boxShadow: "0 0 10px lightgray" }}>
                        <img src={require('../assets/aircraft.png')} alt="" />
                        <h3 style={{ color: "#00A7E2" }}>Our Mission</h3>
                        <br />
                        <p>A credible partner in Training and Human Resources Development in Aviation</p>
                    </Col>
                    <Col className="d-flex flex-column align-items-center justify-content-between text-center p-4 m-3" sm style={{ boxShadow: "0 0 10px lightgray" }}>
                        <img src={require('../assets/radar.png')} alt="" />
                        <h3 style={{ color: "#F47820" }}>Our Vision</h3>
                        <br />
                        <p>Ensure a continuous support to the human resources development of our clients in conformance with ICAO requirements.</p>
                    </Col>
                </Row>
            </section>
            <section className="m-5 p-5" style={{ background: "#F4F6F9" }}>
                <div className="container">
                    <Row>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                        <Col md className="m-2">
                            <img src={require('../assets/partners.png')} alt="" />
                        </Col>
                    </Row>
                </div>
            </section>
            <section>
                <div className="container p-5">
                    <Row>
                        <Col md>
                            <img src={require("../assets/sous.png")} style={{ width: "100%" }} />
                        </Col>

                        <Col md>
                            <h2 className="blueTitle">Accomodation</h2>
                            <img src={require('../assets/dividerOrange.png')} alt="" />
                            <br />
                            <br />
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et…</p>
                            <br /><br />
                            <Button text={"Request accomodation"} />

                        </Col>
                    </Row>
                </div>
            </section>
        </div>
    )
}

export default Apropos