import React from "react";
import heads from "../assets/images/heads.png";
import { Container, Col, Row } from "react-bootstrap";
import people2 from "../assets/images/20180113085645_IM_3@2x.png";

import people from "../assets/images/20190327145457_IM_1@2x.png";
import classM from "../assets/images/20171230113652_IM_IMG_0527@2x.png";
import classM2 from "../assets/images/20190327145501_IM_11@2x.png";
import classM3 from "../assets/images/20190924131707_IM_IMG-20190921-WA0008@2x.png";
import "../assets/css/main.css";
function Media() {
  return (
    <>
      <div className="w-100">
        <img className="img-fluid " src={heads} />
      </div>

      <Container className=" mt-5 h-100 ">
        <h1 className="h1 title text-center">Media & News</h1>
        <hr className="media-divider"></hr>
        <hr className="media-divider-two "></hr>

        <Row className="mt-5">
          <Col md>
            <img src={people} className="img-fluid" />
            <div>
              <p className="text-secondary h4 mt-1">Medias</p>
              <p className=" title h4 bold">
                Clôture de la session de Formation “Imagerie Radioscopique”
              </p>
              <p className="text-info h6 bold">16 JUN 2020</p>
            </div>
          </Col>
          <Col md>
            <div>
              <p className="text-info h4">A la une</p>
              <hr className="contact-underline ml-0" />
              <hr className="contact-underline-two ml-0 " />
              <div>
                {[1, 2, 3].map((item) => (
                  <Row className="mt-1" key={item}>
                    <Col md>
                      <img className="img-fluid" src={people2} />
                    </Col>
                    <Col md>
                      <p className="text-bold font-weight-bold text-secondary">
                        Press
                      </p>
                      <p className="mt-1 title font-weight-bold ">
                        Lorem ipsum dolor sit amet, in quo facete corrumpit.
                      </p>
                      <p className="mt-2 text-info font-weight-bold ">
                        16 juin
                      </p>
                    </Col>
                  </Row>
                ))}
              </div>
            </div>
          </Col>
        </Row>

        <div>
          <p className="text-info h4">Photos albums</p>
          <hr className="contact-underline ml-0" />
          <hr className="contact-underline-two ml-0 " />
        </div>

        <Row className="mt-5">
          <Col md className="mt-3">
            <Row>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
            </Row>
            <Row className="mt-2">
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
            </Row>
            <Row>
              <Col>
                <p className="title font-weight-bold">
                  Clôture de la session de Formation “Imagerie Radioscopique
                </p>
              </Col>
            </Row>
          </Col>
          <Col md className="mt-3">
            <Row>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
            </Row>
            <Row className="mt-2">
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
            </Row>
            <Row>
              <Col>
                <p className="title font-weight-bold">
                  Clôture de la session de Formation “Imagerie Radioscopique
                </p>
              </Col>
            </Row>
          </Col>
          <Col md className="mt-3">
            <Row>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
            </Row>
            <Row className="mt-2">
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
              <Col>
                <img src={classM} className={"h-auto w-100"} />
              </Col>
            </Row>
            <Row>
              <Col>
                <p className="title font-weight-bold">
                  Clôture de la session de Formation “Imagerie Radioscopique
                </p>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
      <div className="bg-light py-5">
        <Container>
          <Row>
            <Col>
              <div>
                <p className="text-info h4">Vidéos & Témoignages</p>
                <hr className="contact-underline ml-0" />
                <hr className="contact-underline-two ml-0 " />
              </div>
            </Col>
          </Row>
          <Row className="mt-3">
            <Col md>
              <div>
                <img src={classM2} className="img-fluid" />
              </div>
              <div className="mt-2 title font-weight-bold">
                Clôture de la session de Formation…
              </div>
            </Col>
            <Col md>
              <div>
                <img src={classM2} className="img-fluid" />
              </div>
              <div className="mt-2 title font-weight-bold">
                Clôture de la session de Formation…
              </div>
            </Col>{" "}
            <Col md>
              <div>
                <img src={classM2} className="img-fluid" />
              </div>
              <div className="mt-2 title font-weight-bold">
                Clôture de la session de Formation…
              </div>
            </Col>
            <Col md>
              <div>
                <img src={classM2} className="img-fluid" />
              </div>
              <div className="mt-2 title font-weight-bold">
                Clôture de la session de Formation…
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <Container>
        <Row>
          <Col>
            <div className="mt-5">
              <p className="text-info h4">communiqués</p>
              <hr className="contact-underline ml-0" />
              <hr className="contact-underline-two ml-0 " />
            </div>
          </Col>
        </Row>
        <Row className="mt-3 mb-5">
          <Col md>
            <div>
              <img className="img-fluid" src={classM3} />
            </div>
            <div>
              <p className="title font-weight-bold">
                Clôture de la session de Formation…
              </p>
              <p className="text-info font-weight-bold mt-0 ">16 Jun 2020</p>
              <div className="font-weight-light">
                Lorem ipsum dolor sit amet,
              </div>
            </div>
          </Col>
          <Col md>
            <div>
              <img className="img-fluid" src={classM3} />
            </div>
            <div>
              <p className="title font-weight-bold">
                Clôture de la session de Formation…
              </p>
              <p className="text-info font-weight-bold mt-0 ">16 Jun 2020</p>
              <div className="font-weight-light">
                Lorem ipsum dolor sit amet,
              </div>
            </div>
          </Col>
          <Col md>
            <div>
              <img className="img-fluid" src={classM3} />
            </div>
            <div>
              <p className="title font-weight-bold">
                Clôture de la session de Formation…
              </p>
              <p className="text-info font-weight-bold mt-0 ">16 Jun 2020</p>
              <div className="font-weight-light">
                Lorem ipsum dolor sit amet,
              </div>
            </div>
          </Col>
          <Col md>
            <div>
              <img className="img-fluid" src={classM3} />
            </div>
            <div>
              <p className="title font-weight-bold">
                Clôture de la session de Formation…
              </p>
              <p className="text-info font-weight-bold mt-0 ">16 Jun 2020</p>
              <div className="font-weight-light">
                Lorem ipsum dolor sit amet,
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Media;
