import React from "react";

function ImageCard({
  text,
  background = " #39464E",
  textColor = "#fff",
  imagePath,
}) {
  return (
    <div className="d-flex align-items-center flex-column">
      <img style={{ width: 217, height: 239 }} src={imagePath} />
      <button
        className="d-flex align-items-center justify-content-center"
        style={{
          width: 217,
          height: 60,
          border: "none",
          color: textColor,
          fontSize: 14,
          textTransform: "uppercase",
          background: background,
        }}
      >
        {text}
      </button>
    </div>
  );
}

export default ImageCard;
