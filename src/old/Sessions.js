import React from 'react'
import { Form } from 'react-bootstrap'
import Session from '../components/Session'

function Sessions() {
    return (
        <div>
            <div style={{ position: "relative", }}>
                <div style={{ position: "absolute", width: "100%", height: "100%", paddingTop: "5%" }}>
                    <div class="desktop" style={{ maxWidth: 1140, margin: "auto", color: "#fff" }}>
                        <h2>Learn anytime</h2>
                        <h2>Learn anywhere</h2>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
tetur sadipscing elitr, </p>
                    </div>
                </div>

                <img className="w-100 img-fluid " src={require('../assets/sessionCover.png')} />
            </div >
            <div className="container p-5">

                <h1 className="h1 title text-left">Upcoming Sessions</h1>
                <img src={require('../assets/dividerOrange.png')} alt="" />
                <p className="sessionP mt-3">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. </p>
            </div>
            <div className="container mb-5">
                <hr />
                <Form className="pl-5 pr-5 pt-4 pb-4" style={{ background: "#F4F4F4" }}>
                    <div class="row">
                        <div class="col-md">
                            <Form.Label>Categorie</Form.Label>
                            <select required placeholder="Choose Category..." class="mt-2 form-control" >
                                <option value="Choose Category..." selected disabled>Choose Category...</option>
                                <option value="Aerodromes">Aerodromes</option>
                            </select>

                        </div>
                        <div class="col-md">
                            <Form.Label>Keyword</Form.Label>
                            <input required placeholder="Course name" class="mt-2 form-control" />
                        </div>
                        <div class="col-md">
                            <Form.Label><span style={{ visibility: "hidden" }}>test</span></Form.Label>
                            <br />
                            <div>
                                <button type="submit" class="mt-2 mr-2 ml-2 btn btn-primary pl-5 pr-5" style={{ background: "#00A7E2" }} >Rechercher</button>
                                <button type="reset" class="mt-2 mr-2 ml-2 btn" style={{ color: "#707070" }}>clear</button>
                            </div>
                        </div>

                    </div>
                </Form>

            </div>
            <div className="container">
                <Session />
            </div>
        </div >
    )
}

export default Sessions
