import React from "react";

import Building from "../assets/images/Soustraction 7@2x.png";
import Phone from "../assets/images/Soustraction 8@2x.png";
import tablet from "../assets/images/Soustraction 8.jpg";
import laptop from "../assets/images/Soustraction 10.jpg";
import plane from "../assets/images/plane2.png";

import Button from "../components/Button";
import Header from "../components/Header";
import Footer from "../components/Footer";

function Contact()
{
  return (
    <>
      <img
          src={require('../assets/contactBan.png')} style={{
          width: "100%",
          marginBottom: "50px"}}
      />
      <div className="container">
        <div>
          <h3 className="title">Contact us</h3>
          <hr className="contact-underline m-0" />
          <hr className="contact-underline-two m-0 mt-2" />
        </div>

        <div className="row mt-4">
          <p className=" col-sm-12 col-md-6  ">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero
          </p>
        </div>
        <div className="row ">
          <div className="col-sm-12 col-lg-6 ">
            <img className="img-fluid section-contact" src={Building} />
          </div>
          <div className="col-sm-12 col-lg-6 ">
            <div className="row h-100  ">
              <div className="d-flex align-items-center  justify-content-sm-center w-100  ">
                <div className="  py-5   bg-light-gray text-section-right ">
                  <h3 className="h3 text-blue">Visit us:</h3>
                  <p className="title">7 Avenue Taha Hussein Montfleury</p>
                  <p className="title"> - 1008 Tunisie</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-5 ">
          <div className="col-sm-12 col-md-6 mr-0">
            <div className="row h-100  ">
              <div className="d-flex align-items-center justify-content-end  w-100  ">
                <div className="  py-5  text-section-left  bg-light-gray px-5  ">
                  <h3 className="h3 text-blue ">Call us us:</h3>
                  <p className="title bold">+216 98 333 330</p>
                  <p className="title">+216 98 333 330</p>
                  <p className="title">+216 20 199 950</p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-12 col-md-6 ">
            <img className="img-fluid section-contact" src={Phone} />
          </div>
        </div>
        <div className="row mt-5 ">
          <div className="col-sm-12 col-lg-6  ">
            <img className="img-fluid section-contact" src={laptop} />
          </div>
          <div className="col-sm-12 col-md-6 ">
            <div className="row h-100  ">
              <div className="d-flex align-items-center  justify-content-sm-center w-100  ">
                <div className=" py-5   bg-light-gray text-section-right ">
                  <h3 className="h3 text-blue">Email us:</h3>
                  <p className="title">training@afsactunisie.com</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-5 ">
          <div className="col-sm-12 col-md-6 mr-0">
            <div className="row h-100  ">
              <div className="d-flex align-items-center justify-content-end  w-100  ">
                <div className=" py-2 text-section-left  bg-light-gray px-5">
                  <h3 className="h3 text-blue ">Social medias:</h3>
                  <p className="title bold">facebook</p>
                  <p className="title">linkedIn</p>
                  <p className="title">YouTube</p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-12 col-md-6 ">
            <img className="img-fluid section-contact" src={tablet} />
          </div>
        </div>

        <div className="container mt-5 text-center h3 text-blue">
          <p className="">
            Would you like to have a training course at your own location?
          </p>
          <p> Click on the button below</p>
        </div>

        <div className="d-flex justify-content-center p-5">
          <Button text={"SUBMIT A REQUEST"} />
        </div>
      </div>
      {/* <div className="mt-5">
        <div className="row bg-blue py-3">
          <div className="col-sm-6  ">
            <p className="text-light">
              Inscrivez-vous a notre newsletter et restez informé de notre
              planning de formations
            </p>
          </div>
          <div className="col-sm-6  ">
            <p>
              Inscrivez-vous a notre newsletter et restez informé de notre
              planning de formations
            </p>
          </div>
        </div>
      </div> */}
    </>
  );
}

export default Contact;
