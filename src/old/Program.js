import React from "react";
import ban from "./../assets/images/gray-air.png";
import { Carousel, Container, Row, Col } from "react-bootstrap";
import Button from "../components/Button";
import svgPlane from "../assets/images/Groupe 437.svg";
import iconArrow from "../assets/images/iconArrow.svg";
import ImageCard from "../components/ImageCard";
import slide from "../assets/7.png";

function Program()
{
  const imagesArr = [
    { name: "AERODROMES", color: "#29337C" },
    { name: "AIR TRANSPORT", color: "#8C99A1" },
    { name: "TRAINING COMPETENCY DEVELOPMENT", color: "#9D050C" },
    { name: "Environement", color: "#118834" },
    { name: "air navigation services", color: "#39464E" },
    { name: "flight safety and safety management", color: "#FCA728" },
    { name: "AVIATION MANAGEMENT", color: "#612687" },
    { name: "Aviation law", color: "#1690CF" },
    { name: "Security and facilitation", color: "#F56E20" },
  ];
  return (
    <div>
      <Carousel>
        <Carousel.Item>
          <img className="d-block w-100" src={ban} alt="First slide" />
          <Carousel.Caption>
            <h3 className="h1">
              Spécialiste en Maintenance des Stations VSAT Aeronautiques
            </h3>

            <div className="d-flex">
              <Button
                text="Consulter les formations OACI"
                svg={svgPlane}
              ></Button>
              <div className="ml-3">
                <Button
                  color="secondary"
                  text="Consulter les formations OACI"
                  svg={iconArrow}
                ></Button>
              </div>
            </div>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>

      <Container className=" mt-5 mb-5 h-100 ">
        <h1 className="h1 title text-center">TRAINAIR PLUS PROGRAM </h1>
        <hr className="media-divider"></hr>
        <hr className="media-divider-two"></hr>
        <div className="mb-4"></div>
        <Row>
          {imagesArr.map((item, index) => (
            <Col md className="mt-3 mb-3  p-0">
              <ImageCard
                background={item.color}
                text={item.name}
                imagePath={require(`../assets/${index + 1}.png`)}
              />
            </Col>
          ))}
          <Col sm className="mt-3 mb-3  p-0" style={{ background: "#EAEAEA" }}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="217"
              height="276"
              viewBox="0 0 217 276"
            >
              <g data-name="Groupe 167" transform="translate(-1082 -1928)">
                <path
                  fill="#eaeaea"
                  d="M0 0H217V276H0z"
                  data-name="Rectangle 756"
                  transform="translate(1082 1928)"
                ></path>
                <g
                  fill="#bcbcbc"
                  data-name="Groupe 165"
                  transform="translate(1100 1850.11)"
                >
                  <path
                    d="M103.891 165.89a37 37 0 00-19.756 5.693 1.953 1.953 0 002.083 3.305 33.2 33.2 0 11-10.438 10.438 1.953 1.953 0 00-3.305-2.083 37.074 37.074 0 001.373 41.513l-3.34 3.34a5.868 5.868 0 00-6.668 1.144l-21.129 21.128a5.871 5.871 0 000 8.286l5.524 5.524a5.871 5.871 0 008.286 0L77.65 243.05a5.884 5.884 0 001.145-6.669l3.338-3.339a37.1 37.1 0 1021.757-67.152zm-29 74.4L53.76 261.416a1.964 1.964 0 01-2.762 0l-5.524-5.524a1.964 1.964 0 010-2.762L66.6 232a1.956 1.956 0 012.761 0l5.524 5.524a1.964 1.964 0 010 2.762zm1.381-6.905l-2.762-2.762 2.811-2.812a37.467 37.467 0 002.763 2.763zm0 0"
                    data-name="Tracé 152"
                  ></path>
                  <path
                    d="M265.424 249.912a1.954 1.954 0 002.647-.787 29.286 29.286 0 10-11.836 11.835 1.953 1.953 0 10-1.861-3.434 25.379 25.379 0 1110.261-10.262 1.954 1.954 0 00.787 2.648zm0 0"
                    data-name="Tracé 153"
                    transform="translate(-138.406 -32.187)"
                  ></path>
                  <path
                    d="M227.675 222.513a1.953 1.953 0 10-1.313-3.4 1.953 1.953 0 001.315 3.4zm0 0"
                    data-name="Tracé 154"
                    transform="translate(-148.643 -42.42)"
                  ></path>
                  <path
                    d="M453.948 448.788a1.953 1.953 0 10-1.381-3.335 1.953 1.953 0 001.382 3.333zm0 0"
                    data-name="Tracé 155"
                    transform="translate(-330.722 -224.501)"
                  ></path>
                </g>
                <g data-name="Groupe 166" transform="translate(935 848)">
                  <text
                    fill="#bcbcbc"
                    data-name="View more"
                    fontFamily="Poppins-SemiBold, Poppins"
                    fontSize="14"
                    fontWeight="600"
                    transform="translate(244 1321)"
                  >
                    <tspan x="-37.765" y="0">
                      View more
            </tspan>
                  </text>
                  <g
                    fill="none"
                    stroke="#bcbcbc"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    data-name="Icon feather-arrow-right"
                    transform="translate(285.5 1302.5)"
                  >
                    <path d="M7.5 13.5h12" data-name="Tracé 28"></path>
                    <path d="M13.5 7.5l6 6-6 6" data-name="Tracé 29"></path>
                  </g>
                </g>
              </g>
            </svg>
          </Col>
        </Row>
      </Container>

      <div className="bg-light p-5">
        <Container className="">
          <h1 className="h1 title text-center mt-5">AVSEC</h1>
          <hr className="media-divider"></hr>
          <hr className="media-divider-two "></hr>

          <Row className="mt-5">
            <Col md>
              <div className="left-carousel">
                <Carousel>
                  <Carousel.Item>
                    <Container>
                      <div className="w-100 left-carousel-shadow">
                        <img
                          style={{ objectFit: "cover" }}
                          className=" w-100 w-100 "
                          src={require("../assets/images/airbus.png")}
                          alt="second slide"
                        />
                      </div>

                      <Carousel.Caption>
                        <h3>AVIATION SECURITY NATIONAL INSTRUCTORS</h3>
                      </Carousel.Caption>
                    </Container>
                  </Carousel.Item>
                </Carousel>
                <div>
                  <div className=" mt-3 d-flex justify-content-center">
                    <Button
                      style={{
                        width: "300px",
                        height: "60px",
                      }}
                      text="Consulter le calendrier"
                      color="secondary"
                      svg={iconArrow}
                    />
                  </div>

                  <div className=" mt-3 d-flex justify-content-center ">
                    <Button
                      text="Voir Plus"
                      color="info"
                      style={{
                        width: "300px",
                        height: "60px",
                      }}
                    />
                  </div>
                </div>
              </div>
            </Col>
            <Col md>
              <div className="right-carousel ">
                <Carousel>
                  <Carousel.Item>
                    <Container className="right-carousel">
                      <div className="w-100 right-carousel-shadow">
                        <img
                          style={{ objectFit: "cover" }}
                          className=" w-100 w-100 "
                          src={require("../assets/images/airPort.png")}
                          alt="second slide"
                        />
                      </div>

                      <Carousel.Caption>
                        <h3>Risk Management Workshop</h3>
                      </Carousel.Caption>
                    </Container>
                  </Carousel.Item>
                  <Carousel.Item>
                    <Container className="right-carousel">
                      <div className="w-100 right-carousel-shadow">
                        <img
                          style={{ objectFit: "cover" }}
                          className=" w-100 w-100 "
                          src={require("../assets/images/airPort.png")}
                          alt="second slide"
                        />
                      </div>

                      <Carousel.Caption>
                        <h3>Risk Management Workshop</h3>
                      </Carousel.Caption>
                    </Container>
                  </Carousel.Item>
                </Carousel>
                <div>
                  <div className=" mt-3 d-flex justify-content-center">
                    <Button
                      style={{
                        width: "300px",
                        height: "60px",
                      }}
                      text="Consulter le calendrier"
                      color="secondary"
                      svg={iconArrow}
                    />
                  </div>

                  <div className=" mt-3 d-flex justify-content-center ">
                    <Button
                      text="Voir Plus"
                      color="info"
                      style={{
                        width: "300px",
                        height: "60px",
                      }}
                    />
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>

      <div>
        <Container className="mb-5">
          <Row>
            <Col md>
              <h1 className="h1 text-blue text-center mt-5">
                virtual classroom
              </h1>
              <hr className="media-divider"></hr>
              <hr className="media-divider-two "></hr>

              <div
                className="mt-5 position-relative "
                style={{ position: "relative" }}
              >
                <img
                  style={{ objectFit: "cover" }}
                  className=" w-100 w-100 "
                  src={require("../assets/images/airPort.png")}
                  alt="second slide"
                />

                <div
                  className=" d-flex position-absolut align-item-center  explain-btn text-blue "
                  style={{
                    background: "white",
                  }}
                >
                  <span className="mr-2">Consulter le calendrier</span>
                  <img src={require("../assets/images/iconArrowBlue.png")} />
                </div>
              </div>
              <div className="mt-5 d-flex justify-content-center ">
                <Button
                  text="Demander une formation "
                  color="info"
                  svg={svgPlane}
                ></Button>
              </div>
            </Col>
            <Col md>
              <h1 className="h1 text-blue text-center mt-5">ONLINE COURSES</h1>
              <hr className="media-divider"></hr>
              <hr className="media-divider-two "></hr>

              <div
                className="mt-5 position-relative "
                style={{ position: "relative" }}
              >
                <img
                  style={{ objectFit: "cover" }}
                  className=" w-100 w-100 "
                  src={require("../assets/images/onlinecourse.png")}
                  alt="second slide"
                />

                <div
                  className=" d-flex position-absolut align-item-center  explain-btn text-blue "
                  style={{
                    background: "white",
                  }}
                >
                  <span className="mr-2">Consulter le catalogue</span>
                  <img src={require("../assets/images/iconArrowBlue.png")} />
                </div>
              </div>
              <div className="mt-5 d-flex justify-content-center ">
                <Button
                  text="Plus d’information "
                  color="info"
                  svg={svgPlane}
                ></Button>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Program;
