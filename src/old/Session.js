import React from 'react'
import { Col, Row } from 'react-bootstrap'

function Session() {
    return (
        <div className="p-5 mt-4 mb-4" style={{ background: "#F4F6F9" }}>
            <Row>
                <Col md={2}>
                    <img src={require('../assets/articleThumb.png')} alt="" />
                </Col>
                <Col md={8} className="pl-4 pr-4">
                    <h2 className="blueTitle">Safety Management</h2>
                    <h4 className="lightBlueTitle">Learning Objectives:</h4>
                    <p className="sessionP">and operation.  Participate in the organization’s SSP/SMS implementation provided in ICAO Safety Management SARPs and guidance material;  Develop SSP/SMS implementation plans based on the framworks management;  Apply the fundamental concepts and principles of aviation safety will be able to:  At the end of the Safety Management Online courses, trainees </p>
                    <Row>
                        <Col sm={3}>
                            <span className="lightBlueTitle">Duration</span><br />
                            <span className="sessionP">13 Hours</span>
                        </Col>
                        <Col sm={3}>
                            <span className="lightBlueTitle">Delivery mode</span><br />
                            <span className="sessionP">Online</span>
                        </Col>
                        <Col sm={2}>
                            <span className="lightBlueTitle">Price</span><br />
                            <span className="sessionP">$500</span>
                        </Col>
                        <Col sm={4}>
                            <span className="lightBlueTitle">Language of instruction</span><br />
                            <span className="sessionP">English</span>
                        </Col>
                    </Row>
                </Col>
                <Col md={2} className="col-md-2 d-flex align-items-end">
                    <button style={{
                        background: "transparent",
                        border: "2px solid #00A7E2",
                        color: "#00A7E2",
                        width: "100%",
                        padding: "10px 0",
                        fontWeight: "bold"
                    }}>Voir details</button>
                </Col>
            </Row>
        </div>
    )
}

export default Session
