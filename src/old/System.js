import React from "react";
import bao from "../assets/images/bao.png";
import group1 from "../assets/images/group1.png";

import { Container, Col, Row, Button } from "react-bootstrap";

import "../assets/css/main.css";
import { Link } from "react-router-dom";
function System() {
  return (
    <>
      <div className="w-100">
        <img className="img-fluid " src={bao} />
      </div>

      <Container className=" mt-5 h-100 ">
        <Row>
          <Col md lg="4">
            <div className="w-100 h-100">
              <Link to="/Passenger" style={{ textDecoration: "none", color: "#fff" }}><button className="button-primary w-100 px-3 py-3 text-center font-weight-bold d-flex justify-content-center">
                Register Now
              </button>
              </Link>
              <button className=" w-100 px-3 py-3 text-center font-weight-bold d-flex justify-content-center btn-outline-orange mt-3">
                Download brochure
              </button>

              <div className="mt-3">
                <img src={group1} className="w-100 h-auto" />
              </div>
            </div>
          </Col>
          <Col md lg="8">
            <Container>
              <h1 className="h1 mt-5 title ">
                Aircraft Systems Troubleshooting
              </h1>

              <div>
                <p className="font-weight-bold text-blue">Training area:</p>
              </div>
              <div className="d-flex align-item-center">
                <div
                  className="rounded-circle  "
                  style={{
                    background: "#FCA728",
                    height: "2rem",
                    width: "2rem",
                  }}
                ></div>
                <p
                  className="font-weight-bold ml-2"
                  style={{
                    color: "#FCA728",
                  }}
                >
                  Flight Safety and Safety Management
                </p>
              </div>

              <div>
                <p className="text-blue font-weight-bold h5">
                  Purpose of the Course:
                </p>

                <article className="mt-5">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero eos et accusam et
                  justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                  sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem
                  ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                  nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero eos et accusam et
                  justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                  sea takimata sanctus est Lorem ipsum dolor sit amet.
                </article>

                <h4 className="text-blue  font-weight-bold mt-5">
                  Learning Objectifs:
                </h4>
                <ul>
                  <li className=" mt-2">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                  </li>
                  <li className=" mt-2">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                  </li>
                  <li className=" mt-2">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                  </li>
                  <li className=" mt-2">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                  </li>
                  <li className=" mt-2">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                  </li>
                  <li className=" mt-2">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                  </li>
                </ul>
              </div>
              <Row>
                <Col>
                  <div className=" d-flex flex-column">
                    <div className=" font-weight-bold text-blue ">Duration</div>
                    <div>13 Hours</div>
                  </div>
                </Col>
                <Col>
                  <div className=" d-flex flex-column">
                    <div className=" font-weight-bold text-blue ">
                      Delivery mode
                    </div>
                    <div>Online</div>
                  </div>
                </Col>
                <Col>
                  <div className=" d-flex flex-column">
                    <div className=" font-weight-bold text-blue ">Price</div>
                    <div>$ 500</div>
                  </div>
                </Col>
                <Col>
                  <div className=" d-flex flex-column">
                    <div className=" font-weight-bold text-blue ">
                      Language of instruction
                    </div>
                    <div>English</div>
                  </div>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
        <Row className="mt-5 mb-5">
          <Col className="py-5 bg-light px-3 mr-5">
            <Row className="bg-light">
              <Col md="4">
                <img
                  style={{
                    height: "160px",
                    width: "160px",
                  }}
                  className="img-fluid"
                  src={require("../assets/images/engin.png")}
                />
              </Col>
              <Col>
                <div className="d-flex flex-column ">
                  <p className="title h4">
                    Airport Passenger Boarding Bridge (PBB) Operations
                  </p>

                  <p>Lorem ipsum dolor sit amet, consetetur sadip</p>
                </div>
              </Col>
            </Row>
          </Col>
          <Col className="py-5 bg-light px-3">
            <Row className="bg-light">
              <Col md="4">
                <img
                  style={{
                    height: "160px",
                    width: "160px",
                  }}
                  className="img-fluid"
                  src={require("../assets/images/engin.png")}
                />
              </Col>
              <Col>
                <div className="d-flex flex-column ">
                  <p className="title h4">
                    Airport Passenger Boarding Bridge (PBB) Operations
                  </p>

                  <p>Lorem ipsum dolor sit amet, consetetur sadip</p>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default System;
