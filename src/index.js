import React from "react";
import ReactDOM from "react-dom";
import App from "./Route";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import 'sweetalert2/src/sweetalert2.scss';
import 'react-image-lightbox/style.css';  
import 'react-modal-video/scss/modal-video.scss';
import 'react-flags-select/css/react-flags-select.css';
import 'react-phone-input-2/lib/style.css'
// import 'react-flags-select/scss/react-flags-select.scss';

import store from './redux/store';
import { Provider } from 'react-redux';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
