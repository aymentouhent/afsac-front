import React, { useEffect } from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import './pages/Pages.css';
import './pages/Media/media.css';

// Scroll to top:
import ScrollToTop from './components/ScrollToTop';

// Navigation
import Header from "./components/Navigation/Header";
import Footer from "./components/Navigation/Footer";

// Home
import Home from './pages/Home/Home';

// Media
import Media from './pages/Media/Media';
import AllMedia from './pages/Media/AllMedia';
import AllMediaAlbums from './pages/Media/AllMediaAlbums';
import AllMediaVideos from './pages/Media/AllMediaVideos';
import SingleMedia from './pages/Media/SingleMedia';

// Sessions
import Session from './pages/Sessions/Session';

import SessionsTpp from './pages/Sessions/SessionsTpp';
import SessionsAvsec from './pages/Sessions/SessionsAvsec';
import SessionsVirtualClassrooms from './pages/Sessions/SessionsVirtualClassrooms';

// Courses 
import TrainingAvsec from './pages/Training/TrainingAvsec';
import TrainingOnlineCourses from './pages/Training/TrainingOnlineCourses';
import TrainingVirtualClassrooms from './pages/Training/TrainingVirtualClassrooms';
import TrainingTpp from './pages/Training/TrainingTpp';

// Solo pages:
import SoloAvsec from './components/Avsec/SoloAvsec';
import SoloTpp from './components/Tpp/SoloTpp';

// Register to a course:
import Registration from './pages/Registration/Registration';

// Contact
import Contact from './pages/Contact/Contact';

// About us
import AboutUs from './pages/AboutUs/AboutUs';

// Authorization
import Accreditation from './pages/Accreditation/Accreditation';

// Search 
import Search from './pages/Search/Search';

// Program 
// import Program from './pages/Program/Program';

import { connect } from 'react-redux';
import { getSiteLanguage } from './redux/actions/AfsacActions';
import Training from "./pages/Training/Training";

function App(props) {

    let {
        // siteLanguage,

        getSiteLanguage
    } = props;

    useEffect(() => {
        getSiteLanguage();
    }, []);

    return (
        <>
            <Router>
                <ScrollToTop />
                <Header />
                <Switch >
                    <Route path={"/"} component={Home} exact />

                    <Route path={"/contact"} component={Contact} exact />

                    <Route path={"/about-us"} component={AboutUs} exact />

                    <Route path={"/training"} component={Training} exact />

                    <Route path={"/our-accreditations-and-recognitions"} component={Accreditation} exact />

                    <Route path={"/training-avsec"} component={TrainingAvsec} exact />

                    <Route path={"/training-online-courses"} component={TrainingOnlineCourses} exact />

                    <Route path={"/training-virtual-classroom"} component={TrainingVirtualClassrooms} exact />

                    <Route path={"/training-tpp"} component={TrainingTpp} exact />
                    <Route path={"/session"} component={Session} exact />

                    <Route path={"/sessions-avsec"} component={SessionsAvsec} exact />

                    <Route path={"/sessions-tpp"} component={SessionsTpp} exact />

                    <Route path={"/sessions-virtual"} component={SessionsVirtualClassrooms} exact />

                    { /* <Route path={"/program"} component={Program} exact /> */}

                    <Route path={"/media-news"} component={Media} exact />
                    <Route path={"/all-media-news"} component={AllMedia} exact />
                    <Route path={"/all-media-albums"} component={AllMediaAlbums} exact />
                    <Route path={"/all-media-videos"} component={AllMediaVideos} exact />

                    <Route path={"/media/:id/:title"} component={SingleMedia} />

                    { /* Solos */}
                    <Route path={"/solo-avsec/:plat/:id/:title"} component={SoloAvsec} exact />

                    <Route path={"/solo-tpp/:plat/:id/:title"} component={SoloTpp} exact />

                    <Route path={"/registration/:title/:plat/:id"} component={Registration} exact />

                    {/* Search */}
                    <Route path={"/search/:searchValue"} component={Search} exact />


                </Switch>
                <Footer />

            </Router>
        </>
    );
}

const mapStateToProps = state => ({
    siteLanguage: state.afsacR.siteLanguage,
});

export default connect(mapStateToProps, { getSiteLanguage })(App);